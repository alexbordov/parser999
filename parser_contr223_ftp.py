import ftplib
import pymysql
import logging
import datetime
import zipfile
import os
import shutil
import xmltodict
import parser223
import sys
import time

DB = 'tenders_test'
SUFFIX = ''
DIR_LOG = 'log_contracts223_ftp_dev'
TEMP_DIR = 'temp223'
file_log = './{1}/contracts223_{0}.log'.format(datetime.date.today(), DIR_LOG)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
except_file = ('Failure', 'contractProcedure', 'contractCancel')


def connect_bd(baza):
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def get_ar(m, path_parse1):
    retry = True
    count = 0
    while retry:
        try:
            host = 'ftp.zakupki.gov.ru'
            ftpuser = 'fz223free'
            password = 'fz223free'
            ftp2 = ftplib.FTP(host)
            ftp2.set_debuglevel(0)
            ftp2.encoding = 'utf8'
            ftp2.login(ftpuser, password)
            ftp2.cwd(path_parse1)
            local_f = './{1}/{0}'.format(m, TEMP_DIR)
            lf = open(local_f, 'wb')
            ftp2.retrbinary('RETR {0}'.format(m), lf.write)
            lf.close()
            retry = False
            ftp2.close()
            if count > 0:
                logging_parser("Удалось скачать архив после попытки", count, m)
            return local_f
        except Exception as ex:
            logging.exception("Ошибка: ")
            logging_parser("Не удалось скачать архив", ex, m)
            if count > 100:
                logging_parser("Не удалось скачать архив за 100 попыток", ex, m)
                return 0
            count += 1
            time.sleep(5)


def get_xml_to_dict(filexml, dirxml, region):
    path_xml = dirxml + '/' + filexml
    with open(path_xml) as fd:
        try:
            s = fd.read()
            s = s.replace("ns2:", "")
            s = s.replace("oos:", "")
            doc = xmltodict.parse(s)
            parser223.parser(doc, path_xml)
        except Exception as ex:
            logging.exception("Ошибка: ")
            logging_parser('Ошибка конвертации в словарь', ex, path_xml)


def bolter(file, l_dir, region):
    file_lower = file.lower()
    for g in except_file:
        if file_lower.find(g.lower()) != -1:
            return
    # print(f)
    if not file_lower.endswith('.xml'):
        return
    try:
        get_xml_to_dict(file, l_dir, region)
    except Exception as exppars:
        logging.exception("Ошибка: ")
        logging_parser("Не удалось пропарсить файл", exppars, file)


def extract_contr(m, path_parse1, region):
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            logging_parser("Не удалось извлечь архив", ea, l)
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
            except Exception as ear:
                logging_parser("Не удалось извлечь архив альтернативным методом", ear, l)
                return
        except Exception as e:
            logging.exception("Ошибка: ")
            logging_parser("Не удалось извлечь архив", e, l)
            return

        try:
            file_list = os.listdir(l_dir)
        except Exception as ex:
            logging.exception("Ошибка: ")
            logging_parser("Не удалось получить список файлов", ex, l_dir)
        else:
            for f in file_list:
                bolter(f, l_dir, region)

        os.remove(l)
        try:
            shutil.rmtree(l_dir, ignore_errors=True)
        except Exception as ex1:
            logging_parser("Не удалось удалить папку", l_dir, ex1)


def get_list_ftp_last(path_parse):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'fz223free'
    password = 'fz223free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            # if i.find(str_f) != -1:
            array_ar.append(i)

    return array_ar


def get_list_ftp_daily(path_parse, region):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'fz223free'
    password = 'fz223free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(DB)
    cur_arhiv = con_arhiv.cursor(pymysql.cursors.DictCursor)
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_contract223{0} WHERE arhiv = %s and region = %s""".format(SUFFIX),
                              (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_contract223{0} SET arhiv = %s, region = %s""".format(SUFFIX)
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                logging_parser("Добавлен новый архив", i)
    cur_arhiv.close()
    con_arhiv.close()
    return array_ar


def main():
    shutil.rmtree(TEMP_DIR, ignore_errors=True)
    os.mkdir(TEMP_DIR)
    con_region = connect_bd(DB)
    cur_region = con_region.cursor()
    cur_region.execute("""SELECT * FROM region""")
    path_array = cur_region.fetchall()
    cur_region.close()
    con_region.close()

    for reg in path_array:
        if len(sys.argv) == 1:
            print(
                    'Недостаточно параметров для запуска, используйте curr для парсинга текущего месяца и last для прошлых')
            exit()
        elif str(sys.argv[1]) == 'last':
            path_parse = '/out/published/{0}/contract'.format(reg['path223'])
        elif str(sys.argv[1]) == 'daily':
            path_parse = '/out/published/{0}/contract/daily'.format(reg['path223'])

        try:
            # получаем список архивов
            if str(sys.argv[1]) == 'daily':
                arr_con = get_list_ftp_daily(path_parse, reg['path223'])
            elif str(sys.argv[1]) == 'last':
                arr_con = get_list_ftp_last(path_parse)
            else:
                arr_con = []
                print('Неверное имя параметра, используйте daily для парсинга текущего месяца и last для прошлых')
                exit()
            for j in arr_con:
                try:
                    extract_contr(j, path_parse, reg['conf'])

                except Exception as exc:
                    logging.exception("Ошибка: ")
                    logging_parser("Ошибка в экстракторе и парсере ", exc, j)
                    continue

        except Exception as ex:
            logging.exception("Ошибка: ")
            logging_parser("Не удалось получить список архивов", ex, path_parse)
            continue


if __name__ == "__main__":
    flog = open(file_log, 'a')
    flog.write('Время начала работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    flog.close()
    main()
    flog = open(file_log, 'a')
    flog.write('Время окончания работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    flog.write('Добавлено customer: {0}\n'.format(''))
    flog.write('Добавлено supplier: {0}\n'.format(''))
    flog.write('Добавлено contract: {0}\n'.format(''))
    flog.write('Добавлено product: {0}\n'.format(''))
    flog.close()
