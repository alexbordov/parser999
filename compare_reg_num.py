import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con_t = connect_bd('contract44')
cur_t = con_t.cursor(pymysql.cursors.DictCursor)
cur_t.execute("""SELECT regnum FROM organization""")
res_t = cur_t.fetchall()
cur_t.close()
con_t.close()
for t in res_t:
    con_o = connect_bd('tender')
    cur_o = con_o.cursor(pymysql.cursors.DictCursor)
    cur_o.execute("""SELECT regNumber FROM od_customer WHERE regNumber=%s""", (t['regnum']))
    res_o = cur_o.fetchone()
    if not res_o:
        with open('compare_regnum.txt', 'a') as f:
            f.write('Неизвестный regnum {0} \n'.format(t['regnum']))
    cur_o.close()
    con_o.close()
