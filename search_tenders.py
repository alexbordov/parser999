import ftplib
import pymysql
import logging
import datetime
import zipfile
import os
import shutil
import xmltodict
import parser_tenders
import sys
import xml
import time

file_log = './log_tenders44/tenders_ftp_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
count_good = 0
count_bad = 0
file_count_god = './log_tenders44/count_good_' + str(datetime.date.today()) + '.log'
file_count_bad = './log_tenders44/count_bad_' + str(datetime.date.today()) + '.log'
need_file = ('DateChange_', 'Prolongation')
unic_files = []
file_xml44 = ('EA44_', 'EP44_', 'OK44_', 'OKD44_', 'OKU44_', 'PO44_', 'ZA44_', 'ZK44_',
              'ZKB44_', 'ZKK44_', 'ZKKD44_', 'ZKKU44_', 'ZP44_', 'ProtocolZKBI_')
file_cancel = ('NotificationCancel_',)
file_sign = ('ContractSign_',)
file_cancelFailure = ('CancelFailure_',)
file_prolongation = ('Prolongation', )
file_datechange = ('DateChange_', )
file_orgchange = ('OrgChange_', )
file_lotcancel = ('LotCancel_', )

def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def get_list_ftp_curr(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd('tenders_test')
    cur_arhiv = con_arhiv.cursor(pymysql.cursors.DictCursor)
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_tenders WHERE arhiv = %s AND region = %s""", (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_tenders SET arhiv = %s, region = %s"""
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                with open(file_log, 'a') as flog5:
                    flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    ftp2.close()
    return array_ar


def get_list_ftp_last(path_parse):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            array_ar.append(i)

    return array_ar


def extract_tend(m, path_parse1, region, region_id):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    global need_file
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        array_xml44 = []
        array_cancel = []
        array_sign = []
        array_cancelFailure = []
        array_prolongation = []
        array_datechange = []
        array_orgchange = []
        array_lotcancel = []
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            print('Не удалось извлечь архив ' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
            except Exception as ear:
                print('Не удалось извлечь архив альтернативным методом')
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            print('Не удалось извлечь архив ' + str(e) + ' ' + l)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flogc:
                flogc.write('Не удалось извлечь архив {0} {1}\n'.format(str(e), l))
            return

        try:
            file_list = os.listdir(l_dir)
            for file_e in file_list:
                countFlag = False
                file_e_lower = file_e.lower()
                for xml44 in file_xml44:
                    if file_e_lower.find(xml44.lower()) != -1:
                        array_xml44.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for prol in file_prolongation:
                    if file_e_lower.find(prol.lower()) != -1:
                        array_prolongation.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for datch in file_datechange:
                    if file_e_lower.find(datch.lower()) != -1:
                        array_datechange.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for orgch in file_orgchange:
                    if file_e_lower.find(orgch.lower()) != -1:
                        array_orgchange.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for lotcan in file_lotcancel:
                    if file_e_lower.find(lotcan.lower()) != -1:
                        array_lotcancel.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for cancel in file_cancel:
                    if file_e_lower.find(cancel.lower()) != -1:
                        array_cancel.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for sign in file_sign:
                    if file_e_lower.find(sign.lower()) != -1:
                        array_sign.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for cancelFailure in file_cancelFailure:
                    if file_e_lower.find(cancelFailure.lower()) != -1:
                        array_cancelFailure.append(file_e)
                        break
        except Exception as ex:
            print('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir + '\n')
        else:
            for f in array_xml44:
                bolter(f, l_dir, region, region_id)

            for y in array_prolongation:
                bolter(y, l_dir, region, region_id)

            for x in array_datechange:
                bolter(x, l_dir, region, region_id)

            for w in array_orgchange:
                bolter(w, l_dir, region, region_id)

            for u in array_lotcancel:
                bolter(u, l_dir, region, region_id)

            for d in array_cancel:
                bolter(d, l_dir, region, region_id)

            for m in array_sign:
                bolter(m, l_dir, region, region_id)

            for s in array_cancelFailure:
                bolter(s, l_dir, region, region_id)

        os.remove(l)
        try:
            shutil.rmtree(l_dir)
        except Exception as ex1:
            print('Не удалось удалить папку' + str(ex1) + ' ' + l_dir)


def unic(f, path):
    begin_file_list = f.split('_')
    con = connect_bd('tenders_test')
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute("""SELECT * FROM unic_files WHERE type_file=%s""", (begin_file_list[0],))
    res = cur.fetchone()
    if not res:
        file_ex = path + '/' + f
        # file_target = './unic_tenders/' + f
        # shutil.copy(file_ex, file_target)
        # cur.execute("""INSERT INTO unic_files SET type_file=%s""", (begin_file_list[0],))
        with open(file_log, 'a') as flog66:
            flog66.write('Добавлен новый тип файлов ' + ' ' + file_ex + '\n\n\n')
    cur.close()
    con.close()


def get_xml_to_dict(filexml, dirxml, region, type_f, reg_id):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    global count_good
    global count_bad
    try:
        unic(filexml, dirxml)
    except Exception as ex1:
        logging.exception("Ошибка копирования файла: ")
        with open(file_log, 'a') as flog9:
            flog9.write('Ошибка копирования файла ' + str(ex1) + ' ' + filexml + '\n\n\n')
    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    fd = open(path_xml, 'r')
    s = fd.read()
    fd.close()
    try:
        # doc = xmltodict.parse(s)
        # parser_tenders.parser(doc, path_xml, filexml, region, type_f, reg_id)
        count_good += 1
    except xml.parsers.expat.ExpatError as ex2:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь, нечитаемые символы ' + str(ex2) + ' ' + path_xml + '\n\n\n')
        str_r = s.replace('', '')
        f = open(path_xml, 'w')
        f.write(str_r)
        f.close()
        with open(path_xml) as fdr:
            try:
                # doc = xmltodict.parse(fdr.read())
                # parser_tenders.parser(doc, path_xml, filexml, region, type_f, reg_id)
                pass
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Удалось пропарсить архив после удаления нечитаемых символов ' + ' ' + path_xml + '\n\n\n')
                count_good += 1
            except Exception as exs:
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка конвертации в словарь, удаление нечитаемых символов не помогло ' + str(exs) + ' '
                            + path_xml + '\n\n\n')
                count_bad += 1
    except Exception as ex:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь ' + str(ex) + ' ' + path_xml + '\n\n\n')
        count_bad += 1


def bolter(file, l_dir, region, reg_id):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    file_lower = file.lower()
    for g in need_file:
        if file_lower.find(g.lower()) != -1:
            try:
                # get_xml_to_dict(file, l_dir, region, g, reg_id)
                path_xml = l_dir + '/' + file
                file_target = './unic_tenders/' + file
                shutil.copy(path_xml, file_target)
            except Exception as exppars:
                # print('Не удалось пропарсить файл ' + str(exppars) + ' ' + file)
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось пропарсить файл ' + str(exppars) + ' ' + file + '\n')
                    # print(f)


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            host = 'ftp.zakupki.gov.ru'
            ftpuser = 'free'
            password = 'free'
            ftp2 = ftplib.FTP(host)
            ftp2.set_debuglevel(0)
            ftp2.encoding = 'utf8'
            ftp2.login(ftpuser, password)
            ftp2.cwd(path_parse1)
            local_f = './temp_tenders/' + str(m)
            lf = open(local_f, 'wb')
            ftp2.retrbinary('RETR ' + str(m), lf.write)
            lf.close()
            retry = False
            ftp2.close()
            if count > 0:
                with open(file_log, 'a') as flog:
                    flog.write('Удалось скачать архив после попытки ' + str(count) + ' ' + m + '\n')
            return local_f
        except Exception as ex:
            # print('Не удалось скачать архив ' + str(ex) + ' ' + m)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив ' + str(ex) + ' ' + m + '\n')
            if count > 100:
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось скачать архив за 100 попыток' + str(ex) + ' ' + m + '\n')
                return 0
            count += 1
            time.sleep(5)


def main():
    temp_dir = 'temp_tenders'
    shutil.rmtree(temp_dir, ignore_errors=True)
    os.mkdir(temp_dir)

    # забираем список регионов из базы данных
    con_region = connect_bd('tenders_test')
    cur_region = con_region.cursor(pymysql.cursors.DictCursor)
    cur_region.execute("""SELECT * FROM region""")
    path_array = cur_region.fetchall()
    cur_region.close()
    con_region.close()

    for reg in path_array:
        if len(sys.argv) == 1:
            print(
                    'Недостаточно параметров для запуска, используйте curr для парсинга текущего месяца и last для прошлых')
            exit()
        elif str(sys.argv[1]) == 'last':
            path_parse = 'fcs_regions/' + reg['path'] + '/notifications/'
        elif str(sys.argv[1]) == 'curr':
            path_parse = 'fcs_regions/' + reg['path'] + '/notifications/currMonth/'

        try:
            # получаем список архивов
            if str(sys.argv[1]) == 'curr':
                arr_tenders = get_list_ftp_curr(path_parse, reg['path'])
            elif str(sys.argv[1]) == 'last':
                arr_tenders = get_list_ftp_last(path_parse)
            else:
                print('Неверное имя параметра, используйте curr для парсинга текущего месяца и last для прошлых')
                arr_tenders = []
                exit()
            for j in arr_tenders:
                try:
                    extract_tend(j, path_parse, reg['conf'], reg['id'])
                except Exception as exc:
                    # print('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j)
                    logging.exception("Ошибка: ")
                    with open(file_log, 'a') as flog:
                        flog.write('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j + '\n')
                    continue

        except Exception as ex:
            # print('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse + '\n')
            continue
    print("Хороших тендеров: {0} , плохих тендеров: {1}".format(str(count_good), str(count_bad)))


if __name__ == "__main__":
    main()
