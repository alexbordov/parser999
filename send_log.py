import datetime
import time
import os
import smtplib
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.utils import formataddr
from os.path import basename
import psutil

user_name = '@@@@@@@@@@@@'
user_passwd = '**********'
port = 25

path_log_contracts223 = 'log_contracts223'
path_log_contracts44_ftp = './ParserContracts44Sharp/log_contracts44'
path_log_contracts223_ftp = './ParserContracts44Sharp/log_contracts223'
path_log_customers = 'log_customers'
path_log_organization = './ParserOrganizations/log_organization'
path_log_organization223 = './ParserOrganizations223/log_organization223'
path_log_suppliers = 'log_suppliers'
path_log_tenders223 = './ParserTenders/log_tenders223'
path_log_sign223 = './ParserTenders/log_sign223'
path_log_tenders44 = './ParserTenders/log_tenders44'
path_log_tenders615 = './ParserTenders/log_tenders615'
path_log_protocols_dev = 'log_protocol_dev'
path_log_prot = './ParserProtocols/log_prot'
path_log_plan_graph = './ParserTenderPlan/log_plan44'
path_log_parser_rnp = './ParserUnfair/log_rnp'
# path_log_parser_bank_guarantee = './ParserUnfair/log_bank'
path_log_parser_complaint = './ParserUnfair/log_complaint'
path_log_parser_complaint_res = './ParserUnfair/log_complaint_result'
path_log_parser_mono = './ParserContracts223Sharp/log_parsing'
path_log_exp223 = './ParserTenders/log_exp223'
path_log_prot223 = './ParserProtocols223/log_protocols'
path_log_icetrade = './ParserWebGo/log_icetrade'
path_log_bico = './UnParserSelen/log_bico'
path_log_rostend = './ParserWebFSharp/log_tenders_rostend'
path_log_maxi = './ParserWebCore/log_maxi'
path_log_chpt = './ParserWebFSharp/log_tenders_chpt'
path_log_tplus = './ParserWebFSharp/log_tenders_tplus'

list_path = [path_log_contracts44_ftp, path_log_organization,
             path_log_organization223, path_log_tenders223, path_log_tenders44, path_log_plan_graph,
             path_log_parser_rnp,
             path_log_prot, path_log_parser_complaint, path_log_parser_complaint_res, path_log_contracts223_ftp,
             path_log_sign223, path_log_exp223,
             path_log_prot223, path_log_tenders615, path_log_icetrade, path_log_bico, path_log_rostend, path_log_maxi,
             path_log_chpt, path_log_tplus]
list_files = []
text = '<div><p>В прикрепленных файлах отчеты о парсинге за прошедшую ночь.<\p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/">Смотреть отчеты по работе парсеров</a></p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/graph">Смотреть все графики по работе парсеров</a></p></div>'
subj = 'Отчеты о парсинге в ночь на {0}'.format(datetime.datetime.now().strftime("%d/%m/%Y"))


def sizeof_fmt(num, suffix='B'):
    """

    :param num:
    :param suffix:
    :return:
    """
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return f"{num:.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} {'Yi'}{suffix}"


def send_mail(send_from, send_to, subject, text, files=None, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param subject:
    :param text:
    :param files:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text.encode('utf-8'), "html", "utf-8"))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
            )
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def send_mail_free_space(send_from, send_to, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    dsk = psutil.disk_usage('/mnt/hdd01')
    free_space = sizeof_fmt(dsk.free)
    msg['Subject'] = f'На диске осталось {free_space} свободного места!'
    tm = time.time() - psutil.boot_time()
    text = f"""Сервер работает без перезагрузки уже {datetime.timedelta(seconds=tm)}\n\nСвободного места на диске {
    free_space} """
    msg.attach(MIMEText(text))
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def main():
    """
    main function
    """
    global list_path, list_files, text, subj
    for p in list_path:
        files = os.listdir(p)
        if files:
            files = [os.path.join(p, file) for file in files]
            files = [file for file in files if os.path.isfile(file)]
            f = max(files, key=os.path.getctime)
            list_files.append(f)
    files_m = os.listdir(path_log_parser_mono)
    if files_m:
        files_m = [os.path.join(path_log_parser_mono, file) for file in files_m]
        files_m = [file for file in files_m if os.path.isfile(file)]
        files_supp = [fs for fs in files_m if fs.find("suppliers") != -1]
        if files_supp:
            fsup = max(files_supp, key=os.path.getctime)
            list_files.append(fsup)
        files_cus = [fs for fs in files_m if fs.find("customers") != -1]
        if files_cus:
            fcus = max(files_cus, key=os.path.getctime)
            list_files.append(fcus)
        files_con223 = [fs for fs in files_m if fs.find("contr223") != -1]
        if files_con223:
            f223 = max(files_con223, key=os.path.getctime)
            list_files.append(f223)

    send_mail('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'], subj, text,
              files=list_files)
    send_mail_free_space('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'])


if __name__ == "__main__":
    main()
