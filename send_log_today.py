import datetime
import time
import os
import smtplib
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.utils import formataddr
from os.path import basename
import psutil

user_name = '@@@@@@@'
user_passwd = '*********'
port = 25

path_log_tenders_web = './ParserTenders/log_tenders_web'
path_log_otc = './ParserOTC/log_tenders_otc'

list_path = [path_log_tenders_web, path_log_otc]
list_path_abs = [os.path.abspath(i) for i in list_path if os.path.exists(i)]
list_files = []
text = '<div><p>В прикрепленных файлах отчеты о парсинге за прошедший день.<\p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/">Смотреть отчеты по работе парсеров</a></p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/graph">Смотреть все графики по работе парсеров</a></p></div>'
subj = f'Отчеты о парсинге за прошедший день {datetime.datetime.now().strftime("%d/%m/%Y")}'


def sizeof_fmt(num, suffix='B'):
    """

    :param num:
    :param suffix:
    :return:
    """
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return f"{num:.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} {'Yi'}{suffix}"


def send_mail(send_from, send_to, subject, text, files=None, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param subject:
    :param text:
    :param files:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text.encode('utf-8'), "html", "utf-8"))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
            )
            part['Content-Disposition'] = f'attachment; filename="{basename(f)}"'
            msg.attach(part)
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def send_mail_free_space(send_from, send_to, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    dsk = psutil.disk_usage('/mnt/hdd01')
    free_space = sizeof_fmt(dsk.free)
    msg['Subject'] = f'На диске осталось {free_space} свободного места!'
    tm = time.time() - psutil.boot_time()
    text = f"""Сервер работает без перезагрузки уже {datetime.timedelta(seconds=tm)}\n\nСвободного места на диске {
    free_space} """
    msg.attach(MIMEText(text))
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def main():
    """
    main function
    """
    global list_path, list_files, text, subj
    for p in list_path:
        files = os.listdir(p)
        if files:
            files = [os.path.join(p, file) for file in files]
            files = [file for file in files if os.path.isfile(file)]
            f = max(files, key=os.path.getctime)
            list_files.append(f)
    send_mail('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'], subj, text,
              files=list_files)
    send_mail_free_space('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'])


def del_dir(d):
    files = os.listdir(d)
    time_new = datetime.datetime.now()
    time_new = time.mktime(time_new.timetuple())
    for f in files:
        time_f = os.path.getctime(f'{d}/{f}')
        if (time_new - time_f) > 15 * 24 * 60 * 60:
            os.remove(f'{d}/{f}')


def cleal_old_logs():
    for dirf in list_path_abs:
        try:
            del_dir(dirf)
        except Exception:
            pass


if __name__ == "__main__":
    cleal_old_logs()
    main()
