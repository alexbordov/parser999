from openpyxl import Workbook
from openpyxl.styles import colors
from openpyxl.styles import Font, PatternFill, Border, Side
import MySQLdb
con = MySQLdb.connect(host="localhost", user="root", passwd="1234", db="test", charset='utf8', init_command='SET NAMES UTF8')
cur = con.cursor(MySQLdb.cursors.DictCursor)

wb = Workbook()
ws = wb.active
ws['A1'] = 'id'
ws['B1'] = 'name'
ws['C1'] = 'fullname'
ft = Font(color=colors.RED)
a1 = ws['A1']
b1 = ws['B1']
c1 = ws['C1']
a1.font = ft
b1.font = ft
c1.font = ft
border = Border(left=Side(border_style='dotted', color='000000'), right=Side(border_style='dotted', color='000000'), top=Side(border_style='dotted', color='000000'),bottom=Side(border_style='dotted', color='000000'))
fill = PatternFill("solid", fgColor="D70D32")
rows = ws['A1:C1']
print(rows)
ws.column_dimensions['C'].width = '30'
#ws.column_dimensions['C'].fill = fill
#ws.row_dimensions[1].fill = fill
#ws.row_dimensions[1].border = border
#ws.column_dimensions['C'].border = border
for row in rows:
    print(row)
    for c in row:
        #c.fill = fill
        #c.border = border
        pass
cur.execute('SELECT * FROM `test`  WHERE 1')
result = cur.fetchall()
#print(result)
cell = 0
for i in result:
    ws.append([i['id'], i['name'], i['fullname']])
    if cell%2:
        #ws.row_dimensions[cell+1].fill = fill
        #ws.row_dimensions[cell+1].border = border
        row1 = ws['A' + str(cell+1) + ':' + 'C' + str(cell+1)]
        for rows1 in row1:
            print(rows1)
            for rows2 in rows1:
                rows2.border = border
                rows2.fill = fill

            pass
    cell+=1
wb.save('test.xlsx')
cur.close()
con.close()