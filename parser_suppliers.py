import json
import pymysql
import datetime
import zipfile
import urllib.request, urllib.error
import os
from urllib.error import HTTPError
import logging

# period = datetime.timedelta(days=1)
# lastdate = datetime.date.today() - period
# format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
# url_customers = 'https://clearspending.ru/download/opendata/customers-' + format_date + '.json.zip'
# archive = 'customers-' + format_date + '.json.zip'
# urllib.request.urlretrieve(url_customers, archive)
file_log = './log_suppliers/suppliers_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
per = 0
down_count = 10
while down_count:
    try:
        period = datetime.timedelta(days=per)
        lastdate = datetime.date.today() - period
        format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
        url_suppliers = 'https://clearspending.ru/download/opendata/suppliers-' + format_date + '.json.zip'
        archive = 'suppliers-' + format_date + '.json.zip'
        # urllib.request.urlretrieve(url_customers, archive)
        f = urllib.request.urlopen(url_suppliers, timeout=900)
        with open(archive, "wb") as code:
            code.write(f.read())
        break
    except urllib.error.HTTPError as httperr:
        # print(httperr)
        with open(file_log, 'a') as flog_err:
            flog_err.write(
                    'Ошибка скачивания: ' + str(datetime.datetime.now()) + ' ' + str(httperr) + str(
                        url_suppliers) + '\n')
        per += 1
        logging.exception("Ошибка cкачивания:")
    except urllib.error.URLError as httperr:
        # print(httperr)
        with open(file_log, 'a') as flog_err:
            flog_err.write(
                    'Ошибка скачивания: ' + str(datetime.datetime.now()) + ' ' + str(httperr) + str(
                        url_suppliers) + '\n')
        logging.exception("Ошибка cкачивания:")
    except Exception as ex:
        with open(file_log, 'a') as flog_err0:
            flog_err0.write(
                    'Неизвестная ошибка: ' + str(datetime.datetime.now()) + ' ' + str(ex) + str(url_suppliers) + '\n')
        logging.exception("Неизвестная ошибка:")
    down_count -= 1
if down_count == 0:
    with open(file_log, 'a') as flog_err1:
        flog_err1.write('Не удалось получить архив за: ' + str(format_date) + ' ' + '\n')
    exit()
z = zipfile.ZipFile(archive, 'r')
z.extractall()
z.close()
file_name = 'suppliers-' + format_date + '.json'
flog = open(file_log, 'a')
flog.write('Время начала работы парсера: ' + str(datetime.datetime.now()) + '\n')
flog.close()
log_update = 0
log_insert = 0
inn_null = 0
f = open(file_name, 'r')
line = f.readline()


def parse(line):
    global log_insert
    global log_update
    global file_log
    global inn_null
    string = str(line)
    if (string.find('[', 0, 1) != -1):
        string = string[1:]
    size = len(string)
    if (string.find(',', (size - 2), size) != -1):
        string = string[: -2]
    if (string.find(']', (size - 2), size) != -1):
        string = string[: -2]
    end_string = string.strip()
    test = json.loads(end_string)
    #regNumber = ''
    inn = ''
    kpp = ''
    contracts_count = 0
    contracts223_count = 0
    contracts_sum = 0.0
    contracts223_sum = 0.0
    ogrn = ''
    regionCode = ''
    organizationName = ''
    postAddress = ''
    contactPhone = ''
    contactFax = ''
    contactEMail = ''
    lastName = ''
    middleName = ''
    firstName = ''
    contact_name = ''
    if 'kpp' in test:
        kpp = str(test['kpp'])
    if 'contracts223Count' in test:
        contracts223_count = int(test['contracts223Count'])
    if 'contractsCount' in test:
        contracts_count =int(test['contractsCount'])
    if 'contracts223Sum' in test:
        contracts223_sum = round(float(test['contracts223Sum']), 2)
    if 'contractsSum' in test:
        contracts_sum = round(float(test['contractsSum']), 2)
    if 'ogrn' in test:
        ogrn = str(test['ogrn'])
    if 'regionCode' in test:
        regionCode = str(test['regionCode'])
    if 'organizationName' in test:
        organizationName = str(test['organizationName'])
    if 'postAddress' in test:
        postAddress = str(test['postAddress'])
    if organizationName == '':
        organizationName = 'не указан'
    if 'contactFax' in test:
        contactFax = str(test['contactFax'])
    if 'contactEMail' in test:
        contactEMail = str(test['contactEMail'])
    if contactEMail == '':
        contactEMail = 'не указан'
    if 'contactPhone' in test:
        contactPhone = str(test['contactPhone'])
    if 'contactInfo' in test:
        contract_person_object = test['contactInfo']
        if 'middleName' in contract_person_object:
            middleName = str(contract_person_object['middleName'])
        if 'firstName' in contract_person_object:
            firstName = str(contract_person_object['firstName'])
        if 'lastName' in contract_person_object:
            lastName = str(contract_person_object['lastName'])
    contact_name = str(firstName + ' ' + middleName + ' ' + lastName)
    if 'inn' in test:
        inn = str(test['inn'])
        inn = inn.strip()
        con = pymysql.connect(host="localhost", user="tender", passwd="Dft56Point", db="tender", charset='utf8',
                              init_command='SET NAMES UTF8')
        cur = con.cursor()
        cur.execute("""SELECT * FROM od_supplier WHERE inn = %s """, (inn,))
        resultinn = cur.fetchone()
        if not resultinn:
            query1 = 'INSERT INTO od_supplier SET  inn = %s, kpp = %s, contracts_count = %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s, contact_name = %s'
            value1 = (inn, kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode, organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name)
            cur.execute(query1, value1)
            con.commit()
            #print('Добавлен поставщик с inn {0}'.format(inn))
            log_insert +=1
        else:
            query2 = 'UPDATE od_supplier SET  kpp = %s, contracts_count = %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s, contact_name = %s WHERE inn = %s'
            value2 = (kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode, organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name, inn)
            cur.execute(query2, value2)
            con.commit()
            #print('Обновлен поставщик с inn {0}'.format(inn))
            log_update += 1
            # flog = open(file_log, 'a')
            # for row in resultregnum:
            #     flog.write('Старый контакт: ' + str(row) + '\n')
            # flog.write('Новый контакт: {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}\n\n'.format(
            #     str(regNumber), str(inn), str(kpp), str(contracts_count), str(contracts223_count), str(contracts_sum),
            #     str(contracts223_sum), str(ogrn), str(regionCode), str(full_name), str(postalAddress), str(phone),
            #     str(fax), str(email), str(contact_name)))
            # flog.close()
        cur.close()
        con.close()
    else:
        inn_null += 1

while line:
    try:
        parse(line)
        line = f.readline()
    except Exception:
        logging.exception("Ошибка: ")
        line = f.readline()
f.close()
os.remove(archive)
os.remove(file_name)
flog = open(file_log, 'a')
flog.write('Добавлено поставщиков: ' + str(log_insert) + '\n')
flog.write('Обновлено поставщиков: ' + str(log_update) + '\n')
flog.write('Поставщиков без inn: ' + str(inn_null) + '\n')
flog.write('Время окончания работы парсера: ' + str(datetime.datetime.now()) + '\n\n\n')
flog.close()
