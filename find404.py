from bs4 import BeautifulSoup
import requests
import requests.exceptions
from urllib.parse import urlsplit
from collections import deque
import re
import urllib.request
import logging

file_log = './log_404' + '.log'
logging.basicConfig(level=logging.INFO, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def parser_404(url_pars):
    # a queue of urls to be crawled
    new_urls = deque([url_pars])

    # a set of urls that we have already crawled
    processed_urls = set()

    # a set of crawled emails
    emails = set()
    count = 0
    # process urls one by one until we exhaust the queue
    while len(new_urls):

        # move next url from the queue to the set of processed urls
        url = new_urls.popleft()
        # print(url)
        processed_urls.add(url)

        # extract base url to resolve relative links
        parts = urlsplit(url)
        # print(parts)
        base_url = "{0.scheme}://{0.netloc}".format(parts)
        # print(base_url)
        # print(url[:url.rfind('/')+1])
        path = url[:url.rfind('/') + 1] if '/' in parts.path else url

        # get url's content
        # print("Processing %s" % url)
        try:
            response = requests.get(url, timeout=200)
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            with open(file_log, 'a') as f:
                f.write('Ошибка 404 на странице: ' + url + '\n\n')
            continue
        except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
            # ignore pages with errors
            continue
        except Exception:
            # ignore pages with errors
            continue
        # extract all email addresses and add them into the resulting set
        new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
        emails.update(new_emails)
        if count < 5:
            # create a beutiful soup for the html document
            soup = BeautifulSoup(response.text)

            # find and process all the anchors in the document
            for anchor in soup.find_all("a"):
                # extract link url from the anchor
                link = anchor.attrs["href"] if "href" in anchor.attrs else ''
                # resolve relative links
                if link.startswith('/'):
                    link = base_url + link
                elif not link.startswith('http'):
                    link = path + link
                    # print(link)
                test_link = urlsplit(link)
                if not test_link.netloc == parts.netloc:
                    continue
                # if re.match(r'.*(exe|pdf|xls|doc|png|jpg|jpeg|gif|doc|docx|xlsx)$', test_link.path):
                #     continue
                # add the new url to the queue if it was not enqueued nor processed yet
                if not link in new_urls and not link in processed_urls:
                    new_urls.append(link)
            count += 1
        else:
            continue

    # print(emails)
    return emails


parser_404('http://enter-it.ru/')
