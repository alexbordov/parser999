import datetime
import logging
import operator
from functools import reduce
from warnings import filterwarnings
import dateutil.parser
import pymysql
from connect_to_db import connect_bd
import pytz
import itertools

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')

SUFFIX = '_new'
DB = 'tender'
LOG_DIR = 'log_protocol_dev'
TEMP_DIR = 'temp_protocol_dev'
filterwarnings('ignore', category=pymysql.Warning)
file_log = './{1}/protocol_ftp_{0}.log'.format(str(datetime.date.today()), LOG_DIR)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


class Type_protocol:
    type_EF1 = 'ProtocolEF1'
    type_EF2 = 'ProtocolEF2'
    type_EF3 = 'ProtocolEF3'
    type_ZK = 'ProtocolZK'
    type_ZKAfterProlong = 'ZKAfterProlong'
    type_EFSingleApp = 'ProtocolEFSingleApp'
    type_EFSinglePart = 'ProtocolEFSinglePart'
    type_OK2 = 'ProtocolOK2'
    type_OKD5 = 'ProtocolOKD5'
    type_OKOU3 = 'ProtocolOKOU3'
    type_ZPFinal = 'ProtocolZPFinal'
    type_Deviation = 'ProtocolDeviation'
    type_EFInvalidation = 'ProtocolEFInvalidation'
    type_OKSingleApp = 'ProtocolOKSingleApp'
    type_OKDSingleApp = 'ProtocolOKDSingleApp'
    type_OKOUSingleApp = 'ProtocolOKOUSingleApp'


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def check_yeld(h) -> bool:
    flag = False
    for i in h:
        if i:
            flag = True
    return flag


def get_from_dict(data_dict, map_list):
    return reduce(operator.getitem, map_list, data_dict)


def generator_univ(c):
    if c == "":
        raise StopIteration
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


def get_el(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = ''
    if res is None:
        res = ''
    if type(res) is str:
        res = res.strip()
    return res


def get_el_list(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = []
    if res is None:
        res = []
    return res


class Participiant:
    def get_inn(self, application):
        d = get_el(application, 'appParticipant', 'inn') or get_el(application, 'appParticipant', 'idNumber')
        return d

    def get_kpp(self, application):
        d = get_el(application, 'appParticipant', 'kpp')
        return d

    def get_organization_name(self, application):
        d = get_el(application, 'appParticipant', 'organizationName')
        return d

    def get_participant_type(self, application):
        d = get_el(application, 'appParticipant', 'participantType')
        return d

    def get_country_full_name(self, application):
        d = get_el(application, 'appParticipant', 'country', 'countryFullName')
        return d

    def get_post_address(self, application):
        d = get_el(application, 'appParticipant', 'postAddress')
        return d


class Protocol:
    def __init__(self, protocol, xml):
        prot = protocol[list(protocol.keys())[0]]
        list_p = [v for v in prot.keys() if 'fcs' in v.lower()]
        self.protocol = prot[list_p[0]]
        self.xml = xml
        if 'protocolLot' in self.protocol:
            if self.protocol['protocolLot'] is None:
                self.applications = []
            elif 'applications' in self.protocol['protocolLot']:
                if 'application' in self.protocol['protocolLot']['applications']:
                    self.applications = generator_univ(self.protocol['protocolLot']['applications']['application'])
                else:
                    self.applications = []
            elif 'application' in self.protocol['protocolLot']:
                self.applications = generator_univ(self.protocol['protocolLot']['application'])
            else:
                self.applications = []
        else:
            self.applications = []

    def get_purchaseNumber(self):
        d = get_el(self.protocol, 'purchaseNumber')
        return d

    def get_id(self):
        d = get_el(self.protocol, 'id')
        return d

    def get_protocol_date(self):
        d = get_el(self.protocol, 'protocolDate')
        if d:
            try:
                dt = dateutil.parser.parse(d)
                d = dt.astimezone(pytz.timezone("Europe/Moscow"))
            except Exception:
                d = ''
        else:
            d = ''
        return str(d)

    def get_url(self):
        d = get_el(self.protocol, 'href')
        return d

    def get_print_form(self):
        d = get_el(self.protocol, 'printForm', 'url')
        if d.startswith("<![CDATA"):
            d = d[9:-3]
        return d

    def get_journal_number(self, application):
        d = get_el(application, 'journalNumber')
        return d


class ProtocolEF2(Protocol):
    add_protocolEF2 = 0
    update_protocolEF2 = 0

    def get_offer_first_price(self, application):
        d = get_el(application, 'priceOffers', 'firstOffer', 'price')
        if not d:
            d = 0.0
        return d

    def get_offer_last_price(self, application):
        d = get_el(application, 'priceOffers', 'lastOffer', 'price')
        if not d:
            d = 0.0
        return d

    def get_offer_quantity(self, application):
        d = get_el(application, 'priceOffers', 'offersQuantity')
        if not d:
            d = 0
        return d


class ProtocolEF1(Protocol):
    add_protocolEF1 = 0
    update_protocolEF1 = 0

    def get_abandoned_reason_name(self):
        d = get_el(self.protocol, 'protocolLot', 'abandonedReason', 'name')
        return d

    def get_admission(self, application):
        d = get_el(application, 'admitted')
        if d == 'true':
            d = 'Допущен'
        if not d:
            d = get_el(application, 'notConsidered')
            if d == 'true':
                d = 'Заявка не рассматривалась'
        if not d:
            appRejectedReason = get_el(application, 'appRejectedReason')
            if appRejectedReason:
                reasons = generator_univ(appRejectedReason)
                if check_yeld(reasons):
                    for r in list(generator_univ(appRejectedReason)):
                        d += "{0} ".format(get_el(r, 'explanation'))
        return d


class ProtocolEF3(Protocol, Participiant):
    add_protocolEF3 = 0
    update_protocolEF3 = 0

    def get_app_rating(self, application):
        d = get_el(application, 'appRating')
        if not d:
            d = 0
        return d

    def get_abandoned_reason_name(self):
        d = get_el(self.protocol, 'protocolLot', 'abandonedReason', 'name')
        return d

    def get_admission(self, application):
        d = get_el(application, 'admitted')
        if d == 'true':
            d = 'Допущен'
        if not d:
            d = get_el(application, 'notConsidered')
            if d == 'true':
                d = 'Заявка не рассматривалась'
        if not d:
            appRejectedReason = get_el(application, 'appRejectedReason')
            if appRejectedReason:
                reasons = generator_univ(appRejectedReason)
                if check_yeld(reasons):
                    for r in list(generator_univ(appRejectedReason)):
                        d += "{0} ".format(get_el(r, 'nsiRejectReason', 'reason'))
        return d

    def get_refusalFact_name(self):
        d = get_el(self.protocol, 'refusalFact', 'foundation', 'name')
        return d

    def get_ref_explation(self):
        d = get_el(self.protocol, 'refusalFact', 'explanation')
        return d


class ProtocolZPFinal(ProtocolEF3):
    add_protocolZPFinal = 0

    def get_price(self, application):
        d = get_el(application, 'lastOffer', 'price')
        return d


class ProtocolZK(Protocol, Participiant):
    add_protocolZK = 0

    def get_abandoned_reason_name(self):
        d = get_el(self.protocol, 'protocolLot', 'abandonedReason', 'name')
        return d

    def test_abandoned_reason_name(self):
        d = get_el(self.protocol, 'protocolLot', 'abandonedReason')
        if type(d) == list:
            logging_parser('множество abandoned_reason', self.xml)

    def get_lot_number(self):
        d = get_el(self.protocol, 'protocolLot', 'lotNumber')
        if not d:
            d = 1
        return d

    def get_result_zk(self, application):
        d = get_el(application, 'admittedInfo', 'resultType')
        return d

    def get_price(self, application):
        d = get_el(application, 'price')
        return d


class ProtocolOK2(Protocol, Participiant):
    add_protocolOK2 = 0

    def __init__(self, protocol, xml):
        super().__init__(protocol, xml)
        self.lots = generator_univ(get_el_list(self.protocol, 'protocolLots', 'protocolLot'))

    def get_lot_number(self, lot):
        d = get_el(lot, 'lotNumber')
        return d

    def get_applications(self, lot):
        d = generator_univ(get_el_list(lot, 'applications', 'application'))
        p, d = itertools.tee(d)
        if not check_yeld(p):
            d = generator_univ(get_el_list(lot, 'application'))
        return d

    def get_participiants(self, application):
        d = generator_univ(get_el_list(application, 'appParticipants', 'appParticipant'))
        return d

    def get_app_rating(self, application):
        d = get_el(application, 'admittedInfo', 'appRating')
        if not d:
            d = 0
        return d

    def get_inn(self, part):
        d = get_el(part, 'inn') or get_el(part, 'idNumber')
        return d

    def get_kpp(self, part):
        d = get_el(part, 'kpp')
        return d

    def get_organization_name(self, part):
        d = get_el(part, 'organizationName')
        return d

    def get_participant_type(self, part):
        d = get_el(part, 'participantType')
        return d

    def get_country_full_name(self, part):
        d = get_el(part, 'country', 'countryFullName')
        return d

    def get_post_address(self, part):
        d = get_el(part, 'postAddress')
        return d

    def get_abandoned_reason_name(self, lot):
        d = get_el(lot, 'abandonedReason', 'name')
        return d

    def get_admission(self, application):
        d = ''
        appRejectedReason = get_el(application, 'admittedInfo', 'appRejectedReason')
        if appRejectedReason:
            reasons = generator_univ(appRejectedReason)
            if check_yeld(reasons):
                for r in list(generator_univ(appRejectedReason)):
                    d += "{0} ".format(get_el(r, 'nsiRejectReason', 'reason'))
        return d


def parserEF1(doc, path_xml, filexml, reg, type_f):
    p = ProtocolEF1(doc, filexml)
    id_p = 0
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    abandoned_reason_name = p.get_abandoned_reason_name()
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute("""SELECT id FROM auction_start_protocol{0} WHERE id_protocol = %s AND purchase_number = %s 
                    AND type_protocol = %s""".format(SUFFIX),
                (id_protocol, purchase_number, type_f))
    res_id = cur.fetchone()
    if res_id:
        logging_parser('такой протокол есть в базе', xml)
        cur.close()
        con.close()
        return
    cancel_status = 0
    date_prot = dateutil.parser.parse(protocol_date[:19])
    cur.execute("""SELECT id, protocol_date FROM auction_start_protocol{0} WHERE purchase_number = %s 
                    AND type_protocol = %s""".format(SUFFIX),
                (purchase_number, type_f))
    res_prot = cur.fetchall()
    if res_prot:
        for r in res_prot:
            if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                cur.execute("""UPDATE auction_start_protocol{0} SET cancel = 1 WHERE id = %s""".format(SUFFIX),
                            (r['id'],))
            else:
                cancel_status = 1
    cur.execute("""INSERT INTO auction_start_protocol{0} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                    url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s, abandoned_reason_name = %s""".format(
            SUFFIX),
            (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status,
             abandoned_reason_name))
    id_p = cur.lastrowid
    if not id_p:
        logging_parser('Не получили id', xml)
    ProtocolEF1.add_protocolEF1 += 1
    for app in p.applications:
        journal_number = p.get_journal_number(app)
        admission = p.get_admission(app)
        cur.execute(
                """INSERT INTO auction_start_applications{0} SET id_auction_protocol = %s, 
                    journal_number = %s, 
                    admission = %s""".format(SUFFIX),
                (
                    id_p, journal_number, admission))
    cur.close()
    con.close()


def parserEF2(doc, path_xml, filexml, reg, type_f):
    p = ProtocolEF2(doc, filexml)
    id_p = 0
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute("""SELECT id FROM auction_protocol{0} WHERE id_protocol = %s AND purchase_number = %s 
                AND type_protocol = %s""".format(SUFFIX),
                (id_protocol, purchase_number, type_f))
    res_id = cur.fetchone()
    if res_id:
        logging_parser('такой протокол есть в базе', xml)
        cur.close()
        con.close()
        return
    cancel_status = 0
    date_prot = dateutil.parser.parse(protocol_date[:19])
    cur.execute(f"""SELECT id, protocol_date FROM auction_protocol{SUFFIX} WHERE purchase_number = %s 
                AND type_protocol = %s""",
                (purchase_number, type_f))
    res_prot = cur.fetchall()
    if res_prot:
        for r in res_prot:
            if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                cur.execute(f"""UPDATE auction_protocol{SUFFIX} SET cancel = 1 WHERE id = %s""",
                            (r['id'],))
            else:
                cancel_status = 1
    cur.execute(f"""INSERT INTO auction_protocol{SUFFIX} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s""",
                (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status))
    id_p = cur.lastrowid
    if not id_p:
        logging_parser('Не получили id', xml)
    ProtocolEF2.add_protocolEF2 += 1
    for app in p.applications:
        journal_number = p.get_journal_number(app)
        offer_first_price = p.get_offer_first_price(app)
        offer_last_price = p.get_offer_last_price(app)
        offer_quantity = p.get_offer_quantity(app)
        cur.execute(
                f"""INSERT INTO auction_applications{SUFFIX} SET id_auction_protocol = %s, 
                    journal_number = %s, 
                    offer_first_price = %s, offer_last_price = %s, offer_quantity = %s""",
                (
                    id_p, journal_number, offer_first_price,
                    offer_last_price,
                    offer_quantity))
    cur.close()
    con.close()


def parserEF3(doc, path_xml, filexml, reg, type_f):
    p = ProtocolEF3(doc, filexml)
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    abandoned_reason_name = p.get_abandoned_reason_name()
    lot_number = 1
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute(
            f"""SELECT id FROM auction_end_protocol{SUFFIX} WHERE id_protocol = %s AND purchase_number = %s 
            AND type_protocol = %s""",
            (id_protocol, purchase_number, type_f))
    res_id = cur.fetchone()
    if res_id:
        logging_parser('такой протокол есть в базе', xml)
        cur.close()
        con.close()
        return
    cancel_status = 0
    ref_fact = p.get_refusalFact_name()
    if ref_fact:
        cancel_status = 1
    ref_fact = f"{ref_fact} {p.get_ref_explation()}".strip()
    date_prot = dateutil.parser.parse(protocol_date[:19])
    cur.execute(f"""SELECT id, protocol_date FROM auction_end_protocol{SUFFIX} WHERE purchase_number = %s 
                AND type_protocol = %s""",
                (purchase_number, type_f))
    res_prot = cur.fetchall()
    if res_prot:
        for r in res_prot:
            if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                cur.execute(f"""UPDATE auction_end_protocol{SUFFIX} SET cancel = 1 WHERE id = %s""",
                            (r['id'],))
            else:
                cancel_status = 1
    cur.execute(f"""INSERT INTO auction_end_protocol{SUFFIX} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                    url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s, abandoned_reason_name = %s, lot_number = %s, refusal_fact = %s""",
                (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status,
                 abandoned_reason_name, lot_number, ref_fact))
    id_p = cur.lastrowid
    if not id_p:
        logging_parser('Не получили id', xml)
    ProtocolEF3.add_protocolEF3 += 1
    for app in p.applications:
        id_participiant = 0
        journal_number = p.get_journal_number(app)
        inn = p.get_inn(app)
        kpp = p.get_kpp(app)
        organization_name = p.get_organization_name(app)
        participant_type = p.get_participant_type(app)
        country_full_name = p.get_country_full_name(app)
        post_address = p.get_post_address(app)
        if inn:
            cur.execute("""SELECT id FROM auction_participant{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX),
                        (inn, kpp))
            res_p = cur.fetchone()
            if res_p:
                id_participiant = res_p['id']
            else:
                cur.execute("""INSERT INTO auction_participant{0} SET inn = %s, kpp = %s, 
                        organization_name = %s, participant_type = %s, country_full_name = %s, 
                        post_address = %s""".format(SUFFIX),
                            (inn, kpp, organization_name, participant_type, country_full_name, post_address))
                id_participiant = cur.lastrowid
        if id_participiant == 0:
            logging_parser('Нет инн', xml, type_f)
        app_rating = p.get_app_rating(app)
        admission = p.get_admission(app)
        cur.execute("""INSERT INTO auction_end_applications{0} SET id_auction_end_protocol = %s, 
                        journal_number = %s, app_rating = %s, admission = %s, 
                        id_participiant = %s""".format(SUFFIX),
                    (id_p, journal_number, app_rating,
                     admission, id_participiant))
    cur.close()
    con.close()


def parserZK(doc, path_xml, filexml, reg, type_f):
    p = ProtocolZK(doc, path_xml)
    # p.test_abandoned_reason_name()
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    abandoned_reason_name = p.get_abandoned_reason_name()
    lot_number = p.get_lot_number()
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute(
            """SELECT id FROM auction_end_protocol{0} WHERE id_protocol = %s AND purchase_number = %s 
            AND type_protocol = %s AND lot_number = %s""".format(SUFFIX),
            (id_protocol, purchase_number, type_f, lot_number))
    res_id = cur.fetchone()
    if res_id:
        logging_parser('такой протокол есть в базе', xml)
        cur.close()
        con.close()
        return
    cancel_status = 0
    date_prot = dateutil.parser.parse(protocol_date[:19])
    cur.execute("""SELECT id, protocol_date FROM auction_end_protocol{0} WHERE purchase_number = %s 
                    AND type_protocol = %s AND lot_number = %s""".format(SUFFIX),
                (purchase_number, type_f, lot_number))
    res_prot = cur.fetchall()
    if res_prot:
        for r in res_prot:
            if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                cur.execute("""UPDATE auction_end_protocol{0} SET cancel = 1 WHERE id = %s""".format(SUFFIX),
                            (r['id'],))
            else:
                cancel_status = 1
    cur.execute("""INSERT INTO auction_end_protocol{0} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                        url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s, 
                        abandoned_reason_name = %s, lot_number = %s""".format(SUFFIX),
                (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status,
                 abandoned_reason_name, lot_number))
    id_p = cur.lastrowid
    if not id_p:
        logging_parser('Не получили id', xml)
    ProtocolZK.add_protocolZK += 1
    for app in p.applications:
        id_participiant = 0
        journal_number = p.get_journal_number(app)
        inn = p.get_inn(app)
        kpp = p.get_kpp(app)
        organization_name = p.get_organization_name(app)
        participant_type = p.get_participant_type(app)
        country_full_name = p.get_country_full_name(app)
        post_address = p.get_post_address(app)
        if inn:
            cur.execute("""SELECT id FROM auction_participant{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX),
                        (inn, kpp))
            res_p = cur.fetchone()
            if res_p:
                id_participiant = res_p['id']
            else:
                cur.execute("""INSERT INTO auction_participant{0} SET inn = %s, kpp = %s, 
                        organization_name = %s, participant_type = %s, country_full_name = %s, 
                        post_address = %s""".format(SUFFIX),
                            (inn, kpp, organization_name, participant_type, country_full_name, post_address))
                id_participiant = cur.lastrowid
        if id_participiant == 0:
            logging_parser('Нет инн', xml, type_f)
        result_zk = p.get_result_zk(app)
        price_zk = p.get_price(app)
        cur.execute("""INSERT INTO auction_end_applications{0} SET id_auction_end_protocol = %s, 
                        journal_number = %s, result_zk = %s, price = %s, 
                        id_participiant = %s""".format(SUFFIX),
                    (id_p, journal_number, result_zk,
                     price_zk, id_participiant))
    cur.close()
    con.close()


def parserOK2(doc, path_xml, filexml, reg, type_f):
    p = ProtocolOK2(doc, filexml)
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    con = connect_bd(DB)
    cur = con.cursor()
    for lot in p.lots:
        lot_number = p.get_lot_number(lot)
        abandoned_reason_name = p.get_abandoned_reason_name(lot)
        cur.execute(
                """SELECT id FROM auction_end_protocol{0} WHERE id_protocol = %s AND purchase_number = %s 
                AND type_protocol = %s AND lot_number = %s""".format(SUFFIX),
                (id_protocol, purchase_number, type_f, lot_number))
        res_id = cur.fetchone()
        if res_id:
            logging_parser('такой протокол есть в базе', xml)
            cur.close()
            con.close()
            return
        cancel_status = 0
        date_prot = dateutil.parser.parse(protocol_date[:19])
        cur.execute("""SELECT id, protocol_date FROM auction_end_protocol{0} WHERE purchase_number = %s 
                            AND type_protocol = %s AND lot_number = %s""".format(SUFFIX),
                    (purchase_number, type_f, lot_number))
        res_prot = cur.fetchall()
        if res_prot:
            for r in res_prot:
                if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                    cur.execute("""UPDATE auction_end_protocol{0} SET cancel = 1 WHERE id = %s""".format(SUFFIX),
                                (r['id'],))
                else:
                    cancel_status = 1
        cur.execute("""INSERT INTO auction_end_protocol{0} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                                url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s, abandoned_reason_name = %s, 
                                lot_number = %s""".format(
                SUFFIX),
                (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status,
                 abandoned_reason_name, lot_number))
        id_p = cur.lastrowid
        if not id_p:
            logging_parser('Не получили id', xml)
        ProtocolOK2.add_protocolOK2 += 1
        applications = p.get_applications(lot)
        for app in applications:
            journal_number = p.get_journal_number(app)
            app_rating = p.get_app_rating(app)
            participiants = p.get_participiants(app)
            admission = p.get_admission(app)
            for part in participiants:
                id_participiant = 0
                inn = p.get_inn(part)
                kpp = p.get_kpp(part)
                organization_name = p.get_organization_name(part)
                participant_type = p.get_participant_type(part)
                country_full_name = p.get_country_full_name(part)
                post_address = p.get_post_address(part)
                if inn:
                    cur.execute("""SELECT id FROM auction_participant{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX),
                                (inn, kpp))
                    res_p = cur.fetchone()
                    if res_p:
                        id_participiant = res_p['id']
                    else:
                        cur.execute("""INSERT INTO auction_participant{0} SET inn = %s, kpp = %s, 
                                            organization_name = %s, participant_type = %s, country_full_name = %s, 
                                            post_address = %s""".format(SUFFIX),
                                    (inn, kpp, organization_name, participant_type, country_full_name, post_address))
                        id_participiant = cur.lastrowid
                if id_participiant == 0:
                    logging_parser('Нет инн', xml, type_f)

                cur.execute("""INSERT INTO auction_end_applications{0} SET id_auction_end_protocol = %s, 
                                            journal_number = %s, app_rating = %s, admission = %s,
                                            id_participiant = %s""".format(SUFFIX),
                            (id_p, journal_number, app_rating, admission, id_participiant))

    cur.close()
    con.close()


def parserZPFinal(doc, path_xml, filexml, reg, type_f):
    p = ProtocolZPFinal(doc, filexml)
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    lot_number = 1
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute(
            """SELECT id FROM auction_end_protocol{0} WHERE id_protocol = %s AND purchase_number = %s 
            AND type_protocol = %s""".format(SUFFIX),
            (id_protocol, purchase_number, type_f))
    res_id = cur.fetchone()
    if res_id:
        logging_parser('такой протокол есть в базе', xml)
        cur.close()
        con.close()
        return
    cancel_status = 0
    date_prot = dateutil.parser.parse(protocol_date[:19])
    cur.execute("""SELECT id, protocol_date FROM auction_end_protocol{0} WHERE purchase_number = %s 
                    AND type_protocol = %s""".format(SUFFIX),
                (purchase_number, type_f))
    res_prot = cur.fetchall()
    if res_prot:
        for r in res_prot:
            if date_prot > datetime.datetime.strptime(str(r['protocol_date']), "%Y-%m-%d %H:%M:%S"):
                cur.execute("""UPDATE auction_end_protocol{0} SET cancel = 1 WHERE id = %s""".format(SUFFIX),
                            (r['id'],))
            else:
                cancel_status = 1
    cur.execute("""INSERT INTO auction_end_protocol{0} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                        url = %s, print_form = %s, xml = %s, type_protocol = %s, cancel = %s, lot_number = %s""".format(
            SUFFIX),
            (id_protocol, protocol_date, purchase_number, url, print_form, xml, type_f, cancel_status, lot_number))
    id_p = cur.lastrowid
    if not id_p:
        logging_parser('Не получили id', xml)
    ProtocolZPFinal.add_protocolZPFinal += 1
    for app in p.applications:
        id_participiant = 0
        journal_number = p.get_journal_number(app)
        inn = p.get_inn(app)
        kpp = p.get_kpp(app)
        organization_name = p.get_organization_name(app)
        participant_type = p.get_participant_type(app)
        country_full_name = p.get_country_full_name(app)
        post_address = p.get_post_address(app)
        if inn:
            cur.execute("""SELECT id FROM auction_participant{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX),
                        (inn, kpp))
            res_p = cur.fetchone()
            if res_p:
                id_participiant = res_p['id']
            else:
                cur.execute("""INSERT INTO auction_participant{0} SET inn = %s, kpp = %s, 
                            organization_name = %s, participant_type = %s, country_full_name = %s, 
                            post_address = %s""".format(SUFFIX),
                            (inn, kpp, organization_name, participant_type, country_full_name, post_address))
                id_participiant = cur.lastrowid
        if id_participiant == 0:
            logging_parser('Нет инн', xml, type_f)
        app_rating = p.get_app_rating(app)
        admission = p.get_admission(app)
        price = p.get_price(app)
        cur.execute("""INSERT INTO auction_end_applications{0} SET id_auction_end_protocol = %s, 
                            journal_number = %s, app_rating = %s, admission = %s, 
                            id_participiant = %s, price = %s""".format(SUFFIX),
                    (id_p, journal_number, app_rating,
                     admission, id_participiant, price))
    cur.close()
    con.close()


def parser(doc, path_xml, filexml, reg, type_f):
    global file_log
    try:
        if type_f == Type_protocol.type_EF1:
            parserEF1(doc, path_xml, filexml, reg, type_f)
        elif type_f == Type_protocol.type_EF2:
            parserEF2(doc, path_xml, filexml, reg, type_f)
        elif (type_f == Type_protocol.type_EF3 or type_f == Type_protocol.type_EFSingleApp or
                      type_f == Type_protocol.type_EFSinglePart or type_f == Type_protocol.type_Deviation or
                      type_f == Type_protocol.type_EFInvalidation):
            parserEF3(doc, path_xml, filexml, reg, type_f)
        elif type_f == Type_protocol.type_ZK or type_f == Type_protocol.type_ZKAfterProlong:
            parserZK(doc, path_xml, filexml, reg, type_f)
        elif (type_f == Type_protocol.type_OK2 or type_f == Type_protocol.type_OKD5 or
                      type_f == Type_protocol.type_OKOU3 or type_f == Type_protocol.type_OKSingleApp or
                      type_f == Type_protocol.type_OKDSingleApp or type_f == Type_protocol.type_OKOUSingleApp):
            parserOK2(doc, path_xml, filexml, reg, type_f)
        elif type_f == Type_protocol.type_ZPFinal:
            parserZPFinal(doc, path_xml, filexml, reg, type_f)
    except Exception as e:
        logging_parser("Ошибка в функции parser", e, type_f)
