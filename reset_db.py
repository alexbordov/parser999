import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('contract44')
cur = con.cursor(pymysql.cursors.DictCursor)

cur.execute("""TRUNCATE TABLE od_contract_new""")
cur.execute("""TRUNCATE TABLE od_contract_product_new""")
cur.execute("""TRUNCATE TABLE od_customer_new""")
cur.execute("""TRUNCATE TABLE od_supplier_new""")

cur.execute("""ALTER TABLE od_contract_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE od_contract_product_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE od_customer_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE od_supplier_new AUTO_INCREMENT = 1""")

cur.close()
con.close()
print('Готово')
