import datetime
import logging
import operator
from functools import reduce
from warnings import filterwarnings
import pymysql
from connect_to_db import connect_bd

SUFFIX = ''
DB = 'tender'
LOG_DIR = 'log_protocol'
filterwarnings('ignore', category=pymysql.Warning)
file_log = './{1}/protocol_ftp_{0}.log'.format(str(datetime.date.today()), LOG_DIR)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
type_EF1 = 'ProtocolEF1'
type_EF2 = 'ProtocolEF2'
type_EF3 = 'ProtocolEF3'

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def get_from_dict(data_dict, map_list):
    return reduce(operator.getitem, map_list, data_dict)



def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


def get_el(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = ''
    if res is None:
        res = ''
    if type(res) is str:
        res = res.strip()
    return res


class Protocol:
    def __init__(self, protocol):
        prot = protocol[list(protocol.keys())[0]]
        list_p = [v for v in prot.keys() if 'fcs' in v.lower()]
        self.protocol = prot[list_p[0]]
        if 'protocolLot' in self.protocol:
            if self.protocol['protocolLot'] is None:
                self.applications = []
            elif 'applications' in self.protocol['protocolLot']:
                if 'application' in self.protocol['protocolLot']['applications']:
                    self.applications = generator_univ(self.protocol['protocolLot']['applications']['application'])
                else:
                    self.applications = []
            else:
                self.applications = []
        else:
            self.applications = []

    def get_purchaseNumber(self):
        d = get_el(self.protocol, 'purchaseNumber')
        return d

    def get_id(self):
        d = get_el(self.protocol, 'id')
        return d

    def get_protocol_date(self):
        d = get_el(self.protocol, 'protocolDate')
        if not d:
            d = '0000-00-00'
        return d

    def get_url(self):
        d = get_el(self.protocol, 'href')
        return d

    def get_print_form(self):
        d = get_el(self.protocol, 'printForm', 'url')
        return d

    def get_journal_number(self, application):
        d = get_el(application, 'journalNumber')
        if not d:
            d = 0
        return d


class ProtocolEF2(Protocol):
    def get_offer_first_price(self, application):
        d = get_el(application, 'priceOffers', 'firstOffer', 'price')
        if not d:
            d = 0.0
        return d

    def get_offer_last_price(self, application):
        d = get_el(application, 'priceOffers', 'lastOffer', 'price')
        if not d:
            d = 0.0
        return d

    def get_offer_quantity(self, application):
        d = get_el(application, 'priceOffers', 'offersQuantity')
        if not d:
            d = 0
        return d


class ProtocolEF3(Protocol):
    def get_inn(self, application):
        d = get_el(application, 'appParticipant', 'inn') or get_el(application, 'appParticipant', 'idNumber')
        return d

    def get_kpp(self, application):
        d = get_el(application, 'appParticipant', 'kpp')
        return d

    def get_organization_name(self, application):
        d = get_el(application, 'appParticipant', 'organizationName')
        return d

    def get_participant_type(self, application):
        d = get_el(application, 'appParticipant', 'participantType')
        return d

    def get_country_full_name(self, application):
        d = get_el(application, 'appParticipant', 'country', 'countryFullName')
        return d

    def get_post_address(self, application):
        d = get_el(application, 'appParticipant', 'postAddress')
        return d

    def get_app_rating(self, application):
        d = get_el(application, 'appRating')
        if not d:
            d = 0
        return d

    def get_admission(self, application):
        d = get_el(application, 'admitted')
        if d == 'true':
            d = 'Допущен'
        if not d:
            d = get_el(application, 'notConsidered')
            if d == 'true':
                d = 'Заявка не рассматривалась'
        if not d:
            appRejectedReason = get_el(application, 'appRejectedReason')
            if appRejectedReason:
                reasons = generator_univ(appRejectedReason)
                if reasons:
                    for r in reasons:
                        d = get_el(r, 'nsiRejectReason', 'reason')
        return d


def parserEF1(doc, path_xml, filexml, reg, type_f):
    pass


def parserEF2(doc, path_xml, filexml, reg, type_f):
    p = ProtocolEF2(doc)
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    if not p.applications:
        logging_parser('У протокола нет списка участников', path_xml)
        return
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    con = connect_bd(DB)
    cur = con.cursor()
    for app in p.applications:
        journal_number = p.get_journal_number(app)
        offer_first_price = p.get_offer_first_price(app)
        offer_last_price = p.get_offer_last_price(app)
        offer_quantity = p.get_offer_quantity(app)
        cur.execute(
                """SELECT id FROM protocolEF2{0} WHERE purchase_number = %s AND journal_number = %s""".format(SUFFIX),
                (purchase_number, journal_number))
        res_pn = cur.fetchone()
        if res_pn:
            # logging_parser("Данные по этому тендеру  и участнику уже есть в базе", purchase_number, path_xml)
            cur.execute(
                    """UPDATE protocolEF2{0} SET id_protocol = %s, protocol_date =  %s,
                        offer_first_price = %s, offer_last_price = %s, offer_quantity = %s, url = %s, 	print_form = %s
                        WHERE purchase_number = %s AND journal_number = %s""".format(SUFFIX),
                    (
                        id_protocol, protocol_date, offer_first_price, offer_last_price,
                        offer_quantity,
                        url, print_form, purchase_number, journal_number))
        else:
            cur.execute(
                    """INSERT INTO protocolEF2{0} SET id_protocol = %s, protocol_date =  %s, purchase_number = %s, 
                        journal_number = %s, 
                        offer_first_price = %s, offer_last_price = %s, offer_quantity = %s, url = %s, 	print_form = %s
                        """.format(SUFFIX),
                    (
                        id_protocol, protocol_date, purchase_number, journal_number, offer_first_price,
                        offer_last_price,
                        offer_quantity,
                        url, print_form))

    cur.close()
    con.close()


def parserEF3(doc, path_xml, filexml, reg, type_f):
    p = ProtocolEF3(doc)
    purchase_number = p.get_purchaseNumber()
    if not purchase_number:
        logging_parser('У протокола нет purchase_number', path_xml)
        return
    if not p.applications:
        logging_parser('У протокола нет списка участников', path_xml)
        return
    # if purchase_number in ['0126300004316000033', '0126300004316000072']:
    #     logging_parser('not inn', path_xml)
    id_protocol = p.get_id()
    url = p.get_url()
    print_form = p.get_print_form()
    protocol_date = p.get_protocol_date()
    con = connect_bd(DB)
    cur = con.cursor()
    for app in p.applications:
        id_participiant = 0
        journal_number = p.get_journal_number(app)
        inn = p.get_inn(app)
        kpp = p.get_kpp(app)
        organization_name = p.get_organization_name(app)
        participant_type = p.get_participant_type(app)
        country_full_name = p.get_country_full_name(app)
        post_address = p.get_post_address(app)
        if inn:
            cur.execute("""SELECT id FROM participant{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX), (inn, kpp))
            res_p = cur.fetchone()
            if res_p:
                id_participiant = res_p['id']
            else:
                cur.execute("""INSERT INTO participant{0} SET inn = %s, kpp = %s, 
                        organization_name = %s, participant_type = %s, country_full_name = %s, 
                        post_address = %s""".format(SUFFIX),
                            (inn, kpp, organization_name, participant_type, country_full_name, post_address))
                id_participiant = cur.lastrowid
        app_rating = p.get_app_rating(app)
        admission = p.get_admission(app)
        cur.execute(
                """SELECT id FROM protocolEF3{0} WHERE purchase_number = %s AND journal_number = %s""".format(SUFFIX),
                (purchase_number, journal_number))
        res_pn = cur.fetchone()
        if res_pn:
            # logging_parser("Данные по этому тендеру и участнику уже есть в базе", purchase_number, path_xml)
            cur.execute("""UPDATE protocolEF3{0} SET id_protocol = %s, protocol_date = %s, url = %s, print_form = %s, 
            app_rating = %s, admission = %s, id_participiant = %s 
            WHERE purchase_number = %s AND journal_number = %s""".format(
                    SUFFIX),
                    (id_protocol, protocol_date, url, print_form, app_rating, admission, id_participiant,
                     purchase_number, journal_number))
        else:
            cur.execute("""INSERT INTO protocolEF3{0} SET id_protocol = %s, protocol_date = %s, purchase_number = %s, 
                        url = %s, print_form = %s, journal_number = %s, app_rating = %s, admission = %s, 
                        id_participiant = %s""".format(SUFFIX),
                        (id_protocol, protocol_date, purchase_number, url, print_form, journal_number, app_rating,
                         admission, id_participiant))
    cur.close()
    con.close()


def parser(doc, path_xml, filexml, reg, type_f):
    global file_log
    try:
        # if type_f == type_EF1:
        #     parserEF1(doc, path_xml, filexml, reg, type_f)
        if type_f == type_EF2:
            parserEF2(doc, path_xml, filexml, reg, type_f)
        elif type_f == type_EF3:
            parserEF3(doc, path_xml, filexml, reg, type_f)
    except Exception as e:
        logging_parser("Ошибка в функции parser", e, type_f)
