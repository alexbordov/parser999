import pymysql
import logging
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import datetime
import smtplib

file_log = './log_send_email/log_send_email_update_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def connect():
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db="tender", charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

def send_email(em):
    me = 'info@enter-it.ru'
    you = em
    text = """<p>Уважаемый пользователь, мы приготовили для Вас новую версию конфигурации «Рабочее место специалиста по тендерам»</p>
    <p>Данное обновление включает в себя множество улучшений и дополнений, делающих работу с нашей конфигурацией еще приятнее.</p>
    <p>Ссылка на новейшую версию конфигурации: <a href="http://enter-it.ru/distributives/tenders/get_1c_conf.php">Конфигурация "Рабочее место специалиста по тендерам"</a></p>
    <p>Инструкции по установке и обновлению конфигурации Вы можете скачать по ссылкам ниже:</p>
    <p><a href="http://enter-it.ru/distributives/documents/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20%D0%BE%D0%B1%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8E.docx">Инструкция по обновлению конфигурации</a></p>
    <p><a href="http://enter-it.ru/distributives/documents/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B5%20%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D0%B8.docx">Инструкция по установке конфигурации</a></p>
    <p>Спасибо, что выбрали нас.<br />Если у Вас возникли какие-то вопросы, связаться с нами можно по телефону: 8 (900) 365 55 40 или через e-mail: info@enter-it.ru</p>
    <p> </p>"""
    subj = 'Обновление конфигурации «Рабочее место специалиста по тендерам»'
    server = "smtp.jino.ru"
    port = 25
    user_name = "info@enter-it.ru"
    user_passwd = "********"
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = you
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, you, msg.as_string())
    s.quit()

conn = connect()
cur = conn.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT login FROM user """)
for usr in cur.fetchall():
    try:
        addr = usr['login']
        if not addr == 'olgakumar@mail.ru':
            send_email(addr)
            print('Письмо на адрес: ' + addr + ' отправлено')
    except Exception:
        logging.exception('Ошибка отправки письма: ')
        with open(file_log, 'a') as flog:
            flog.write('Ошибка отправки письма на адрес: ' + str(usr['login']) + '\n\n')

conn.close()
cur.close()
