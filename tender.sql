-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Апр 09 2018 г., 13:49
-- Версия сервера: 10.2.6-MariaDB-log
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tender`
--

-- --------------------------------------------------------

--
-- Структура таблицы `archiv`
--

CREATE TABLE IF NOT EXISTS `archiv` (
  `id_archiv` int(11) NOT NULL AUTO_INCREMENT,
  `id_region` int(11) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `pars` int(11) NOT NULL,
  PRIMARY KEY (`id_archiv`),
  UNIQUE KEY `id_archiv` (`id_archiv`),
  KEY `name` (`name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `archiv_plan_graphs`
--

CREATE TABLE IF NOT EXISTS `archiv_plan_graphs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22552 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_bank`
--

CREATE TABLE IF NOT EXISTS `arhiv_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=120436 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_complaint`
--

CREATE TABLE IF NOT EXISTS `arhiv_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3207 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_complaint_result`
--

CREATE TABLE IF NOT EXISTS `arhiv_complaint_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1320 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_contract`
--

CREATE TABLE IF NOT EXISTS `arhiv_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75299 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_contract223`
--

CREATE TABLE IF NOT EXISTS `arhiv_contract223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106698 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_explanation223`
--

CREATE TABLE IF NOT EXISTS `arhiv_explanation223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33381 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_organization_from_ftp223`
--

CREATE TABLE IF NOT EXISTS `arhiv_organization_from_ftp223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1578 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_prot`
--

CREATE TABLE IF NOT EXISTS `arhiv_prot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`),
  KEY `arhiv_2` (`arhiv`(255),`region`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16672 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_protocols223`
--

CREATE TABLE IF NOT EXISTS `arhiv_protocols223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=408235 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_protocols_new`
--

CREATE TABLE IF NOT EXISTS `arhiv_protocols_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`),
  KEY `arhiv_2` (`arhiv`(255),`region`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=191 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_tender223_sign`
--

CREATE TABLE IF NOT EXISTS `arhiv_tender223_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  `region` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255)),
  KEY `region` (`region`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=98864 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_tenders`
--

CREATE TABLE IF NOT EXISTS `arhiv_tenders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=342429 ;

-- --------------------------------------------------------

--
-- Структура таблицы `arhiv_unfair_suppliers`
--

CREATE TABLE IF NOT EXISTS `arhiv_unfair_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=683 ;

-- --------------------------------------------------------

--
-- Структура таблицы `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `id_attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_tender` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `url` varchar(2000) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `attach_text` longtext NOT NULL,
  `attach_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_attachment`),
  KEY `id_tender` (`id_tender`),
  KEY `attach_add` (`attach_add`),
  FULLTEXT KEY `attach_text` (`attach_text`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28140568 ;

-- --------------------------------------------------------

--
-- Структура таблицы `attach_complaint`
--

CREATE TABLE IF NOT EXISTS `attach_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_complaint` int(11) NOT NULL,
  `publishedContentId` varchar(200) NOT NULL,
  `fileName` varchar(1000) NOT NULL,
  `docDescription` varchar(2000) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_complaint` (`id_complaint`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=656382 ;

-- --------------------------------------------------------

--
-- Структура таблицы `attach_complaint_res`
--

CREATE TABLE IF NOT EXISTS `attach_complaint_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_complaint_res` int(11) NOT NULL,
  `fileName` varchar(1000) NOT NULL,
  `docDescription` varchar(2000) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_complaint_res` (`id_complaint_res`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=200895 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_applications`
--

CREATE TABLE IF NOT EXISTS `auction_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `offer_first_price` decimal(30,2) NOT NULL,
  `offer_last_price` decimal(30,2) NOT NULL,
  `offer_quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_auction_protocol` (`id_auction_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9123670 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_applications_new`
--

CREATE TABLE IF NOT EXISTS `auction_applications_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `offer_first_price` decimal(30,2) NOT NULL,
  `offer_last_price` decimal(30,2) NOT NULL,
  `offer_quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_auction_protocol` (`id_auction_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65006 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_end_applications`
--

CREATE TABLE IF NOT EXISTS `auction_end_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_end_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `app_rating` int(11) NOT NULL,
  `admission` varchar(1024) NOT NULL,
  `id_participiant` int(11) NOT NULL,
  `result_zk` varchar(255) NOT NULL,
  `price` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_participiant` (`id_participiant`),
  KEY `id_auction_end_protocol` (`id_auction_end_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11474013 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_end_applications_new`
--

CREATE TABLE IF NOT EXISTS `auction_end_applications_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_end_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `app_rating` int(11) NOT NULL,
  `admission` varchar(1024) NOT NULL,
  `id_participiant` int(11) NOT NULL,
  `result_zk` varchar(255) NOT NULL,
  `price` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_participiant` (`id_participiant`),
  KEY `id_auction_end_protocol` (`id_auction_end_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94079 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_end_protocol`
--

CREATE TABLE IF NOT EXISTS `auction_end_protocol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `abandoned_reason_name` varchar(2000) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `refusal_fact` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`),
  KEY `lot_number` (`lot_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5126241 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_end_protocol_new`
--

CREATE TABLE IF NOT EXISTS `auction_end_protocol_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `abandoned_reason_name` varchar(2000) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `refusal_fact` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`),
  KEY `lot_number` (`lot_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46644 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_participant`
--

CREATE TABLE IF NOT EXISTS `auction_participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(255) NOT NULL,
  `kpp` varchar(255) NOT NULL,
  `organization_name` varchar(2000) NOT NULL,
  `participant_type` varchar(20) NOT NULL,
  `country_full_name` varchar(200) NOT NULL,
  `post_address` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`),
  KEY `inn_2` (`inn`,`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=541965 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_participant_new`
--

CREATE TABLE IF NOT EXISTS `auction_participant_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(255) NOT NULL,
  `kpp` varchar(255) NOT NULL,
  `organization_name` varchar(2000) NOT NULL,
  `participant_type` varchar(20) NOT NULL,
  `country_full_name` varchar(200) NOT NULL,
  `post_address` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`),
  KEY `inn_2` (`inn`,`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10602 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_protocol`
--

CREATE TABLE IF NOT EXISTS `auction_protocol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2507736 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_protocol_new`
--

CREATE TABLE IF NOT EXISTS `auction_protocol_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17984 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_start_applications`
--

CREATE TABLE IF NOT EXISTS `auction_start_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `admission` varchar(1024) NOT NULL,
  `id_participiant` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_auction_protocol` (`id_auction_protocol`),
  KEY `id_participiant` (`id_participiant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11795809 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_start_applications_new`
--

CREATE TABLE IF NOT EXISTS `auction_start_applications_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_auction_protocol` int(11) NOT NULL,
  `journal_number` varchar(255) NOT NULL,
  `admission` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `journal_number` (`journal_number`),
  KEY `id_auction_protocol` (`id_auction_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78599 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_start_protocol`
--

CREATE TABLE IF NOT EXISTS `auction_start_protocol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `abandoned_reason_name` varchar(2000) NOT NULL,
  `lot_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`),
  KEY `lot_number` (`lot_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3100774 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auction_start_protocol_new`
--

CREATE TABLE IF NOT EXISTS `auction_start_protocol_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` varchar(50) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `type_protocol` varchar(128) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `abandoned_reason_name` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_protocol` (`id_protocol`),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23533 ;

-- --------------------------------------------------------

--
-- Структура таблицы `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(100) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `shortName` varchar(1000) NOT NULL,
  `factAddress` varchar(2000) NOT NULL,
  `INN` varchar(100) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  `subjectRFName` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=543 ;

-- --------------------------------------------------------

--
-- Структура таблицы `bank_attach`
--

CREATE TABLE IF NOT EXISTS `bank_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_guar` int(11) NOT NULL,
  `fileName` varchar(2000) NOT NULL,
  `docDescription` varchar(4000) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_guar` (`id_guar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4323044 ;

-- --------------------------------------------------------

--
-- Структура таблицы `bank_customer`
--

CREATE TABLE IF NOT EXISTS `bank_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(100) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `factAddress` varchar(2000) NOT NULL,
  `INN` varchar(100) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  `registrationDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93821 ;

-- --------------------------------------------------------

--
-- Структура таблицы `bank_guarantee`
--

CREATE TABLE IF NOT EXISTS `bank_guarantee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_guarantee` varchar(100) NOT NULL,
  `regNumber` varchar(100) NOT NULL,
  `docNumber` varchar(100) NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `docPublishDate` datetime NOT NULL,
  `purchaseNumber` varchar(100) NOT NULL,
  `lotNumber` int(11) NOT NULL,
  `guaranteeDate` datetime NOT NULL,
  `guaranteeAmount` decimal(30,2) NOT NULL,
  `currencyCode` varchar(10) NOT NULL,
  `expireDate` datetime NOT NULL,
  `entryForceDate` datetime NOT NULL,
  `href` varchar(200) NOT NULL,
  `print_form` varchar(200) NOT NULL,
  `xml` varchar(1000) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `id_placer` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNumber` (`regNumber`),
  KEY `versionNumber` (`versionNumber`),
  KEY `docPublishDate` (`docPublishDate`),
  KEY `purchaseNumber` (`purchaseNumber`),
  KEY `lotNumber` (`lotNumber`),
  KEY `id_bank` (`id_bank`),
  KEY `id_customer` (`id_customer`),
  KEY `id_supplier` (`id_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2249071 ;

-- --------------------------------------------------------

--
-- Структура таблицы `bank_supplier`
--

CREATE TABLE IF NOT EXISTS `bank_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participantType` varchar(100) NOT NULL,
  `inn` varchar(100) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `ogrn` varchar(100) NOT NULL,
  `organizationName` varchar(2000) NOT NULL,
  `firmName` varchar(1000) NOT NULL,
  `registrationDate` datetime NOT NULL,
  `subjectRFName` varchar(100) NOT NULL,
  `address` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=123471 ;

-- --------------------------------------------------------

--
-- Структура таблицы `clarifications`
--

CREATE TABLE IF NOT EXISTS `clarifications` (
  `id_clarification` int(11) NOT NULL AUTO_INCREMENT,
  `id_xml` varchar(50) NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `doc_publish_date` datetime NOT NULL,
  `href` varchar(2000) NOT NULL,
  `doc_number` varchar(1200) NOT NULL,
  `question` varchar(5000) NOT NULL,
  `topic` varchar(5000) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  PRIMARY KEY (`id_clarification`),
  KEY `id_xml` (`id_xml`),
  KEY `purchase_number` (`purchase_number`),
  KEY `doc_publish_date` (`doc_publish_date`),
  KEY `doc_number` (`doc_number`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=973711 ;

-- --------------------------------------------------------

--
-- Структура таблицы `clarif_attachments`
--

CREATE TABLE IF NOT EXISTS `clarif_attachments` (
  `id_clarif_attachments` int(11) NOT NULL AUTO_INCREMENT,
  `id_clarification` int(11) NOT NULL,
  `file_name` varchar(2000) NOT NULL,
  `url` varchar(2000) NOT NULL,
  `description` varchar(5000) NOT NULL,
  PRIMARY KEY (`id_clarif_attachments`),
  KEY `id_clarification` (`id_clarification`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1042421 ;

-- --------------------------------------------------------

--
-- Структура таблицы `complaint`
--

CREATE TABLE IF NOT EXISTS `complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_complaint` varchar(100) NOT NULL,
  `complaintNumber` varchar(200) NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `planDecisionDate` datetime NOT NULL,
  `id_registrationKO` int(11) NOT NULL,
  `id_considerationKO` int(11) NOT NULL,
  `regDate` datetime NOT NULL,
  `notice_number` varchar(200) NOT NULL,
  `notice_acceptDate` datetime NOT NULL,
  `id_createOrganization` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `id_publishOrganization` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `applicant_fullName` varchar(2000) NOT NULL,
  `applicant_INN` varchar(50) NOT NULL,
  `applicant_KPP` varchar(50) NOT NULL,
  `purchaseNumber` varchar(200) NOT NULL,
  `lotNumbers` varchar(100) NOT NULL,
  `lots_info` varchar(2000) NOT NULL,
  `purchaseName` varchar(2000) NOT NULL,
  `purchasePlacingDate` datetime NOT NULL,
  `text_complaint` varchar(5000) NOT NULL,
  `printForm` varchar(2000) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `tender_suspend` varchar(50) NOT NULL,
  `returnInfo` varchar(4000) NOT NULL,
  `regNumber` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_complaint` (`id_complaint`),
  KEY `complaintNumber` (`complaintNumber`),
  KEY `versionNumber` (`versionNumber`),
  KEY `planDecisionDate` (`planDecisionDate`),
  KEY `id_registrationKO` (`id_registrationKO`),
  KEY `id_considerationKO` (`id_considerationKO`),
  KEY `id_createOrganization` (`id_createOrganization`),
  KEY `id_publishOrganization` (`id_publishOrganization`),
  KEY `id_customer` (`id_customer`),
  KEY `applicant_INN` (`applicant_INN`),
  KEY `applicant_KPP` (`applicant_KPP`),
  KEY `purchaseNumber` (`purchaseNumber`),
  KEY `cancel` (`cancel`),
  KEY `tender_suspend` (`tender_suspend`),
  KEY `regNumber` (`regNumber`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248591 ;

-- --------------------------------------------------------

--
-- Структура таблицы `contract_sign`
--

CREATE TABLE IF NOT EXISTS `contract_sign` (
  `id_contract_sign` int(11) NOT NULL AUTO_INCREMENT,
  `id_tender` int(11) NOT NULL,
  `id_sign` varchar(255) NOT NULL,
  `purchase_number` varchar(200) NOT NULL,
  `sign_number` varchar(50) NOT NULL,
  `sign_date` datetime NOT NULL,
  `id_customer` int(11) NOT NULL,
  `customer_reg_num` varchar(200) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `contract_sign_price` decimal(30,2) NOT NULL,
  `sign_currency` varchar(100) NOT NULL,
  `conclude_contract_right` int(11) NOT NULL,
  `protocole_date` datetime NOT NULL,
  `supplier_contact` varchar(1000) NOT NULL,
  `supplier_email` varchar(500) NOT NULL,
  `supplier_contact_phone` varchar(100) NOT NULL,
  `supplier_contact_fax` varchar(100) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  PRIMARY KEY (`id_contract_sign`),
  KEY `id_customer` (`id_customer`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_sign` (`id_sign`),
  KEY `id_tender` (`id_tender`),
  KEY `sign_number` (`sign_number`),
  KEY `id_supplier` (`id_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6938846 ;

-- --------------------------------------------------------

--
-- Структура таблицы `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `reg_num` varchar(255) NOT NULL,
  `full_name` varchar(2000) NOT NULL,
  `inn` varchar(50) NOT NULL,
  `is223` int(11) NOT NULL,
  PRIMARY KEY (`id_customer`),
  UNIQUE KEY `reg_num` (`reg_num`),
  KEY `inn` (`inn`),
  KEY `full_name` (`full_name`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=200669 ;

-- --------------------------------------------------------

--
-- Структура таблицы `customer223`
--

CREATE TABLE IF NOT EXISTS `customer223` (
  `id_customer223` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(2000) NOT NULL,
  `inn` varchar(50) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `ogrn` varchar(50) NOT NULL,
  `post_address` varchar(2000) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_customer223`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8154 ;

-- --------------------------------------------------------

--
-- Структура таблицы `customer_complaint`
--

CREATE TABLE IF NOT EXISTS `customer_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(50) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `INN` varchar(50) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17815 ;

-- --------------------------------------------------------

--
-- Структура таблицы `customer_requirement`
--

CREATE TABLE IF NOT EXISTS `customer_requirement` (
  `id_customer_requirement` int(11) NOT NULL AUTO_INCREMENT,
  `id_lot` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `kladr_place` varchar(5000) NOT NULL,
  `delivery_place` varchar(2000) NOT NULL,
  `delivery_term` varchar(2000) NOT NULL,
  `application_guarantee_amount` decimal(30,2) NOT NULL,
  `application_settlement_account` varchar(255) NOT NULL,
  `application_personal_account` varchar(255) NOT NULL,
  `application_bik` varchar(255) NOT NULL,
  `contract_guarantee_amount` decimal(30,2) NOT NULL,
  `contract_settlement_account` varchar(255) NOT NULL,
  `contract_personal_account` varchar(255) NOT NULL,
  `contract_bik` varchar(255) NOT NULL,
  `max_price` varchar(255) NOT NULL,
  PRIMARY KEY (`id_customer_requirement`),
  KEY `id_lot` (`id_lot`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9995661 ;

-- --------------------------------------------------------

--
-- Структура таблицы `etp`
--

CREATE TABLE IF NOT EXISTS `etp` (
  `id_etp` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(2000) NOT NULL,
  `conf` int(11) NOT NULL,
  PRIMARY KEY (`id_etp`),
  KEY `code` (`code`),
  KEY `name` (`name`),
  KEY `conf` (`conf`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=155 ;

-- --------------------------------------------------------

--
-- Структура таблицы `goszakup_kz_attach`
--

CREATE TABLE IF NOT EXISTS `goszakup_kz_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_notif` int(11) NOT NULL,
  `name_doc` varchar(1024) NOT NULL,
  `url_doc` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_lot` (`id_notif`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82164 ;

-- --------------------------------------------------------

--
-- Структура таблицы `goszakup_kz_lots`
--

CREATE TABLE IF NOT EXISTS `goszakup_kz_lots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_notif` int(11) NOT NULL,
  `num_lot` varchar(255) NOT NULL,
  `customer` varchar(1024) NOT NULL,
  `name_lot` varchar(1024) NOT NULL,
  `exend_info` varchar(2000) NOT NULL,
  `price` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `okei` varchar(1024) NOT NULL,
  `sum` varchar(100) NOT NULL,
  `status_lot` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tender` (`id_notif`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43946 ;

-- --------------------------------------------------------

--
-- Структура таблицы `goszakup_kz_notif`
--

CREATE TABLE IF NOT EXISTS `goszakup_kz_notif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_num_notif` varchar(128) NOT NULL,
  `short_num_notif` int(128) NOT NULL,
  `ver_num_notif` int(5) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `status` varchar(1024) NOT NULL,
  `pub_date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `change_reason` varchar(2000) NOT NULL,
  `way_notif` varchar(1024) NOT NULL,
  `type_notif` varchar(1024) NOT NULL,
  `sum_notif` varchar(100) NOT NULL,
  `feature_notif` varchar(1024) NOT NULL,
  `view_object_notif` varchar(1024) NOT NULL,
  `url_notif` varchar(1024) NOT NULL,
  `id_customer` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `full_num_notif` (`full_num_notif`),
  KEY `short_num_notif` (`short_num_notif`),
  KEY `ver_num_notif` (`ver_num_notif`),
  KEY `cancel` (`cancel`),
  KEY `pub_date` (`pub_date`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `id_cistomer` (`id_customer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19358 ;

-- --------------------------------------------------------

--
-- Структура таблицы `goszakup_kz_org`
--

CREATE TABLE IF NOT EXISTS `goszakup_kz_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_name` varchar(1024) NOT NULL,
  `org_reg_office` varchar(1024) NOT NULL,
  `org_person` varchar(1024) NOT NULL,
  `org_position` varchar(1024) NOT NULL,
  `org_tel` varchar(1024) NOT NULL,
  `org_email` varchar(1024) NOT NULL,
  `org_bank` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `org_name` (`org_name`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4861 ;

-- --------------------------------------------------------

--
-- Структура таблицы `lot`
--

CREATE TABLE IF NOT EXISTS `lot` (
  `id_lot` int(11) NOT NULL AUTO_INCREMENT,
  `lot_number` int(11) NOT NULL,
  `id_tender` int(11) NOT NULL,
  `max_price` decimal(30,2) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `finance_source` varchar(2000) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_lot`),
  KEY `id_tender` (`id_tender`),
  KEY `max_price` (`max_price`),
  KEY `lot_number` (`lot_number`),
  KEY `id_tender_2` (`id_tender`,`lot_number`),
  KEY `finance_source` (`finance_source`(1024)),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12630008 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_contract`
--

CREATE TABLE IF NOT EXISTS `od_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contract` varchar(50) NOT NULL,
  `p_number` varchar(50) NOT NULL,
  `regnum` varchar(255) NOT NULL,
  `current_contract_stage` varchar(20) NOT NULL,
  `placing` varchar(300) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `sign_date` date NOT NULL,
  `single_customer_reason_code` varchar(20) NOT NULL,
  `single_customer_reason_name` varchar(1024) NOT NULL,
  `fz` varchar(100) NOT NULL,
  `notification_number` varchar(100) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `contract_price` decimal(30,2) NOT NULL,
  `currency` varchar(500) NOT NULL,
  `version_number` int(11) NOT NULL,
  `execution_start_date` date NOT NULL,
  `execution_end_date` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `cancel` int(1) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `sign_number` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_contract` (`id_contract`),
  KEY `id_customer` (`id_customer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `region_code` (`region_code`),
  KEY `sign_date` (`sign_date`),
  KEY `notification_number` (`notification_number`),
  KEY `cancel` (`cancel`),
  KEY `version_number` (`version_number`),
  KEY `regnum` (`regnum`),
  KEY `sign_number` (`sign_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16513014 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_contract_product`
--

CREATE TABLE IF NOT EXISTS `od_contract_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_od_contract` int(11) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `okpd2_code` varchar(50) NOT NULL,
  `okpd_code` varchar(50) NOT NULL,
  `okpd2_group_code` int(11) NOT NULL,
  `okpd_group_code` int(11) NOT NULL,
  `okpd2_group_level1_code` varchar(11) NOT NULL,
  `okpd_group_level1_code` varchar(11) NOT NULL,
  `price` decimal(30,2) NOT NULL,
  `okpd2_name` varchar(1024) NOT NULL,
  `okpd_name` varchar(1024) NOT NULL,
  `quantity` decimal(30,5) NOT NULL,
  `okei` varchar(50) NOT NULL,
  `sum` decimal(30,2) NOT NULL,
  `sid` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `okpd2_group_code` (`okpd2_group_code`,`okpd2_group_level1_code`),
  KEY `id_od_contract` (`id_od_contract`),
  KEY `okpd2_group_code_2` (`okpd2_group_code`),
  KEY `okpd2_group_level1_code` (`okpd2_group_level1_code`),
  KEY `okpd2_name` (`okpd2_name`(255)),
  KEY `okei` (`okei`),
  KEY `okpd2_code` (`okpd2_code`),
  KEY `price` (`price`),
  KEY `quantity` (`quantity`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66913862 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_customer`
--

CREATE TABLE IF NOT EXISTS `od_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNumber` varchar(255) DEFAULT NULL,
  `inn` varchar(50) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `contracts_count` int(11) NOT NULL,
  `contracts223_count` int(11) NOT NULL,
  `contracts_sum` decimal(50,2) NOT NULL,
  `contracts223_sum` decimal(50,2) NOT NULL,
  `ogrn` varchar(50) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `full_name` varchar(2000) NOT NULL,
  `postal_address` varchar(2000) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_name` varchar(300) NOT NULL,
  `short_name` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `regNumber` (`regNumber`),
  KEY `inn` (`inn`),
  KEY `full_name` (`full_name`(255)),
  KEY `kpp` (`kpp`) USING BTREE,
  KEY `inn_2` (`inn`,`kpp`) USING BTREE,
  KEY `short_name` (`short_name`(1024))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=299436 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_customer_from_ftp`
--

CREATE TABLE IF NOT EXISTS `od_customer_from_ftp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNumber` varchar(255) DEFAULT NULL,
  `inn` varchar(50) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `contracts_count` int(11) NOT NULL,
  `contracts223_count` int(11) NOT NULL,
  `contracts_sum` decimal(50,2) NOT NULL,
  `contracts223_sum` decimal(50,2) NOT NULL,
  `ogrn` varchar(50) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `full_name` varchar(2000) NOT NULL,
  `short_name` varchar(2000) NOT NULL,
  `postal_address` varchar(2000) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `regNumber` (`regNumber`),
  KEY `inn` (`inn`),
  KEY `full_name` (`full_name`(255)),
  KEY `kpp` (`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=298909 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_customer_from_ftp223`
--

CREATE TABLE IF NOT EXISTS `od_customer_from_ftp223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNumber` varchar(255) DEFAULT NULL,
  `inn` varchar(50) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `contracts_count` int(11) NOT NULL,
  `contracts223_count` int(11) NOT NULL,
  `contracts_sum` decimal(50,2) NOT NULL,
  `contracts223_sum` decimal(50,2) NOT NULL,
  `ogrn` varchar(50) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `full_name` varchar(2000) NOT NULL,
  `short_name` varchar(2000) NOT NULL,
  `postal_address` varchar(2000) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_name` varchar(300) NOT NULL,
  `code` varchar(255) NOT NULL,
  `okato` varchar(255) NOT NULL,
  `oktmo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `regNumber` (`regNumber`),
  KEY `inn` (`inn`),
  KEY `full_name` (`full_name`(255)),
  KEY `code` (`code`),
  KEY `okato` (`okato`),
  KEY `oktmo` (`oktmo`),
  KEY `kpp` (`kpp`),
  KEY `ogrn` (`ogrn`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89090 ;

-- --------------------------------------------------------

--
-- Структура таблицы `od_supplier`
--

CREATE TABLE IF NOT EXISTS `od_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(50) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `contracts_count` int(11) NOT NULL,
  `contracts223_count` int(11) NOT NULL,
  `contracts_sum` decimal(50,2) NOT NULL,
  `contracts223_sum` decimal(50,2) NOT NULL,
  `ogrn` varchar(50) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `organizationName` varchar(2000) NOT NULL,
  `postal_address` varchar(2000) NOT NULL,
  `contactPhone` varchar(50) NOT NULL,
  `contactFax` varchar(50) NOT NULL,
  `contactEMail` varchar(100) NOT NULL,
  `contact_name` varchar(300) NOT NULL,
  `organizationShortName` varchar(2000) NOT NULL,
  `all_names` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `region_code` (`region_code`),
  KEY `inn` (`inn`),
  KEY `contactPhone` (`contactPhone`),
  KEY `organizationName` (`organizationName`(255)),
  KEY `kpp` (`kpp`),
  KEY `inn_2` (`inn`,`kpp`),
  KEY `organizationShortName` (`organizationShortName`(1024))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1514320 ;

-- --------------------------------------------------------

--
-- Структура таблицы `okpd2_sprav`
--

CREATE TABLE IF NOT EXISTS `okpd2_sprav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `level` tinyint(2) DEFAULT 0,
  `parent` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `level` (`level`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 COMMENT='Справочник ОКПД2' AUTO_INCREMENT=17433 ;

-- --------------------------------------------------------

--
-- Структура таблицы `okpd_group`
--

CREATE TABLE IF NOT EXISTS `okpd_group` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `okpd_item`
--

CREATE TABLE IF NOT EXISTS `okpd_item` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `okrug`
--

CREATE TABLE IF NOT EXISTS `okrug` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `organizer`
--

CREATE TABLE IF NOT EXISTS `organizer` (
  `id_organizer` int(11) NOT NULL AUTO_INCREMENT,
  `reg_num` varchar(64) NOT NULL,
  `full_name` varchar(2000) NOT NULL,
  `post_address` varchar(2000) NOT NULL,
  `fact_address` varchar(2000) NOT NULL,
  `inn` varchar(255) NOT NULL,
  `kpp` varchar(255) NOT NULL,
  `responsible_role` varchar(255) NOT NULL,
  `contact_person` varchar(510) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `contact_fax` varchar(255) NOT NULL,
  PRIMARY KEY (`id_organizer`),
  KEY `reg_num` (`reg_num`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`),
  KEY `inn_2` (`inn`,`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=186433 ;

-- --------------------------------------------------------

--
-- Структура таблицы `org_complaint`
--

CREATE TABLE IF NOT EXISTS `org_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(50) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `INN` varchar(50) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=182 ;

-- --------------------------------------------------------

--
-- Структура таблицы `placer_org`
--

CREATE TABLE IF NOT EXISTS `placer_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(100) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `shortName` varchar(1000) NOT NULL,
  `factAddress` varchar(2000) NOT NULL,
  `INN` varchar(100) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  `subjectRFName` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=500 ;

-- --------------------------------------------------------

--
-- Структура таблицы `placing_way`
--

CREATE TABLE IF NOT EXISTS `placing_way` (
  `id_placing_way` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `conformity` int(11) NOT NULL,
  PRIMARY KEY (`id_placing_way`),
  KEY `code` (`code`),
  KEY `conformity` (`conformity`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64227 ;

-- --------------------------------------------------------

--
-- Структура таблицы `preferense`
--

CREATE TABLE IF NOT EXISTS `preferense` (
  `id_preferens` int(11) NOT NULL AUTO_INCREMENT,
  `id_lot` int(11) NOT NULL,
  `name` varchar(5000) NOT NULL,
  PRIMARY KEY (`id_preferens`),
  KEY `id_lot` (`id_lot`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3383582 ;

-- --------------------------------------------------------

--
-- Структура таблицы `protocols223`
--

CREATE TABLE IF NOT EXISTS `protocols223` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `protocol_date` datetime NOT NULL,
  `url` varchar(1024) NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `type_protocol` varchar(1024) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `missed_contest` varchar(50) NOT NULL,
  `missed_reason` varchar(1024) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`),
  KEY `protocol_date` (`protocol_date`),
  KEY `purchase_number` (`purchase_number`),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4056921 ;

-- --------------------------------------------------------

--
-- Структура таблицы `protocols223_attach`
--

CREATE TABLE IF NOT EXISTS `protocols223_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` int(11) NOT NULL,
  `filename` varchar(1024) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `url` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_protocol` (`id_protocol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4684757 ;

-- --------------------------------------------------------

--
-- Структура таблицы `protocols223_supp`
--

CREATE TABLE IF NOT EXISTS `protocols223_supp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) NOT NULL,
  `inn` varchar(255) NOT NULL,
  `kpp` varchar(255) NOT NULL,
  `address` varchar(2000) NOT NULL,
  `ogrn` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=418799 ;

-- --------------------------------------------------------

--
-- Структура таблицы `ptotocols223_appl`
--

CREATE TABLE IF NOT EXISTS `ptotocols223_appl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_protocol` int(11) NOT NULL,
  `app_number` varchar(255) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `price_info` varchar(1024) NOT NULL,
  `accepted` varchar(255) NOT NULL,
  `winner_indication` varchar(50) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `rejection_reason` varchar(2000) NOT NULL,
  `last_price` decimal(30,2) NOT NULL,
  `last_price_info` varchar(2000) NOT NULL,
  `application_rate` varchar(200) NOT NULL,
  `ordinal_number` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_protocol` (`id_protocol`),
  KEY `app_number` (`app_number`),
  KEY `id_supplier` (`id_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7279457 ;

-- --------------------------------------------------------

--
-- Структура таблицы `purchase_object`
--

CREATE TABLE IF NOT EXISTS `purchase_object` (
  `id_purchase_object` int(11) NOT NULL AUTO_INCREMENT,
  `okpd_code` varchar(255) NOT NULL,
  `okpd2_group_level1_code` int(11) NOT NULL,
  `okpd2_code` varchar(100) NOT NULL,
  `okpd2_group_code` int(11) NOT NULL,
  `okpd_name` varchar(1000) NOT NULL,
  `id_lot` int(11) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `quantity_value` varchar(255) NOT NULL,
  `price` decimal(30,2) NOT NULL,
  `okei` varchar(255) NOT NULL,
  `sum` decimal(30,2) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `customer_quantity_value` varchar(255) NOT NULL,
  PRIMARY KEY (`id_purchase_object`),
  KEY `id_lot` (`id_lot`),
  KEY `id_customer` (`id_customer`),
  KEY `okpd_name` (`okpd_name`(255)),
  KEY `okpd2_code` (`okpd2_code`),
  KEY `name` (`name`(255)),
  KEY `okpd2_group_code` (`okpd2_group_code`),
  KEY `okpd2_group_level1_code` (`okpd2_group_level1_code`),
  KEY `okpd_code` (`okpd_code`),
  KEY `okpd2_group_code_2` (`okpd2_group_code`,`okpd2_group_level1_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43397927 ;

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `okrug_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `path` varchar(255) NOT NULL,
  `conf` varchar(11) NOT NULL,
  `path223` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Структура таблицы `requirement`
--

CREATE TABLE IF NOT EXISTS `requirement` (
  `id_requirement` int(11) NOT NULL AUTO_INCREMENT,
  `id_lot` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `content` varchar(5000) NOT NULL,
  PRIMARY KEY (`id_requirement`),
  KEY `id_lot` (`id_lot`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12486715 ;

-- --------------------------------------------------------

--
-- Структура таблицы `restricts`
--

CREATE TABLE IF NOT EXISTS `restricts` (
  `id_restrict` int(11) NOT NULL AUTO_INCREMENT,
  `id_lot` int(11) NOT NULL,
  `foreign_info` varchar(5000) NOT NULL,
  `info` varchar(5000) NOT NULL,
  PRIMARY KEY (`id_restrict`),
  KEY `id_lot` (`id_lot`),
  FULLTEXT KEY `info` (`info`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7787282 ;

-- --------------------------------------------------------

--
-- Структура таблицы `res_complaint`
--

CREATE TABLE IF NOT EXISTS `res_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkResultNumber` varchar(200) NOT NULL,
  `complaintNumber` varchar(200) NOT NULL,
  `versionNumber` int(11) NOT NULL,
  `purchaseNumber` varchar(200) NOT NULL,
  `regNumber` varchar(200) NOT NULL,
  `createDate` datetime NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `printForm` varchar(2000) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `decisionText` varchar(4000) NOT NULL,
  `complaintResult` varchar(200) NOT NULL,
  `complaintResultInfo` varchar(2000) NOT NULL,
  `checkResult` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `checkResultNumber` (`checkResultNumber`),
  KEY `complaintNumber` (`complaintNumber`),
  KEY `versionNumber` (`versionNumber`),
  KEY `purchaseNumber` (`purchaseNumber`),
  KEY `regNumber` (`regNumber`),
  KEY `createDate` (`createDate`),
  KEY `cancel` (`cancel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107485 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id_settings` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(200) NOT NULL,
  `count_archivs` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_xml_type` int(11) NOT NULL,
  PRIMARY KEY (`id_settings`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `settings223`
--

CREATE TABLE IF NOT EXISTS `settings223` (
  `id_settings` int(11) NOT NULL AUTO_INCREMENT,
  `folder` varchar(200) NOT NULL,
  `count_archivs` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `month` varchar(50) NOT NULL,
  PRIMARY KEY (`id_settings`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `participant_type` varchar(20) NOT NULL,
  `inn_supplier` varchar(20) NOT NULL,
  `kpp_supplier` varchar(20) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `country_full_name` varchar(200) NOT NULL,
  `factual_address` varchar(1000) NOT NULL,
  `post_address` varchar(1000) NOT NULL,
  `contact` varchar(1000) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  PRIMARY KEY (`id_supplier`),
  KEY `inn_supplier` (`inn_supplier`),
  KEY `kpp_supplier` (`kpp_supplier`),
  KEY `inn_supplier_2` (`inn_supplier`,`kpp_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=515122 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender`
--

CREATE TABLE IF NOT EXISTS `tender` (
  `id_tender` int(11) NOT NULL AUTO_INCREMENT,
  `id_xml` varchar(50) NOT NULL,
  `purchase_number` varchar(255) NOT NULL,
  `doc_publish_date` datetime NOT NULL,
  `href` varchar(1200) NOT NULL,
  `purchase_object_info` varchar(5000) NOT NULL,
  `id_organizer` int(11) NOT NULL,
  `id_placing_way` int(11) NOT NULL,
  `id_etp` int(11) NOT NULL,
  `end_date` datetime NOT NULL,
  `scoring_date` datetime NOT NULL,
  `bidding_date` datetime NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `cancel_failure` tinyint(1) NOT NULL,
  `id_region` int(11) NOT NULL,
  `type_fz` int(11) NOT NULL,
  `tender_kwords` longtext NOT NULL,
  `date_version` datetime NOT NULL,
  `num_version` int(11) NOT NULL,
  `notice_version` varchar(2000) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  `extend_scoring_date` varchar(2000) NOT NULL,
  `extend_bidding_date` varchar(2000) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  PRIMARY KEY (`id_tender`),
  KEY `doc_publish_date` (`doc_publish_date`),
  KEY `end_date` (`end_date`),
  KEY `purchase_object_info` (`purchase_object_info`(255)),
  KEY `cancel` (`cancel`),
  KEY `id_etp` (`id_etp`),
  KEY `id_organizer` (`id_organizer`),
  KEY `id_placing_way` (`id_placing_way`),
  KEY `purchase_number` (`purchase_number`),
  KEY `id_xml_2` (`id_xml`),
  KEY `type_fz` (`type_fz`),
  KEY `id_region` (`id_region`),
  KEY `id_xml` (`id_xml`,`id_region`,`purchase_number`) USING BTREE,
  KEY `num_version` (`num_version`),
  FULLTEXT KEY `tender_kwords` (`tender_kwords`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12055879 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender_plan`
--

CREATE TABLE IF NOT EXISTS `tender_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_xml` varchar(50) NOT NULL,
  `plan_number` varchar(255) NOT NULL,
  `num_version` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `purchase_plan_number` varchar(255) NOT NULL,
  `year` year(4) NOT NULL,
  `create_date` datetime NOT NULL,
  `confirm_date` datetime NOT NULL,
  `publish_date` datetime NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_owner` int(11) NOT NULL,
  `print_form` varchar(1024) NOT NULL,
  `cancel` tinyint(1) NOT NULL,
  `sum_pushases_small_business_total` decimal(30,2) NOT NULL,
  `sum_pushases_small_business_current_year` decimal(30,2) NOT NULL,
  `sum_pushases_request_total` decimal(30,2) NOT NULL,
  `sum_pushases_request_current_year` decimal(30,2) NOT NULL,
  `finance_support_total` decimal(30,2) NOT NULL,
  `finance_support_current_year` decimal(30,2) NOT NULL,
  `xml` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_number` (`plan_number`),
  KEY `num_version` (`num_version`),
  KEY `id_region` (`id_region`),
  KEY `id_customer` (`id_customer`),
  KEY `id_owner` (`id_owner`),
  KEY `cancel` (`cancel`),
  KEY `id_xml` (`id_xml`,`id_region`,`plan_number`,`num_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender_plan_attach`
--

CREATE TABLE IF NOT EXISTS `tender_plan_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plan` int(11) NOT NULL,
  `file_name` varchar(2000) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `url` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_plan` (`id_plan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender_plan_placing_way`
--

CREATE TABLE IF NOT EXISTS `tender_plan_placing_way` (
  `id_placing_way` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `conformity` int(11) NOT NULL,
  PRIMARY KEY (`id_placing_way`),
  KEY `code` (`code`,`conformity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender_plan_position`
--

CREATE TABLE IF NOT EXISTS `tender_plan_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plan` int(11) NOT NULL,
  `position_number` varchar(200) NOT NULL,
  `purchase_plan_position_number` varchar(200) NOT NULL,
  `purchase_object_name` varchar(2000) NOT NULL,
  `start_month` varchar(2) NOT NULL,
  `end_month` varchar(2) NOT NULL,
  `id_placing_way` int(11) NOT NULL,
  `finance_total` decimal(30,2) NOT NULL,
  `finance_total_current_year` decimal(30,2) NOT NULL,
  `max_price` decimal(30,2) NOT NULL,
  `OKPD2_code` varchar(100) NOT NULL,
  `OKPD2_name` varchar(1000) NOT NULL,
  `OKEI_code` varchar(100) NOT NULL,
  `OKEI_name` varchar(1000) NOT NULL,
  `pos_description` varchar(2000) NOT NULL,
  `products_quantity_total` varchar(100) NOT NULL,
  `products_quantity_current_year` varchar(100) NOT NULL,
  `purchase_fin_condition` varchar(2000) NOT NULL,
  `contract_fin_condition` varchar(2000) NOT NULL,
  `advance_fin_condition` varchar(2000) NOT NULL,
  `purchase_graph` varchar(2000) NOT NULL,
  `bank_support_info` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_plan` (`id_plan`),
  KEY `id_placing_way` (`id_placing_way`),
  KEY `OKPD2_code` (`OKPD2_code`),
  KEY `OKPD2_name` (`OKPD2_name`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tender_plan_pref_rec`
--

CREATE TABLE IF NOT EXISTS `tender_plan_pref_rec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_plan_prod` int(11) NOT NULL,
  `group_code` varchar(2000) NOT NULL,
  `group_name` varchar(2000) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `add_info` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_plan_prod` (`id_plan_prod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `ufas`
--

CREATE TABLE IF NOT EXISTS `ufas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Структура таблицы `unfair`
--

CREATE TABLE IF NOT EXISTS `unfair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publishDate` datetime NOT NULL,
  `approveDate` datetime NOT NULL,
  `registryNum` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `createReason` varchar(1000) NOT NULL,
  `approveReason` varchar(1000) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_org` int(11) NOT NULL,
  `purchaseNumber` varchar(1000) NOT NULL,
  `purchaseObjectInfo` varchar(1000) NOT NULL,
  `lotNumber` int(20) NOT NULL,
  `contract_regNum` varchar(100) NOT NULL,
  `contract_productInfo` varchar(2000) NOT NULL,
  `contract_OKPD_code` varchar(100) NOT NULL,
  `contract_OKPD_name` varchar(1000) NOT NULL,
  `contract_currency_code` varchar(100) NOT NULL,
  `contract_price` decimal(30,2) NOT NULL,
  `contract_cancel_signDate` datetime NOT NULL,
  `contract_cancel_performanceDate` datetime NOT NULL,
  `contract_cancel_base_name` varchar(2000) NOT NULL,
  `contract_cancel_cancelDate` datetime NOT NULL,
  `full_name_supplier` varchar(2000) NOT NULL,
  `placefullName_supplier` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `publishDate` (`publishDate`),
  KEY `approveDate` (`approveDate`),
  KEY `registryNum` (`registryNum`),
  KEY `state` (`state`),
  KEY `id_customer` (`id_customer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_org` (`id_org`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28961 ;

-- --------------------------------------------------------

--
-- Структура таблицы `unfair_customer`
--

CREATE TABLE IF NOT EXISTS `unfair_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(100) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `INN` varchar(50) NOT NULL,
  `KPP` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`),
  KEY `INN` (`INN`),
  KEY `fullName` (`fullName`(255)),
  KEY `KPP` (`KPP`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11428 ;

-- --------------------------------------------------------

--
-- Структура таблицы `unfair_publish_org`
--

CREATE TABLE IF NOT EXISTS `unfair_publish_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNum` varchar(50) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regNum` (`regNum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Структура таблицы `unfair_suppplier`
--

CREATE TABLE IF NOT EXISTS `unfair_suppplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(100) NOT NULL,
  `kpp` varchar(50) NOT NULL,
  `fullName` varchar(2000) NOT NULL,
  `placefullName` varchar(1000) NOT NULL,
  `email` varchar(100) NOT NULL,
  `founders_names` varchar(1000) NOT NULL,
  `founders_inn` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inn` (`inn`),
  KEY `kpp` (`kpp`),
  KEY `fullName` (`fullName`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13611 ;

-- --------------------------------------------------------

--
-- Структура таблицы `unic_files`
--

CREATE TABLE IF NOT EXISTS `unic_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_file` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_file` (`type_file`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Структура таблицы `web_nmck_index`
--

CREATE TABLE IF NOT EXISTS `web_nmck_index` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `index_year` year(4) NOT NULL,
  `index_month` int(11) NOT NULL,
  `index_product_type` varchar(100) NOT NULL,
  `index_value` decimal(12,2) NOT NULL,
  KEY `index_id` (`index_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nmck_okei`
--
CREATE TABLE IF NOT EXISTS `web_nmck_okei` (
`okei` varchar(50)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nmck_product`
--
CREATE TABLE IF NOT EXISTS `web_nmck_product` (
`contract` int(11)
,`product_name` varchar(2000)
,`okpd2` varchar(50)
,`okpd` varchar(50)
,`price` decimal(30,2)
,`okpd_name` varchar(1024)
,`quantity` decimal(30,5)
,`okei` varchar(50)
,`region_code` varchar(50)
,`sign_date` date
,`fz` varchar(100)
,`contract_start` date
,`contract_end` date
,`oos_url` varchar(1024)
,`id_placing_way` int(11)
,`pw_code` int(11)
,`p_w_name` varchar(255)
,`id_etp` int(11)
,`etp_name` varchar(255)
,`id_region` int(11)
,`region_name` text
,`id_customer` int(11)
,`summ` decimal(30,2)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nsi_etp`
--
CREATE TABLE IF NOT EXISTS `web_nsi_etp` (
`id_etp` int(11)
,`code` varchar(32)
,`name` varchar(255)
,`url` varchar(2000)
,`conf` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nsi_okpd2`
--
CREATE TABLE IF NOT EXISTS `web_nsi_okpd2` (
`id` int(11) unsigned
,`code` text
,`name` text
,`level` tinyint(2)
,`parent` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nsi_pw`
--
CREATE TABLE IF NOT EXISTS `web_nsi_pw` (
`id_placing_way` int(11)
,`code` varchar(32)
,`name` varchar(255)
,`conformity` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_nsi_regions`
--
CREATE TABLE IF NOT EXISTS `web_nsi_regions` (
`id` int(11)
,`okrug_id` int(11)
,`name` text
,`path` varchar(255)
,`conf` varchar(11)
,`path223` varchar(100)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_organizations`
--
CREATE TABLE IF NOT EXISTS `web_organizations` (
`Роль_организации` varchar(8)
,`Полное_наименование` varchar(2000)
,`ИНН` varchar(50)
,`Идентификатор` varchar(255)
,`Дата_последнего_действия` datetime
,`Рeгион` text
,`Действующий` int(1)
,`Последнее_действие` varchar(18)
,`Контактные_данные` varchar(13)
,`id_region` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_org_customer`
--
CREATE TABLE IF NOT EXISTS `web_org_customer` (
`Роль_организации` varchar(8)
,`Полное_наименование` varchar(2000)
,`ИНН` varchar(50)
,`Идентификатор` varchar(255)
,`Дата_последнего_действия` datetime
,`Рeгион` text
,`Действующий` int(1)
,`Последнее_действие` varchar(18)
,`Контактные_данные` varchar(13)
,`id_region` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_org_supplier`
--
CREATE TABLE IF NOT EXISTS `web_org_supplier` (
`Роль_организации` varchar(9)
,`Полное_наименование` varchar(2000)
,`ИНН` varchar(50)
,`Идентификатор` varchar(255)
,`Дата_последнего_действия` date
,`Рeгион` text
,`Действующий` int(1)
,`Последнее_действие` varchar(20)
,`Контактные_данные` varchar(13)
,`id_region` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_proc_contracts`
--
CREATE TABLE IF NOT EXISTS `web_proc_contracts` (
`contract_id` int(11)
,`Наименование контракта` varchar(5000)
,`Номер контракта ЕИС` varchar(255)
,`Код ОКПД2` varchar(50)
,`Наименование ОКПД2` varchar(1024)
,`Дата подписания контракта` date
,`Наименование заказчика` varchar(2000)
,`ИНН заказчика` varchar(50)
,`Наименование победителя` varchar(2000)
,`ИНН победителя` varchar(50)
,`etp_name` varchar(255)
,`etp_id` int(11)
,`pw_code` int(11)
,`pw_name` varchar(255)
,`Регион поставки` text
,`region_id` int(11)
,`ФЗ` varchar(100)
,`НМЦК` decimal(30,2)
,`Обеспечение контракта` decimal(30,2)
,`Обеспечение заявки` decimal(30,2)
,`Код КБК` varchar(3)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `web_proc_tenders`
--
CREATE TABLE IF NOT EXISTS `web_proc_tenders` (
`ten_id` int(11)
,`Наименование тендера` varchar(5000)
,`Номер тендера ЕИС` varchar(255)
,`Код ОКПД2` varchar(100)
,`Наименование ОКПД2` varchar(1000)
,`Дата публикации тендера` datetime
,`Наименование заказчика` varchar(2000)
,`ИНН заказчика` varchar(50)
,`Регион поставки` text
,`region_id` int(11)
,`ФЗ` int(11)
,`НМЦК` decimal(30,2)
,`Обеспечение контракта` decimal(30,2)
,`Обеспечение заявки` decimal(30,2)
,`Код КБК` varchar(3)
,`Способ закупки` varchar(255)
,`pw_code` int(11)
,`ЕЭТП` varchar(255)
,`etp_id` int(11)
);
-- --------------------------------------------------------

--
-- Структура таблицы `xml_type`
--

CREATE TABLE IF NOT EXISTS `xml_type` (
  `id_xml_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_xml_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__arhiv_tenders`
--

CREATE TABLE IF NOT EXISTS `_old__arhiv_tenders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arhiv` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `arhiv` (`arhiv`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__log`
--

CREATE TABLE IF NOT EXISTS `_old__log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `query` mediumtext NOT NULL,
  `filter_struct` mediumtext NOT NULL,
  `user` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `user_2` (`user`(255)),
  KEY `date` (`date`),
  KEY `query` (`query`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45338 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__payment_history`
--

CREATE TABLE IF NOT EXISTS `_old__payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date` date NOT NULL,
  `balance_motion` decimal(10,0) NOT NULL,
  `info` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__rate_type`
--

CREATE TABLE IF NOT EXISTS `_old__rate_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `info` varchar(500) NOT NULL,
  `count_month` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__update_notice`
--

CREATE TABLE IF NOT EXISTS `_old__update_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL,
  `notice` varchar(1000) NOT NULL,
  `version_date` varchar(50) NOT NULL,
  `images` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=181 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__user`
--

CREATE TABLE IF NOT EXISTS `_old__user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `hash_password` varchar(500) NOT NULL,
  `password` varchar(255) NOT NULL,
  `end_date` date NOT NULL,
  `tel` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `id_rate_type` int(11) NOT NULL,
  `user_name` varchar(500) NOT NULL,
  `user_mail` varchar(200) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `login` (`login`),
  KEY `datetime` (`datetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=638 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__UTO_UsersActivity`
--

CREATE TABLE IF NOT EXISTS `_old__UTO_UsersActivity` (
  `ACT_Id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор события',
  `ACT_DateTime` datetime DEFAULT NULL COMMENT 'Время события',
  `ACT_WorkStationId` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Идентификатор рабочей станции пользователя',
  `ACT_UserId` int(11) DEFAULT NULL COMMENT 'Идентификатор пользователи УТО',
  `ACT_Method` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Вызываемый метод API',
  `ACT_MethodDescription` varchar(128) NOT NULL DEFAULT '0' COMMENT 'Описание вызываемого метода API',
  `ACT_Parametries` varchar(4096) NOT NULL DEFAULT '0' COMMENT 'Параметры отправляемые пользователем в метод',
  `ACT_MethodQuery` varchar(4096) DEFAULT NULL COMMENT 'Запрос выполняемы пользователем',
  `ACT_CompletedWithErrors` int(1) DEFAULT NULL COMMENT '1 если доступ был запрещен и 0 если все прошло удачно',
  PRIMARY KEY (`ACT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5396 ;

-- --------------------------------------------------------

--
-- Структура таблицы `_old__UTO_UsersWorkStations`
--

CREATE TABLE IF NOT EXISTS `_old__UTO_UsersWorkStations` (
  `WS_IdWorkStation` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Идентификатор рабочей станции',
  `WS_OsVersion` varchar(50) DEFAULT '0' COMMENT 'Версия операционной системы',
  `WS_1cVersion` varchar(50) DEFAULT '0' COMMENT 'Версия платформы 1С:Предприятия',
  `WS_SoftInfo` varchar(50) DEFAULT '0' COMMENT 'Дополнительная информация о клментском приложении',
  `WS_Ram` varchar(50) DEFAULT '0' COMMENT 'Объем оперативной памяти',
  `WS_Cpu` varchar(50) DEFAULT '0' COMMENT 'Описание процессора',
  PRIMARY KEY (`WS_IdWorkStation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Эта таблица с описанием клиентских ПК и серверов. ';

-- --------------------------------------------------------

--
-- Структура для представления `web_nmck_okei`
--
DROP TABLE IF EXISTS `web_nmck_okei`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nmck_okei` AS select `product`.`okei` AS `okei` from `od_contract_product` `product` group by `product`.`okei`;

-- --------------------------------------------------------

--
-- Структура для представления `web_nmck_product`
--
DROP TABLE IF EXISTS `web_nmck_product`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nmck_product` AS select `prod`.`id_od_contract` AS `contract`,`prod`.`name` AS `product_name`,`prod`.`okpd2_code` AS `okpd2`,`prod`.`okpd_code` AS `okpd`,`prod`.`price` AS `price`,`prod`.`okpd_name` AS `okpd_name`,`prod`.`quantity` AS `quantity`,`prod`.`okei` AS `okei`,`contract`.`region_code` AS `region_code`,`contract`.`sign_date` AS `sign_date`,`contract`.`fz` AS `fz`,`contract`.`execution_start_date` AS `contract_start`,`contract`.`execution_end_date` AS `contract_end`,`contract`.`url` AS `oos_url`,`ten`.`id_placing_way` AS `id_placing_way`,`p_w`.`conformity` AS `pw_code`,`p_w`.`name` AS `p_w_name`,`ten`.`id_etp` AS `id_etp`,`eatp`.`name` AS `etp_name`,`ten`.`id_region` AS `id_region`,`regi_on`.`name` AS `region_name`,`contract`.`id_customer` AS `id_customer`,`prod`.`sum` AS `summ` from (((((`od_contract_product` `prod` left join `od_contract` `contract` on(`contract`.`id` = `prod`.`id_od_contract`)) left join `tender` `ten` on(`ten`.`purchase_number` = `contract`.`notification_number`)) left join `placing_way` `p_w` on(`p_w`.`id_placing_way` = `ten`.`id_placing_way`)) left join `etp` `eatp` on(`eatp`.`id_etp` = `ten`.`id_etp`)) left join `region` `regi_on` on(`regi_on`.`id` = `ten`.`id_region`)) where `contract`.`cancel` = 0;

-- --------------------------------------------------------

--
-- Структура для представления `web_nsi_etp`
--
DROP TABLE IF EXISTS `web_nsi_etp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nsi_etp` AS select `etp`.`id_etp` AS `id_etp`,`etp`.`code` AS `code`,`etp`.`name` AS `name`,`etp`.`url` AS `url`,`etp`.`conf` AS `conf` from `etp`;

-- --------------------------------------------------------

--
-- Структура для представления `web_nsi_okpd2`
--
DROP TABLE IF EXISTS `web_nsi_okpd2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nsi_okpd2` AS select `okpd2_sprav`.`id` AS `id`,`okpd2_sprav`.`code` AS `code`,`okpd2_sprav`.`name` AS `name`,`okpd2_sprav`.`level` AS `level`,`okpd2_sprav`.`parent` AS `parent` from `okpd2_sprav`;

-- --------------------------------------------------------

--
-- Структура для представления `web_nsi_pw`
--
DROP TABLE IF EXISTS `web_nsi_pw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nsi_pw` AS select `placing_way`.`id_placing_way` AS `id_placing_way`,`placing_way`.`code` AS `code`,`placing_way`.`name` AS `name`,`placing_way`.`conformity` AS `conformity` from `placing_way` group by `placing_way`.`conformity`;

-- --------------------------------------------------------

--
-- Структура для представления `web_nsi_regions`
--
DROP TABLE IF EXISTS `web_nsi_regions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_nsi_regions` AS select `region`.`id` AS `id`,`region`.`okrug_id` AS `okrug_id`,`region`.`name` AS `name`,`region`.`path` AS `path`,`region`.`conf` AS `conf`,`region`.`path223` AS `path223` from `region`;

-- --------------------------------------------------------

--
-- Структура для представления `web_organizations`
--
DROP TABLE IF EXISTS `web_organizations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_organizations` AS select 'Заказчик' AS `Роль_организации`,`od_customer`.`full_name` AS `Полное_наименование`,`od_customer`.`inn` AS `ИНН`,(select `tnd`.`purchase_number` from (((`tender` `tnd` left join `lot` on(`lot`.`id_tender` = `tnd`.`id_tender`)) left join `customer_requirement` `c_r` on(`c_r`.`id_lot` = `lot`.`id_lot`)) left join `customer` `cus` on(`cus`.`id_customer` = `c_r`.`id_customer`)) where `cus`.`reg_num` = `od_customer`.`regNumber` order by `tnd`.`doc_publish_date` desc limit 1) AS `Идентификатор`,(select `tnd_2`.`doc_publish_date` from `tender` `tnd_2` where `tnd_2`.`purchase_number` = `Идентификатор` order by `tnd_2`.`doc_publish_date` desc limit 1) AS `Дата_последнего_действия`,`region`.`name` AS `Рeгион`,1 AS `Действующий`,'Публикация закупки' AS `Последнее_действие`,'Потом добавим' AS `Контактные_данные`,`region`.`id` AS `id_region` from (`od_customer` left join `region` on(`region`.`conf` = `od_customer`.`region_code`));

-- --------------------------------------------------------

--
-- Структура для представления `web_org_customer`
--
DROP TABLE IF EXISTS `web_org_customer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_org_customer` AS select 'Заказчик' AS `Роль_организации`,`od_customer`.`full_name` AS `Полное_наименование`,`od_customer`.`inn` AS `ИНН`,(select `tnd`.`purchase_number` from (((`tender` `tnd` left join `lot` on(`lot`.`id_tender` = `tnd`.`id_tender`)) left join `customer_requirement` `c_r` on(`c_r`.`id_lot` = `lot`.`id_lot`)) left join `customer` `cus` on(`cus`.`id_customer` = `c_r`.`id_customer`)) where `cus`.`reg_num` = `od_customer`.`regNumber` order by `tnd`.`doc_publish_date` desc limit 1) AS `Идентификатор`,(select `tnd_2`.`doc_publish_date` from `tender` `tnd_2` where `tnd_2`.`purchase_number` = `Идентификатор` order by `tnd_2`.`doc_publish_date` desc limit 1) AS `Дата_последнего_действия`,`region`.`name` AS `Рeгион`,1 AS `Действующий`,'Публикация закупки' AS `Последнее_действие`,'Потом добавим' AS `Контактные_данные`,`region`.`id` AS `id_region` from (`od_customer` left join `region` on(`region`.`conf` = `od_customer`.`region_code`));

-- --------------------------------------------------------

--
-- Структура для представления `web_org_supplier`
--
DROP TABLE IF EXISTS `web_org_supplier`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_org_supplier` AS select 'Поставщик' AS `Роль_организации`,`od_supplier`.`organizationName` AS `Полное_наименование`,`od_supplier`.`inn` AS `ИНН`,(select `cont`.`regnum` from (`od_contract` `cont` left join `od_supplier` `sup` on(`cont`.`id_supplier` = `sup`.`id`)) where `sup`.`id` = `od_supplier`.`id` order by `cont`.`sign_date` desc limit 1) AS `Идентификатор`,(select `cont`.`sign_date` from `od_contract` `cont` where `cont`.`regnum` = `Идентификатор` order by `cont`.`sign_date` desc limit 1) AS `Дата_последнего_действия`,`region`.`name` AS `Рeгион`,1 AS `Действующий`,'Заключение контракта' AS `Последнее_действие`,'Потом добавим' AS `Контактные_данные`,`region`.`id` AS `id_region` from (`od_supplier` left join `region` on(`region`.`conf` = `od_supplier`.`region_code`)) where `od_supplier`.`organizationName` <> '';

-- --------------------------------------------------------

--
-- Структура для представления `web_proc_contracts`
--
DROP TABLE IF EXISTS `web_proc_contracts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_proc_contracts` AS select `contr`.`id` AS `contract_id`,`ten`.`purchase_object_info` AS `Наименование контракта`,`contr`.`regnum` AS `Номер контракта ЕИС`,`prod`.`okpd2_code` AS `Код ОКПД2`,`prod`.`okpd2_name` AS `Наименование ОКПД2`,`contr`.`sign_date` AS `Дата подписания контракта`,`cus`.`full_name` AS `Наименование заказчика`,`cus`.`inn` AS `ИНН заказчика`,`sup`.`organizationName` AS `Наименование победителя`,`sup`.`inn` AS `ИНН победителя`,`_etp`.`name` AS `etp_name`,`ten`.`id_etp` AS `etp_id`,`p_w`.`conformity` AS `pw_code`,`p_w`.`name` AS `pw_name`,`reg`.`name` AS `Регион поставки`,`reg`.`id` AS `region_id`,`contr`.`fz` AS `ФЗ`,`lot`.`max_price` AS `НМЦК`,`obs`.`contract_guarantee_amount` AS `Обеспечение контракта`,`obs`.`application_guarantee_amount` AS `Обеспечение заявки`,'КБК' AS `Код КБК` from (((((((((`od_contract` `contr` left join `od_contract_product` `prod` on(`contr`.`id` = `prod`.`id_od_contract`)) left join `od_supplier` `sup` on(`sup`.`id` = `contr`.`id_supplier`)) left join `od_customer` `cus` on(`cus`.`id` = `contr`.`id_customer`)) left join `tender` `ten` on(`ten`.`purchase_number` = `contr`.`notification_number`)) left join `lot` on(`ten`.`id_tender` = `lot`.`id_tender`)) left join `region` `reg` on(`ten`.`id_region` = `reg`.`id`)) left join `etp` `_etp` on(`_etp`.`id_etp` = `ten`.`id_etp`)) left join `placing_way` `p_w` on(`p_w`.`id_placing_way` = `ten`.`id_placing_way`)) left join `customer_requirement` `obs` on(`obs`.`id_lot` = `lot`.`id_lot`)) where `contr`.`cancel` = 0 and `ten`.`cancel` = 0 and `lot`.`lot_number` = `contr`.`lot_number`;

-- --------------------------------------------------------

--
-- Структура для представления `web_proc_tenders`
--
DROP TABLE IF EXISTS `web_proc_tenders`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kevbrin_mike_v_kev`@`%` SQL SECURITY DEFINER VIEW `web_proc_tenders` AS select `ten`.`id_tender` AS `ten_id`,`ten`.`purchase_object_info` AS `Наименование тендера`,`ten`.`purchase_number` AS `Номер тендера ЕИС`,`po`.`okpd2_code` AS `Код ОКПД2`,`po`.`okpd_name` AS `Наименование ОКПД2`,`ten`.`doc_publish_date` AS `Дата публикации тендера`,`cus`.`full_name` AS `Наименование заказчика`,`cus`.`inn` AS `ИНН заказчика`,`reg`.`name` AS `Регион поставки`,`reg`.`id` AS `region_id`,`ten`.`type_fz` AS `ФЗ`,`lot`.`max_price` AS `НМЦК`,`obs`.`contract_guarantee_amount` AS `Обеспечение контракта`,`obs`.`application_guarantee_amount` AS `Обеспечение заявки`,'КБК' AS `Код КБК`,`pw`.`name` AS `Способ закупки`,`pw`.`conformity` AS `pw_code`,`etp`.`name` AS `ЕЭТП`,`ten`.`id_etp` AS `etp_id` from (((((((`tender` `ten` left join `lot` on(`ten`.`id_tender` = `lot`.`id_tender`)) left join `purchase_object` `po` on(`lot`.`id_lot` = `po`.`id_lot`)) left join `customer` `cus` on(`cus`.`id_customer` = `po`.`id_customer`)) left join `placing_way` `pw` on(`pw`.`id_placing_way` = `ten`.`id_placing_way`)) left join `etp` on(`etp`.`id_etp` = `ten`.`id_etp`)) left join `region` `reg` on(`reg`.`id` = `ten`.`id_region`)) left join `customer_requirement` `obs` on(`obs`.`id_lot` = `lot`.`id_lot`)) where `ten`.`cancel` = 0;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
