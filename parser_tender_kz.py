import os
import time
from datetime import datetime, date
import lxml.html as html
import pymysql
from grab import Grab
import random
import proxy_check

DB = "tenders_test"
log_dir = "Log_tender_kz"
file_log = './{1}/tenders_kz_{0}.log'.format(str(date.today()), log_dir)
proxy_in = "./proxy/proxy.txt"


def get_proxy():
    clear_p = set()

    with open(proxy_in) as f:
        clear_p = {n for n in f.readlines() if n.find(":80") != -1}
    if clear_p:
        with open(proxy_in, 'w') as f:
            f.writelines(clear_p)
    proxy_check.proxy_checker()


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def connect_bd(baza):
    con = pymysql.connect(host="localhost", port=3306, user="parser", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


class Tender:
    def __init__(self, href, date_start, date_end, num_lot, sum_t, name_t, okei, price, region, desc_t):
        self.href = href
        self.date_start = date_start
        self.date_end = date_end
        self.num_lot = num_lot
        self.sum_t = sum_t
        self.name_t = name_t
        self.okei = okei
        self.price = price
        self.region = region
        self.desc_t = desc_t

    def insert_tender(self):
        if self.num_lot == 0:
            print("no lot_number")
            return
        con = connect_bd(DB)
        cur = con.cursor()
        cur.execute("""SELECT id FROM tender_kz WHERE num_lot=%s""", (self.num_lot,))
        res_t = cur.fetchone()
        if res_t:
            str_update_t = """UPDATE tender_kz SET href=%s, date_start=%s, date_end=%s, sum_t=%s, 
                    name_t=%s, desc_t=%s, okei=%s, price=%s, region=%s WHERE id=%s"""
            tuple_par_upd = (
                self.href, self.date_start, self.date_end, self.sum_t, self.name_t, self.desc_t, self.okei, self.price,
                self.region, res_t["id"])
            cur.execute(str_update_t, tuple_par_upd)
        else:
            str_insert_t = """INSERT INTO tender_kz SET href=%s, date_start=%s, date_end=%s, num_lot=%s, sum_t=%s, 
                    name_t=%s, desc_t=%s, okei=%s, price=%s, region=%s """
            tuple_par = (
                self.href, self.date_start, self.date_end, self.num_lot, self.sum_t, self.name_t, self.desc_t,
                self.okei, self.price,
                self.region)
            cur.execute(str_insert_t, tuple_par)
        cur.close()
        con.close()


def main():
    # get_proxy()
    count_pages = get_count_page()
    if count_pages > 0:
        for i in range(count_pages, 1, -1):
            try:
                parsing_list_tenders(i)
            except Exception as e:
                logging_parser("Не смогли пропарсить страницу",
                               f"https://tender.kz/?unique_id=1&page_num={i}&currCurrency=KZT", e)


def get_count_page():
    page = 0
    count = 10
    while True:
        try:
            g = Grab(log_file=f'./{log_dir}/out.html')
            resp = g.go('https://tender.kz/')
            text_page = resp.unicode_body()
            doc_ten = html.document_fromstring(text_page)
            for i in doc_ten.cssselect(
                    "html body div.wrapper div.w1 div.main div.content div.finded div.paging ul li a"):
                page = i.text
            break
        except Exception:
            count += 1
        if count > 10:
            logging_parser("Не смогли получить количество страниц")
            break

    return int(page)


def parsing_list_tenders(page):
    tenders = []
    count = 1
    url = f"https://tender.kz/?unique_id=1&page_num={page}&currCurrency=KZT"
    while True:
        try:
            g = Grab(log_file=f'./{log_dir}/out.html')
            # g.proxylist.load_file("./proxy.txt", proxy_type='http')
            # g.change_proxy()
            resp = g.go(url)
            text_page = resp.unicode_body()
            doc_ten = html.document_fromstring(text_page)
            ten = doc_ten.cssselect(
                    "html body div.wrapper div.w1 div.main div.content div.finded div.relative-frame div.table-holder table#MainContent_tenders_table.editable_table tbody tr")
            if ten:
                tenders = list(ten)
                tenders.reverse()
        except Exception as e:
            # logging_parser("Не смогли получить страницу", url, "попытка", count, e)
            pass
        if not tenders:
            random.seed()
            r = random.randint(25, 30)
            time.sleep(r)
        else:
            if count > 1:
                logging_parser("Получили страницу", url, "попытка", count)
            break
        if count > 20:
            logging_parser("Не получили страницу за 20 попыток", url)
            break
        count += 1
    if tenders:
        for i in tenders:
            parsing_tender(i)


def get_text(el, text, ind=0):
    res = ""
    try:
        res = el.cssselect(text)[ind].text
    except Exception:
        pass
    return "" if res is None else res.strip()


def get_attr(el, text, attr, ind=0):
    res = ""
    try:
        res = el.cssselect(text)[ind].get(attr)
    except Exception:
        pass
    return "" if res is None else res.strip()


def parsing_tender_old(t):
    href = ""
    date_start = ""
    date_end = ""
    num_lot = ""
    sum_t = ""
    name_t = ""
    okei = ""
    price = ""
    region = ""
    href = get_attr(t, "td form.lot a", "href")
    if href:
        temp = href.split("-")
        num_lot = temp[-1]
        href = f"https://tender.kz{href}"
    date_start = get_text(t, "td em.date")
    date_end = get_text(t, "td em.date", 1)
    if date_start:
        date_start = f"{date_start}.2017"
        date_start = datetime.strptime(date_start, "%d.%m.%Y")
    if date_end:
        date_end = f"{date_end}.2017"
        date_end = datetime.strptime(date_end, "%d.%m.%Y")
    name_t = get_text(t, "td form.lot a span.lot-text")
    sum_t = get_text(t, "td:nth-child(5)")
    okei = get_text(t, "td:nth-child(6)")
    price = get_text(t, "td:nth-child(9)")
    region = get_text(t, "td:nth-child(10)")
    t = Tender(href, date_start, date_end, num_lot, sum_t, name_t, okei, price, region)
    t.insert_tender()


def parsing_tender(t):
    count = 1
    doc_ten = ""
    href = ""
    region = ""
    m_test = ""
    region = get_text(t, "td:nth-child(10)")
    href = get_attr(t, "td form.lot a", "href")
    if href:
        temp = href.split("-")
        num_lot = temp[-1]
        href = f"https://tender.kz{href}"
        while True:
            try:
                g = Grab(log_file=f'./{log_dir}/out_t.html')
                # g.proxylist.load_file("./proxy.txt", proxy_type='http')
                # g.change_proxy()
                resp = g.go(href)
                text_page = resp.unicode_body()
                doc_ten = html.document_fromstring(text_page)
                m_test = get_text(doc_ten, "ul.lot-list:nth-child(4) > li:nth-child(1) > strong:nth-child(2)")
            except Exception as e:
                pass
                # logging_parser("Не смогли получить страницу", href, "попытка", count, e)
            if m_test:
                if count > 1:
                    logging_parser("Получили страницу", href, "попытка", count)
                try:
                    parsing_lot(doc_ten, href, num_lot, region)
                except Exception:
                    logging_parser("Не смогли пропарсить страницу", href)
                break
            else:
                random.seed()
                r = random.randint(25, 30)
                time.sleep(r)
            if count > 20:
                logging_parser("Не получили страницу за 20 попыток", href)
                break
            count += 1


def parsing_lot(doc, href, num_lot, region):
    date_start = ""
    date_end = ""
    sum_t = ""
    name_t = ""
    desc_t = ""
    okei = ""
    price = ""
    date_start = get_text(doc, "ul.lot-list:nth-child(3) > li:nth-child(9) > strong:nth-child(2)")
    if date_start:
        try:
            date_start = datetime.strptime(date_start, "%d.%m.%Y %H:%M:%S")
        except Exception:
            logging_parser("Не пропарсили дату", href, date_start)
    date_end = get_text(doc, "ul.lot-list:nth-child(3) > li:nth-child(10) > strong:nth-child(2)")
    if date_end:
        try:
            date_end = datetime.strptime(date_end, "%d.%m.%Y %H:%M:%S")
        except Exception:
            logging_parser("Не пропарсили дату", href, date_end)
    sum_t = get_text(doc, "ul.lot-list:nth-child(4) > li:nth-child(4) > strong:nth-child(2)")
    name_t = get_text(doc, "ul.lot-list:nth-child(4) > li:nth-child(2) > strong:nth-child(2)")
    desc_t = get_text(doc, "ul.lot-list:nth-child(4) > li:nth-child(3) > strong:nth-child(2)")
    okei = get_text(doc, "ul.lot-list:nth-child(4) > li:nth-child(6) > strong:nth-child(2)")
    price = get_text(doc, "ul.lot-list:nth-child(4) > li:nth-child(7) > strong:nth-child(2)")
    t = Tender(href, date_start, date_end, num_lot, sum_t, name_t, okei, price, region, desc_t)
    t.insert_tender()


if __name__ == "__main__":
    if not os.path.exists(f"./{log_dir}"):
        os.mkdir(log_dir)
    logging_parser("Начало парсинга")
    main()
    logging_parser("Конец парсинга")
