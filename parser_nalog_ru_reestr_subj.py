import pymysql
import logging
import datetime
import os
import xmltodict
import zipfile
import shutil
import urllib.request, urllib.error


file_log = './log_parser_subj/subject_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
work_cat = './tmp_subject'


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def get_okved(okv, main, id_sup):
    con = connect_bd('partners_1c')
    cur = con.cursor(pymysql.cursors.DictCursor)
    name_okv = ''
    if '@НаимОКВЭД' in okv:
        name_okv = okv['@НаимОКВЭД']
    if '@КодОКВЭД' in okv:
        kod_okv = okv['@КодОКВЭД']
        cur.execute("""SELECT id FROM okved WHERE kod_okved = %s """, (kod_okv,))
        result_okved = cur.fetchone()
        if result_okved:
            id_okved = result_okved['id']
        else:
            cur.execute("""INSERT INTO okved SET kod_okved = %s, name_okved = %s """, (kod_okv, name_okv))
            id_okved = con.insert_id()
    cur.execute("""INSERT INTO okved_suppliers SET id_supplier = %s, id_kod_okved = %s, main_okved = %s""",
                (id_sup, id_okved, main))
    cur.close()
    con.close()


def get_org(subj):
    inn = ''
    kpp = ''
    contracts_count = 0
    contracts223_count = 0
    contracts_sum = 0.0
    contracts223_sum = 0.0
    ogrn = ''
    region_code = ''
    organizationName = ''
    postal_address = ''
    contactPhone = ''
    contactFax = ''
    contactEMail = ''
    contact_name = ''
    city = ''
    street = ''
    if '@ИННЮЛ' in subj['ОргВклМСП']:
        inn = subj['ОргВклМСП']['@ИННЮЛ']
    else:
        with open(file_log, 'a') as flog:
            flog.write('У ЮЛ нет ИНН ' + ' ' + str(subj['ОргВклМСП']) + '\n\n\n')
        return
    if not inn:
        with open(file_log, 'a') as flog:
            flog.write('У ЮЛ пустой ИНН ' + ' ' + str(subj['ОргВклМСП']) + '\n\n\n')
        return
    con = connect_bd('partners_1c')
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute("""SELECT id FROM od_supplier WHERE inn = %s """, (inn,))
    result_supplier = cur.fetchone()
    if result_supplier:
        cur.close()
        con.close()
        with open(file_log, 'a') as flog:
            flog.write('Такой инн ЮЛ уже есть в базе ' + ' ' + inn + '\n\n\n')
        return
    if '@НаимОрг' in subj['ОргВклМСП']:
        organizationName = subj['ОргВклМСП']['@НаимОрг']
    if 'СведМН' in subj:
        if '@КодРегион' in subj['СведМН']:
            region_code = subj['СведМН']['@КодРегион']
        if 'Регион' in subj['СведМН']:
            if '@Наим' in subj['СведМН']['Регион']:
                city = subj['СведМН']['Регион']['@Наим']
        if 'НаселПункт' in subj['СведМН']:
            if '@Наим' in subj['СведМН']['НаселПункт']:
                street = subj['СведМН']['НаселПункт']['@Наим']
    postal_address = city + ' ,' + street
    query_add_supplier = """INSERT INTO od_supplier SET  inn = %s, kpp = %s, contracts_count = %s,
                    contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s,
                    organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s,
                    contact_name = %s """
    value_add_supplier = (
        inn, kpp, contracts_count, contracts223_count,
        contracts_sum, contracts223_sum, ogrn, region_code,
        organizationName, postal_address, contactPhone, contactFax,
        contactEMail, contact_name)
    cur.execute(query_add_supplier, value_add_supplier)
    id_supplier = con.insert_id()
    cur.close()
    con.close()
    if 'СвОКВЭД' in subj:
        if subj['СвОКВЭД'] is None:
            return
        if 'СвОКВЭДОсн' in subj['СвОКВЭД']:
            try:
                get_okved(subj['СвОКВЭД']['СвОКВЭДОсн'], 'yes', id_supplier)
            except Exception as ex0:
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка добавления основного ОКВЭД  {0} {1}\n\n\n'.format(str(subj['СвОКВЭД']['СвОКВЭДОсн']),
                                                                                      str(ex0)))
        if 'СвОКВЭДДоп' in subj['СвОКВЭД']:
            test = 0
            try:
                b = subj['СвОКВЭД']['СвОКВЭДДоп'][0]
                test = 2
            except KeyError:
                test = 1
            if test == 1:
                try:
                    get_okved(subj['СвОКВЭД']['СвОКВЭДДоп'], 'no', id_supplier)
                except Exception:
                    with open(file_log, 'a') as flog:
                        flog.write(
                                'Ошибка добавления дополнительного ОКВЭД  {0}\n\n\n'.format(
                                        str(subj['СвОКВЭД']['СвОКВЭДДоп'])))
            elif test == 2:
                for ok in subj['СвОКВЭД']['СвОКВЭДДоп']:
                    try:
                        get_okved(ok, 'no', id_supplier)
                    except Exception:
                        with open(file_log, 'a') as flog:
                            flog.write(
                                    'Ошибка добавления дополнительного ОКВЭД  {0}\n\n\n'.format(
                                            str(ok)))
            else:
                with open(file_log, 'a') as flog:
                    flog.write(
                            'У supplier не смогли добавить дополнительный ОКВЭД  {0}\n\n\n'.format(
                                    str(subj['СвОКВЭД']['СвОКВЭДДоп'])))


def get_ip(subj):
    inn = ''
    kpp = ''
    contracts_count = 0
    contracts223_count = 0
    contracts_sum = 0.0
    contracts223_sum = 0.0
    ogrn = ''
    region_code = ''
    organizationName = ''
    postal_address = ''
    contactPhone = ''
    contactFax = ''
    contactEMail = ''
    contact_name = ''
    lastname = ''
    firstname = ''
    middlename = ''
    city = ''
    street = ''
    if '@ИННФЛ' in subj['ИПВклМСП']:
        inn = subj['ИПВклМСП']['@ИННФЛ']
    else:
        with open(file_log, 'a') as flog:
            flog.write('У ИП нет ИНН  {0}\n\n\n'.format(str(subj['ИПВклМСП'])))
        return
    if not inn:
        with open(file_log, 'a') as flog:
            flog.write('У ИП пустой инн  {0}\n\n\n'.format(str(subj['ИПВклМСП'])))
        return
    con = connect_bd('partners_1c')
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute("""SELECT id FROM od_supplier WHERE inn = %s """, (inn,))
    result_supplier = cur.fetchone()
    if result_supplier:
        cur.close()
        con.close()
        with open(file_log, 'a') as flog:
            flog.write('Такой инн ИП уже есть в базе ' + ' ' + inn + '\n\n\n')
        return
    if 'ФИОИП' in subj['ИПВклМСП']:
        if '@Фамилия' in subj['ИПВклМСП']['ФИОИП']:
            lastname = subj['ИПВклМСП']['ФИОИП']['@Фамилия']
        if '@Имя' in subj['ИПВклМСП']['ФИОИП']:
            firstname = subj['ИПВклМСП']['ФИОИП']['@Имя']
        if '@Отчество' in subj['ИПВклМСП']['ФИОИП']:
            middlename = subj['ИПВклМСП']['ФИОИП']['@Отчество']
    organizationName = lastname + ' ' + firstname + ' ' + middlename
    if 'СведМН' in subj:
        if '@КодРегион' in subj['СведМН']:
            region_code = subj['СведМН']['@КодРегион']
        if 'Регион' in subj['СведМН']:
            if '@Наим' in subj['СведМН']['Регион']:
                city = subj['СведМН']['Регион']['@Наим']
        if 'НаселПункт' in subj['СведМН']:
            if '@Наим' in subj['СведМН']['НаселПункт']:
                street = subj['СведМН']['НаселПункт']['@Наим']
    postal_address = city + ' ,' + street
    query_add_supplier = """INSERT INTO od_supplier SET  inn = %s, kpp = %s, contracts_count = %s,
                contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s,
                organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s,
                contact_name = %s """
    value_add_supplier = (
        inn, kpp, contracts_count, contracts223_count,
        contracts_sum, contracts223_sum, ogrn, region_code,
        organizationName, postal_address, contactPhone, contactFax,
        contactEMail, contact_name)
    cur.execute(query_add_supplier, value_add_supplier)
    id_supplier = con.insert_id()
    cur.close()
    con.close()
    if 'СвОКВЭД' in subj:
        if subj['СвОКВЭД'] is None:
            return
        if 'СвОКВЭДОсн' in subj['СвОКВЭД']:
            try:
                get_okved(subj['СвОКВЭД']['СвОКВЭДОсн'], 'yes', id_supplier)
            except Exception:
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка добавления основного ОКВЭД  {0}\n\n\n'.format(str(subj['СвОКВЭД']['СвОКВЭДОсн'])))
        if 'СвОКВЭДДоп' in subj['СвОКВЭД']:
            test = 0
            try:
                b = subj['СвОКВЭД']['СвОКВЭДДоп'][0]
                test = 2
            except KeyError:
                test = 1
            if test == 1:
                try:
                    get_okved(subj['СвОКВЭД']['СвОКВЭДДоп'], 'no', id_supplier)
                except Exception as ex1:
                    with open(file_log, 'a') as flog:
                        flog.write(
                                'Ошибка добавления дополнительного ОКВЭД  {0} {1}\n\n\n'.format(
                                        str(subj['СвОКВЭД']['СвОКВЭДДоп']), str(ex1)))
            elif test == 2:
                for ok in subj['СвОКВЭД']['СвОКВЭДДоп']:
                    try:
                        get_okved(ok, 'no', id_supplier)
                    except Exception as ex1:
                        with open(file_log, 'a') as flog:
                            flog.write(
                                    'Ошибка добавления дополнительного ОКВЭД  {0} {1}\n\n\n'.format(
                                            str(ok), str(ex1)))
            else:
                with open(file_log, 'a') as flog:
                    flog.write(
                            'У supplier не смогли добавить дополнительный ОКВЭД  {0}\n\n\n'.format(
                                    str(subj['СвОКВЭД']['СвОКВЭДДоп'])))


def parse(path_f):
    with open(path_f) as fd:
        document = xmltodict.parse(fd.read())
        if 'Файл' in document:
            if 'Документ' in document['Файл']:
                for subj in document['Файл']['Документ']:
                    if 'ИПВклМСП' in subj:
                        try:
                            get_ip(subj)
                        except Exception as ex:
                            with open(file_log, 'a') as flog:
                                flog.write('Не удалось пропарсить ИП ' + str(ex) + ' ' + path_f + '\n' +
                                           str(subj) + '\n')
                    elif 'ОргВклМСП' in subj:
                        try:
                            get_org(subj)
                        except Exception as ex:
                            with open(file_log, 'a') as flog:
                                flog.write('Не удалось пропарсить ЮЛ ' + str(ex) + ' ' + path_f + '\n' +
                                           str(subj) + '\n')
                    else:
                        with open(file_log, 'a') as flog:
                            flog.write('Не удалось определить найти тип субъекта  {0}\n'.format(str(subj)))
            else:
                with open(file_log, 'a') as flog:
                    flog.write('Нет тега Документ ' + ' ' + path_f + '\n')
        else:
            with open(file_log, 'a') as flog:
                flog.write('Нет тега Файл ' + ' ' + path_f + '\n')


def parser_xml(file):
    path_f = work_cat + '/' + file + '/' + file
    path_del = work_cat + '/' + file
    print(path_f)
    try:
        parse(path_f)
    except Exception as ex:
        print('Не удалось пропарсить файл {0} {1}'.format(str(ex), path_f))
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(ex), path_f))
    shutil.rmtree(path_del, ignore_errors=True)


def main():
    archive = work_cat + '/supp_nalog.zip'
    url = 'https://www.nalog.ru/opendata/7707329152-rsmp/data-01112017-structure-08012016.zip'
    try:
        urllib.request.urlretrieve(url, archive)
    except urllib.error.HTTPError as httperr:
        with open(file_log, 'a') as flog_err:
            flog_err.write(
                'Ошибка скачивания: ' + str(datetime.datetime.now()) + ' ' + str(httperr) + ' ' + str(url) + '\n')
        exit()
    z = zipfile.ZipFile(archive, 'r')
    try:
        file_list_zip = z.namelist()
    except Exception as ex:
        print('Не удалось получить список файлов  в архиве {0} {1}'.format(str(ex), work_cat))
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось получить список файлов в архиве {0} {1}\n'.format(str(ex), work_cat))
        exit()
    count_file = len(file_list_zip)
    for file in file_list_zip:
        extract_f = work_cat + '/' + file
        try:
            z.extract(file, extract_f)
        except Exception as ex:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось извлечь файл {0} {1}\n'.format(str(ex), file))
            continue
        try:
            parser_xml(file)
        except Exception as ex:
            print('Ошибка в функции parser_xml {0} {1}'.format(str(ex), file))
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка в функции parser_xml {0} {1}\n'.format(str(ex), file))
        count_file -= 1
        print('Осталось пропарсить: {0}'.format(str(count_file)))
    z.close()

if __name__ == "__main__":
    main()
