import pymysql


def connect_bd(baza):
    con = pymysql.connect(host="localhost", port=3306, user="parser", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def main():
    con = connect_bd("tender")
    cur = con.cursor()
    update_purch_obj(cur)
    update_contr(cur)
    cur.close()
    con.close()


def update_purch_obj(cur):
    cur.execute("""SELECT count(id_purchase_object) AS cnt FROM purchase_object WHERE 1""")
    count_p = cur.fetchone()
    count_p = int(count_p['cnt'])
    for i in range(1, (count_p + 1)):
        try:
            cur.execute("""SELECT price, sum FROM purchase_object WHERE id_purchase_object=%s""", (i,))
            res_pur = cur.fetchone()
            price = str(res_pur['price'])
            price = price.replace(',', '.')
            sum_p = str(res_pur['sum'])
            sum_p = sum_p.replace(',', '.')
            cur.execute("""UPDATE purchase_object SET price=%s, sum=%s WHERE id_purchase_object=%s""",
                        (price, sum_p, i))
        except Exception as e:
            print(e)


def update_contr(cur):
    cur.execute("""SELECT count(id_contract_sign) AS c FROM  contract_sign WHERE 1""")
    count_c = cur.fetchone()
    count_c = int(count_c['c'])
    for i in range(1, (count_c + 1)):
        try:
            cur.execute("""SELECT contract_sign_price FROM contract_sign WHERE id_contract_sign=%s""", (i,))
            res_c = cur.fetchone()
            sum_c = str(res_c['contract_sign_price'])
            sum_c = sum_c.replace(',', '.')
            cur.execute("""UPDATE contract_sign SET contract_sign_price=%s WHERE id_contract_sign=%s""", (sum_c, i))
        except Exception as e:
            print(e)


if __name__ == "__main__":
    try:
        main()
    except Exception as exm:
        print(exm)
