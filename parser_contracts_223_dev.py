#!/usr/bin/python3.5
import datetime
import json
import logging
import os
import urllib.request
import zipfile
from urllib.error import HTTPError
import pymysql

file_log = './log_contracts223/contracts223_{0}.log'.format(str(datetime.date.today()))
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
currentyear = datetime.datetime.strftime(datetime.date.today(), "%Y")
# print(int(currentyear))
range_month = ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12')
years_temp = int(currentyear)
log_add_contract = 0
log_add_supplier = 0
log_add_customer = 0
log_add_product = 0
DB = 'tenders_test'
SUFFIX = ''


def connect_bd(baza):
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def contract223(line):
    global log_add_contract
    global log_add_supplier
    global log_add_customer
    global log_add_product
    global file_log
    string = str(line)
    if (string.find('[', 0, 1) != -1):
        string = string[1:]
    size = len(string)
    if (string.find(',', (size - 2), size) != -1):
        string = string[: -2]
    if (string.find(']', (size - 2), size) != -1):
        string = string[: -2]
    end_string = string.strip()
    contract = json.loads(end_string)
    con = connect_bd(DB)
    cur = con.cursor()
    # Инициализация переменных customer
    # regnumber_custimer = ''
    inn_customer = ''
    kpp_customer = ''
    contracts_count_customer = 0
    contracts223_count_customer = 0
    contracts_sum_customer = 0.0
    contracts223_sum_customer = 0.0
    ogrn_customer = ''
    region_code_customer = ''
    full_name_customer = ''
    postal_address_customer = ''
    phone_customer = ''
    fax_customer = ''
    email_customer = ''
    contact_name_customer = ''

    # Инициализация переменных supplier
    kpp_supplier = ''
    contracts_count_supplier = 0
    contracts223_count_supplier = 0
    contracts_sum_supplier = 0.0
    contracts223_sum_supplier = 0.0
    ogrn_supplier = ''
    region_code_supplier = ''
    organizationname_supplier = ''
    postal_address_supplier = ''
    contactphone_supplier = ''
    contactfax_supplier = ''
    contactemail_supplier = ''
    contact_name_supplier = ''

    # Инициализация переменных контрактов
    result_exist_contract = 0
    id_contract = ''  # id
    p_number = ''  # number
    # current_contract_stage = ''
    file_version = ''
    # placing = ''
    region_code = ''
    # url = ''
    sign_date = '0000-00-00'  # contractCreateDate
    # single_customer_reason_code = ''
    # single_customer_reason_name = ''
    fz = ''
    # placing_way_code = ''
    notification_number = ''  # purchaseNoticeNumber
    lot_number = 1
    contract_price = 0.0  # price
    currency = ''  # currency name
    version_number = 0  # versionNumber
    # execution_start_date = '0000-00-00'
    # execution_end_date = '0000-00-00'
    id_customer = 0
    id_supplier = 0
    id_od_contract = 0

    # Инициализация переменных продуктов
    name = ''
    okpd2_code = ''
    okpd2_group_code = 0
    okpd2_group_level1_code = 0
    price = 0.0
    okpd2_name = ''
    quantity = 0.0
    sum = 0.0
    sid = ''
    customer_regnumber = ''
    supplier_inn = ''
    okei = ''
    if 'price' in contract:
        contract_price = float(contract['price'])
    if 'customer' in contract:
        if 'regNum' in contract['customer']:
            customer_regnumber = str(contract['customer']['regNum'])
    if 'suppliers' in contract:
        if 'inn' in contract['suppliers'][0]:
            supplier_inn = str(contract['suppliers'][0]['inn'])
            supplier_inn = supplier_inn.strip()
    if customer_regnumber:
        cur.execute("""SELECT id FROM od_customer{0} WHERE regNumber = %s """.format(SUFFIX), (customer_regnumber,))
        result_customer = cur.fetchone()
        if result_customer:
            id_customer = result_customer['id']
        if not id_customer:
            if 'kpp' in contract['customer']:
                kpp_customer = str(contract['customer']['kpp'])
            if 'fullName' in contract['customer']:
                full_name_customer = str(contract['customer']['fullName'])
            if 'inn' in contract['customer']:
                inn_customer = str(contract['customer']['inn'])
            if 'postalAddress' in contract['customer']:
                postal_address_customer = str(contract['customer']['postalAddress'])
            if 'fax' in contract['customer']:
                fax_customer = str(contract['customer']['fax'])
            if 'OGRN' in contract['customer']:
                ogrn_customer = str(contract['customer']['OGRN'])
            if 'phone' in contract['customer']:
                phone_customer = str(contract['customer']['phone'])
            if 'email' in contract['customer']:
                email_customer = str(contract['customer']['email'])
            contracts223_count_customer = 1
            contracts223_sum_customer = contract_price
            query_add_customer = """INSERT INTO od_customer{0} SET regNumber = %s, inn = %s, kpp = %s, 
                                    contracts_count = %s, contracts223_count = %s, contracts_sum = %s, 
                                    contracts223_sum = %s, ogrn = %s, region_code = %s, full_name = %s, 
                                    postal_address = %s, phone = %s, fax = %s, email = %s, 
                                    contact_name = %s""".format(SUFFIX)
            value_add_customer = (customer_regnumber, inn_customer, kpp_customer, contracts_count_customer,
                                  contracts223_count_customer, contracts_sum_customer,
                                  contracts223_sum_customer, ogrn_customer, region_code_customer,
                                  full_name_customer, postal_address_customer, phone_customer, fax_customer,
                                  email_customer, contact_name_customer)
            result_add_customer = cur.execute(query_add_customer, value_add_customer)
            # id_customer = con.insert_id()
            if result_add_customer:
                log_add_customer += 1
                # print('Добавлен customer с regNumber {0}'.format(customer_regnumber))
            else:
                flog1 = open(file_log, 'a')
                flog1.write('Customer  с regNumber: {0} не добавлен\n'.format(str(customer_regnumber)))
                flog1.close()
    if supplier_inn:
        cur.execute("""SELECT id FROM od_supplier{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX),
                    (supplier_inn, kpp_supplier))
        result_supplier = cur.fetchone()
        if result_supplier:
            id_supplier = result_supplier['id']
        if not id_supplier:
            if 'kpp' in contract['suppliers']:
                kpp_supplier = str(contract['suppliers']['kpp'])
                kpp_supplier = kpp_supplier.strip()
            if 'contactPhone' in contract['suppliers']:
                contactphone_supplier = str(contract['suppliers']['contactPhone'])
            if 'contactEMail' in contract['suppliers']:
                contactemail_supplier = str(contract['suppliers']['contactEMail'])
            if 'organizationName' in contract['suppliers']:
                organizationname_supplier = str(contract['suppliers']['organizationName'])
            contracts223_count_supplier = 1
            contracts223_sum_supplier = contract_price
            query_add_supplier = """INSERT INTO od_supplier{0} SET  inn = %s, kpp = %s, contracts_count = %s, 
                                    contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, 
                                    region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, 
                                    contactFax = %s, contactEMail = %s, contact_name = %s""".format(SUFFIX)
            value_add_supplier = (
                supplier_inn, kpp_supplier, contracts_count_supplier, contracts223_count_supplier,
                contracts_sum_supplier, contracts223_sum_supplier, ogrn_supplier, region_code_supplier,
                organizationname_supplier, postal_address_supplier, contactphone_supplier, contactfax_supplier,
                contactemail_supplier, contact_name_supplier)
            result_add_supplier = cur.execute(query_add_supplier, value_add_supplier)
            # id_supplier = con.insert_id()
            if result_add_supplier:
                log_add_supplier += 1
                # print('Добавлен supplier с inn {0}'.format(supplier_inn))
            else:
                flog2 = open(file_log, 'a')
                flog2.write('Supplier  с inn: {0} не добавлен\n'.format(str(supplier_inn)))
                flog2.close()
    cur.close()
    con.close()


def main():
    global years_temp
    while True:
        if years_temp >= 2015:
            for month in range_month:
                per = 0
                down_count = 10
                while down_count:
                    try:
                        period = datetime.timedelta(days=per)
                        lastdate = datetime.date.today() - period
                        format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
                        url_contract = 'https://clearspending.ru/download/opendata/contracts_223fz_{0}{1}-{2}.json.zip'.format(
                                str(years_temp), str(month), format_date)
                        # print(url_contract)
                        archive = 'contracts_223fz_' + str(years_temp) + str(month) + '-' + format_date + '.json.zip'
                        urllib.request.urlretrieve(url_contract, archive)
                        break
                    except HTTPError as httperr:
                        # print(httperr)
                        with open(file_log, 'a') as flog_err:
                            flog_err.write(
                                    'Ошибка скачивания: {0} {1}{2}\n'.format(str(datetime.datetime.now()), str(httperr),
                                                                             str(url_contract)))
                        per += 1
                        logging.exception("Ошибка cкачивания:")
                    except Exception as ex:
                        with open(file_log, 'a') as flog_err:
                            flog_err.write(
                                    'Неизвестная ошибка при скачивании: {0} {1}{2}\n'.format(
                                            str(datetime.datetime.now()),
                                            str(ex), str(url_contract)))
                        logging.exception("Неизвестная ошибка при скачивании:")
                    down_count -= 1
                    # print(down_count)
                if down_count == 0:
                    with open(file_log, 'a') as flog_err1:
                        flog_err1.write('Не удалось получить архив за: {0} {1}\n'.format(str(years_temp), str(month)))
                    continue
                try:
                    z = zipfile.ZipFile(archive, 'r')
                    z.extractall()
                    z.close()
                except Exception:
                    with open(file_log, 'a') as flog_err1:
                        flog_err1.write('Не удалось распаковать архив: {0} {1}\n'.format(str(years_temp), str(month)))
                    continue
                file_name = 'contracts_223fz_{0}{1}-{2}.json'.format(str(years_temp), str(month), format_date)
                # print(file_name)
                f = open(file_name, 'r')
                for line in f:
                    try:
                        if contract223(line):
                            continue
                    except Exception:
                        logging.exception("Ошибка:")
                f.close()
                flog = open(file_log, 'a')
                flog.write('Пропарсили архив: {0}\n\n\n'.format(str(file_name)))
                flog.close()
                os.remove(archive)
                os.remove(file_name)

            years_temp -= 1
            # print(years_temp)
        else:
            break


if __name__ == "__main__":
    flog = open(file_log, 'a')
    flog.write('Время начала работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    flog.close()
    main()
    flog = open(file_log, 'a')
    flog.write('Время окончания работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    flog.write('Добавлено customer: {0}\n'.format(str(log_add_customer)))
    flog.write('Добавлено supplier: {0}\n'.format(str(log_add_supplier)))
    flog.write('Добавлено contract: {0}\n'.format(str(log_add_contract)))
    flog.write('Добавлено product: {0}\n'.format(str(log_add_product)))
    flog.close()
