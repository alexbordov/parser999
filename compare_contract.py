import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)
cur2 = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT count(*)  as c FROM od_contract_old""")
res_cus = cur.fetchone()
len_contract = int(res_cus['c'])
i = 1
while i <= len_contract:
    cur.execute("""SELECT id_contract, regnum FROM od_contract_old WHERE id=%s""", (i,))
    res_cus = cur.fetchone()
    if res_cus is None:
        break
    cur2.execute("""SELECT id FROM od_contract WHERE id_contract=%s""", (res_cus['id_contract']))
    res_cus_2 = cur2.fetchone()
    if not res_cus_2:
        with open('_compare_contracts.txt', 'a') as f:
            f.write("{0}  {1} \n".format(res_cus['id_contract'], res_cus['regnum']))
    i += 1

cur.close()
cur2.close()
con.close()
