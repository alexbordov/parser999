import pymysql
import logging
import datetime
from warnings import filterwarnings
from connect_to_db import connect_bd

filterwarnings('ignore', category=pymysql.Warning)
file_log = './log_organization/organization_ftp_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
SUFFIX = '_from_ftp_dev'
SUFFIXOD = ''
DB = 'tender'

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


def get_el(d, *kwargs):
    res = ''
    l = len(kwargs)
    if l == 1:
        try:
            res = d[kwargs[0]]
        except Exception:
            res = ''
    elif l == 2:
        try:
            res = d[kwargs[0]][kwargs[1]]
        except Exception:
            res = ''
    elif l == 3:
        try:
            res = d[kwargs[0]][kwargs[1]][kwargs[2]]
        except Exception:
            res = ''
    elif l == 4:
        try:
            res = d[kwargs[0]][kwargs[1]][kwargs[2]][kwargs[3]]
        except Exception:
            res = ''
    elif l == 5:
        try:
            res = d[kwargs[0]][kwargs[1]][kwargs[2]][kwargs[3]][kwargs[4]]
        except Exception:
            res = ''
    elif l == 6:
        try:
            res = d[kwargs[0]][kwargs[1]][kwargs[2]][kwargs[3]][kwargs[4]][kwargs[5]]
        except Exception:
            res = ''
    if res is None:
        res = ''
    return res


def logging_parser(*kwargs):
    s_log = str(datetime.datetime.now()) + ' '
    for i in kwargs:
        s_log += ('{0} '.format(str(i)))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


class Organization:
    log_insert = 0
    log_update = 0
    regnumber_null = 0
    add_new_customer = 0
    update_new_customer = 0

    def __init__(self, org):
        self.org = org

    def get_org(self):
        if 'export' in self.org:
            if 'nsiOrganizationList' in self.org['export']:
                if 'nsiOrganization' in self.org['export']['nsiOrganizationList']:
                    orgs = generator_univ(self.org['export']['nsiOrganizationList']['nsiOrganization'])
                else:
                    return []
            else:
                return []
        else:
            return []
        return orgs

    @staticmethod
    def regNumber(og):
        return get_el(og, 'regNumber')

    @staticmethod
    def inn(og):
        return get_el(og, 'INN')

    @staticmethod
    def kpp(og):
        return get_el(og, 'KPP')

    @staticmethod
    def ogrn(og):
        return get_el(og, 'OGRN')

    @staticmethod
    def region_code(og):
        r_c = get_el(og, 'factualAddress', 'region', 'kladrCode')
        if not r_c:
            r_c = get_el(og, 'factualAddress', 'city', 'kladrCode')
        if not r_c:
            r_c = get_el(og, 'factualAddress', 'area', 'kladrCode')
        if r_c:
            r_c = r_c[:2]
        return r_c

    @staticmethod
    def full_name(og):
        return get_el(og, 'fullName')

    @staticmethod
    def short_name(og):
        return get_el(og, 'shortName')

    @staticmethod
    def postal_address(og):
        return get_el(og, 'postalAddress')

    @staticmethod
    def phone(og):
        return get_el(og, 'phone')

    @staticmethod
    def fax(og):
        return get_el(og, 'fax')

    @staticmethod
    def email(og):
        return get_el(og, 'email')

    @staticmethod
    def contact_name(og):
        lastName = get_el(og, 'contactPerson', 'lastName')
        firstName = get_el(og, 'contactPerson', 'firstName')
        middleName = get_el(og, 'contactPerson', 'middleName')
        return '{0} {1} {2}'.format(firstName, middleName, lastName)


def parser_o(org, path):
    regNumber = Organization.regNumber(org)
    inn = Organization.inn(org)
    if not regNumber:
        Organization.regnumber_null += 1
        logging_parser('У организации нет regnum', path, inn)
        return
    kpp = Organization.kpp(org)
    ogrn = Organization.ogrn(org)
    region_code = Organization.region_code(org)
    full_name = Organization.full_name(org)
    short_name = Organization.short_name(org)
    postal_address = Organization.postal_address(org)
    phone = Organization.phone(org)
    fax = Organization.fax(org)
    email = Organization.email(org)
    contact_name = Organization.contact_name(org)
    contracts_count = 0
    contracts223_count = 0
    contracts_sum = 0.0
    contracts223_sum = 0.0
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute("""SELECT * FROM od_customer{0} WHERE regNumber = %s """.format(SUFFIX), (regNumber,))
    resultregnum = cur.fetchone()
    if not resultregnum:
        query1 = """INSERT INTO od_customer{0} SET regNumber = %s, inn = %s, kpp = %s, contracts_count = %s, 
                contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, 
                full_name = %s, short_name = %s, postal_address = %s, phone = %s, fax = %s, email = %s, 
                contact_name = %s""".format(SUFFIX)
        value1 = (
            regNumber, inn, kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn,
            region_code,
            full_name, short_name, postal_address, phone, fax, email, contact_name)
        cur.execute(query1, value1)
        Organization.log_insert += 1
        cur.execute("""SELECT id FROM od_customer{0} WHERE regNumber=%s""".format(SUFFIXOD), (regNumber,))
        res_c = cur.fetchone()
        if not res_c:
            query3 = """INSERT INTO od_customer{0} SET regNumber = %s, inn = %s, kpp = %s, contracts_count = %s, 
                            contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, 
                            region_code = %s, 
                            full_name = %s, postal_address = %s, phone = %s, fax = %s, email = %s, 
                            contact_name = %s""".format(SUFFIXOD)
            cur.execute(query3, value1)
            Organization.add_new_customer += 1
    else:
        query2 = """UPDATE od_customer{0} SET inn = %s, kpp = %s, contracts_count = %s, contracts223_count = %s, 
                contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, full_name = %s, short_name = %s,
                postal_address = %s, phone = %s, fax = %s, email = %s, 
                contact_name = %s WHERE regNumber = %s""".format(SUFFIX)
        value2 = (
            inn, kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, region_code,
            full_name, short_name,
            postal_address, phone, fax, email, contact_name, regNumber)
        cur.execute(query2, value2)
        Organization.log_update += 1
        # query4 = """UPDATE od_customer{0} SET inn = %s, kpp = %s, contracts_count = %s, contracts223_count = %s,
        #                 contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, full_name = %s,
        #                 postal_address = %s, phone = %s, fax = %s, email = %s,
        #                 contact_name = %s WHERE regNumber = %s""".format(SUFFIX)
        # cur.execute(query4, value2)
        # Organization.update_new_customer += 1
    cur.close()
    con.close()


def parser(doc, path_xml):
    global file_log
    o = Organization(doc)
    orgs = o.get_org()
    if not orgs:
        logging_parser('В файле нет списка организаций:', path_xml)
    for org in orgs:
        try:
            parser_o(org, path_xml)
        except Exception:
            logging.exception('Ошибка парсера {0}'.format(path_xml))
