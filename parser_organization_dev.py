import ftplib
import pymysql
import logging
import datetime
import zipfile
import os
import shutil
import xmltodict
import parser_org_dev as parser_org
from connect_to_db import connect_bd

file_log = './log_organization/organization_ftp_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
count_good = 0
count_bad = 0
except_file = ()
unic_files = []


def get_xml_to_dict(filexml, dirxml):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    global count_good
    global count_bad

    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    with open(path_xml) as fd:
        try:
            s = fd.read()
            s = s.replace("ns2:", "")
            s = s.replace("oos:", "")
            doc = xmltodict.parse(s)
            parser_org.parser(doc, path_xml)
            count_good += 1
            # with open(file_count_god, 'a') as good:
            #     good.write(str(count_good) + '\n')

        except Exception as ex:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка конвертации в словарь {0} {1}\n\n\n'.format(str(ex), path_xml))
            count_bad += 1
            # with open(file_count_bad, 'a') as bad:
            #     bad.write(str(count_bad) + '\n')
            # return


def bolter(file, l_dir):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    # print(f)
    try:
        get_xml_to_dict(file, l_dir)
    except Exception as exppars:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(exppars), file))


def get_list_ftp(path_parse):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            array_ar.append(i)

    return array_ar


def extract_prot(m, path_parse1):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            print('Не удалось извлечь архив ' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
            except Exception as ear:
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flogc:
                flogc.write('Не удалось извлечь архив {0} {1}\n'.format(str(e), l))
            return

        try:
            file_list = os.listdir(l_dir)
        except Exception as ex:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов {0} {1}\n'.format(str(ex), l_dir))
        else:
            for f in file_list:
                bolter(f, l_dir)

        os.remove(l)
        shutil.rmtree(l_dir, ignore_errors=True)


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            host = 'ftp.zakupki.gov.ru'
            ftpuser = 'free'
            password = 'free'
            ftp2 = ftplib.FTP(host)
            ftp2.set_debuglevel(0)
            ftp2.encoding = 'utf8'
            ftp2.login(ftpuser, password)
            ftp2.cwd(path_parse1)
            local_f = './temp_organization/' + str(m)
            lf = open(local_f, 'wb')
            ftp2.retrbinary('RETR ' + str(m), lf.write)
            lf.close()
            retry = False
            return local_f
        except Exception as ex:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив {0} {1}\n'.format(str(ex), m))
            if count > 10:
                return 0
            count += 1


def main():
    with open(file_log, 'a') as flog:
        flog.write('Время начала работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    temp_dir = 'temp_organization'
    shutil.rmtree(temp_dir, ignore_errors=True)
    os.mkdir(temp_dir)
    path_parse = 'fcs_nsi/nsiOrganization/'
    try:
        # получаем список архивов
        arr_con = get_list_ftp(path_parse)
        for j in arr_con:
            try:
                extract_prot(j, path_parse)
            except Exception as exc:
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка в экстракторе и парсере {0} {1}\n'.format(str(exc), j))
                continue

    except Exception as ex:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось получить список архивов {0} {1}\n'.format(str(ex), path_parse))
    with open(file_log, 'a') as flog:
        flog.write('Добавлено заказчиков: {0}\n'.format(str(parser_org.Organization.log_insert)))
        flog.write('Обновлено заказчиков: {0}\n'.format(str(parser_org.Organization.log_update)))
        flog.write('Заказчиков без RegNumber: {0}\n'.format(str(parser_org.Organization.regnumber_null)))
        flog.write('Добавлено в od_customer: {0}\n'.format(str(parser_org.Organization.add_new_customer)))
        flog.write('Обновлено в od_customer: {0}\n'.format(str(parser_org.Organization.update_new_customer)))
        flog.write('Время окончания работы парсера: {0}\n\n\n'.format(str(datetime.datetime.now())))


if __name__ == "__main__":
    try:
        main()
    except Exception as exm:
        with open(file_log, 'a') as flogm:
            flogm.write('Ошибка в функции main: {0}\n\n\n'.format(str(exm)))
