import datetime
import ftplib
import logging
import os
import shutil
import sys
import time
import zipfile
import pymysql
import timeout_decorator
import xmltodict
from warnings import filterwarnings
import parser44
from connect_to_db import connect_bd

filterwarnings('ignore', category=pymysql.Warning)
suffix = ''
name_db = 'tender'
log_dir = 'log_contracts44_ftp'
temp_dir = 'temp44'
file_log = './{1}/contracts44_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
count_good = 0
count_bad = 0
except_file = ('Failure', 'contractProcedure', 'contractCancel')


@timeout_decorator.timeout(300)
def down_timeout(m, path_parse1):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse1)
    local_f = './{1}/{0}'.format(str(m), temp_dir)
    lf = open(local_f, 'wb')
    ftp2.retrbinary('RETR ' + str(m), lf.write)
    lf.close()
    ftp2.close()
    return local_f


def bolter(file, l_dir, region):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    file_lower = file.lower()
    if not file_lower.endswith('.xml'):
        return
    for g in except_file:
        if file_lower.find(g.lower()) != -1:
            return
    # print(f)
    try:
        get_xml_to_dict(file, l_dir, region)
    except Exception as exppars:
        print('Не удалось пропарсить файл ' + str(exppars) + ' ' + file)
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл ' + str(exppars) + ' ' + file + '\n')


def get_xml_to_dict(filexml, dirxml, region):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    global count_good
    global count_bad
    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    with open(path_xml) as fd:
        try:
            doc = xmltodict.parse(fd.read())
            parser44.parser(doc, path_xml, region)
            count_good += 1
            # with open(file_count_god, 'a') as good:
            #     good.write(str(count_good) + '\n')

        except Exception as ex:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка конвертации в словарь ' + str(ex) + ' ' + path_xml + '\n\n\n')
            count_bad += 1
            # with open(file_count_bad, 'a') as bad:
            #     bad.write(str(count_bad) + '\n')
            # return


def get_list_ftp_curr(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(name_db)
    cur_arhiv = con_arhiv.cursor()
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_contract{0} WHERE arhiv = %s and region = %s""".format(suffix),
                              (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_contract{0} SET arhiv = %s, region = %s""".format(suffix)
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    return array_ar


def get_list_ftp_prev(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(name_db)
    cur_arhiv = con_arhiv.cursor()
    searchstring = datetime.datetime.now().strftime('%Y%m%d')
    for i in data:
        i_prev = "prev_{0}".format(i)
        if i.find(searchstring) != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_contract{0} WHERE arhiv = %s and region = %s""".format(suffix),
                              (i_prev, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_contract{0} SET arhiv = %s, region = %s""".format(suffix)
                query_par = (i_prev, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    return array_ar


def get_list_ftp_last(path_parse, pth):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    str_f = 'contract_' + pth + '_201702'
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            # if i.find(str_f) != -1:
            array_ar.append(i)

    return array_ar


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            lf = down_timeout(m, path_parse1)
            retry = False
            if count > 0:
                with open(file_log, 'a') as flog:
                    flog.write('Удалось скачать архив после попытки ' + str(count) + ' ' + m + '\n')
            return lf
        except Exception as ex:
            # print('Не удалось скачать архив ' + str(ex) + ' ' + m)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив ' + str(ex) + ' ' + m + '\n')
            if count > 50:
                return ''
            count += 1
            time.sleep(5)


def extract_contr(m, path_parse1, region):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    global need_file
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            print('Не удалось извлечь архив ошибка UnicodeDecodeError' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
                with open(file_log, 'a') as floga:
                    floga.write('Извлекли архив альтернативным методом {0}\n'.format(l))
            except Exception as ear:
                print('Не удалось извлечь архив альтернативным методом')
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            print('Не удалось извлечь архив ' + str(e) + ' ' + l)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось извлечь архив ' + str(e) + ' ' + l + '\n')
            return

        try:
            file_list = os.listdir(l_dir)
        except Exception as ex:
            print('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir + '\n')
        else:
            for f in file_list:
                bolter(f, l_dir, region)

        os.remove(l)
        try:
            shutil.rmtree(l_dir)
        except Exception as ex1:
            print('Не удалось удалить папку' + str(ex1) + ' ' + l_dir)


flog = open(file_log, 'a')
flog.write('\n\nВремя начала работы парсера: {0}\n'.format(str(datetime.datetime.now())))
flog.close()
shutil.rmtree(temp_dir, ignore_errors=True)
os.mkdir(temp_dir)

# забираем список регионов из базы данных
con_region = connect_bd(name_db)
cur_region = con_region.cursor()
cur_region.execute("""SELECT * FROM region""")
path_array = cur_region.fetchall()
cur_region.close()
con_region.close()

for reg in path_array:
    if len(sys.argv) == 1:
        print('Недостаточно параметров для запуска, используйте curr для парсинга текущего месяца и last для прошлых')
        exit()
    elif str(sys.argv[1]) == 'last':
        path_parse = 'fcs_regions/' + reg['path'] + '/contracts/'
    elif str(sys.argv[1]) == 'curr':
        path_parse = 'fcs_regions/' + reg['path'] + '/contracts/currMonth/'
    elif str(sys.argv[1]) == 'prev':
        path_parse = 'fcs_regions/' + reg['path'] + '/contracts/prevMonth/'

    try:
        # получаем список архивов
        if str(sys.argv[1]) == 'curr':
            arr_con = get_list_ftp_curr(path_parse, reg['path'])
        elif str(sys.argv[1]) == 'last':
            arr_con = get_list_ftp_last(path_parse, reg['path'])
        elif str(sys.argv[1]) == 'prev':
            arr_con = get_list_ftp_prev(path_parse, reg['path'])
        else:
            arr_con = []
            print('Неверное имя параметра, используйте curr для парсинга текущего месяца last для прошлых и prev на '
                  'всякий случай')
            exit()
        for j in arr_con:
            try:
                extract_contr(j, path_parse, reg['conf'])
            except Exception as exc:
                print('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j)
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j + '\n')
                continue

    except Exception as ex:
        print('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse)
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse + '\n')
        continue

flog = open(file_log, 'a')
flog.write('Время окончания работы парсера: ' + str(datetime.datetime.now()) + '\n')
flog.write('Добавлено customer: ' + str(parser44.Contract.count_cus) + '\n')
flog.write('Добавлено supplier: ' + str(parser44.Contract.count_sup) + '\n')
flog.write('Добавлено contract: ' + str(parser44.Contract.count_ins) + '\n')
flog.write('Обновлено contract: ' + str(parser44.Contract.count_upd) + '\n')
flog.write('Добавлено product: ' + str(parser44.Contract.count_prod) + '\n')
flog.close()
