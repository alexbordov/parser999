import multiprocessing
import textract


def fun(x):
    for d in x:
        text = textract.process(x[d], encoding='utf_8', method='tesseract', language='rus')
        text = text.decode('utf-8')
        with open('ocr_pdf' + str(x[d]) + '.txt', 'a') as f:
            f.write(text)


if __name__ == '__main__':
    p = multiprocessing.Pool(2)
    dictionary = [{1: 'тепло.pdf'}, {2: 'тепло1.pdf'}, {3: 'тепло2.pdf'}, {4: 'тепло3.pdf'}]
    print(p.map(fun, dictionary))
    # print(p.map(fun, ['тепло.pdf', 'тепло1.pdf', 'тепло2.pdf', 'тепло3.pdf']))