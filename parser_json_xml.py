import xmltodict

#XML_FILE = '0000000025_purchaseNoticeEP_20161107_115924_002.xml'
#f = open(XML_FILE, 'rb')
# string = f.read()
# print(string)
# parsexml = xmltodict.parse(f.read())
# print (parsexml)
with open('0000000025_purchaseNoticeEP_20161107_115924_002.xml') as fd:
    doc = xmltodict.parse(fd.read())
    # print(doc)
    print(doc['ns2:purchaseNoticeEP']['ns2:body']['ns2:item']['guid'])
    print(doc['ns2:purchaseNoticeEP']['ns2:body']['ns2:item']['ns2:purchaseNoticeEPData']['ns2:customer']['mainInfo'][
              'fullName'])
    # print(doc['ns2:purchaseNoticeEP']['ns2:body']['ns2:item']['ns2:purchaseNoticeEPData']['ns2:lots']['lot'])

    for i in doc['ns2:purchaseNoticeEP']['ns2:body']['ns2:item']['ns2:purchaseNoticeEPData']['ns2:lots']['lot']:
        print(i['lotData']['subject'])
        pass

    try:
        s = doc['ns2:purchaseNoticeEP']['ns2:body']['ns2:item']['ns2:purchaseNoticeEPData']['ns2:purchaseMethodCode1']
        print(s)
    except KeyError:
        s = 'ключа нет'
        print(s)


def test_key(*args):
    len_args = len(args)
    if len_args == 1:
        try:
            s = doc[args[0]]
            print(s)
        except KeyError:
            s = 'ключа нет'
            print(s)
    elif len_args == 2:
        try:
            s = doc[args[0]][args[1]]
            print(s)
        except KeyError:
            s = 'ключа нет'
            print(s)
    elif len_args == 3:
        try:
            s = doc[args[0]][args[1]][args[2]]
            print(s)
        except KeyError:
            s = 'ключа нет'
            print(s)
    elif len_args == 4:
        try:
            s = doc[args[0]][args[1]][args[2]][args[3]]
            print(s)
        except KeyError:
            s = 'ключа нет'
            print(s)
    elif len_args == 5:
        try:
            s = doc[args[0]][args[1]][args[2]][args[3]][args[4]]
            print(s)
        except KeyError:
            s = 'ключа нет'
            print(s)
    else:
        s = 'неверные параметры'
        print(s)

test_key('ns2:purchaseNoticeEP', 'ns2:body', 'ns2:item', 'ns2:purchaseNoticeEPData', 'ns2:purchaseMethodCode')
