import pymysql

suffix = '_new'
name_db = 'tender'


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def check_inn():
    con_ch = connect_bd(name_db)
    cur_ch = con_ch.cursor(pymysql.cursors.DictCursor)
    cur_ch1 = con_ch.cursor(pymysql.cursors.DictCursor)
    cur_ch2 = con_ch.cursor(pymysql.cursors.DictCursor)
    cur_ch.execute("""SELECT * FROM customer{0} WHERE inn = ''""".format(suffix))
    while True:
        res_cus_null = cur_ch.fetchone()
        if res_cus_null is None:
            break
        cur_ch1.execute("""SELECT * FROM organizer{0} WHERE reg_num =%s""".format(suffix), (res_cus_null['reg_num'],))
        res_reg_num = cur_ch1.fetchone()
        if res_reg_num:
            cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                            (res_reg_num['inn'], res_reg_num['reg_num']))
        else:
            cur_ch1.execute("""SELECT * FROM od_customer WHERE regNumber =%s""".format(suffix),
                            (res_cus_null['reg_num'],))
            res_reg_num_od = cur_ch1.fetchone()
            if res_reg_num_od:
                cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                                (res_reg_num_od['inn'], res_reg_num_od['regNumber']))
    cur_ch.close()
    cur_ch1.close()
    cur_ch2.close()
    con_ch.close()

check_inn()
