import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pymysql
import logging
import os
import random
import string
import hashlib
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import datetime
import time
from functools import reduce


def get_sheet():
    SCOPE = ["https://spreadsheets.google.com/feeds"]
    credentials = ServiceAccountCredentials.from_json_keyfile_name('APITableEdit-cd1bcc590a07.json', scopes=SCOPE)
    gc = gspread.authorize(credentials)
    # print("Доступные таблицы")
    # for sheet in gc.openall():
    #     print("{} - {}".format(sheet.title, sheet.id))
    # workbook = gc.open(SPREADSHEET)
    workbook = gc.open_by_key('124N9mC9fAYwKbn9Wsqk9_qyEKup1dabvFpptv6V4N-8')
    return workbook.get_worksheet(0)


def get_data_user(sheet, row_num):
    if not sheet.acell('B' + str(row_num)).value:
        return 'null'
    email = sheet.acell('B' + str(row_num)).value
    email.strip()
    phone = sheet.acell('G' + str(row_num)).value
    phone.strip()
    username = sheet.acell('C' + str(row_num)).value
    username.strip()
    username.title()
    period = datetime.timedelta(days=90)
    nextdate = datetime.date.today() + period
    data_expired = str(nextdate)
    datereg = sheet.acell('A' + str(row_num)).value
    datereg.strip()
    conv = time.strptime(datereg, "%d.%m.%Y %H:%M:%S")
    datereg = time.strftime("%Y-%m-%d %H:%M:%S", conv)
    #print(username, email)
    #print('\n\n')
    return dict(usr=username, mail=email, data_e=data_expired, phone=phone, datereg=datereg, count=1)


def get_list_user():
    list_user = []
    sheet = get_sheet()
    row_num = 2
    while True:
        try:
            s = get_data_user(sheet, row_num)
            if s == 'null':
                break
            list_user.append(s)
        except Exception:
            logging.exception('Ошибка: ')

        row_num += 1
    return list_user


def f(d, x):
    k = x['mail']
    if k in d:
        d[k]['count'] += x['count']
    else:
        d[k] = x
    return d

s = reduce(f, get_list_user(), {})
# print
d = {}
for i in get_list_user():
    k = i['mail']
    if k in d:
        d[k]['count'] += i['count']
    else:
        d[k] = i
for n in d.values():
    print(n)


