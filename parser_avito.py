import lxml.html as html
import urllib.request
import pymysql
import smtplib
from email.mime.text import MIMEText

def avito(url, name_adv):
    result_nout = 0
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db="avito", charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    cur = con.cursor(pymysql.cursors.DictCursor)
    try:
        req = urllib.request.Request(url=url, headers={
            'User-Agent': ' Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'})
        handler = urllib.request.urlopen(req)
    except Exception as ex:
        print('Ошибка получения страницы: ' + str(ex))

    doc_avito = html.document_fromstring(handler.read())
    #print(doc_avito.cssselect('h2')[0].text)
    for post in doc_avito.cssselect('.item.item_table.clearfix.js-catalog-item-enum.c-b-0'):
        id_elem = post.get('id')
        url_nout = post.cssselect('h3 a')
        url_nout = url_nout[0].get('href')
        url_nout = 'https://www.avito.ru' + url_nout
        url_ob = url_nout
        #print(post)
        req = urllib.request.Request(url=url_nout, headers={
            'User-Agent': ' Mozilla/6.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/18.0'})
        handler = urllib.request.urlopen(req)
        doc_nout = html.document_fromstring(handler.read())
        #print(post)
        try:
            price = doc_nout.cssselect('.price-value-string')
            price = price[0].text
            price = price.strip(' \t\n')
        except Exception as ex:
            print('Ошибка получения данных: ' + str(ex))
            price = 'нет цены'
        try:
            txt = ''
            desc = doc_nout.cssselect('.item-description-text p')
            for i in desc[0].itertext():
                txt += str(i)
                txt += '\n'
            desc = txt
        except Exception as ex:
            print('Ошибка отправки письма: ' + str(ex))
            desc = 'нет описания'
        name = doc_nout.cssselect('.sticky-header-prop.sticky-header-title')
        name = name[0].text
        name = name.strip('\t\n')
        date = doc_nout.cssselect('.title-info-metadata-item')
        date = date[0].text
        date = date.strip('\t\n')

        cur.execute("""SELECT * FROM nouts WHERE id_nout = %s """, (id_elem,))
        result_nout = cur.fetchone()
        if not result_nout:
            # отправка письма
            try:
                me = 'tender@enter-it.ru'
                you = 'rummolprod999@gmail.com'
                text = 'На авито появился новый ' + name_adv + ' по цене ' + price + ' р. \nНазвание объявления: ' + name + '\nОписание: ' + desc + '\nДата: ' + date + '\nCсылка: ' + url_ob
                subj = 'Новый ' + name_adv + ' на авито'
                server = "mail.enter-it.ru"
                port = 25
                user_name = "tender@enter-it.ru"
                user_passwd = "*******"
                msg = MIMEText(text.encode('utf-8'), "plain", "utf-8")
                msg['Subject'] = subj
                msg['From'] = me
                msg['To'] = you
                s = smtplib.SMTP(server, port)
                s.ehlo()
                s.starttls()
                s.ehlo()
                s.login(user_name, user_passwd)
                s.sendmail(me, you, msg.as_string())
                s.quit()
            except Exception as ex:
                print('Ошибка отправки письма: ' + str(ex))
                continue
            else:
                query_add_nout = 'INSERT INTO nouts SET id_nout = %s, price = %s, descr = %s, url = %s, name = %s, date = %s'
                value_add_nout = (id_elem, price, desc, url_ob, name, date)
                cur.execute(query_add_nout, value_add_nout)
    cur.close()
    con.close()


try:
    avito('https://www.avito.ru/klintsy/noutbuki', 'ноутбук')
except Exception as ex:
    print('Ошибка парсинга: ' + str(ex))
try:
    avito('https://www.avito.ru/klintsy/nastolnye_kompyutery', 'компьютер')
except Exception as ex1:
    print('Ошибка парсинга: ' + str(ex1))
try:
    avito('https://www.avito.ru/klintsy/tovary_dlya_kompyutera', 'товар для компьютера')
except Exception as ex2:
    print('Ошибка парсинга: ' + str(ex2))

