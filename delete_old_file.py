import datetime
import os
import time


def del_dir(d):
    files = os.listdir(d)
    time_new = datetime.datetime.now()
    time_new = time.mktime(time_new.timetuple())
    for f in files:
        time_f = os.path.getctime(d + '/' + f)
        if (time_new - time_f) > 7200:
            os.remove(d + '/' + f)


dirs_for_delete = ['/var/www/admin/data/www/devtenders.enter-it.ru/FileCash',
                   '/var/www/admin/data/www/tenders.enter-it.ru/FileCash']
for dir in dirs_for_delete:
    try:
        del_dir(dir)
    except Exception:
        pass
