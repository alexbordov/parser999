import urllib.parse
import urllib.request
from lxml import etree
from bs4 import BeautifulSoup
import logging
import re

count_part = 1
file_log = './log_franch_1c.log'
url_part = 'http://1c.ru/rus/partners/franch-citylist.jsp?partv8=120'
print(url_part + str(count_part))
logging.basicConfig(level=logging.DEBUG, filename=file_log)
req = urllib.request.Request(url=url_part , headers={
            'User-Agent': ' Mozilla/6.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/18.0'})
handler = urllib.request.urlopen(req, timeout=120)
soup = BeautifulSoup(handler.read(), 'html.parser')
nn = soup.find_all('a', style="color: #333333;")
for i in nn:
    print(i['href'])
    print(i.parent)
    try:
        name = i.parent.find('a', href=re.compile("franch.*")).string
        print(name)
    except Exception:
        name = i.parent.parent.find('a', href=re.compile("franch.*")).string
        print(name)