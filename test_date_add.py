import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con
dt = '2015-12-03T18:12:51.570+03:00'
con = connect_bd('test')
cur = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""INSERT INTO test_data SET data_add=%s""", (dt,))
cur.execute("""SELECT id FROM test_data WHERE id=%s""", (5,))
res = cur.fetchone()
print(res)
if res:
    print(res)