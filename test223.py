import pymysql
import logging
import datetime
import xmltodict


file = 'purchaseNotice_Brianskaya_obl_20160801_000000_20160831_235959_001.xml'

file_log = './log_tenders223/tenders223_ftp_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


class Tender223:

    def __init__(self, tender223):
        self.tender223 = None
        tender_body = None
        tender_purchase = None
        tender_root = list(tender223.keys())
        tender_child = tender223[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('body') or i.startswith('ns2:body') or i.startswith('oos:body'):
                tender_body = tender_child[i]
                break
        if tender_body is not None:
            tender_item_list = list(tender_body.keys())
            for j in tender_item_list:
                if j.startswith('item') or j.startswith('ns2:item') or j.startswith('oos:item'):
                    tender_purchase = tender_body[j]
                    break
        if tender_purchase is not None:
            tender_purchase_list = list(tender_purchase.keys())
            for m in tender_purchase_list:
                if m.startswith('purchase') or m.startswith('ns2:purchase') or m.startswith('oos:purchase'):
                    self.tender223 = tender_purchase[m]
                    break

    @property
    def id_t(self):
        id_ten = 0
        if 'ns2:guid' in self.tender223:
            id_ten = self.tender223['ns2:guid']
        elif 'oos:guid' in self.tender223:
            id_ten = self.tender223['oos:guid']
        elif 'guid' in self.tender223:
            id_ten = self.tender223['guid']
        if id_ten is None:
            id_ten = 0
        return id_ten

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'ns2:registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['ns2:registrationNumber']
        elif 'oos:registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['oos:registrationNumber']
        elif 'registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['registrationNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def docPublishDate(self):
        docPublishDate = ''
        if 'ns2:publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['ns2:publicationDateTime']
        elif 'oos:publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['oos:registrationNumber']
        elif 'publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['publicationDateTime']
        if docPublishDate is None:
            docPublishDate = ''
        return docPublishDate

    def organizer_mainInfo(self, d):
        try:
            organizer_mainInfo = self.tender223['ns2:placer']['mainInfo'][d]
        except Exception:
            try:
                organizer_mainInfo = self.tender223['oos:placer']['mainInfo'][d]
            except Exception:
                try:
                    organizer_mainInfo = self.tender223['placer']['mainInfo'][d]
                except Exception:
                    organizer_mainInfo = ''

        if organizer_mainInfo is None:
            organizer_mainInfo = ''
        return organizer_mainInfo

    @property
    def placingWay_code(self):
        placingWay_code = ''
        if 'ns2:purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['ns2:purchaseMethodCode']
        elif 'oos:purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['oos:purchaseMethodCode']
        elif 'purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['purchaseMethodCode']
        if placingWay_code is None:
            placingWay_code = ''
        return placingWay_code

    def electronicPlaceInfo(self, d):
        try:
            electronicPlaceInfo = self.tender223['ns2:electronicPlaceInfo'][d]
        except Exception:
            try:
                electronicPlaceInfo = self.tender223['oos:electronicPlaceInfo'][d]
            except Exception:
                try:
                    electronicPlaceInfo = self.tender223['electronicPlaceInfo'][d]
                except Exception:
                    electronicPlaceInfo = ''

        if electronicPlaceInfo is None:
            electronicPlaceInfo = ''
        return electronicPlaceInfo

    def get_attachments(self):
        if 'ns2:attachments' in self.tender223:
            if self.tender223['ns2:attachments'] is None:
                return []
            elif 'document' in self.tender223['ns2:attachments']:
                return generator_univ(self.tender223['ns2:attachments']['document'])
            else:
                return []
        elif 'oos:attachments' in self.tender223:
            if self.tender223['oos:attachments'] is None:
                return []
            elif 'document' in self.tender223['oos:attachments']:
                return generator_univ(self.tender223['oos:attachments']['document'])
            else:
                return []
        elif 'attachments' in self.tender223:
            if self.tender223['attachments'] is None:
                return []
            elif 'document' in self.tender223['attachments']:
                return generator_univ(self.tender223['attachments']['document'])
            else:
                return []
        else:
            return []

    def attach(self, attachment, d):
        try:
            ret_attach = attachment[d]
        except Exception:
            ret_attach = ''
        if ret_attach is None:
            ret_attach = ''
        return ret_attach

    def customer(self, d):
        try:
            customer_ret = self.tender223['ns2:customer']['mainInfo'][d]
        except Exception:
            try:
                customer_ret = self.tender223['oos:customer']['mainInfo'][d]
            except Exception:
                try:
                    customer_ret = self.tender223['customer']['mainInfo'][d]
                except Exception:
                    customer_ret = ''

        if customer_ret is None:
            customer_ret = ''
        return customer_ret

    def contact(self, d):
        try:
            contact_ret = self.tender223['ns2:contact'][d]
        except Exception:
            try:
                contact_ret = self.tender223['oos:contact'][d]
            except Exception:
                try:
                    contact_ret = self.tender223['contact'][d]
                except Exception:
                    contact_ret = ''

        if contact_ret is None:
            contact_ret = ''
        return contact_ret

    def get_lots(self):
        if 'ns2:lots' in self.tender223:
            if self.tender223['ns2:lots'] is None:
                return []
            elif 'lot' in self.tender223['ns2:lots']:
                return generator_univ(self.tender223['ns2:lots']['lot'])
            else:
                return []
        elif 'ns2:lot' in self.tender223:
            if self.tender223['ns2:lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['ns2:lot'])
        elif 'oos:lots' in self.tender223:
            if self.tender223['oos:lots'] is None:
                return []
            elif 'oos:lot' in self.tender223['oos:lots']:
                return generator_univ(self.tender223['oos:lots']['oos:lot'])
            else:
                return []
        elif 'oos:lot' in self.tender223:
            if self.tender223['oos:lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['oos:lot'])
        elif 'lots' in self.tender223:
            if self.tender223['lots'] is None:
                return []
            elif 'lot' in self.tender223['lots']:
                return generator_univ(self.tender223['lots']['lot'])
            else:
                return []
        elif 'lot' in self.tender223:
            if self.tender223['lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['lot'])
        else:
            return []

    def get_lotitems(self, lot):
        if 'lotData' in lot:
            if 'lotItems' in lot['lotData']:
                if lot['lotData']['lotItems'] is None:
                    return []
                elif 'lotItem' in lot['lotData']['lotItems']:
                    return generator_univ(lot['lotData']['lotItems']['lotItem'])
                else:
                    return []
            else:
                return []
        else:
            return []

    def lot_max_price(self, lot):
        try:
            lot_max_price = lot['lotData']['initialSum']
        except Exception:
            lot_max_price = ''
        if lot_max_price is None:
            lot_max_price = ''
        return lot_max_price

    def okpd2_code(self, item):
        try:
            okpd2_code = item['okpd2']['code']
        except Exception:
            okpd2_code = ''
        if okpd2_code is None:
            okpd2_code = ''
        return okpd2_code


def parser_type_223(doc, path_xml, filexml, reg, reg_id):
    tender = Tender223(doc)
    id_t = tender.id_t
    lots = tender.get_lots()
    for lot in lots:
        lot_max_price = tender.lot_max_price(lot)
        print(lot_max_price)
        lotitems = tender.get_lotitems(lot)
        for lotitem in lotitems:
            okpd2_code = tender.okpd2_code(lotitem)
            print(okpd2_code)



def parser(doc, path_xml, filexml, reg, reg_id):
    global file_log
    try:
        parser_type_223(doc, path_xml, filexml, reg, reg_id)
    except Exception:
        logging.exception("Ошибка при парсинге тендера типа 223: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка при парсинге тендера типа 223 ' + ' ' + path_xml + ' ' + '\n\n\n')

with open(file) as f:
    doc = xmltodict.parse(f.read())
parser(doc, file, file, 32, 32)