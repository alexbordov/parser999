import ftplib
import pymysql
import logging
import datetime
import zipfile
import os
import shutil
import xmltodict
import parser_protocol
import sys
import timeout_decorator
from connect_to_db import connect_bd

SUFFIX = ''
DB = 'tender'
TEMP_DIR = 'temp_protocol'
LOG_DIR = 'log_protocol'
file_log = './{1}/protocol_ftp_{0}.log'.format(str(datetime.date.today()), LOG_DIR)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
except_file = ()
unic_files = []
type_EF1 = 'ProtocolEF1'
type_EF2 = 'ProtocolEF2'
type_EF3 = 'ProtocolEF3'


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def unic(f, path):
    global unic_files
    begin_file_list = f.split('_')
    if not begin_file_list[0] in unic_files:
        unic_files.append(begin_file_list[0])
        file_ex = path + '/' + f
        file_target = './unic_protocol/' + f
        shutil.copy(file_ex, file_target)


def get_xml_to_dict(filexml, dirxml, region, type_f):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    # try:
    #     unic(filexml, dirxml)
    # except Exception as ex1:
    #     logging.exception("Ошибка копирования файла: ")
    #     with open(file_log, 'a') as flog9:
    #         flog9.write('Ошибка копирования файла {0} {1}\n\n\n'.format(str(ex1), filexml))
    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    with open(path_xml) as fd:
        try:
            doc = xmltodict.parse(fd.read())
            parser_protocol.parser(doc, path_xml, filexml, region, type_f)

            # with open(file_count_god, 'a') as good:
            #     good.write(str(count_good) + '\n')

        except Exception as ex:
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка конвертации в словарь ' + str(ex) + ' ' + path_xml + '\n\n\n')

                # with open(file_count_bad, 'a') as bad:
                #     bad.write(str(count_bad) + '\n')
                # return


def bolter(file, l_dir, region, type_f):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    file_lower = file.lower()
    if not file_lower.endswith('.xml'):
        return
    for g in except_file:
        if file_lower.find(g.lower()) != -1:
            return
    # print(f)
    try:
        get_xml_to_dict(file, l_dir, region, type_f)
    except Exception as exppars:
        # print('Не удалось пропарсить файл ' + str(exppars) + ' ' + file)
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(exppars), file))


def get_list_ftp_curr(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(DB)
    cur_arhiv = con_arhiv.cursor()
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_protocols{0} WHERE arhiv = %s AND region = %s""".format(SUFFIX),
                              (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_protocols{0} SET arhiv = %s, region = %s""".format(SUFFIX)
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    return array_ar


def get_list_ftp_last(path_parse):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    for i in data:
        if i.find('2016') != -1 or i.find('2017') != -1:
            array_ar.append(i)

    return array_ar


def extract_prot(m, path_parse1, region):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    global need_file
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        list_type_EF1 = []
        list_type_EF2 = []
        list_type_EF3 = []
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            # print('Не удалось извлечь архив ошибка UnicodeDecodeError ' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив ошибка UnicodeDecodeError {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
                logging_parser("Извлекли архив альтернативным методом", l)
            except Exception as ear:
                # print('Не удалось извлечь архив альтернативным методом')
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            # print('Не удалось извлечь архив ' + str(e) + ' ' + l)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flogc:
                flogc.write('Не удалось извлечь архив {0} {1}\n'.format(str(e), l))
            return

        try:
            file_list = os.listdir(l_dir)
            # list_type_EF1 = [file for file in file_list if file.find(type_EF1) != -1]
            list_type_EF2 = [file for file in file_list if file.find(type_EF2) != -1]
            list_type_EF3 = [file for file in file_list if file.find(type_EF3) != -1]
        except Exception as ex:
            # print('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir + '\n')
        else:
            # for f1 in list_type_EF1:
            #     bolter(f1, l_dir, region, type_EF1)
            for f2 in list_type_EF2:
                bolter(f2, l_dir, region, type_EF2)
            for f3 in list_type_EF3:
                bolter(f3, l_dir, region, type_EF3)

        os.remove(l)
        try:
            shutil.rmtree(l_dir, ignore_errors=True)
        except Exception:
            pass


@timeout_decorator.timeout(300)
def down_timeout(m, path_parse1):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse1)
    local_f = './{0}/{1}'.format(TEMP_DIR, str(m))
    lf = open(local_f, 'wb')
    ftp2.retrbinary('RETR ' + str(m), lf.write)
    lf.close()
    return local_f


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            lf = down_timeout(m, path_parse1)
            retry = False
            return lf
        except Exception as ex:
            # print('Не удалось скачать архив ' + str(ex) + ' ' + m)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив ' + str(ex) + ' ' + m + '\n')
            if count > 50:
                return 0
            count += 1


def main():
    shutil.rmtree(TEMP_DIR, ignore_errors=True)
    os.mkdir(TEMP_DIR)
    con_region = connect_bd(DB)
    cur_region = con_region.cursor()
    cur_region.execute("""SELECT * FROM region""")
    path_array = cur_region.fetchall()
    cur_region.close()
    con_region.close()

    for reg in path_array:
        if len(sys.argv) == 1:
            print(
                    'Недостаточно параметров для запуска, используйте curr для парсинга текущего месяца и last для '
                    'прошлых')
            exit()
        elif str(sys.argv[1]) == 'last':
            path_parse = 'fcs_regions/' + reg['path'] + '/protocols/'
        elif str(sys.argv[1]) == 'curr':
            path_parse = 'fcs_regions/' + reg['path'] + '/protocols/currMonth/'

        try:
            # получаем список архивов
            if str(sys.argv[1]) == 'curr':
                arr_con = get_list_ftp_curr(path_parse, reg['path'])
            elif str(sys.argv[1]) == 'last':
                arr_con = get_list_ftp_last(path_parse)
            else:
                arr_con = []
                print('Неверное имя параметра, используйте curr для парсинга текущего месяца и last для прошлых')
                exit()
            for j in arr_con:
                try:
                    extract_prot(j, path_parse, reg['conf'])
                except Exception as exc:
                    print('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j)
                    logging.exception("Ошибка: ")
                    with open(file_log, 'a') as flog:
                        flog.write('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j + '\n')
                    continue

        except Exception as ex:
            # print('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список архивов {0} {1}\n'.format(str(ex), path_parse))
            continue


if __name__ == "__main__":
    logging_parser("Начало парсинга")
    main()
    logging_parser("Конец парсинга")
