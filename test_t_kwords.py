from connect_to_db import connect_bd_localhost as connect_bd

suffix = ''
name_db = 'tender'


def add_tender_kwords(id_tender):
    con = connect_bd(name_db)
    cur = con.cursor()
    res_string = ''
    set_po = set()
    cur.execute("""SELECT DISTINCT po.name, po.okpd_name, cus.inn, cus.full_name FROM customer{0} AS cus RIGHT JOIN purchase_object{0} AS po 
        ON cus.id_customer = po.id_customer LEFT JOIN lot{0} AS l ON l.id_lot = po.id_lot
        WHERE l.id_tender=%s""".format(suffix), (id_tender,))
    while True:
        row = cur.fetchone()
        if not row:
            break
        string_name = row['name']
        string_name = string_name.strip()
        string_okpd_name = row['okpd_name']
        string_okpd_name = string_okpd_name.strip()
        inn_c = row['inn'] if row['inn'] else ''
        full_name_c = row['full_name'] if row['full_name'] else ''
        res = ("{0} {1} {2} {3}".format(string_name, string_okpd_name, inn_c, full_name_c)).strip()
        set_po.add(res)
    for s in set_po:
        res_string += s

    set_att = set()
    cur.execute("""SELECT file_name FROM attachment{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
    while True:
        row_1 = cur.fetchone()
        if not row_1:
            break
        sring_att = row_1['file_name']
        sring_att = sring_att.strip()
        res_a = ' ' + sring_att
        set_att.add(res_a)
    for n in set_att:
        res_string += n
    id_org = 0
    cur.execute("""SELECT purchase_object_info, id_organizer FROM tender{0} WHERE id_tender=%s""".format(suffix),
                (id_tender,))
    res_po = cur.fetchone()
    if res_po:
        id_org = res_po["id_organizer"]
        string_po = res_po['purchase_object_info']
        string_po = string_po.strip()
        res_string = string_po + ' ' + res_string
    if id_org:
        cur.execute("""SELECT full_name, inn FROM organizer{0} WHERE id_organizer=%s""".format(suffix), (id_org,))
        res_org = cur.fetchone()
        if res_org:
            res_string += " {0} {1}".format(res_org["inn"], res_org["full_name"])
    cur.execute("""UPDATE tender{0} SET tender_kwords=%s WHERE id_tender=%s""".format(suffix),
                (res_string, id_tender))
    cur.execute("""SELECT tender_kwords FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
    res_tk = cur.fetchone()
    if res_tk and (res_tk['tender_kwords'] is None):
        print('Не смогли обновить тендер с номером ' + id_tender)
    cur.close()
    con.close()

add_tender_kwords(1)
