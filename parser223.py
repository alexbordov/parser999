import pymysql
import logging
import datetime
import operator
from functools import reduce

DB = 'tenders_test'
SUFFIX = ''
DIR_LOG = 'log_contracts223_ftp_dev'
TEMP_DIR = 'temp223'
file_log = './{1}/contracts223_{0}.log'.format(datetime.date.today(), DIR_LOG)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def connect_bd(baza):
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


def get_from_dict(data_dict, map_list):
    return reduce(operator.getitem, map_list, data_dict)


def get_el(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = ''
    if res is None:
        res = ''
    return res


class Tender223:
    def __init__(self, contr):
        self.contract = get_el(contr, "contract", "body", "item", "contractData")
        if not self.contract:
            self.contract = get_el(contr, "subcontractorInfo", "body", "item", "subcontractorInfoData")

    @property
    def contract_price(self):
        val = get_el(self.contract, "price")
        val = val if val else 0.0
        return val

    @property
    def inn_customer(self):
        val = get_el(self.contract, "customer", "mainInfo", "inn")
        return val

    @property
    def customer_regnumber(self):
        val = get_el(self.contract, "customer", "mainInfo", "regNum")
        return val


def parser(doc, path_xml):
    t = Tender223(doc)
    if not t.contract:
        logging_parser("У контракта не могу найти тег с данными", path_xml)
    customer_regnumber = t.customer_regnumber
    if customer_regnumber:
        print(customer_regnumber, path_xml)
        logging_parser(customer_regnumber, path_xml)
    contract_price = t.contract_price
    inn_customer = t.inn_customer
