import pymysql

def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT * FROM customer WHERE full_name=%s""", ('ФЕДЕРАЛЬНОЕ ГОСУДАРСТВЕННОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ "ДИРЕКЦИЯ ПРОГРАММЫ ПО РАЗВИТИЮ ФИЗИЧЕСКОЙ КУЛЬТУРЫ И СПОРТА"',))
res = cur.fetchone()
print(res)
con.close()