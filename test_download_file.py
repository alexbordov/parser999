import datetime
import random
import urllib.request

import pymysql

USER_AGENTS_FILE = 'user_agents.txt'


def connect_bd(baza):
    con = pymysql.connect(host="86.57.133.250", port=3306, user="parser", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def LoadUserAgents(uafile=USER_AGENTS_FILE):
    uas = []
    with open(uafile, 'rb') as uaf:
        for ua in uaf.readlines():
            if ua:
                uas.append(ua.strip()[1:-1 - 1])
    random.shuffle(uas)
    return uas


def main():
    dt = datetime.datetime.now()
    dt = str(dt).split(' ')[0]
    connect = connect_bd("tenders_test")
    cur = connect.cursor()
    cur.execute(
            """SELECT url, file_name FROM attachment AS att LEFT JOIN tender AS t ON att.id_tender= t.id_tender 
              WHERE t.end_date > %s AND t.cancel = 0""", (dt,))
    res_att = cur.fetchall()
    count = 0
    user_agents = LoadUserAgents()
    for r in res_att:
        try:
            ua = random.choice(user_agents)
            req1 = urllib.request.Request(url=r['url'], headers={"Connection": "close",
                                                                 'User-Agent': ua})
            f = urllib.request.urlopen(req1, timeout=40)
            file_name = f"./download/{r['file_name']}"
            with open(file_name, 'wb') as file:
                file.write(f.read())
            count += 1
            print(count)
        except Exception as e:
            print(e)


if __name__ == "__main__":
    try:
        main()
    except Exception as exm:
        print(exm)
