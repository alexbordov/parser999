#!/usr/bin/python3
import json
import bs4
import requests
import cgi
import sys
print('Content-type: text/html\n')
form = cgi.FieldStorage()
inn = str(form.getfirst("inn", 'not'))
# inn = str(3241012664)
headers = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.59 '
                        'Safari/537.36', 'Content-Type':'application/json '
           ,'Connection':'keep-alive','x-date-format': 'iso '}
payload = {"Page":1,"Count":25,"Courts":[],"DateFrom":'null',"DateTo":'null',"Sides":[{"Name":inn,"Type":-1,"ExactMatch":'false'}],"Judges":[],"CaseNumbers":[],"WithVKSInstances":'false'}
session = requests.Session()
r = session.post('http://kad.arbitr.ru/Kad/SearchInstances', data=json.dumps(payload), headers=headers, timeout=10)
yourstring = (r.content.decode('utf-8'))
page = bs4.BeautifulSoup(yourstring)
# with open('test_kod.txt', 'a') as f:
#     f.write(yourstring)
print(page.encode('ascii', 'ignore').decode('ascii'))
