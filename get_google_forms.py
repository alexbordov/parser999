import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pymysql
import logging
import os
import random
import string
import hashlib
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import datetime
import time
import requests
from connect_to_db import connect_bd

server = "smtp.gmail.com"
port = 587
user_name = "8888888"
user_passwd = "8888888"
me = '888888'
chenchikova_mail = "88888888888"
info_enter_mail = "88888888888"
count_sent = 0
file_log = './log_send_email/log_send_email_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.INFO, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def send_to_bitrix(title, message):
    TITLE = "Необходимо отправить данные для демо-доступа клиенту"
    RESPONSIBLE_ID = 46
    AUDITORS = 22
    #task_url = f'88888888888/task.item.add?arNewTaskData[TITLE]={TITLE}&arNewTaskData[DESCRIPTION]={message}&arNewTaskData[AUDITORS][]={AUDITORS}&arNewTaskData[RESPONSIBLE_ID]={RESPONSIBLE_ID}'
    task_url = f'88888888888'
    url = '8888888888888888'
    query_data = {'POST_MESSAGE': message, 'POST_TITLE': title}
    query_data_task = {"arNewTaskData[TITLE]": TITLE, "arNewTaskData[DESCRIPTION]": message, "arNewTaskData[RESPONSIBLE_ID]": RESPONSIBLE_ID, "arNewTaskData[AUDITORS][]": AUDITORS}
    r = ""
    try:
        r = requests.post(url, data=query_data)
        r = requests.post(task_url, data=query_data_task)
    except Exception as e:
        with open(file_log, 'a') as flog8:
            flog8.write('Ошибка отправки сообщения в битрикс: {0} {1}\n\n'.format(str(e), r))


def get_sheet():
    SCOPE = ["https://spreadsheets.google.com/feeds"]
    credentials = ServiceAccountCredentials.from_json_keyfile_name('APITableEdit-cd1bcc590a07.json', scopes=SCOPE)
    gc = gspread.authorize(credentials)
    # print("Доступные таблицы")
    # for sheet in gc.openall():
    #     print("{} - {}".format(sheet.title, sheet.id))
    # workbook = gc.open(SPREADSHEET)
    workbook = gc.open_by_key('1SYitbKomaAafKEf6bTPRPLrhXB1tFZuWZ5YrXVzOe2s')
    return workbook.get_worksheet(0)


def get_list_user_new():
    """

    :return: list
    """
    sheet = get_sheet()
    list_user = []
    period = datetime.timedelta(days=30)
    nextdate = datetime.date.today() + period
    data_expired = str(nextdate)
    try:
        d = sheet.get_all_records()
    except Exception as ex:
        with open(file_log, 'a') as flog8:
            flog8.write(f'Ошибка получения данных из таблицы: {str(ex)}\n\n')
        return []
    for i in d:
        email = str(i['Адрес электронной почты']).strip()
        phone = str(i['Телефон']).strip()
        username = str(i['Введите Ваши ФИО']).strip().title()
        datereg = str(i['Отметка времени']).strip()
        conv = time.strptime(datereg, "%d.%m.%Y %H:%M:%S")
        datereg = time.strftime("%Y-%m-%d %H:%M:%S", conv)

        inn = str(i['ИНН Вашей компании']).strip()
        type_of_bussines = str(i['Вид деятельности Вашей компании']).strip()
        position = str(i['Ваша должность']).strip()
        where = str(i[' Где вы узнали о нас?']).strip()
        what_is_problem = str(i['Какую проблему Вы пытаетесь решить?']).strip()
        add_dict = {'ИНН Вашей компании': inn, 'Вид деятельности Вашей компании': type_of_bussines, 'Ваша должность': position,
                    'Где вы узнали о нас?': where, 'Какую проблему Вы пытаетесь решить?': what_is_problem}
        add_info = json.dumps(add_dict, ensure_ascii=False)
        # print(username, email)
        # print('\n\n')
        list_user.append(
            dict(usr=username, mail=email, data_e=data_expired, phone=phone, datereg=datereg, add_info=add_info))
    return list_user


def get_data_user(sheet, row_num):
    if not sheet.acell('B' + str(row_num)).value:
        return 'null'
    period = datetime.timedelta(days=7)
    nextdate = datetime.date.today() + period
    data_expired = str(nextdate)
    email = sheet.acell('B' + str(row_num)).value.strip()
    phone = sheet.acell('G' + str(row_num)).value.strip()
    username = sheet.acell('C' + str(row_num)).value.strip().title()
    try:
        datereg = sheet.acell('A' + str(row_num)).value
    except gspread.exceptions.HTTPError:
        time.sleep(5)
        datereg = sheet.acell('A' + str(row_num)).value
        with open(file_log, 'a') as flog8:
            flog8.write(
                    f"Ошибка получения данных из ячейки, пробуем получить новое значение: {str(datereg)}\n\n")
    datereg.strip()
    conv = time.strptime(datereg, "%d.%m.%Y %H:%M:%S")
    datereg = time.strftime("%Y-%m-%d %H:%M:%S", conv)
    # print(username, email)
    # print('\n\n')
    return dict(usr=username, mail=email, data_e=data_expired, phone=phone, datereg=datereg)


def passwd():
    length = 10
    chars = string.ascii_letters + string.digits + '!'
    random.seed = (os.urandom(1024))
    pwd = ''.join(random.choice(chars) for i in range(length))
    return pwd


def md(pw):
    return hashlib.md5(pw.encode('utf-8')).hexdigest()


# cell_list = sheet.range('B2:C22')
# for i in cell_list:
#     print(i.value)


def get_list_user():
    list_user = []
    sheet = get_sheet()
    row_num = 2
    while True:
        try:
            s = get_data_user(sheet, row_num)
            if s == 'null':
                break
            list_user.append(s)
        except Exception:
            logging.exception('Ошибка: ')

        row_num += 1
    return list_user


def sendemails_rec(email, name, passwrd, end_dat):
    global count_sent
    period = datetime.timedelta(days=1)
    nextdate = datetime.date.today() + period
    you = chenchikova_mail
    text = """
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; mso-fareast-language: RU;"><br /> <span style="background: white;">Данные для авторизации:</span></span></p>
<table class="MsoNormalTable" style="width: 224.25pt; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;" border="0" width="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">Логин:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;"><a href="mailto:"""
    text += email
    text += """" rel="noreferrer">"""
    text += email
    text += """</a></span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">Пароль:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">"""
    text += passwrd
    text += f"""</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 2;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">ФИО:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">"""
    text += name
    text += f"""</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 3;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">Тестовый доступ действителен до:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">"""
    text += str(end_dat)
    text += f"""</span></p>
</td>
</tr>
</tbody>
</table>"""
    subj = 'Данные пользователя при восстановлении доступа'
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = you
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, you, msg.as_string())
    s.sendmail(me, info_enter_mail, msg.as_string())
    count_sent += 1
    s.quit()


def sendemails(email, name, passwrd):
    global count_sent
    period = datetime.timedelta(days=1)
    nextdate = datetime.date.today() + period
    you = chenchikova_mail
    text = """
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; mso-fareast-language: RU;"><br /> <span style="background: white;">Данные для авторизации:</span></span></p>
<table class="MsoNormalTable" style="width: 224.25pt; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;" border="0" width="0" cellpadding="0">
<tbody>
<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">Логин:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;"><a href="mailto:"""
    text += email
    text += """" rel="noreferrer">"""
    text += email
    text += """</a></span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 1;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">Пароль:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">"""
    text += passwrd
    text += f"""</span></p>
</td>
</tr>
<tr style="mso-yfti-irow: 2;">
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">ФИО:</span></p>
</td>
<td style="padding: .75pt .75pt .75pt .75pt;">
<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 10.5pt; font-family: 'Helvetica',sans-serif; mso-fareast-font-family: 'Times New Roman'; color: #535c69; background: white; mso-fareast-language: RU;">"""
    text += name
    text += f"""</span></p>
</td>
</tr>
</tbody>
</table>"""
    subj = 'Данные нового пользователя'
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = you
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, you, msg.as_string())
    s.sendmail(me, info_enter_mail, msg.as_string())
    count_sent += 1
    s.quit()


def sendemails_new(email, name, passwrd, phone=""):
    global count_sent
    period = datetime.timedelta(days=1)
    nextdate = datetime.date.today() + period
    you = email
    text = f"""<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<font color="#434343">Здравствуйте, {name}! </font>
</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<font color="#434343">Спасибо за проявленный
интерес к нашему решению. </font>
</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<font color="#434343">По Вашему запросу Вам будет
предоставлен демонстрационный доступ
к программе сроком 14 дней. </font>
</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<br/>

</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<font color="#434343">Перед использованием
программы “</font><font color="#434343"><b>Управление
тендерным отделом</b></font><font color="#434343">”:</font></p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<br/>

</p>
<ul>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">обязательно
    ознакомьтесь с информацией по
    использованию   по ссылке</font><font color="#535c69">
    </font><a href="https://enter-it.ru/oferta.docx"><font color="#0186ba"><u>Лицензионное
    соглашение</u></font></a></p>
</ul>
<p style="margin-bottom: 0cm; line-height: 115%"><br/>

</p>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">В
ближайшее время с Вами свяжется Ваш
персональный менеджер Ченчикова Ольга
и уточнит настройки доступа к нашему
сервису. </font>
</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 115%"><font color="#333333">рабочий
телефон <br/>
+7 (495) 118 43 28, доб. 101 <br/>
Skype
rummolprod_9<br/>
электронная почта
o.chenchikova@enter-it.ru <br/>
время работы: с 9-00 до
18-30 по МСК<br/>
</font><br/>

</p>
<p align="justify" style="margin-bottom: 0cm; line-height: 115%"><font color="#434343"><b>Полезные
ссылки</b></font></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">Кому
будет полезна эта программа,  узнать
</font><a href="https://enter-it.ru/1s-uto"><font color="#0186ba"><u>здесь</u></font></a><font color="#0186ba">
</font>
</p>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">Сколько
стоит и отличия тарифов </font><a href="https://enter-it.ru/1s-uto/sravnenie-versij"><font color="#0186ba"><u>здесь</u></font></a></p>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">Приобрести
</font><a href="https://enter-it.ru/1s-uto/kupit"><font color="#0186ba"><u>здесь</u></font></a></p>
<p style="margin-bottom: 0cm; line-height: 115%">Как настроить
поисковый запрос - прочитать <a href="https://enter-it.ru/item/120-kak-postroit-horoshij-poiskovyj-sapros-v-progpamme-upravlenie-tendernym-otdelom"><font color="#0186ba"><u>здесь</u></font></a>
</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<br/>

</p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<font color="#434343"><b>Мы в интернете:</b></font></p>
<ul>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#0186ba"><u><a href="https://enter-it.ru/">С</a><a href="https://enter-it.ru/">айт
    разработчика</a></u></font></p>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><a href="https://wiki.enter-it.ru/"><font color="#0186ba"><u>Онлайн-справка</u></font></a></p>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><a href="https://vk.com/enteritgroup"><font color="#0186ba"><u>Группа
    ВКонтакте</u></font></a></p>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><a href="https://twitter.com/EnterItGroup"><font color="#0186ba"><u>Twitter</u></font></a></p>
    <li/>
<p style="margin-bottom: 0cm; line-height: 115%"><a href="https://www.youtube.com/channel/UCTswp...MvJghfyuyw"><font color="#0186ba"><u>YouTube-канал</u></font></a></p>
</ul>
<p style="margin-bottom: 0cm; line-height: 106%; background: #ffffff">
<br/>

</p>
<p style="margin-bottom: 0cm; line-height: 106%; background: #ffffff">
<font color="#434343">Спасибо, что заинтересовались
нашей программой.  </font>
</p>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343">С
уважением к Вам и Вашему бизнесу!<br/>
ООО
Энтер Групп, многоканальный:</font><font color="#434343">+7
(495) 118 43 28</font><font color="#434343"><br/>
e-mail:
</font><font color="#0186ba">info@enter-it.ru,</font><font color="#333333"><br/>
</font><a href="https://enter-it.ru/"><font color="#0186ba"><u>https://enter-it.ru</u></font></a></p>
<p align="justify" style="margin-bottom: 0cm; line-height: 115%"><br/>

</p>
<p style="margin-bottom: 0cm; line-height: 115%"><font color="#434343"><font size="2" style="font-size: 10pt">================================================================================</font></font></p>
<p style="margin-bottom: 0cm; line-height: 115%; background: #ffffff">
<br/><br/>"""
    subj = 'Запрос на демонстрационный доступ к конфигурации: "1С Управление тендерным отделом"'
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = you
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, you, msg.as_string())
    s.sendmail(me, info_enter_mail, msg.as_string())
    s.sendmail(me, chenchikova_mail, msg.as_string())
    count_sent += 1
    s.quit()


def main():
    for n in get_list_user_new():
        conn = connect_bd("uto_users")
        cur = conn.cursor()
        cur.execute("""SELECT * FROM user WHERE login = %s """, (n['mail'],))
        us = cur.fetchone()
        if us:
            cur.execute("""SELECT * FROM user WHERE login=%s  AND datetime < %s""", (n['mail'], n['datereg']))
            m = cur.fetchone()
            if m:
                try:
                    password_rec = passwd()
                    hash_rec = md(password_rec)
                    end_d = m['end_date']
                    sendemails_rec(m['login'], n['usr'], password_rec, end_d)
                    sendemails_new(n['mail'], n['usr'], password_rec, n['phone'])
                    title_bitrix = f"""Запрос на демонстрационный доступ к УТО"""
                    text_bitrix = f"""Данные для входа:\nЛогин: {n['mail']}\nПароль: {password_rec}\nФИО: {n['usr']}\nТелефон: {n['phone']}\nОтправить данные клиенту можно через форму по ссылке: http://tenders.enter-it.ru:9000 (Логин: ch, Пароль: Landa)\nПосмотреть подробную информацию о пользователе можно по ссылке: http://tenders.enter-it.ru/admin_panel/?main=useredit&iduser={m['id_user']}"""
                    send_to_bitrix(title_bitrix, text_bitrix)
                    with open(file_log, 'a') as flog5:
                        flog5.write(f'Письмо восстановления доступа отправлено на адрес: {str(n["mail"])}\n\n')
                except Exception:
                    logging.exception('Ошибка отправки письма восстановления: ')
                    with open(file_log, 'a') as flog7:
                        flog7.write(f'Ошибка отправки письма восстановления на адрес: {str(n["mail"])}\n\n')
                else:
                    try:
                        cur.execute(
                                """UPDATE user SET datetime = %s, hash_password = %s,  password = %s, tel = %s  WHERE login =%s""",
                                (n['datereg'], hash_rec, password_rec, n['phone'], n['mail']))

                    except Exception:
                        # print(str(cur._last_executed))
                        logging.exception('Ошибка записи обновления в базу данных: ')
                        with open(file_log, 'a') as flog6:
                            flog6.write(f'Ошибка записи обновления в базу данных: {str(n["mail"])}\n\n')

            cur.close()
            conn.close()
            continue
        password = passwd()
        hash = md(password)
        # print(password, hash, n['mail'], n['usr'])
        try:
            sendemails(n['mail'], n['usr'], password)
            sendemails_new(n['mail'], n['usr'], password, n['phone'])
            # sendemails('info@enter-it.ru', n['usr'], password)
            with open(file_log, 'a') as flog1:
                flog1.write(f'Письмо отправлено на адрес: {str(n["mail"])}\n\n')
        except Exception:
            logging.exception('Ошибка отправки письма: ')
            with open(file_log, 'a') as flog:
                flog.write(f'Ошибка отправки письма на адрес: {str(n["mail"])}\n\n')
            cur.close()
            conn.close()
        else:
            try:
                query_add = 'INSERT INTO user SET login = %s, hash_password = %s, password = %s, end_date = %s, tel = %s, datetime=%s, user_name=%s, user_mail=%s, questionnaire=%s'
                query_value = (
                n['mail'], hash, password, n['data_e'], n['phone'], n['datereg'], n['usr'], n['mail'], n['add_info'])
                cur.execute(query_add, query_value)
                last_id = cur.lastrowid
                cur.close()
                conn.close()
                title_bitrix = f"""Запрос на демонстрационный доступ к УТО"""
                text_bitrix = f"""Данные для входа:\nЛогин: {n['mail']}\nПароль: {password}\nФИО: {n['usr']}\nТелефон: {n['phone']}\nОтправить данные клиенту можно через форму по ссылке: http://tenders.enter-it.ru:9000 (Логин: ch, Пароль: Landa)\nПосмотреть подробную информацию о пользователе можно по ссылке: http://tenders.enter-it.ru/admin_panel/?main=useredit&iduser={last_id}"""
                send_to_bitrix(title_bitrix, text_bitrix)
                with open(file_log, 'a') as flog3:
                    flog3.write(f'Данные в базу записаны по пользователю: {str(n["mail"])}\n\n')
            except Exception:
                logging.exception('Ошибка записи в базу данных: ')
                with open(file_log, 'a') as flog4:
                    flog4.write(f'Ошибка записи в базу по пользователю: {str(n["mail"])}\n\n')
                cur.close()
                conn.close()

    if count_sent == 0:
        with open(file_log, 'a') as flog2:
            flog2.write('Новых пользователей не было \n\n')


if __name__ == "__main__":
    try:
        main()
    except Exception as exm:
        with open(file_log, 'a') as flogm:
            flogm.write(f'Ошибка в функции main: {str(exm)}\n\n\n')
