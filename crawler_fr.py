from bs4 import BeautifulSoup
import requests
import requests.exceptions
from urllib.parse import urlsplit
from collections import deque
import re
import urllib.request
import pymysql
import logging
import sys

def crawler(start, end, num_log):

    file_log = './crawler_fr/log_franch_1c' + str(num_log) + '.log'
    logging.basicConfig(level=logging.DEBUG, filename=file_log,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    def parser_fr(url_pars):

        # a queue of urls to be crawled
        new_urls = deque([url_pars])

        # a set of urls that we have already crawled
        processed_urls = set()

        # a set of crawled emails
        emails = set()
        count = 0
        # process urls one by one until we exhaust the queue
        while len(new_urls):

            # move next url from the queue to the set of processed urls
            url = new_urls.popleft()
            # print(url)
            processed_urls.add(url)

            # extract base url to resolve relative links
            parts = urlsplit(url)
            # print(parts)
            base_url = "{0.scheme}://{0.netloc}".format(parts)
            # print(base_url)
            # print(url[:url.rfind('/')+1])
            path = url[:url.rfind('/') + 1] if '/' in parts.path else url

            # get url's content
            #print("Processing %s" % url)
            try:
                response = requests.get(url, timeout=200)
            except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
                # ignore pages with errors
                continue
            except Exception:
                # ignore pages with errors
                continue
            # extract all email addresses and add them into the resulting set
            new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
            emails.update(new_emails)
            if count < 1:
                # create a beutiful soup for the html document
                soup = BeautifulSoup(response.text)

                # find and process all the anchors in the document
                for anchor in soup.find_all("a"):
                    # extract link url from the anchor
                    link = anchor.attrs["href"] if "href" in anchor.attrs else ''
                    # resolve relative links
                    if link.startswith('/'):
                        link = base_url + link
                    elif not link.startswith('http'):
                        link = path + link
                        # print(link)
                    test_link = urlsplit(link)
                    if not test_link.netloc == parts.netloc:
                        continue
                    if re.match(r'.*(exe|pdf|xls|doc|png|jpg|jpeg|gif|doc|docx|xlsx)$', test_link.path):
                        continue
                    # add the new url to the queue if it was not enqueued nor processed yet
                    if not link in new_urls and not link in processed_urls:
                        new_urls.append(link)
                count += 1
            else:
                continue

        #print(emails)
        return emails

    def get_url(i):
        ref = i['href']
        # print(i.parent)
        try:
            name = i.parent.find('a', href=re.compile("franch.*")).string
            # print(name)
        except Exception:
            name = i.parent.parent.find('a', href=re.compile("franch.*")).string
            # print(name)
        return {'ref': ref, 'name': name}

    def connect():
        con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db="partners_1c", charset='utf8',
                              init_command='SET NAMES UTF8')
        con.autocommit(True)
        return con

    count_part = start
    url_part = 'http://1c.ru/rus/partners/franch-citylist.jsp?partv8='
    while True:
        if count_part > end:
            break
        try:
            #print(url_part + str(count_part))
            logging.basicConfig(level=logging.DEBUG, filename=file_log)
            req = urllib.request.Request(url=url_part + str(count_part), headers={
                'User-Agent': ' Mozilla/6.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/18.0'})
            handler = urllib.request.urlopen(req, timeout=300)
            soup = BeautifulSoup(handler.read(), 'html.parser')
            nn = soup.find_all('a', style="color: #333333;")
            for i in nn:
                try:
                    franch_d = get_url(i)
                    emailsf = parser_fr(franch_d['ref'])
                    if emailsf:
                        em = ','.join(emailsf)
                        conn = connect()
                        cur = conn.cursor(pymysql.cursors.DictCursor)
                        query = 'INSERT INTO crawler SET name = %s, email = %s'
                        value = (franch_d['name'], em)
                        cur.execute(query, value)
                        cur.close()
                        conn.close()

                except Exception as exc:
                    logging.exception('Какая-то ошибка: ' + str(exc))
                    continue
            with open(file_log, 'a') as log:
                log.write('Прошли страницу: ' + url_part + str(count_part) + '\n\n')
            count_part += 1
        except Exception as exc:
            logging.exception('Какая-то ошибка: ' + str(exc))
            count_part += 1
            continue

crawler(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
