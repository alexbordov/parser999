import datetime
import logging
from warnings import filterwarnings
import dateutil.parser
import pymysql
from functools import reduce
import operator
import re
import dateparser

filterwarnings('ignore', category=pymysql.Warning)
suffix = '_new'
name_db = 'tender'
log_dir = 'log_tenders223_dev'
file_log = './{1}/tenders223_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def get_el_list(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = []
    if res is None:
        res = []
    return res


def find_date(d):
    dt = ''
    pattern = re.compile(r'((\d{,2})\.(\d{,2})\.(\d{4}))', re.IGNORECASE)
    s = re.match(pattern, d)
    if s is not None:
        dtm = s.group()
        try:
            dt = dateparser.parse(dtm) or ''
        except Exception:
            pass
    if not dt:
        try:
            dt = dateparser.parse(d) or ''
        except Exception:
            pass
    return str(dt)


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


def get_el(d, *kwargs):
    try:
        res = get_from_dict(d, kwargs)
    except Exception:
        res = ''
    if res is None:
        res = ''
    if type(res) is str:
        res = res.strip()
    return res


def get_from_dict(data_dict, map_list):
    return reduce(operator.getitem, map_list, data_dict)


class Tender223:
    count_add_tender = 0

    def __init__(self, tender223, path_xml):
        self.path_xml = path_xml
        self.extend_scoring_date = ''
        self.extend_bidding_date = ''
        self.tender223 = None
        tender_body = None
        tender_purchase = None
        tender_root = list(tender223.keys())
        tender_child = tender223[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('body') or i.startswith('ns2:body') or i.startswith('oos:body'):
                tender_body = tender_child[i]
                break
        if tender_body is not None:
            tender_item_list = list(tender_body.keys())
            for j in tender_item_list:
                if j.startswith('item') or j.startswith('ns2:item') or j.startswith('oos:item'):
                    tender_purchase = tender_body[j]
                    break
        if tender_purchase is not None:
            tender_purchase_list = list(tender_purchase.keys())
            for m in tender_purchase_list:
                if m.startswith('purchase') or m.startswith('ns2:purchase') or m.startswith('oos:purchase'):
                    self.tender223 = tender_purchase[m]
                    break

    @property
    def id_t(self):
        id_ten = 0
        if 'ns2:guid' in self.tender223:
            id_ten = self.tender223['ns2:guid']
        elif 'oos:guid' in self.tender223:
            id_ten = self.tender223['oos:guid']
        elif 'guid' in self.tender223:
            id_ten = self.tender223['guid']
        if id_ten is None:
            id_ten = 0
        return id_ten

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'ns2:registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['ns2:registrationNumber']
        elif 'oos:registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['oos:registrationNumber']
        elif 'registrationNumber' in self.tender223:
            purchaseNumber = self.tender223['registrationNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        purchaseNumber = purchaseNumber.strip()
        return purchaseNumber

    @property
    def docPublishDate(self):
        docPublishDate = ''
        if 'ns2:publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['ns2:publicationDateTime']
        elif 'oos:publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['oos:publicationDateTime']
        elif 'publicationDateTime' in self.tender223:
            docPublishDate = self.tender223['publicationDateTime']
        if docPublishDate is None:
            docPublishDate = ''
        return docPublishDate

    @property
    def href(self):
        href = ''
        if 'ns2:urlVSRZ' in self.tender223:
            href = self.tender223['ns2:urlVSRZ']
        elif 'oos:urlVSRZ' in self.tender223:
            href = self.tender223['oos:urlVSRZ']
        elif 'urlVSRZ' in self.tender223:
            href = self.tender223['urlVSRZ']
        if href is None:
            href = ''
        return href

    @property
    def print_form(self):
        d = ''
        d = get_el(self.tender223, 'ns2:urlOOS') or get_el(self.tender223, 'oos:urlOOS') or get_el(self.tender223,
                                                                                                   'urlOOS')
        if d and d.find('CDATA') != -1:
            d = d[9:-3]
        return d

    @property
    def purchaseObjectInfo(self):
        purchaseObjectInfo = ''
        if 'ns2:name' in self.tender223:
            purchaseObjectInfo = self.tender223['ns2:name']
        elif 'oos:name' in self.tender223:
            purchaseObjectInfo = self.tender223['oos:name']
        elif 'name' in self.tender223:
            purchaseObjectInfo = self.tender223['name']
        if purchaseObjectInfo is None:
            purchaseObjectInfo = ''
        return purchaseObjectInfo

    def organizer_mainInfo(self, d):
        try:
            organizer_mainInfo = self.tender223['ns2:placer']['mainInfo'][d]
        except Exception:
            try:
                organizer_mainInfo = self.tender223['oos:placer']['mainInfo'][d]
            except Exception:
                try:
                    organizer_mainInfo = self.tender223['placer']['mainInfo'][d]
                except Exception:
                    organizer_mainInfo = ''

        if organizer_mainInfo is None:
            organizer_mainInfo = ''
        organizer_mainInfo = organizer_mainInfo.strip()
        return organizer_mainInfo

    @property
    def placingWay_code(self):
        placingWay_code = ''
        if 'ns2:purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['ns2:purchaseMethodCode']
        elif 'oos:purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['oos:purchaseMethodCode']
        elif 'purchaseMethodCode' in self.tender223:
            placingWay_code = self.tender223['purchaseMethodCode']
        if placingWay_code is None:
            placingWay_code = ''
        return placingWay_code

    @property
    def placingWay_name(self):
        placingWay_name = ''
        if 'ns2:purchaseCodeName' in self.tender223:
            placingWay_name = self.tender223['ns2:purchaseCodeName']
        elif 'oos:purchaseCodeName' in self.tender223:
            placingWay_name = self.tender223['oos:purchaseCodeName']
        elif 'purchaseCodeName' in self.tender223:
            placingWay_name = self.tender223['purchaseCodeName']
        if placingWay_name is None:
            placingWay_name = ''
        return placingWay_name

    def get_conformity(self, name):
        name_l = name.lower()
        if name_l.find('открыт') != -1:
            return 5
        elif name_l.find('аукцион') != -1:
            return 1
        elif name_l.find('котиров') != -1:
            return 2
        elif name_l.find('предложен') != -1:
            return 3
        elif name_l.find('единств') != -1:
            return 4
        else:
            return 6

    def electronicPlaceInfo(self, d):
        try:
            electronicPlaceInfo = self.tender223['ns2:electronicPlaceInfo'][d]
        except Exception:
            try:
                electronicPlaceInfo = self.tender223['oos:electronicPlaceInfo'][d]
            except Exception:
                try:
                    electronicPlaceInfo = self.tender223['electronicPlaceInfo'][d]
                except Exception:
                    electronicPlaceInfo = ''

        if electronicPlaceInfo is None:
            electronicPlaceInfo = ''
        return electronicPlaceInfo

    def get_attachments(self):
        if 'ns2:attachments' in self.tender223:
            if self.tender223['ns2:attachments'] is None:
                return []
            elif 'document' in self.tender223['ns2:attachments']:
                return generator_univ(self.tender223['ns2:attachments']['document'])
            else:
                return []
        elif 'oos:attachments' in self.tender223:
            if self.tender223['oos:attachments'] is None:
                return []
            elif 'document' in self.tender223['oos:attachments']:
                return generator_univ(self.tender223['oos:attachments']['document'])
            else:
                return []
        elif 'attachments' in self.tender223:
            if self.tender223['attachments'] is None:
                return []
            elif 'document' in self.tender223['attachments']:
                return generator_univ(self.tender223['attachments']['document'])
            else:
                return []
        else:
            return []

    def attach(self, attachment, d):
        try:
            ret_attach = attachment[d]
        except Exception:
            ret_attach = ''
        if ret_attach is None:
            ret_attach = ''
        return ret_attach

    @property
    def end_date(self):
        end_date = ''
        if 'ns2:submissionCloseDateTime' in self.tender223:
            end_date = self.tender223['ns2:submissionCloseDateTime']
        elif 'oos:submissionCloseDateTime' in self.tender223:
            end_date = self.tender223['oos:submissionCloseDateTime']
        elif 'submissionCloseDateTime' in self.tender223:
            end_date = self.tender223['submissionCloseDateTime']
        if end_date is None:
            end_date = ''
        return end_date

    @property
    def scoring_date(self):
        scoring_date = get_el(self.tender223, 'ns2:placingProcedure', 'ns2:examinationDateTime') or get_el(
                self.tender223, 'oos:placingProcedure', 'oos:examinationDateTime') or get_el(self.tender223,
                                                                                             'placingProcedure',
                                                                                             'examinationDateTime')
        if not scoring_date:
            scoring_date = get_el(self.tender223, 'ns2:applExamPeriodTime') or get_el(
                    self.tender223, 'oos:applExamPeriodTime') or get_el(self.tender223, 'applExamPeriodTime')
        if not scoring_date:
            scoring_date = get_el(self.tender223, 'ns2:examinationDateTime') or get_el(
                    self.tender223, 'oos:examinationDateTime') or get_el(self.tender223, 'examinationDateTime')
        return scoring_date

    @property
    def scoring_dateOK(self):
        scoring_date = get_el(self.tender223, 'ns2:envelopeOpeningTime') or get_el(
                self.tender223, 'oos:envelopeOpeningTime') or get_el(self.tender223, 'envelopeOpeningTime')
        return scoring_date

    @property
    def bidding_date(self):
        bidding_date = get_el(self.tender223, 'ns2:auctionTime') or get_el(self.tender223, 'oos:auctionTime') or get_el(
                self.tender223, 'auctionTime')

        return bidding_date

    @property
    def bidding_dateOK(self):
        bidding_date = get_el(self.tender223, 'ns2:examinationDateTime') or get_el(self.tender223,
                                                                                   'oos:examinationDateTime') or get_el(
                self.tender223, 'examinationDateTime')

        return bidding_date

    @property
    def dateZK(self):
        scoring_date = get_el(self.tender223, 'ns2:quotationExaminationTime', ) or get_el(
                self.tender223, 'oos:quotationExaminationTime') or get_el(self.tender223, 'quotationExaminationTime')
        return scoring_date

    def customer(self, d):
        try:
            customer_ret = self.tender223['ns2:customer']['mainInfo'][d]
        except Exception:
            try:
                customer_ret = self.tender223['oos:customer']['mainInfo'][d]
            except Exception:
                try:
                    customer_ret = self.tender223['customer']['mainInfo'][d]
                except Exception:
                    customer_ret = ''

        if customer_ret is None:
            customer_ret = ''
        customer_ret = customer_ret.strip()
        return customer_ret

    def contact(self, d):
        try:
            contact_ret = self.tender223['ns2:contact'][d]
        except Exception:
            try:
                contact_ret = self.tender223['oos:contact'][d]
            except Exception:
                try:
                    contact_ret = self.tender223['contact'][d]
                except Exception:
                    contact_ret = ''

        if contact_ret is None:
            contact_ret = ''
        return contact_ret

    def get_lots(self):
        if 'ns2:lots' in self.tender223:
            if self.tender223['ns2:lots'] is None:
                return []
            elif 'lot' in self.tender223['ns2:lots']:
                return generator_univ(self.tender223['ns2:lots']['lot'])
            else:
                return []
        elif 'ns2:lot' in self.tender223:
            if self.tender223['ns2:lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['ns2:lot'])
        elif 'oos:lots' in self.tender223:
            if self.tender223['oos:lots'] is None:
                return []
            elif 'oos:lot' in self.tender223['oos:lots']:
                return generator_univ(self.tender223['oos:lots']['oos:lot'])
            else:
                return []
        elif 'oos:lot' in self.tender223:
            if self.tender223['oos:lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['oos:lot'])
        elif 'lots' in self.tender223:
            if self.tender223['lots'] is None:
                return []
            elif 'lot' in self.tender223['lots']:
                return generator_univ(self.tender223['lots']['lot'])
            else:
                return []
        elif 'lot' in self.tender223:
            if self.tender223['lot'] is None:
                return []
            else:
                return generator_univ(self.tender223['lot'])
        else:
            return []

    def lot_max_price(self, lot):
        try:
            lot_max_price = lot['lotData']['initialSum']
        except Exception:
            lot_max_price = 0.0
        if lot_max_price is None:
            lot_max_price = 0.0
        return lot_max_price

    def lot_currency(self, lot):
        try:
            lot_currency = lot['lotData']['currency']['name']
        except Exception:
            lot_currency = ''
        if lot_currency is None:
            lot_currency = ''
        return lot_currency

    def get_lotitems(self, lot):
        if 'lotData' in lot:
            if 'lotItems' in lot['lotData']:
                if lot['lotData']['lotItems'] is None:
                    return []
                elif 'lotItem' in lot['lotData']['lotItems']:
                    return generator_univ(lot['lotData']['lotItems']['lotItem'])
                else:
                    return []
            else:
                return []
        else:
            return []

    def okpd2_code(self, item):
        try:
            okpd2_code = item['okpd2']['code']
        except Exception:
            okpd2_code = ''
        if okpd2_code is None:
            okpd2_code = ''
        return okpd2_code

    def okpd_name(self, item):
        try:
            okpd_name = item['okpd2']['name']
        except Exception:
            okpd_name = ''
        if okpd_name is None:
            okpd_name = ''
        return okpd_name

    def quantity_value(self, item):
        try:
            quantity_value = item['qty']
        except Exception:
            quantity_value = 0
        if quantity_value is None:
            quantity_value = 0
        return quantity_value

    def okei(self, item):
        try:
            okei = item['okei']['name']
        except Exception:
            okei = ''
        if okei is None:
            okei = ''
        return okei

    @property
    def date_version(self):
        date_version = get_el(self.tender223, 'ns2:modificationDate') or get_el(
                self.tender223, 'oos:modificationDate') or get_el(self.tender223, 'modificationDate')
        return date_version

    @property
    def num_version(self):
        num_version = get_el(self.tender223, 'ns2:version') or get_el(
                self.tender223, 'oos:version') or get_el(self.tender223, 'version')
        return num_version

    @property
    def notice_version(self):
        notice_version = get_el(self.tender223, 'ns2:modificationDescription') or get_el(
                self.tender223, 'oos:modificationDescription') or get_el(self.tender223, 'modificationDescription')
        return notice_version

    def add_tender_kwords(self, id_tender):
        con = connect_bd(name_db)
        cur = con.cursor(pymysql.cursors.DictCursor)
        res_string = ''
        set_po = set()
        cur.execute("""SELECT po.name, po.okpd_name FROM purchase_object{0} AS po
            LEFT JOIN lot{0} AS l ON l.id_lot = po.id_lot
            WHERE l.id_tender=%s""".format(suffix), (id_tender,))
        while True:
            row = cur.fetchone()
            if not row:
                break
            string_name = row['name']
            string_name = string_name.strip()
            string_okpd_name = row['okpd_name']
            string_okpd_name = string_okpd_name.strip()
            res = ' ' + string_name + ' ' + string_okpd_name
            set_po.add(res)
        for s in set_po:
            res_string += s

        set_att = set()
        cur.execute("""SELECT file_name FROM attachment{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        while True:
            row_1 = cur.fetchone()
            if not row_1:
                break
            sring_att = row_1['file_name']
            sring_att = sring_att.strip()
            res_a = ' ' + sring_att
            set_att.add(res_a)
        for n in set_att:
            res_string += n
        cur.execute("""SELECT purchase_object_info FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        res_po = cur.fetchone()
        if res_po:
            string_po = res_po['purchase_object_info']
            string_po = string_po.strip()
            res_string = string_po + ' ' + res_string
        cur.execute("""UPDATE tender{0} SET tender_kwords=%s WHERE id_tender=%s""".format(suffix),
                    (res_string, id_tender))
        cur.execute("""SELECT tender_kwords FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        res_tk = cur.fetchone()
        if res_tk and (res_tk['tender_kwords'] is None):
            with open(file_log, 'a') as flog:
                flog.write('Не записали tender_kwords' + ' ' + id_tender + '\n\n\n')
        cur.close()
        con.close()

    @property
    def scoring_date_rec(self):
        d = ''
        list_NEF = generator_univ(get_el_list(self.tender223, 'ns2:extendFields', 'noticeExtendField'))
        for n in list_NEF:
            list_EF = generator_univ(get_el_list(n, 'extendField'))
            for l in list_EF:
                if (get_el(l, 'description')).lower().find('дата') != -1 and (get_el(l, 'description')).lower().find(
                        'рассмотр') != -1:
                    dm = get_el(l, 'value', 'text') or get_el(l, 'value', 'dateTime')
                    # logging_parser(dm, self.path_xml)
                    d = find_date(dm)
                    # logging_parser(dm)
                    if not d:
                        self.extend_scoring_date = dm
                    return d
        return d

    @property
    def bidding_date_rec(self):
        d = ''
        list_NEF = generator_univ(get_el_list(self.tender223, 'ns2:extendFields', 'noticeExtendField'))
        for n in list_NEF:
            list_EF = generator_univ(get_el_list(n, 'extendField'))
            for l in list_EF:
                if (get_el(l, 'description')).lower().find('дата') != -1 and (get_el(l, 'description')).lower().find(
                        'подвед') != -1:
                    dm = get_el(l, 'value', 'text') or get_el(l, 'value', 'dateTime')
                    # logging_parser(dm, self.path_xml)
                    d = find_date(dm)
                    # logging_parser(dm)
                    if not d:
                        self.extend_bidding_date = dm
                    return d
        return d


def parser_type_223(doc, path_xml, filexml, reg, reg_id, purchase):
    tender = Tender223(doc, path_xml)
    if tender.tender223 is None:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег purchase в xml ' + ' ' + path_xml + '\n\n\n')
        return

    id_t = tender.id_t
    if not id_t:
        with open(file_log, 'a') as flog:
            flog.write('У тенедера нет guid ' + ' ' + path_xml + '\n\n\n')
        return
    # if id_t in ['993b4522-d35d-4989-86d8-555f568c739d', 'dcfcca5f-394f-4af1-94f5-1a7b0d436fe9',
    #             'eb59d891-342c-48de-8c9c-a50b9a65d93e']:
    #     logging_parser('не найдены даты', path_xml)
    purchaseNumber = tender.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('У тендера нет purchaseNumber ' + ' ' + path_xml + '\n\n\n')
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    con_t = connect_bd(name_db)
    cur_t = con_t.cursor(pymysql.cursors.DictCursor)
    cur_t.execute(
            """SELECT id_tender FROM tender{0} WHERE id_xml=%s AND id_region=%s AND purchase_number=%s""".format(
                    suffix),
            (id_t, reg_id, purchaseNumber))
    res_id_tender = cur_t.fetchone()
    if res_id_tender:
        # with open(file_log, 'a') as flog:
        #     flog.write('Такой тендер уже есть в базе ' + ' ' + path_xml + '\n\n\n')
        cur_t.close()
        con_t.close()
        return
    # cur_t.execute("""SELECT id_tender FROM tender{0} WHERE purchase_number=%s""".format(suffix), (purchaseNumber,))
    # res_pur_n = cur_t.fetchone()
    # if res_pur_n:
    #     cur_t.execute("""UPDATE tender{0} SET cancel = 1 WHERE purchase_number=%s""".format(suffix), (purchaseNumber,))
    docPublishDate = tender.docPublishDate
    cancel_status = 0
    if docPublishDate:
        cur_t.execute(
                """SELECT id_tender, doc_publish_date FROM tender{0} WHERE id_region = %s AND purchase_number=%s""".format(
                        suffix), (reg_id, purchaseNumber))
        res_pur_n = cur_t.fetchall()
        if res_pur_n:
            for pur in res_pur_n:
                date_new = docPublishDate[:19]
                date_new = dateutil.parser.parse(date_new)
                date_old = datetime.datetime.strptime(str(pur['doc_publish_date']), "%Y-%m-%d %H:%M:%S")
                if date_new >= date_old:
                    cur_t.execute("""UPDATE tender{0} SET cancel = 1 WHERE id_tender=%s""".format(suffix),
                                  (pur['id_tender'],))
                else:
                    cancel_status = 1
    href = tender.href
    purchaseObjectInfo = tender.purchaseObjectInfo
    date_version = tender.date_version
    num_version = tender.num_version
    notice_version = tender.notice_version
    print_form = tender.print_form
    query_add_tender = """INSERT INTO tender{0} SET id_region=%s, id_xml=%s, purchase_number=%s, doc_publish_date=%s,
                          href=%s, purchase_object_info=%s, type_fz=%s, cancel=%s, date_version=%s, 
                          num_version=%s, notice_version=%s, xml=%s, print_form=%s""".format(suffix)
    value_add_tender = (reg_id, id_t, purchaseNumber, docPublishDate, href, purchaseObjectInfo, 223, cancel_status,
                        date_version, num_version, notice_version, xml, print_form)
    try:
        cur_t.execute(query_add_tender, value_add_tender)
        Tender223.count_add_tender += 1
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи в базу данных тендера:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_t.execute(
            """SELECT id_tender FROM tender{0} WHERE id_xml = %s AND id_region = %s AND purchase_number=%s""".format(
                    suffix),
            (id_t, reg_id, purchaseNumber))
    res_id_tender_add = cur_t.fetchone()
    id_tender = res_id_tender_add['id_tender']
    organizer_full_name = tender.organizer_mainInfo('fullName')
    organizer_post_address = tender.organizer_mainInfo('postalAddress')
    organizer_fact_address = tender.organizer_mainInfo('legalAddress')
    organizer_inn = tender.organizer_mainInfo('inn')
    organizer_kpp = tender.organizer_mainInfo('kpp')
    organizer_email = tender.organizer_mainInfo('email')
    organizer_phone = tender.organizer_mainInfo('phone')
    organizer_fax = tender.organizer_mainInfo('fax')
    id_organizer = 0
    if organizer_inn:
        cur_t.execute("""SELECT id_organizer FROM organizer{0} WHERE inn=%s AND kpp=%s""".format(suffix),
                      (organizer_inn, organizer_kpp))
        res_id_org = cur_t.fetchone()
        if res_id_org:
            id_organizer = res_id_org['id_organizer']
        else:
            query_add_organizer = """INSERT INTO organizer{0} SET full_name=%s, post_address=%s, fact_address=%s,
                                  inn=%s, kpp=%s, contact_email=%s, contact_phone=%s, contact_fax=%s""".format(suffix)
            query_value_organizer = (organizer_full_name, organizer_post_address, organizer_fact_address,
                                     organizer_inn, organizer_kpp, organizer_email, organizer_phone, organizer_fax)
            try:
                cur_t.execute(query_add_organizer, query_value_organizer)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных organizer:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_t.execute("""SELECT id_organizer FROM organizer{0} WHERE inn=%s AND kpp=%s""".format(suffix),
                          (organizer_inn, organizer_kpp))
            res_id_organizer = cur_t.fetchone()
            id_organizer = res_id_organizer['id_organizer']
    else:
        with open(file_log, 'a') as flog:
            flog.write('Нет organizer_inn' + ' ' + path_xml + '\n\n\n')
    id_placing_way = 0
    placingWay_code = tender.placingWay_code
    placingWay_name = tender.placingWay_name
    conformity = tender.get_conformity(placingWay_name)
    if placingWay_code:
        cur_t.execute("""SELECT id_placing_way FROM placing_way{0} WHERE code=%s""".format(suffix), (placingWay_code,))
        res_id_placing_way = cur_t.fetchone()
        if res_id_placing_way:
            id_placing_way = res_id_placing_way['id_placing_way']
        else:
            try:
                cur_t.execute("""INSERT INTO placing_way{0} SET code=%s, name=%s, conformity=%s""".format(suffix),
                              (placingWay_code, placingWay_name, conformity))
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных placing_way:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_t.execute("""SELECT id_placing_way FROM placing_way{0} WHERE code=%s""".format(suffix),
                          (placingWay_code,))
            res_add_id_placing_way = cur_t.fetchone()
            id_placing_way = res_add_id_placing_way['id_placing_way']
    ETP_code = tender.electronicPlaceInfo('electronicPlaceId')
    ETP_name = tender.electronicPlaceInfo('name')
    ETP_url = tender.electronicPlaceInfo('url')
    id_etp = 0
    if ETP_code:
        cur_t.execute("""SELECT id_etp FROM etp{0} WHERE code=%s""".format(suffix), (ETP_code,))
        res_ETP_code = cur_t.fetchone()
        if res_ETP_code:
            id_etp = res_ETP_code['id_etp']
        else:
            try:
                cur_t.execute("""INSERT INTO etp{0} SET code=%s, name=%s, url=%s""".format(suffix),
                              (ETP_code, ETP_name, ETP_url))
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных ETP:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_t.execute("""SELECT id_etp FROM etp{0} WHERE code=%s""".format(suffix), (ETP_code,))
            res_add_ETP_code = cur_t.fetchone()
            if res_add_ETP_code:
                id_etp = res_add_ETP_code['id_etp']
    attachments = tender.get_attachments()
    for attachment in attachments:
        attach_name = tender.attach(attachment, 'fileName')
        attach_description = tender.attach(attachment, 'description')
        attach_url = tender.attach(attachment, 'url')
        query_add_attachment = """INSERT INTO attachment{0} SET id_tender=%s, file_name=%s, url=%s,
                                description=%s""".format(suffix)
        query_value_attachment = (id_tender, attach_name, attach_url, attach_description)
        try:
            cur_t.execute(query_add_attachment, query_value_attachment)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка записи в базу данных attachment:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    end_date = tender.end_date
    scoring_date = tender.scoring_date
    bidding_date = tender.bidding_date
    if not scoring_date:
        scoring_date = tender.scoring_date_rec
    if not bidding_date:
        bidding_date = tender.bidding_date_rec
    if purchase == 'purchaseNotice':
        bidding_date = bidding_date or scoring_date
    if purchase == 'purchaseNoticeOK':
        scoring_date = tender.scoring_dateOK
        bidding_date = tender.bidding_dateOK
    if purchase == 'purchaseNoticeZK':
        scoring_date = bidding_date = tender.dateZK
    extend_scoring_date = tender.extend_scoring_date
    extend_bidding_date = tender.extend_bidding_date
    try:
        cur_t.execute("""UPDATE tender{0} SET id_organizer=%s, id_placing_way=%s, id_etp=%s,
                      end_date=%s, scoring_date = %s, 	bidding_date = %s, extend_scoring_date = %s, 
                      extend_bidding_date = %s WHERE id_tender=%s""".format(suffix),
                      (id_organizer, id_placing_way, id_etp, end_date, scoring_date, bidding_date,
                       extend_scoring_date, extend_bidding_date, id_tender))
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка обновления tender в базе данных:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    customer_inn = tender.customer('inn')
    customer_full_name = tender.customer('fullName')
    customer_kpp = tender.customer('kpp')
    customer_ogrn = tender.customer('ogrn')
    customer_post_address = tender.customer('postalAddress')
    customer_phone = tender.customer('phone')
    customer_fax = tender.customer('fax')
    customer_email = tender.customer('email')
    cus_ln = tender.contact('lastName')
    cus_fn = tender.contact('firstName')
    cus_mn = tender.contact('middleName')
    cus_contact = cus_ln + ' ' + cus_fn + ' ' + cus_mn
    id_customer = 0
    if customer_inn:
        cur_t.execute("""SELECT regNumber FROM od_customer WHERE inn=%s AND kpp=%s""", (customer_inn, customer_kpp))
        res_customer_regNumber = cur_t.fetchone()
        if not res_customer_regNumber:
            cur_t.execute("""SELECT regNumber FROM od_customer_from_ftp WHERE inn=%s AND kpp=%s""",
                          (customer_inn, customer_kpp))
            res_customer_regNumber = cur_t.fetchone()
        if not res_customer_regNumber:
            cur_t.execute("""SELECT regNumber FROM od_customer_from_ftp223 WHERE inn=%s AND kpp=%s""",
                          (customer_inn, customer_kpp))
            res_customer_regNumber = cur_t.fetchone()
        if res_customer_regNumber:
            regNum_cust = res_customer_regNumber['regNumber']
            if regNum_cust:
                cur_t.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix), (regNum_cust,))
                res_id_customer = cur_t.fetchone()
                if res_id_customer:
                    id_customer = res_id_customer['id_customer']
                else:
                    try:
                        cur_t.execute("""INSERT INTO customer{0} SET reg_num=%s, full_name=%s, inn=%s,
                                      is223=1""".format(suffix), (regNum_cust, customer_full_name, customer_inn))
                    except Exception as e:
                        with open(file_log, 'a') as flog:
                            flog.write(
                                    'Ошибка добавления customer в базу данных:' + ' ' + path_xml + ' ' + str(
                                            e) + '\n\n\n')
                    cur_t.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix),
                                  (regNum_cust,))
                    res_add_customer = cur_t.fetchone()
                    if res_add_customer:
                        id_customer = res_add_customer['id_customer']
        else:
            cur_t.execute("""SELECT id_customer FROM customer{0} WHERE inn=%s""".format(suffix), (customer_inn,))
            res_id_customer_el = cur_t.fetchone()
            if res_id_customer_el:
                id_customer = res_id_customer_el['id_customer']
            else:
                reg_num223 = '00000223' + customer_inn
                try:
                    cur_t.execute("""INSERT INTO customer{0} SET inn=%s, full_name=%s, reg_num=%s,
                                  is223=1""".format(suffix), (customer_inn, customer_full_name, reg_num223))
                except Exception as e:
                    with open(file_log, 'a') as flog:
                        flog.write(
                                'Ошибка добавления customer в базу данных:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
                cur_t.execute("""SELECT id_customer FROM customer{0} WHERE inn=%s""".format(suffix), (customer_inn,))
                res_add_id_customer_el = cur_t.fetchone()
                if res_add_id_customer_el:
                    id_customer = res_add_id_customer_el['id_customer']
                query_add_customer223 = """INSERT INTO customer223{0} SET inn=%s, full_name=%s, contact=%s, kpp=%s,
                                        ogrn=%s, post_address=%s, phone=%s, fax=%s, email=%s""".format(suffix)
                value_add_customer223 = (customer_inn, customer_full_name, cus_contact, customer_kpp, customer_ogrn,
                                         customer_post_address, customer_phone, customer_fax, customer_email,
                                         )
                try:
                    cur_t.execute(query_add_customer223, value_add_customer223)
                except Exception as e:
                    with open(file_log, 'a') as flog:
                        flog.write('Ошибка добавления customer223 в базу данных:' + ' ' + path_xml + ' ' + str(e) +
                                   '\n\n\n')
    else:
        with open(file_log, 'a') as flog:
            flog.write('У customer нет inn:' + ' ' + path_xml + ' ' + '\n\n\n')
    lotNumber = 1
    lots = tender.get_lots()
    for lot in lots:
        lot_max_price = tender.lot_max_price(lot)
        lot_currency = tender.lot_currency(lot)
        try:
            cur_t.execute("""INSERT INTO lot{0} SET id_tender=%s, lot_number=%s, max_price=%s,
                          currency=%s""".format(suffix), (id_tender, lotNumber, lot_max_price, lot_currency))
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка добавления lot в базу данных:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        cur_t.execute(
                """SELECT id_lot FROM lot{0} WHERE id_tender=%s AND lot_number=%s AND max_price=%s""".format(suffix),
                (id_tender, lotNumber, lot_max_price))
        res_id_lot = cur_t.fetchone()
        id_lot = res_id_lot['id_lot']
        lotNumber += 1
        lotitems = tender.get_lotitems(lot)
        for lotitem in lotitems:
            okpd2_code = tender.okpd2_code(lotitem)
            okpd_name = name = tender.okpd_name(lotitem)
            quantity_value = tender.quantity_value(lotitem)
            okei = tender.okei(lotitem)
            okpd2_group_code = ''
            okpd2_group_level1_code = ''
            if okpd2_code:
                if len(okpd2_code) > 1:
                    dot1 = okpd2_code.find('.')
                    if dot1 == -1:
                        dot1 = None
                    okpd2_group_code_temp = okpd2_code[:dot1]
                    okpd2_group_code = okpd2_group_code_temp[:2]
                    if len(okpd2_code) > 3:
                        if dot1:
                            okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
            query_add_item = """INSERT INTO purchase_object{0} SET id_lot=%s, id_customer=%s, okpd2_code=%s,
                              okpd2_group_code=%s, okpd2_group_level1_code=%s, okpd_name=%s, name=%s,
                              quantity_value=%s, okei=%s, customer_quantity_value=%s""".format(suffix)
            query_value_item = (id_lot, id_customer, okpd2_code, okpd2_group_code, okpd2_group_level1_code, okpd_name,
                                name, quantity_value, okei, quantity_value)
            try:
                cur_t.execute(query_add_item, query_value_item)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка добавления lotitem в базу данных:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_t.close()
    con_t.close()
    try:
        tender.add_tender_kwords(id_tender)
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи tender_kwords: ' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')


def parser(doc, path_xml, filexml, reg, reg_id, purchase):
    global file_log
    try:
        parser_type_223(doc, path_xml, filexml, reg, reg_id, purchase)
    except Exception:
        logging.exception("Ошибка при парсинге тендера типа 223: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка при парсинге тендера типа 223 ' + ' ' + path_xml + ' ' + '\n\n\n')
