import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)
cur2 = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT * FROM old_od_customer""")
while True:
    res_cus = cur.fetchone()
    if res_cus is None:
        break
    cur2.execute("""SELECT id FROM od_customer WHERE regNumber=%s""", (res_cus['regNumber']))
    res_cus_2 = cur2.fetchone()
    if not res_cus_2:
        print(res_cus['id'])
        query_add_customer = """INSERT INTO od_customer SET regNumber = %s, inn = %s, kpp = %s, contracts_count =
                    %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s,
                    full_name = %s, postal_address = %s, phone = %s, fax = %s, email = %s, contact_name = %s """
        value_add_customer = (res_cus['regNumber'], res_cus['inn'], res_cus['kpp'], res_cus['contracts_count'],
                              res_cus['contracts223_count'], res_cus['contracts_sum'],
                              res_cus['contracts223_sum'], res_cus['ogrn'], res_cus['region_code'],
                              res_cus['full_name'], res_cus['postal_address'], res_cus['phone'], res_cus['fax'],
                              res_cus['email'], res_cus['contact_name'])
        cur2.execute(query_add_customer, value_add_customer)

cur.close()
cur2.close()
con.close()
