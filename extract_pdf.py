import encodings.utf_8
import textract

text = textract.process("11111.doc", encoding='utf_8', method='tesseract', language='rus')

text = text.decode('utf-8')
with open('ocr_pdf.txt', 'a') as f:
    f.write(text)
