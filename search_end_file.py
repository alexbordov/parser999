import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT count(*) FROM attachment""")
attach = set()
count_res = cur.fetchone()
count = int(count_res['count(*)'])
i = 1
while i <= count:
    cur.execute("""SELECT file_name FROM attachment WHERE id_attachment=%s""", (i,))
    res = cur.fetchone()
    try:
        s = res['file_name']
        ind = s.rindex('.')
        s = s[ind:]
        attach.add(s)
    except Exception as e:
        print(res['file_name  '] + str(e))
    i += 1
cur.close()
con.close()
with open('attach.txt', 'a') as f:
    for i in attach:
        f.write(i + '\n')

