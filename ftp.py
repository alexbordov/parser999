# coding=utf-8
import ftplib


def connect():
    """
    соединение с ftp
    :return:
    """
    host = 'ftp.apple-fon.nichost.ru'
    ftpuser = 'apple-fon_ftp'
    password = 'SkBXp8c/'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(2)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd('apple-fon.ru/docs/images/stories/demo')
    return ftp2


def download(f, ftp1):
    """
    скачивание файла
    :param ftp1:
    :param f:
    :return:
    """
    local_f = './server/' + str(f)
    lf = open(local_f, 'wb')
    a = ftp1.retrbinary('RETR ' + str(f), lf.write)
    lf.close()
    mass = a.split(' ')
    if mass[0] == '226':
        return local_f
    else:
        return 0


def get_ftp_file_list(ftp1):
    """
    получение списка файлов
    :return:
    """
    data = ftp1.nlst()
    data = data[2:]
    return data

ftp = connect()
for i in get_ftp_file_list(ftp):
    file = open('./server/dir.txt', 'w')
    file.write(str(i))
    file.write('\n')
    n = download(i, ftp)
    if n:
        print(n)
    else:
        print('не скачали')
    file.close()
ftp.quit()
