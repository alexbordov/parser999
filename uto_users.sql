-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Апр 09 2018 г., 13:50
-- Версия сервера: 10.2.6-MariaDB-log
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `uto_users`
--

-- --------------------------------------------------------

--
-- Структура таблицы `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `query` mediumtext NOT NULL,
  `filter_struct` mediumtext NOT NULL,
  `user` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `user_2` (`user`(255)),
  KEY `date` (`date`),
  KEY `query` (`query`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45337 ;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_history`
--

CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date` date NOT NULL,
  `balance_motion` decimal(10,0) NOT NULL,
  `info` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Структура таблицы `rate_type`
--

CREATE TABLE IF NOT EXISTS `rate_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `info` varchar(500) NOT NULL,
  `count_month` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Структура таблицы `update_notice`
--

CREATE TABLE IF NOT EXISTS `update_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL,
  `notice` varchar(1000) NOT NULL,
  `version_date` varchar(50) NOT NULL,
  `images` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=182 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `hash_password` varchar(500) NOT NULL,
  `password` varchar(255) NOT NULL,
  `end_date` date NOT NULL,
  `tel` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `id_rate_type` int(11) NOT NULL,
  `user_name` varchar(500) NOT NULL,
  `user_mail` varchar(200) NOT NULL,
  `questionnaire` varchar(1024) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `login` (`login`),
  KEY `datetime` (`datetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=668 ;

-- --------------------------------------------------------

--
-- Структура таблицы `UTO_UsersActivity`
--

CREATE TABLE IF NOT EXISTS `UTO_UsersActivity` (
  `ACT_Id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор события',
  `ACT_DateTime` datetime DEFAULT NULL COMMENT 'Время события',
  `ACT_WorkStationId` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Идентификатор рабочей станции пользователя',
  `ACT_UserId` int(11) DEFAULT NULL COMMENT 'Идентификатор пользователи УТО',
  `ACT_Method` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Вызываемый метод API',
  `ACT_MethodDescription` varchar(128) NOT NULL DEFAULT '0' COMMENT 'Описание вызываемого метода API',
  `ACT_Parametries` varchar(4096) NOT NULL DEFAULT '0' COMMENT 'Параметры отправляемые пользователем в метод',
  `ACT_MethodQuery` varchar(4096) DEFAULT NULL COMMENT 'Запрос выполняемы пользователем',
  `ACT_CompletedWithErrors` int(1) DEFAULT NULL COMMENT '1 если доступ был запрещен и 0 если все прошло удачно',
  PRIMARY KEY (`ACT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15154 ;

-- --------------------------------------------------------

--
-- Структура таблицы `UTO_UsersWorkStations`
--

CREATE TABLE IF NOT EXISTS `UTO_UsersWorkStations` (
  `WS_IdWorkStation` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Идентификатор рабочей станции',
  `WS_OsVersion` varchar(50) DEFAULT '0' COMMENT 'Версия операционной системы',
  `WS_1cVersion` varchar(50) DEFAULT '0' COMMENT 'Версия платформы 1С:Предприятия',
  `WS_SoftInfo` varchar(50) DEFAULT '0' COMMENT 'Дополнительная информация о клментском приложении',
  `WS_Ram` varchar(50) DEFAULT '0' COMMENT 'Объем оперативной памяти',
  `WS_Cpu` varchar(50) DEFAULT '0' COMMENT 'Описание процессора',
  PRIMARY KEY (`WS_IdWorkStation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Эта таблица с описанием клиентских ПК и серверов. ';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
