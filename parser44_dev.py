import datetime
import logging
import pymysql
from warnings import filterwarnings

filterwarnings('ignore', category=pymysql.Warning)
suffix = '_new'
name_db = 'tender'
log_dir = 'log_contracts44_ftp_dev'
file_log = './{1}/contracts44_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def add_product(pr, id_contract):
    name = ''
    okpd2_code = ''
    okpd2_group_code = 0
    okpd2_group_level1_code = ''
    okpd_code = ''
    okpd_group_code = 0
    okpd_group_level1_code = ''
    price = 0.0
    okpd2_name = ''
    okpd_name = ''
    quantity = 0.0
    sum_p = 0.0
    sid = ''
    okei = ''
    if 'name' in pr:
        name = pr['name']
    if name is None:
        name = 'Нет названия'
    if 'OKPD2' in pr:
        if 'code' in pr['OKPD2']:
            okpd2_code = str(pr['OKPD2']['code'])
            if len(okpd2_code) > 1:
                dot1 = okpd2_code.find('.')
                if dot1 == -1:
                    dot1 = None
                okpd2_group_code_temp = okpd2_code[:dot1]
                okpd2_group_code = okpd2_group_code_temp[:2]
                if len(okpd2_code) > 3:
                    if dot1:
                        okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
                if len(okpd2_code) > 4:
                    if dot1:
                        okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
        if 'name' in pr['OKPD2']:
            okpd2_name = str(pr['OKPD2']['name'])
    if 'OKPD' in pr:
        if 'code' in pr['OKPD']:
            okpd_code = str(pr['OKPD']['code'])
            if len(okpd_code) > 1:
                dot1 = okpd_code.find('.')
                if dot1 == -1:
                    dot1 = None
                okpd_group_code_temp = okpd_code[:dot1]
                okpd_group_code = okpd_group_code_temp[:2]
                if len(okpd_code) > 3:
                    if dot1:
                        okpd_group_level1_code = okpd_code[dot1 + 1:dot1 + 2]
                if len(okpd_code) > 4:
                    if dot1:
                        okpd_group_level1_code = okpd_code[dot1 + 1:dot1 + 2]
        if 'name' in pr['OKPD']:
            okpd_name = str(pr['OKPD']['name'])
    if 'price' in pr:
        price = float(pr['price'])
    if 'quantity' in pr:
        quantity = float(pr['quantity'])
    if 'sum' in pr:
        sum_p = float(pr['sum'])
    if 'sid' in pr:
        sid = str(pr['sid'])
    if 'OKEI' in pr:
        if 'nationalCode' in pr['OKEI']:
            okei = str(pr['OKEI']['nationalCode'])

    con_p = connect_bd(name_db)
    cur_p = con_p.cursor(pymysql.cursors.DictCursor)
    query_add_products = """INSERT INTO od_contract_product{0} SET  id_od_contract = %s, name = %s, okpd2_code = %s,
    okpd_code = %s, okpd2_group_code = %s, okpd_group_code = %s, okpd2_group_level1_code = %s,
    okpd_group_level1_code = %s, price = %s, okpd2_name = %s, okpd_name = %s, quantity = %s, okei = %s,
    sum = %s, sid = %s """.format(suffix)
    value_add_products = (
        id_contract, name, okpd2_code, okpd_code, okpd2_group_code, okpd_group_code,
        okpd2_group_level1_code, okpd_group_level1_code, price, okpd2_name, okpd_name, quantity, okei, sum_p, sid)
    cur_p.execute(query_add_products, value_add_products)
    Contract.count_prod += 1
    cur_p.close()
    con_p.close()


class Contract:
    count_ins = 0
    count_upd = 0
    count_sup = 0
    count_cus = 0
    count_prod = 0

    def __init__(self, contr):
        self.contract = contr

    def get_id(self):
        try:
            idcontract = self.contract['ns2:export']['ns2:contract']['id']
        except Exception:
            idcontract = ''
        return idcontract

    def get_p_number(self):
        try:
            p_number = self.contract['ns2:export']['ns2:contract']['id']
        except Exception:
            p_number = ''
        return p_number

    def get_regnum(self):
        try:
            regnum = self.contract['ns2:export']['ns2:contract']['regNum']
        except Exception:
            regnum = ''
        return regnum

    def get_current_contract_stage(self):
        try:
            current_contract_stage = self.contract['ns2:export']['ns2:contract']['currentContractStage']
        except Exception:
            current_contract_stage = ''
        return current_contract_stage

    def get_placing(self):
        try:
            placing = self.contract['ns2:export']['ns2:contract']['foundation']['fcsOrder']['order']['placing']
        except Exception:
            placing = ''
        if not placing:
            try:
                placing = self.contract['ns2:export']['ns2:contract']['foundation']['oosOrder']['order']['placing']
            except Exception:
                placing = ''
        return placing

    def get_url(self):
        try:
            url = self.contract['ns2:export']['ns2:contract']['href']
        except Exception:
            url = ''
        return url

    def get_sign_date(self):
        try:
            sign_date = self.contract['ns2:export']['ns2:contract']['signDate']
        except Exception:
            sign_date = '0000-00-00'
        return sign_date

    def get_single_customer_reason_code(self):
        try:
            single_customer_reason_code = \
                self.contract['ns2:export']['ns2:contract']['foundation']['fcsOrder']['order']['singleCustomer'][
                    'reason'][
                    'code']
        except Exception:
            single_customer_reason_code = ''
        if not single_customer_reason_code:
            try:
                single_customer_reason_code = \
                    self.contract['ns2:export']['ns2:contract']['foundation']['oosOrder']['order']['singleCustomer'][
                        'reason'][
                        'code']
            except Exception:
                single_customer_reason_code = ''
        return single_customer_reason_code

    def get_single_customer_reason_name(self):
        try:
            single_customer_reason_name = \
                self.contract['ns2:export']['ns2:contract']['foundation']['fcsOrder']['order']['singleCustomer'][
                    'reason'][
                    'name']
        except Exception:
            single_customer_reason_name = ''
        if not single_customer_reason_name:
            try:
                single_customer_reason_name = \
                    self.contract['ns2:export']['ns2:contract']['foundation']['oosOrder']['order']['singleCustomer'][
                        'reason'][
                        'name']
            except Exception:
                single_customer_reason_name = ''
        return single_customer_reason_name

    def get_fz(self):
        return '44'

    def get_notification_number(self):
        try:
            notification_number = \
                self.contract['ns2:export']['ns2:contract']['foundation']['fcsOrder']['order']['notificationNumber']
            if notification_number is None:
                notification_number = ''
        except Exception:
            notification_number = ''
        if not notification_number:
            try:
                notification_number = \
                    self.contract['ns2:export']['ns2:contract']['foundation']['oosOrder']['order']['notificationNumber']
                if notification_number is None:
                    notification_number = ''
            except Exception:
                notification_number = ''
        notification_number = (lambda x: x if x else 'Нет номера')(notification_number)
        return notification_number

    def get_lot_number(self):
        try:
            lot_number = self.contract['ns2:export']['ns2:contract']['foundation']['fcsOrder']['order']['lotNumber']
        except Exception:
            lot_number = ''
        if not lot_number:
            try:
                lot_number = self.contract['ns2:export']['ns2:contract']['foundation']['oosOrder']['order']['lotNumber']
            except Exception:
                lot_number = ''
        lot_number = (lambda x: x if x else 1)(lot_number)
        return lot_number

    def get_contract_price(self):
        try:
            contract_price = self.contract['ns2:export']['ns2:contract']['priceInfo']['price']
        except Exception:
            contract_price = 0.0
        return contract_price

    def get_currency(self):
        try:
            currency = self.contract['ns2:export']['ns2:contract']['priceInfo']['currency']['name']
        except Exception:
            currency = ''
        return currency

    def get_version_number(self):
        try:
            version_number = self.contract['ns2:export']['ns2:contract']['versionNumber']
        except Exception:
            version_number = 0
        return version_number

    def get_execution_start_date(self):
        try:
            execution_start_date = self.contract['ns2:export']['ns2:contract']['executionPeriod']['startDate']
        except Exception:
            execution_start_date = '0000-00-00'
        return execution_start_date

    def get_execution_end_date(self):
        try:
            execution_end_date = self.contract['ns2:export']['ns2:contract']['executionPeriod']['endDate']
        except Exception:
            execution_end_date = '0000-00-00'
        return execution_end_date

    def get_customer_regnumber(self):
        try:
            customer_regnumber = self.contract['ns2:export']['ns2:contract']['customer']['regNum']
        except Exception:
            customer_regnumber = ''
        return customer_regnumber

    def get_supplier_inn(self):
        try:
            supplier_inn = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityRF']['INN']
        except Exception:
            try:
                supplier_inn = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityRF'][
                    'INN']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old(self):
        try:
            supplier_inn = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                'INN']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                        'INN']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old0(self):
        try:
            supplier_inn = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityForeignState'][
                    'INN']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityForeignState'][
                        'INN']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old1(self):
        try:
            supplier_inn = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityForeignState'][
                    'taxPayerCode']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityForeignState'][
                        'taxPayerCode']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old2(self):
        try:
            supplier_inn = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'registerInRFTaxBodies']['INN']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState'][
                        'registerInRFTaxBodies']['INN']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old3(self):
        try:
            supplier_inn = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'taxPayerCode']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState'][
                        'taxPayerCode']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_supplier_inn_old4(self):
        try:
            supplier_inn = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'INN']
        except Exception:
            try:
                supplier_inn = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState'][
                        'INN']
            except Exception:
                supplier_inn = ''
        return supplier_inn

    def get_kpp_customer(self):
        try:
            kpp_customer = self.contract['ns2:export']['ns2:contract']['customer']['kpp']
        except Exception:
            kpp_customer = ''
        return kpp_customer

    def get_inn_customer(self):
        try:
            inn_customer = self.contract['ns2:export']['ns2:contract']['customer']['inn']
        except Exception:
            inn_customer = ''
        return inn_customer

    def get_full_name_customer(self):
        try:
            full_name_customer = self.contract['ns2:export']['ns2:contract']['customer']['fullName']
        except Exception:
            full_name_customer = ''
        return full_name_customer

    def get_kpp_supplier(self):
        try:
            kpp_supplier = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityRF']['KPP']
        except Exception:
            try:
                kpp_supplier = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityRF'][
                    'KPP']
            except Exception:
                kpp_supplier = ''
        return kpp_supplier

    def get_kpp_supplier_old(self):
        try:
            kpp_supplier = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                'KPP']
        except Exception:
            try:
                kpp_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                        'KPP']
            except Exception:
                kpp_supplier = ''
        return kpp_supplier

    def get_kpp_supplier_old1(self):
        try:
            kpp_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'registerInRFTaxBodies']['KPP']
        except Exception:
            try:
                kpp_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState']['registerInRFTaxBodies']['KPP']
            except Exception:
                kpp_supplier = ''
        return kpp_supplier

    def get_contactphone_supplier(self):
        try:
            contactphone_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityRF']['contactPhone']
        except Exception:
            try:
                contactphone_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityRF'][
                        'contactPhone']
            except Exception:
                contactphone_supplier = ''
        return contactphone_supplier

    def get_contactphone_supplier_old(self):
        try:
            contactphone_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                    'contactPhone']
        except Exception:
            try:
                contactphone_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                        'contactPhone']
            except Exception:
                contactphone_supplier = ''
        return contactphone_supplier

    def get_contactphone_supplier_old1(self):
        try:
            contactphone_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityForeignState'][
                    'placeOfStayInRegCountry']['contactPhone']
        except Exception:
            try:
                contactphone_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityForeignState'][
                        'placeOfStayInRegCountry'][
                        'contactPhone']
            except Exception:
                contactphone_supplier = ''
        return contactphone_supplier

    def get_contactphone_supplier_old2(self):
        try:
            contactphone_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'placeOfStayInRegCountry']['contactPhone']
        except Exception:
            try:
                contactphone_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState'][
                        'placeOfStayInRegCountry'][
                        'contactPhone']
            except Exception:
                contactphone_supplier = ''
        return contactphone_supplier

    def get_contactemail_supplier(self):
        try:
            contactemail_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityRF']['contactEMail']
        except Exception:
            try:
                contactemail_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityRF'][
                        'contactEMail']
            except Exception:
                contactemail_supplier = ''
        return contactemail_supplier

    def get_contactemail_supplier_old(self):
        try:
            contactemail_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                    'contactEMail']
        except Exception:
            try:
                contactemail_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                        'contactEMail']
            except Exception:
                contactemail_supplier = ''
        return contactemail_supplier

    def get_contactemail_supplier_old1(self):
        try:
            contactemail_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityForeignState'][
                    'placeOfStayInRegCountry']['contactEMail']
        except Exception:
            try:
                contactemail_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityForeignState'][
                        'placeOfStayInRegCountry'][
                        'contactEMail']
            except Exception:
                contactemail_supplier = ''
        return contactemail_supplier

    def get_contactemail_supplier_old2(self):
        try:
            contactemail_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonForeignState'][
                    'placeOfStayInRegCountry']['contactEMail']
        except Exception:
            try:
                contactemail_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                        'individualPersonForeignState'][
                        'placeOfStayInRegCountry'][
                        'contactEMail']
            except Exception:
                contactemail_supplier = ''
        return contactemail_supplier

    def get_organizationname_supplier(self):
        try:
            organizationname_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityRF']['fullName']
        except Exception:
            try:
                organizationname_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityRF']['fullName']
            except Exception:
                organizationname_supplier = ''
        return organizationname_supplier

    def get_organizationname_supplier_old(self):
        try:
            try:
                lastname = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                    'lastName']
            except Exception:
                try:
                    lastname = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                            'lastName']
                except Exception:
                    lastname = ''
            try:
                firstname = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                    'firstName']
            except Exception:
                try:
                    firstname = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                            'firstName']
                except Exception:
                    firstname = ''
            try:
                middlename = self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['individualPersonRF'][
                    'middleName']
            except Exception:
                try:
                    middlename = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['individualPersonRF'][
                            'middleName']
                except Exception:
                    middlename = ''
            if not lastname and not firstname and not middlename:
                organizationname_supplier = ''
            else:
                organizationname_supplier = lastname + ' ' + firstname + ' ' + middlename
        except Exception:
            organizationname_supplier = ''
        return organizationname_supplier

    def get_organizationname_supplier_old1(self):
        try:
            organizationname_supplier = \
                self.contract['ns2:export']['ns2:contract']['suppliers']['supplier']['legalEntityForeignState'][
                    'fullName']
        except Exception:
            try:
                organizationname_supplier = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0]['legalEntityForeignState'][
                        'fullName']
            except Exception:
                organizationname_supplier = ''
        return organizationname_supplier

    def get_organizationname_supplier_old2(self):
        try:
            try:
                lastname = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][
                        'individualPersonForeignState'][
                        'lastName']
            except Exception:
                try:
                    lastname = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                            'individualPersonForeignState'][
                            'lastName']
                except Exception:
                    lastname = ''
            try:
                firstname = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][
                        'individualPersonForeignState'][
                        'firstName']
            except Exception:
                try:
                    firstname = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                            'individualPersonForeignState'][
                            'firstName']
                except Exception:
                    firstname = ''
            try:
                middlename = \
                    self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][
                        'individualPersonForeignState'][
                        'middleName']
            except Exception:
                try:
                    middlename = \
                        self.contract['ns2:export']['ns2:contract']['suppliers']['supplier'][0][
                            'individualPersonForeignState'][
                            'middleName']
                except Exception:
                    middlename = ''
            if not lastname and not firstname and not middlename:
                organizationname_supplier = ''
            else:
                organizationname_supplier = lastname + ' ' + firstname + ' ' + middlename
        except Exception:
            organizationname_supplier = ''
        return organizationname_supplier

    def get_products(self):
        try:
            products = self.contract['ns2:export']['ns2:contract']['products']['product']
        except Exception:
            products = ''
        return products

    def test_foundation(self, file_log, path_xml):
        if 'ns2:export' in self.contract:
            if 'ns2:contract' in self.contract['ns2:export']:
                if 'foundation' in self.contract['ns2:export']['ns2:contract']:
                    key_f = list((self.contract['ns2:export']['ns2:contract']['foundation']).keys())
                    for i in key_f:
                        if not i.startswith('oosOrder') and not i.startswith('fcsOrder'):
                            with open(file_log, 'a') as flog_err:
                                flog_err.write(
                                        'У контракта нет Order: ' + str(
                                                datetime.datetime.now()) + ' ' + path_xml + '\n')
                else:
                    with open(file_log, 'a') as flog_err:
                        flog_err.write(
                                'У контракта нет foundation: ' + str(
                                        datetime.datetime.now()) + ' ' + path_xml + '\n')


def parser(doc, path_xml, reg):
    global file_log
    id_customer = 0
    id_supplier = 0
    con = connect_bd(name_db)
    cur = con.cursor(pymysql.cursors.DictCursor)
    c = Contract(doc)
    id_contract = c.get_id()
    id_contract = id_contract.strip()
    if not id_contract:
        cur.close()
        con.close()
        with open(file_log, 'a') as flog_err:
            flog_err.write(
                    'У контракта нет id: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
        return
    # c.test_foundation(file_log, path_xml)
    p_number = c.get_p_number()
    regnum = c.get_regnum()
    current_contract_stage = c.get_current_contract_stage()
    placing = c.get_placing()
    region_code = reg
    url = c.get_url()
    sign_date = c.get_sign_date()
    single_customer_reason_code = c.get_single_customer_reason_code()
    single_customer_reason_name = c.get_single_customer_reason_name()
    fz = c.get_fz()
    notification_number = c.get_notification_number()
    lot_number = c.get_lot_number()
    contract_price = c.get_contract_price()
    currency = c.get_currency()
    version_number = c.get_version_number()
    cancel = 0
    if regnum and version_number:
        cur.execute("""SELECT MAX(version_number) as m FROM od_contract{0} WHERE regnum = %s""".format(suffix),
                    (regnum,))
        res_max = cur.fetchone()
        if res_max['m'] is not None:
            if int(version_number) > int(res_max['m']):
                try:
                    cur.execute("""UPDATE od_contract{0} SET cancel=1 WHERE regnum=%s""".format(suffix), (regnum,))
                except Exception as e:
                    with open(file_log, 'a') as flog_err:
                        flog_err.write(
                                'Ошибка обновления cancel у контрактов: ' + str(e) + ' ' + str(
                                        datetime.datetime.now()) + ' ' + path_xml + '\n')
            else:
                cancel = 1
    execution_start_date = c.get_execution_start_date()
    execution_end_date = c.get_execution_end_date()
    customer_regnumber = c.get_customer_regnumber()
    customer_regnumber = customer_regnumber.strip()
    supplier_inn = c.get_supplier_inn()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old0()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old2()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old4()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old3()
    if not supplier_inn:
        supplier_inn = c.get_supplier_inn_old1()
    supplier_inn = supplier_inn.strip()

    if customer_regnumber:
        cur.execute("""SELECT id FROM od_customer{0} WHERE regNumber = %s """.format(suffix), (customer_regnumber,))
        result_customer = cur.fetchone()
        if result_customer:
            id_customer = result_customer['id']
        if not id_customer:
            kpp_customer = c.get_kpp_customer()
            full_name_customer = c.get_full_name_customer()
            inn_customer = c.get_inn_customer()
            postal_address_customer = ''
            contracts_count_customer = 1
            contracts_sum_customer = contract_price
            contracts223_count_customer = 0
            contracts223_sum_customer = 0.0
            ogrn_customer = ''
            region_code_customer = ''
            phone_customer = ''
            fax_customer = ''
            email_customer = ''
            contact_name_customer = ''
            query_add_customer = """INSERT INTO od_customer{0} SET regNumber = %s, inn = %s, kpp = %s, contracts_count =
            %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s,
            full_name = %s, postal_address = %s, phone = %s, fax = %s, email = %s, contact_name = %s """.format(suffix)
            value_add_customer = (customer_regnumber, inn_customer, kpp_customer, contracts_count_customer,
                                  contracts223_count_customer, contracts_sum_customer,
                                  contracts223_sum_customer, ogrn_customer, region_code_customer,
                                  full_name_customer, postal_address_customer, phone_customer, fax_customer,
                                  email_customer, contact_name_customer)
            cur.execute(query_add_customer, value_add_customer)
            id_customer = cur.lastrowid
            Contract.count_cus += 1

    if supplier_inn:
        kpp_supplier = c.get_kpp_supplier()
        if not kpp_supplier:
            kpp_supplier = c.get_kpp_supplier_old()
        if not kpp_supplier:
            kpp_supplier = c.get_kpp_supplier_old1()
        kpp_supplier = kpp_supplier.strip()
        cur.execute("""SELECT id FROM od_supplier{0} WHERE inn = %s AND kpp = %s""".format(suffix),
                    (supplier_inn, kpp_supplier))
        result_supplier = cur.fetchone()
        if result_supplier:
            id_supplier = result_supplier['id']
        if not id_supplier:
            contactphone_supplier = c.get_contactphone_supplier()
            if not contactphone_supplier:
                contactphone_supplier = c.get_contactphone_supplier_old()
            if not contactphone_supplier:
                contactphone_supplier = c.get_contactphone_supplier_old1()
            if not contactphone_supplier:
                contactphone_supplier = c.get_contactphone_supplier_old2()

            contactemail_supplier = c.get_contactemail_supplier()
            if not contactemail_supplier:
                contactemail_supplier = c.get_contactemail_supplier_old()
            if not contactemail_supplier:
                contactemail_supplier = c.get_contactemail_supplier_old1()
            if not contactemail_supplier:
                contactemail_supplier = c.get_contactemail_supplier_old2()

            organizationname_supplier = c.get_organizationname_supplier()
            if not organizationname_supplier:
                organizationname_supplier = c.get_organizationname_supplier_old()
            if not organizationname_supplier:
                organizationname_supplier = c.get_organizationname_supplier_old1()
            if not organizationname_supplier:
                organizationname_supplier = c.get_organizationname_supplier_old2()

            contracts_count_supplier = 1
            contracts_sum_supplier = contract_price
            contracts223_count_supplier = 0
            contracts223_sum_supplier = 0.0
            ogrn_supplier = ''
            region_code_supplier = ''
            postal_address_supplier = ''
            contactfax_supplier = ''
            contact_name_supplier = ''
            query_add_supplier = """INSERT INTO od_supplier{0} SET  inn = %s, kpp = %s, contracts_count = %s,
            contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s,
            organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s,
            contact_name = %s """.format(suffix)
            value_add_supplier = (
                supplier_inn, kpp_supplier, contracts_count_supplier, contracts223_count_supplier,
                contracts_sum_supplier, contracts223_sum_supplier, ogrn_supplier, region_code_supplier,
                organizationname_supplier, postal_address_supplier, contactphone_supplier, contactfax_supplier,
                contactemail_supplier, contact_name_supplier)
            cur.execute(query_add_supplier, value_add_supplier)
            id_supplier = cur.lastrowid
            Contract.count_sup += 1

    cur.execute("""SELECT id FROM od_contract{0} WHERE id_contract = %s """.format(suffix), (id_contract,))
    exist_contract = cur.fetchone()
    if exist_contract:
        cur.execute("""DELETE FROM od_contract_product{0} WHERE id_od_contract = %s """.format(suffix),
                    (exist_contract['id'],))
        query_add_contract = """UPDATE od_contract{0} SET p_number = %s, regnum = %s,
            current_contract_stage = %s, placing = %s, region_code = %s, url = %s, sign_date = %s,
            single_customer_reason_code = %s, single_customer_reason_name = %s, fz = %s, notification_number = %s,
            lot_number = %s, contract_price = %s, currency = %s, version_number = %s, execution_start_date = %s,
            execution_end_date = %s, id_customer = %s, id_supplier = %s WHERE id_contract = %s""".format(suffix)
        value_add_contract = (
            p_number, regnum, current_contract_stage, placing, region_code, url, sign_date,
            single_customer_reason_code, single_customer_reason_name, fz, notification_number,
            lot_number, contract_price, currency, version_number, execution_start_date, execution_end_date,
            id_customer, id_supplier, id_contract)
        cur.execute(query_add_contract, value_add_contract)
        # with open(file_log, 'a') as flog_err:
        #     flog_err.write(
        #             'Контракт обновлен: ' + str(id_contract) + ' ' + str(
        #                     datetime.datetime.now()) + ' ' + path_xml + '\n')
        Contract.count_upd += 1

    else:
        query_add_contract = """INSERT INTO od_contract{0} SET  id_contract = %s, p_number = %s, regnum = %s,
            current_contract_stage = %s, placing = %s, region_code = %s, url = %s, sign_date = %s,
            single_customer_reason_code = %s, single_customer_reason_name = %s, fz = %s, notification_number = %s,
            lot_number = %s, contract_price = %s, currency = %s, version_number = %s, execution_start_date = %s,
            execution_end_date = %s, id_customer = %s, id_supplier = %s, cancel = %s""".format(suffix)
        value_add_contract = (
            id_contract, p_number, regnum, current_contract_stage, placing, region_code, url, sign_date,
            single_customer_reason_code, single_customer_reason_name, fz, notification_number,
            lot_number, contract_price, currency, version_number, execution_start_date, execution_end_date,
            id_customer, id_supplier, cancel)
        cur.execute(query_add_contract, value_add_contract)
        Contract.count_ins += 1

    # if not id_supplier:
    #     with open(file_log, 'a') as flog_sup:
    #         flog_sup.write(
    #                 'У контракта нет supplier: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
    #         try:
    #             flog_sup.write(
    #                     '{0}\n\n\n'.format(str(doc['ns2:export']['ns2:contract']['suppliers']['supplier'])))
    #         except Exception:
    #             flog_sup.write(
    #                     'И тега такого нет тоже: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
    cur.execute("""SELECT id FROM od_contract{0} WHERE id_contract = %s """.format(suffix), (id_contract,))
    result_id_contract = cur.fetchone()
    if result_id_contract:
        id_od_contract = result_id_contract['id']

    cur.close()
    con.close()
    if id_od_contract:
        products = c.get_products()
        if products:
            a = 0
            try:
                n = products[0]
                a = 1
            except KeyError:
                a = 2
            if a == 1:
                for line in products:
                    try:
                        add_product(line, id_od_contract)
                    except Exception:
                        with open(file_log, 'a') as flog_err7:
                            flog_err7.write(
                                    'Ошибка добавления товара: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
            elif a == 2:
                try:
                    add_product(products, id_od_contract)
                except Exception:
                    with open(file_log, 'a') as flog_err8:
                        flog_err8.write(
                                'Ошибка добавления товара: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
            else:
                with open(file_log, 'a') as flog_err1:
                    flog_err1.write(
                            'У контракта нет товаров: ' + str(datetime.datetime.now()) + ' ' + path_xml + '\n')
