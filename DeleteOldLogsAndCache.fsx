open System
open System.Linq
open System.IO
System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)

type LogOrCache =
    | Cache
    | CacheAndLog

let (|Cdate|) (d:DateTime) = 
    match d with
    | x when x.Hour >= 10 && x.Hour < 14 -> CacheAndLog
    | _ -> Cache

let listDirLog = ["ParserTenders/log_tenders223"; "ParserTenders/log_tenders44"; "ParserContracts223Sharp/log_parsing/contr223"; 
                  "ParserContracts44Sharp/log_contracts44"; "ParserContracts44Sharp/log_contracts223"; "ParserTenderPlan/log_plan44"; 
                  "ParserOrganizations/log_organization"; "ParserOrganizations223/log_organization223"; "log_send_email";
                  "ParserUnfair/log_bank"; "ParserUnfair/log_rnp"; "ParserProtocols/log_prot"; "ParserUnfair/log_complaint"; 
                  "ParserUnfair/log_complaint_result"; "ParserTenders/log_sign223"; "ParserTenders/log_gazprom"; "ParserRosneft/LogRosneft"; 
                  "ParserFabrikant/LogFabrikant"; "ParserB2B/LogB2B"; "ParserTenderPro/log_tender_pro"; "ParserTenders/log_exp223";
                  "ParserProtocols223/log_protocols"; "ParserTenders/log_gnt_web"; "ParserTenders/log_obtorg_web";
                  "ParserTenders/log_spectorg_web"; "ParserKotlin/log_tender_komos"; "ParserWebUniversal/log_etprf";
                  "ParserTenders/log_mrsk"; "ParserWebUniversal/log_gpn"; "ParserWebUniversal/log_pol";
                  "ParserWebUniversal/log_luk"; "ParserWebUniversal/log_tat"; "ParserWebUniversal/log_rts";
                  "ParserWebUniversal/log_sibur"; "ParserTenders/log_rosneft"; "ParserTenders/log_sakhalin";
                  "ParserWebUniversal/log_ural"; "ParserTenders/log_tektorg_gazprom"; "ParserWebUniversal/log_miratorg";
                  "ParserTenders/log_tektorg_interrao"; "ParserTenders/log_tektorg_rzd"; "ParserWebGo/log_x5group";
                  "ParserWebUniversal/log_stg"; "ParserWebGo/log_dixy"; "ParserWebGo/log_rusneft"; "ParserWebFSharp/log_tenders_irkutskoil";
                  "ParserWebUniversal/log_bashneft"; "UnParserSelen/log_tander"; "ParserWebFSharp/log_tenders_akd";
                  "ParserWebFSharp/log_tenders_lsr"; "ParserWebGo/log_phosagro"; "ParserWebFSharp/log_tenders_butb";
                  "ParserTenders/log_tenders615"; "UnParserSelen/log_safmar"; "ParserUkr/log_tenders_ukr";
                  "UnParserSelen/log_talan"; "UnParserSelen/log_mvideo"; "ParserWebGo/log_komtech";
                  "ParserWebGo/log_onlinecontract"; "ParserWebFSharp/log_tenders_rossel"; "ParserWebGo/log_cpc";
                  "UnParserSelen/log_mosreg"; "ParserWebUniversal/log_rfp"; "ParserWebFSharp/log_tenders_neft";
                  "ParserWebFSharp/log_tenders_slav"; "ParserWebFSharp/log_tenders_aero"; "ParserWebGo/log_novatek";
                  "ParserWebGo/log_azot"; "UnParserSelen/log_ugmk"; "ParserWebUniversal/log_zakupki";
                  "ParserWebGo/log_uva"; "ParserWebCore/log_agrocomplex"; "UnParserSelen/log_imptorgov";
                  "ParserWebCore/log_kzgroup"; "ParserWebCore/log_agrotomsk"; "ParserWebFSharp/log_tenders_stroytorgi";
                  "UnParserSelen/log_sibprime"; "ParserWebCore/log_sibintek"; "ParserWebFSharp/log_tenders_asgor";
                  "ParserWebCore/log_setonline"; "UnParserSelen/log_crimeabt"; "ParserKotlinNew/logdir_tenders_salavat";
                  "ParserWebFSharp/log_tenders_gosyakut"; "ParserWebGo/log_icetrade"; "ParserContracts223Sharp/log_parsing/customer"; 
                  "ParserContracts223Sharp/log_parsing/supplier"; "UnParserSelen/log_belmarket"; "ParserKotlinNew/logdir_tenders_umz";
                  "ParserWebCore/log_mzvoron"; "UnParserSelen/log_bico"; "ParserWebFSharp/log_tenders_rostend"; "ParserWebCore/log_maxi"; 
                  "ParserWebFSharp/log_tenders_chpt"; "ParserWebCore/log_tver"; "ParserWebCore/log_murman"; "ParserWebCore/log_kalug"; 
                  "ParserWebCore/log_smol"; "ParserWebCore/log_samar"; "ParserWebCore/log_udmurt"; "UnParserSelen/log_rostov"; 
                  "UnParserSelen/log_simferop"; "UnParserSelen/log_kostroma"; "UnParserSelen/log_tomsk"; "UnParserSelen/log_zmo";
                  "ParserWebFSharp/log_tenders_tplus"]
let listDirCache = ["/srv/tenders.enter-it.ru/FileCash"]
let del (fn:FileInfo->bool) dir =
    try
          System.IO.Directory.GetFiles(dir).Select(fun f -> new FileInfo(f)).Where(fn).ToList().ForEach(fun f -> f.Delete())
    with
    | _ -> printfn "error"

//listDirLog |> List.iter del

//let CheckDate (d:DateTime) = 
//    match d with
//    | x when x.Hour >= 10 && x.Hour < 14 -> listDirLog |> List.iter dellog
//                                            listDirCache |> List.iter delcache
//    | _ -> listDirCache |> List.iter delcache
//CheckDate DateTime.Now
let delLog = del (fun (f:FileInfo) -> f.LastWriteTime < DateTime.Now.AddDays(-31.))
let delCache = del (fun (f:FileInfo) -> f.LastWriteTime < DateTime.Now.AddHours(-2.))
let b = 
    match DateTime.Now with
    | Cdate Cache -> listDirCache |> List.iter delCache
    | Cdate CacheAndLog -> listDirLog |> List.iter delLog
                           listDirCache |> List.iter delCache