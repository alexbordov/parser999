import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con
id_t = 2
con = connect_bd('tenders_test')
cur = con.cursor(pymysql.cursors.DictCursor)
cur.execute("""SELECT po.name, po.okpd_name FROM purchase_object as po
LEFT JOIN lot as l on l.id_lot = po.id_lot
WHERE l.id_tender=%s""", (id_t, ))
res_string = ''
while True:
    row = cur.fetchone()
    if not row:
        break
    string_name = row['name']
    string_name = string_name.strip()
    string_okpd_name = row['okpd_name']
    string_okpd_name = string_okpd_name.strip()
    res_string = res_string + ' ' + string_name + ' ' + string_okpd_name
cur.execute("""SELECT file_name FROM attachment WHERE id_tender=%s""", (id_t, ))
while True:
    row_1 = cur.fetchone()
    if not row_1:
        break
    sring_att = row_1['file_name']
    sring_att = sring_att.strip()
    res_string = res_string + ' ' + sring_att
print(res_string)
cur.close()
con.close()