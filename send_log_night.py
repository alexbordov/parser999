import datetime
import time
import os
import smtplib
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.utils import formataddr
from os.path import basename
import psutil

user_name = '@@@@@@@@@@@'
user_passwd = '***********'
port = 25

path_log_otc = './ParserOTC/log_tenders_otc'
path_log_gazprom = './ParserTenders/log_gazprom'
path_log_butb = './ParserWebFSharp/log_tenders_butb'
path_log_rosneft = './ParserRosneft/LogRosneft'
path_log_b2b = './ParserB2B/LogB2B'
path_log_tender_pro = './ParserTenderPro/log_tender_pro'
path_log_gntweb = './ParserTenders/log_gnt_web'
path_log_obtorgweb = './ParserTenders/log_obtorg_web'
path_log_spectorgweb = './ParserTenders/log_spectorg_web'
path_log_mrsk = './ParserTenders/log_mrsk'
path_log_komos = './ParserKotlin/log_tender_komos'
path_log_fabrikant = './ParserFabrikant/LogFabrikant'
path_log_sibur = './ParserWebUniversal/log_sibur'
path_log_gpn = './ParserWebUniversal/log_gpn'
path_log_pol = './ParserWebUniversal/log_pol'
path_log_luk = './ParserWebUniversal/log_luk'
path_log_tat = './ParserWebUniversal/log_tat'
path_log_rts = './ParserWebUniversal/log_rts'
path_log_etp_rf = './ParserWebUniversal/log_etprf'
path_log_rosneft = './ParserTenders/log_rosneft'
path_log_sakhalin = './ParserTenders/log_sakhalin'
path_log_ural = './ParserWebUniversal/log_ural'
path_log_tek_gpm = './ParserTenders/log_tektorg_gazprom'
path_log_miratorg = './ParserWebUniversal/log_miratorg'
path_log_tek_interrao = './ParserTenders/log_tektorg_interrao'
path_log_tek_rzd = './ParserTenders/log_tektorg_rzd'
path_log_x5 = './ParserWebGo/log_x5group'
path_log_stg = './ParserWebUniversal/log_stg'
path_log_dixy = './ParserWebGo/log_dixy'
path_log_rusneft = './ParserWebGo/log_rusneft'
path_log_irkutskoil = './ParserWebFSharp/log_tenders_irkutskoil'
path_log_bashneft = './ParserWebUniversal/log_bashneft'
path_log_tander = './UnParserSelen/log_tander'
path_log_akd = './ParserWebFSharp/log_tenders_akd'
path_log_lsr = './ParserWebFSharp/log_tenders_lsr'
path_log_phosagro = './ParserWebGo/log_phosagro'
path_log_safmarg = './UnParserSelen/log_safmar'
path_log_talan = './UnParserSelen/log_talan'
path_log_mvideo = './UnParserSelen/log_mvideo'
path_log_ukr = './ParserUkr/log_tenders_ukr'
path_log_komtech = './ParserWebGo/log_komtech'
path_log_ocontract = './ParserWebGo/log_onlinecontract'
path_log_rosel = './ParserWebFSharp/log_tenders_rossel'
path_log_cpc = './ParserWebGo/log_cpc'
path_log_mosreg = './UnParserSelen/log_mosreg'
path_log_rfp = './ParserWebUniversal/log_rfp'
path_log_neft = './ParserWebFSharp/log_tenders_neft'
path_log_slav = './ParserWebFSharp/log_tenders_slav'
path_log_aero = './ParserWebFSharp/log_tenders_aero'
path_log_novatek = './ParserWebGo/log_novatek'
path_log_azot = './ParserWebGo/log_azot'
path_log_ugmk = './UnParserSelen/log_ugmk'
path_log_zakupki = './ParserWebUniversal/log_zakupki'
path_log_uva = './ParserWebGo/log_uva'
path_log_agrocomplex = './ParserWebCore/log_agrocomplex'
path_log_imptorgov = './UnParserSelen/log_imptorgov'
path_log_kzgroup = './ParserWebCore/log_kzgroup'
path_log_agrotomsk = './ParserWebCore/log_agrotomsk'
path_log_stroytorgi = './ParserWebFSharp/log_tenders_stroytorgi'
path_log_sibprime = './UnParserSelen/log_sibprime'
path_log_sibintek = './ParserWebCore/log_sibintek'
path_log_asgor = './ParserWebFSharp/log_tenders_asgor'
path_log_setonline = './ParserWebCore/log_setonline'
path_log_crimeabt = './UnParserSelen/log_crimeabt'
path_log_salavat = './ParserKotlinNew/logdir_tenders_salavat'
path_log_gosyakut = './ParserWebFSharp/log_tenders_gosyakut'
path_log_belmarket = './UnParserSelen/log_belmarket'
path_log_umz = './ParserKotlinNew/logdir_tenders_umz'
path_log_mzvoron = './ParserWebCore/log_mzvoron'
path_log_tver = './ParserWebCore/log_tver'
path_log_murman = './ParserWebCore/log_murman'
path_log_kalug = './ParserWebCore/log_kalug'
path_log_smol = './ParserWebCore/log_smol'
path_log_samar = './ParserWebCore/log_samar'
path_log_udmurt = './ParserWebCore/log_udmurt'
path_log_rostov = './UnParserSelen/log_rostov'
path_log_simferop = './UnParserSelen/log_simferop'
path_log_kostroma = './UnParserSelen/log_kostroma'
path_log_tomsk = './UnParserSelen/log_tomsk'
path_log_zmo = './UnParserSelen/log_zmo'

list_path = [path_log_otc, path_log_gazprom, path_log_rosneft, path_log_fabrikant, path_log_b2b,
             path_log_tender_pro, path_log_gntweb, path_log_obtorgweb, path_log_spectorgweb, path_log_mrsk,
             path_log_komos,
             path_log_etp_rf, path_log_gpn, path_log_pol, path_log_luk, path_log_tat, path_log_rts, path_log_sibur,
             path_log_rosneft, path_log_sakhalin, path_log_ural, path_log_tek_gpm, path_log_miratorg,
             path_log_tek_interrao, path_log_tek_rzd, path_log_x5, path_log_stg, path_log_dixy, path_log_rusneft,
             path_log_irkutskoil, path_log_bashneft, path_log_tander, path_log_akd, path_log_lsr, path_log_phosagro,
             path_log_butb, path_log_safmarg, path_log_ukr, path_log_talan, path_log_mvideo, path_log_komtech,
             path_log_ocontract, path_log_rosel, path_log_cpc, path_log_mosreg, path_log_rfp, path_log_neft,
             path_log_slav, path_log_aero, path_log_novatek, path_log_azot, path_log_ugmk, path_log_zakupki,
             path_log_uva, path_log_agrocomplex, path_log_imptorgov, path_log_kzgroup, path_log_agrotomsk,
             path_log_stroytorgi, path_log_sibprime, path_log_sibintek, path_log_asgor, path_log_setonline,
             path_log_crimeabt, path_log_salavat, path_log_gosyakut, path_log_belmarket, path_log_umz, path_log_mzvoron,
             path_log_tver, path_log_murman, path_log_kalug, path_log_smol, path_log_samar, path_log_udmurt,
             path_log_rostov,
             path_log_simferop, path_log_kostroma, path_log_tomsk, path_log_zmo]
list_files = []
text = '<div><p>В прикрепленных файлах отчеты о парсинге не ЕИС ресурсов.<\p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/">Смотреть отчеты по работе парсеров</a></p><p><a ' \
       'href="http://tenders.enter-it.ru:3000/graph">Смотреть все графики по работе парсеров</a></p></div>'
subj = 'Отчеты о парсинге не ЕИС ресурсов в ночь на {0}'.format(datetime.datetime.now().strftime("%d/%m/%Y"))


def sizeof_fmt(num, suffix='B'):
    """

    :param num:
    :param suffix:
    :return:
    """
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return f"{num:.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} {'Yi'}{suffix}"


def send_mail(send_from, send_to, subject, text, files=None, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param subject:
    :param text:
    :param files:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text.encode('utf-8'), "html", "utf-8"))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
            )
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def send_mail_free_space(send_from, send_to, server="mail.enter-it.ru"):
    """

    :param send_from:
    :param send_to:
    :param server:
    """
    assert isinstance(send_to, list)
    msg = MIMEMultipart()
    # msg['From'] = send_from
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    dsk = psutil.disk_usage('/mnt/hdd01')
    free_space = sizeof_fmt(dsk.free)
    msg['Subject'] = f'На диске осталось {free_space} свободного места!'
    tm = time.time() - psutil.boot_time()
    text = f"""Сервер работает без перезагрузки уже {datetime.timedelta(seconds=tm)}\n\nСвободного места на диске {
    free_space} """
    msg.attach(MIMEText(text))
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def main():
    """
    main function
    """
    global list_path, list_files, text, subj
    for p in list_path:
        files = os.listdir(p)
        if files:
            files = [os.path.join(p, file) for file in files]
            files = [file for file in files if os.path.isfile(file)]
            f = max(files, key=os.path.getctime)
            list_files.append(f)
    send_mail('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'], subj, text,
              files=list_files)
    send_mail_free_space('info@enter-it.ru', ['rummolprod999@gmail.com', 'info@enter-it.ru'])


if __name__ == "__main__":
    main()
