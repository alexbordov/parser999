import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)

cur.execute("""TRUNCATE TABLE auction_applications_new""")
cur.execute("""TRUNCATE TABLE auction_end_applications_new""")
cur.execute("""TRUNCATE TABLE auction_end_protocol_new""")
cur.execute("""TRUNCATE TABLE auction_participant_new""")
cur.execute("""TRUNCATE TABLE auction_protocol_new""")

cur.execute("""ALTER TABLE auction_applications_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_end_applications_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_end_protocol_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_participant_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_protocol_new AUTO_INCREMENT = 1""")

cur.close()
con.close()
print('Готово')
