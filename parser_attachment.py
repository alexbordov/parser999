import shutil
import subprocess
import logging
import textract
import pymysql
import urllib.request
import os
import re
import datetime
import time
import random
import asyncio
from proxybroker import Broker
from proxylist import ProxyList
import zipfile
import rarfile

data_baza = 'tenders_test'
suffix = '_new'
USER_AGENTS_FILE = 'user_agents.txt'
file_log = './attachment_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.INFO, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


async def use(proxies):
    try:
        os.remove('proxy.txt')
    except Exception:
        pass
    while True:
        proxy = await proxies.get()
        if proxy is None:
            break
        # print('Found proxy: %s:%s' % (proxy.host, proxy.port))
        with open('proxy.txt', 'a') as prox:
            prox.write('%s:%s\n' % (proxy.host, proxy.port))


async def find(proxies, loop):
    broker = Broker(queue=proxies,
                    timeout=10,
                    attempts_conn=3,
                    max_concurrent_conn=200,
                    judges=['https://httpheader.net/', 'http://httpheader.net/'],
                    providers=['http://www.proxylists.net/', 'http://fineproxy.org/eng/'],
                    verify_ssl=False,
                    loop=loop)

    # only anonymous & high levels of anonymity for http protocol and high for others:
    types = [('HTTP', ('Anonymous', 'High')), 'HTTPS', 'SOCKS4', 'SOCKS5']
    countries = []
    limit = 50

    await broker.find(types=types, countries=countries, limit=limit)


def LoadUserAgents(uafile=USER_AGENTS_FILE):
    uas = []
    with open(uafile, 'rb') as uaf:
        for ua in uaf.readlines():
            if ua:
                uas.append(ua.strip()[1:-1 - 1])
    random.shuffle(uas)
    return uas


def add_text(text, x, m=0):
    # text = re.sub(' +', ' ', text)
    # text = text.replace('\n', ' ')
    # text = text.replace('\t', ' ')
    # text = text.replace('\r', ' ')
    text = re.sub('\s+', ' ', text)
    con_a = connect_bd(data_baza)
    cur_a = con_a.cursor(pymysql.cursors.DictCursor)
    if m:
        cur_a.execute("""SELECT attach_text FROM attachment{0} WHERE id_attachment=%s""".format(suffix),
                      (x['id_attachment'],))
        res_t = cur_a.fetchone()
        if res_t:
            text += res_t['attach_text']
    cur_a.execute("""UPDATE attachment{0} SET attach_text=%s WHERE id_attachment=%s""".format(suffix),
                  (text, x['id_attachment']))
    cur_a.execute("""UPDATE attachment{0} SET attach_add = 1 WHERE id_attachment=%s""".format(suffix),
                  (x['id_attachment'],))
    cur_a.close()
    con_a.close()


def type_files(s, file_name, x, dir_a):
    if s == '.doc':
        try:
            parse_doc(file_name, x)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл doc {0} {1}\n'.format(str(e), file_name))
    elif s == '.docx':
        try:
            parse_docx(file_name, x)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл docx {0} {1}\n'.format(str(e), file_name))
    elif s == '.pdf':
        try:
            parse_pdf(file_name, x)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл pdf {0} {1}\n'.format(str(e), file_name))
    elif s == '.zip':
        try:
            parse_zip(file_name, x, dir_a)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл zip {0} {1}\n'.format(str(e), file_name))
    elif s == '.rar':
        try:
            parse_rar(file_name, x, dir_a)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл rar {0} {1}\n'.format(str(e), file_name))


def parse_zip(file_name, x, dir_a):
    # path_arr = '{0}/{1}'.format(file_name, 'zip')
    z = zipfile.ZipFile(file_name, 'r')
    list_f = z.namelist()
    for f in list_f:
        if f.endswith('.doc') or f.endswith('.docx'):
            data = z.read(f)
            myfile_path = os.path.join(dir_a, f.split("/")[-1])
            myfile = open(myfile_path, "wb")
            myfile.write(data)
            myfile.close()
    z.close()
    files = os.listdir(dir_a)
    for file in files:
        if file.endswith('.doc'):
            parse_doc('{0}/{1}'.format(dir_a, file), x, 1)
        elif file.endswith('.docx'):
            parse_doc('{0}/{1}'.format(dir_a, file), x, 1)


def parse_rar(file_name, x, dir_a):
    # path_arr = '{0}/{1}'.format(file_name, 'zip')
    r = rarfile.RarFile(file_name, 'r')
    list_f = r.infolist()
    for f in list_f:
        if f.endswith('.doc') or f.endswith('.docx'):
            data = r.open(f).read()
            myfile_path = os.path.join(dir_a, f.split("/")[-1])
            myfile = open(myfile_path, "wb")
            myfile.write(data)
            myfile.close()
    r.close()
    files = os.listdir(dir_a)
    for file in files:
        if file.endswith('.doc'):
            parse_doc('{0}/{1}'.format(dir_a, file), x, 1)
        elif file.endswith('.docx'):
            parse_doc('{0}/{1}'.format(dir_a, file), x, 1)


def parse_doc(file_name, x, multi=0):
    text = ''
    try:
        text = textract.process(file_name, encoding='utf_8')
        text = text.decode('utf-8')
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(e), file_name))
        f_txt = './' + str(x['id_attachment']) + '.txt'
        try:
            subprocess.call(
                    ['/opt/libreoffice5.2/program/soffice.bin', '--headless', '--convert-to', 'txt:Text',
                     file_name], shell=False)
            text = open(f_txt).read()
            os.remove(f_txt)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Не удалось пропарсить файл subprocess {0} {1}\n'.format(str(e), file_name))
    # text = text.decode('utf-8')
    if text:
        if multi:
            add_text(text, x, m=1)
        else:
            add_text(text, x)


def parse_docx(file_name, x, multi=0):
    text = ''
    try:
        text = textract.process(file_name, encoding='utf_8')
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(e), file_name))
    text = text.decode('utf-8')
    if text:
        if multi:
            add_text(text, x, m=1)
        else:
            add_text(text, x)


def parse_pdf(file_name, x):
    text = ''
    try:
        text = textract.process(file_name, encoding='utf_8', method='tesseract', language='rus')
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл {0} {1}\n'.format(str(e), file_name))
    text = text.decode('utf-8')
    if text:
        add_text(text, x)


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def except_file(id_att):
    con = connect_bd(data_baza)
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute("""UPDATE attachment{0} SET attach_add = 1 WHERE id_attachment=%s""".format(suffix), (id_att,))
    cur.close()
    con.close()


def parser(x):
    pl = ProxyList()
    pl.load_file('./proxy.txt')
    s = ''
    type_f = ['.doc', '.docx', '.zip', '.rar']
    con_a = connect_bd(data_baza)
    cur_a = con_a.cursor(pymysql.cursors.DictCursor)
    dir_a = ''
    try:
        cur_a.execute("""SELECT url, file_name FROM attachment{0} WHERE id_attachment = %s""".format(suffix),
                      (x['id_attachment']))
        res = cur_a.fetchone()
        cur_a.close()
        con_a.close()
        if res:
            try:
                s = res['file_name']
                ind = s.rindex('.')
                s = s[ind:]
                s = s.lower()
                if s not in type_f:
                    except_file(x['id_attachment'])
                    return
                dir_a = './' + str(x['id_attachment'])
                os.mkdir(dir_a)
                count = 0
                while True:
                    if count >= 10:
                        with open(file_log, 'a') as flog:
                            flog.write('Не удалось скачать файл за 10 попыток {0}\n'.format(s))
                        return
                    try:
                        p = pl.random().address()
                        user_agents = LoadUserAgents()
                        ua = random.choice(user_agents)
                        proxies = {'http': p}
                        req1 = urllib.request.Request(url=res['url'], headers={"Connection": "close",
                                                                               'User-Agent': ua})
                        proxy_support = urllib.request.ProxyHandler(proxies)
                        opener = urllib.request.build_opener(proxy_support)
                        urllib.request.install_opener(opener)
                        f = urllib.request.urlopen(req1, timeout=40)
                        file_name = dir_a + '/' + str(x['id_attachment']) + s
                        with open(file_name, 'wb') as file:
                            file.write(f.read())
                        if count > 0:
                            with open(file_log, 'a') as flog:
                                flog.write(
                                        'Удалось скачать файл после попытки {0} файл номер {1}\n'.format(str(count),
                                                                                                         str(x[
                                                                                                                 'id_attachment'])))
                        break
                    except Exception as e:
                        with open(file_log, 'a') as flog:
                            flog.write(
                                    'Не удалось скачать файл, ошибка {0} попытка номер {1} файл номер {2}\n'.format(
                                            str(e), str(count), str(x['id_attachment'])))
                        count += 1
                        continue

                type_files(s, file_name, x, dir_a)

            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Неизвестная ошибка {0} {1}\n'.format(str(e), str(x['id_attachment'])))
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Общая ошибка функции {0} {1}\n'.format(str(e), str(x['id_attachment'])))
    finally:
        time.sleep(1)
        shutil.rmtree(dir_a, ignore_errors=True)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    proxies = asyncio.Queue(loop=loop)
    tasks = asyncio.gather(find(proxies, loop), use(proxies))
    loop.run_until_complete(tasks)
    # p = multiprocessing.Pool(5)
    dt = datetime.datetime.now()
    dt = str(dt).split(' ')[0]
    con = connect_bd(data_baza)
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute(
            """SELECT id_attachment FROM attachment{0} AS att LEFT JOIN tender_new AS t ON att.id_tender= t.id_tender 
              WHERE t.end_date > %s AND att.attach_add = 0 AND t.cancel = 0""".format(suffix), (dt,))

    result_t = cur.fetchall()
    con.close()
    cur.close()
    c = 0
    if result_t is None:
        exit()
    else:
        count = len(result_t)
        print(str(count))
        for res in result_t:
            try:
                print(str(c))
                c += 1
                parser(res)
            except Exception as e:
                print(str(e))
