import datetime
import json
import logging
import os
import urllib.error
import urllib.request
import zipfile
import pymysql

DB = 'tenders_test'
SUFFIX = ''
log_update = 0
log_insert = 0
inn_null = 0
# period = datetime.timedelta(days=1)
# lastdate = datetime.date.today() - period
# format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
# url_customers = 'https://clearspending.ru/download/opendata/customers-' + format_date + '.json.zip'
# archive = 'customers-' + format_date + '.json.zip'
# urllib.request.urlretrieve(url_customers, archive)
file_log = './log_suppliers/suppliers_{0}.log'.format(str(datetime.date.today()))
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def connect_bd(baza):
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def parse(line):
    global log_insert
    global log_update
    global file_log
    global inn_null
    string = str(line)
    if (string.find('[', 0, 1) != -1):
        string = string[1:]
    size = len(string)
    if (string.find(',', (size - 2), size) != -1):
        string = string[: -2]
    if (string.find(']', (size - 2), size) != -1):
        string = string[: -2]
    end_string = string.strip()
    test = json.loads(end_string)
    # regNumber = ''
    inn = ''
    kpp = ''
    contracts_count = 0
    contracts223_count = 0
    contracts_sum = 0.0
    contracts223_sum = 0.0
    ogrn = ''
    regionCode = ''
    organizationName = ''
    postAddress = ''
    contactPhone = ''
    contactFax = ''
    contactEMail = ''
    lastName = ''
    middleName = ''
    firstName = ''
    contact_name = ''
    if 'kpp' in test:
        kpp = str(test['kpp'])
        kpp = kpp.strip()
    if 'contracts223Count' in test:
        contracts223_count = int(test['contracts223Count'])
    if 'contractsCount' in test:
        contracts_count = int(test['contractsCount'])
    if 'contracts223Sum' in test:
        contracts223_sum = round(float(test['contracts223Sum']), 2)
    if 'contractsSum' in test:
        contracts_sum = round(float(test['contractsSum']), 2)
    if 'ogrn' in test:
        ogrn = str(test['ogrn'])
    if 'regionCode' in test:
        regionCode = str(test['regionCode'])
    if 'organizationName' in test:
        organizationName = str(test['organizationName'])
    if 'postAddress' in test:
        postAddress = str(test['postAddress'])
    if organizationName == '':
        organizationName = 'не указан'
    if 'contactFax' in test:
        contactFax = str(test['contactFax'])
    if 'contactEMail' in test:
        contactEMail = str(test['contactEMail'])
    if contactEMail == '':
        contactEMail = 'не указан'
    if 'contactPhone' in test:
        contactPhone = str(test['contactPhone'])
    if 'contactInfo' in test:
        contract_person_object = test['contactInfo']
        if 'middleName' in contract_person_object:
            middleName = str(contract_person_object['middleName'])
        if 'firstName' in contract_person_object:
            firstName = str(contract_person_object['firstName'])
        if 'lastName' in contract_person_object:
            lastName = str(contract_person_object['lastName'])
    contact_name = str('{0} {1} {2}'.format(firstName, middleName, lastName))
    if 'inn' in test:
        inn = str(test['inn'])
        inn = inn.strip()
    if inn:
        con = connect_bd(DB)
        cur = con.cursor()
        if kpp:
            cur.execute("""SELECT id FROM od_supplier{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX), (inn, kpp))
            resultkppinn = cur.fetchone()
            if not resultkppinn:
                query1 = """INSERT INTO od_supplier{0} SET  inn = %s, kpp = %s, contracts_count = %s, 
                            contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, 
                            region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, 
                            contactFax = %s, contactEMail = %s, contact_name = %s""".format(SUFFIX)
                value1 = (
                    inn, kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode,
                    organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name)
                cur.execute(query1, value1)
                log_insert += 1

            else:
                query2 = """UPDATE od_supplier{0} SET contracts_count = %s, contracts223_count = %s, 
                            contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, 
                            organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, 
                            contactEMail = %s, contact_name = %s WHERE inn = %s AND kpp = %s""".format(SUFFIX)
                value2 = (contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode,
                          organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name, inn, kpp)
                cur.execute(query2, value2)
                log_update += 1
        else:
            cur.execute("""SELECT id FROM od_supplier{0} WHERE inn = %s AND kpp = %s""".format(SUFFIX), (inn, kpp))
            resultinn = cur.fetchone()
            if not resultinn:
                query1 = """INSERT INTO od_supplier{0} SET  inn = %s, kpp = %s, contracts_count = %s, 
                            contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, 
                            region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, 
                            contactFax = %s, contactEMail = %s, contact_name = %s""".format(SUFFIX)
                value1 = (
                    inn, kpp, contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode,
                    organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name)
                cur.execute(query1, value1)
                log_insert += 1
            else:
                query2 = """UPDATE od_supplier{0} SET contracts_count = %s, contracts223_count = %s, 
                            contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, 
                            organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, 
                            contactEMail = %s, contact_name = %s WHERE inn = %s AND kpp = %s""".format(SUFFIX)
                value2 = (contracts_count, contracts223_count, contracts_sum, contracts223_sum, ogrn, regionCode,
                          organizationName, postAddress, contactPhone, contactFax, contactEMail, contact_name, inn, kpp)
                cur.execute(query2, value2)
                log_update += 1
        cur.close()
        con.close()
    else:
        inn_null += 1


def main():
    per = 0
    down_count = 10
    archive = ''
    while down_count:
        try:
            period = datetime.timedelta(days=per)
            lastdate = datetime.date.today() - period
            format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
            url_suppliers = 'https://clearspending.ru/download/opendata/suppliers-{0}.json.zip'.format(format_date)
            archive = 'suppliers-' + format_date + '.json.zip'
            # urllib.request.urlretrieve(url_customers, archive)
            f = urllib.request.urlopen(url_suppliers, timeout=900)
            with open(archive, "wb") as code:
                code.write(f.read())
            break
        except urllib.error.HTTPError as httperr:
            # print(httperr)
            with open(file_log, 'a') as flog_err:
                flog_err.write(
                        'Ошибка скачивания: {0} {1}{2}\n'.format(str(datetime.datetime.now()), str(httperr), str(
                                url_suppliers)))
            per += 1
            logging.exception("Ошибка cкачивания:")
        except urllib.error.URLError as httperr:
            # print(httperr)
            with open(file_log, 'a') as flog_err:
                flog_err.write(
                        'Ошибка скачивания: {0} {1}{2}\n'.format(str(datetime.datetime.now()), str(httperr), str(
                                url_suppliers)))
            logging.exception("Ошибка cкачивания:")
        except Exception as ex:
            with open(file_log, 'a') as flog_err0:
                flog_err0.write(
                        'Неизвестная ошибка: {0} {1}{2}\n'.format(str(datetime.datetime.now()), str(ex), str(
                                url_suppliers)))
            logging.exception("Неизвестная ошибка:")
        down_count -= 1
    if down_count == 0:
        with open(file_log, 'a') as flog_err1:
            flog_err1.write('Не удалось получить архив за: {0} \n'.format(str(format_date)))
        exit()
    z = zipfile.ZipFile(archive, 'r')
    z.extractall()
    z.close()
    file_name = 'suppliers-{0}.json'.format(format_date)
    flog = open(file_log, 'a')
    flog.write('Время начала работы парсера: {0}\n'.format(str(datetime.datetime.now())))
    flog.close()

    f = open(file_name, 'r')
    for line in f:
        try:
            parse(line)
        except Exception:
            logging.exception("Ошибка: ")
    f.close()
    os.remove(archive)
    os.remove(file_name)
    flog = open(file_log, 'a')
    flog.write('Добавлено поставщиков: {0}\n'.format(str(log_insert)))
    flog.write('Обновлено поставщиков: {0}\n'.format(str(log_update)))
    flog.write('Поставщиков без inn: ' + str(inn_null) + '\n')
    flog.write('Время окончания работы парсера: {0}\n\n\n'.format(str(datetime.datetime.now())))
    flog.close()


if __name__ == "__main__":
    main()
