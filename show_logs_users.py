import sys, time
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtSql import *
from PyQt5.QtCore import *


class Log_users(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        db = QSqlDatabase.addDatabase('QMYSQL')
        db.setDatabaseName('test')
        db.setHostName('localhost')
        db.setUserName('root')
        db.setPassword('1234')
        db.open()

        view = QTableView(self)
        model = QSqlQueryModel(self)
        model.setQuery('SELECT id FROM test')
        view.setModel(model)
        view.move(10, 10)
        view.resize(617, 315)

        # Buttons:
        button1 = QPushButton('Exit', self)
        button1.resize(button1.sizeHint())
        button1.move(50, 400)

        def But1Click():
            if db.close():
                print('close')
            else:
                print('db dont close')
                sys.exit()

        button1.clicked.connect(But1Click)

        # Window:
        self.setGeometry(300, 100, 650, 450)
        self.setWindowTitle('Icon')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Log_users()
    sys.exit(app.exec_())