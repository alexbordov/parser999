import logging
import smtplib
from email.header import Header
from email.mime.text import MIMEText
from email.utils import formataddr

from connect_to_db import connect_bd

DB = 'uto_users'
file_log = 'payments.log'
server = "smtp.gmail.com"
port = 587
user_name = "********"
user_passwd = "**********"
me = '*********'
post_m = "info@enter-it.ru"
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def sendemails(id_user_list: list):
    text = ''
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute(
            f"""SELECT login, end_date, user_name FROM user WHERE id_user IN ({', '.join(['%s']*len(id_user_list))})""",
            id_user_list)
    for u in cur.fetchall():
        text = f"""{text} Доступ для пользователя с логином:{u['login']} и ФИО:{u['user_name']} продлен до {u['end_date']}\n"""
    subj = 'Продление доступа к конфигурации'
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = post_m
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, post_m, msg.as_string())
    s.quit()


def main():
    users_update = []
    con = connect_bd(DB)
    cur = con.cursor()
    cur.execute(
            f'SELECT u.id_user AS id_u, r.id AS id_r, r.price AS price_r, r.name AS name_r, (SELECT SUM(p.balance_motion) FROM payment_history AS p WHERE p.id_user = u.id_user) AS pay_sum , (end_date = DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)) AS chk FROM user AS u LEFT JOIN rate_type AS r ON r.id = u.id_rate_type WHERE r.id <> 0 AND u.end_date < CURRENT_DATE()'
    )
    users = cur.fetchall()
    for u in users:
        tmp = -1 if u['pay_sum'] is None else u['pay_sum']
        if u['chk'] == 1:
            cur.execute(
                    f'INSERT INTO payment_history SET id_user = %s, date = CURRENT_DATE(), balance_motion = %s, info = %s',
                    (u['id_u'], -1 * u['price_r'], f'Списание денежных средств согласно тарифу {u["name_r"]}'))
            logging.info(f"insert NEW PAYMENT from user {u['id_u']}, price: {-1*int(u['price_r'])}")
            tmp = u['pay_sum'] - u['price_r']
        if tmp >= 0:
            cur.execute(f'UPDATE user SET end_date = DATE_ADD(CURRENT_DATE(), INTERVAL 12 MONTH) WHERE id_user = %s',
                        (u['id_u'],))
            logging.info(f"update END DATE for user {u['id_u']}")
            users_update.append(u['id_u'])
    cur.close()
    con.close()
    if users_update:
        try:
            sendemails(users_update)
        except Exception as ex:
            logging.error('Exception of type {0!s} in function sendmails(): {1}'.format(type(ex).__name__, ex))
    logging.info(f"Script exit success")


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logging.error('Exception of type {0!s} in function main(): {1}'.format(type(e).__name__, e))
