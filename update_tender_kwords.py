from connect_to_db import connect_bd
import re

suffix = ""
name_db = 'tender'


def add_tender_kwords(id_tender, con):
    cur = con.cursor()
    res_string = ''
    set_po = set()
    cur.execute("""SELECT DISTINCT po.name, po.okpd_name FROM purchase_object{0} AS po LEFT JOIN lot{0} AS l ON l.id_lot = po.id_lot
    WHERE l.id_tender=%s""".format(suffix), (id_tender,))
    while True:
        row = cur.fetchone()
        if not row:
            break
        string_name = row['name']
        string_name = string_name.strip()
        string_okpd_name = row['okpd_name']
        string_okpd_name = string_okpd_name.strip()
        res = "{0} {1} ".format(string_name, string_okpd_name)
        set_po.add(res)
    for s in set_po:
        res_string += s

    set_att = set()
    cur.execute("""SELECT file_name FROM attachment{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
    while True:
        row_1 = cur.fetchone()
        if not row_1:
            break
        sring_att = row_1['file_name']
        sring_att = sring_att.strip()
        res_a = ' ' + sring_att
        set_att.add(res_a)
    for n in set_att:
        res_string += n
    id_org = 0
    cur.execute("""SELECT purchase_object_info, id_organizer FROM tender{0} WHERE id_tender=%s""".format(suffix),
                (id_tender,))
    res_po = cur.fetchone()
    if res_po:
        id_org = res_po["id_organizer"]
        string_po = res_po['purchase_object_info']
        string_po = string_po.strip()
        res_string = string_po + ' ' + res_string
    if id_org:
        cur.execute("""SELECT full_name, inn FROM organizer{0} WHERE id_organizer=%s""".format(suffix), (id_org,))
        res_org = cur.fetchone()
        if res_org:
            res_string += " {0} {1}".format(res_org["inn"], res_org["full_name"])
    cur.execute(
            """SELECT DISTINCT cus.inn, cus.full_name FROM customer{0} AS cus LEFT JOIN purchase_object{0} AS po 
    ON cus.id_customer = po.id_customer LEFT JOIN lot{0} AS l ON l.id_lot = po.id_lot WHERE l.id_tender=%s""".format(
                    suffix), (id_tender,))
    res_cus = cur.fetchall()
    if res_cus:
        for cus in res_cus:
            res_string += " {0} {1}".format(cus['inn'], cus['full_name'])
    res_string = re.sub(r'\s+', ' ', res_string)
    res_string = res_string.strip()
    cur.execute("""UPDATE tender{0} SET tender_kwords=%s WHERE id_tender=%s""".format(suffix),
                (res_string, id_tender))
    cur.execute("""SELECT tender_kwords FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
    res_tk = cur.fetchone()
    if res_tk and (res_tk['tender_kwords'] is None):
        print('Не смогли обновить тендер с номером ' + id_tender)
    cur.close()


con = connect_bd(name_db)
cur2 = con.cursor()
cur2.execute("""SELECT id_tender FROM tender{0} WHERE cancel = 0""".format(suffix))
while True:
    res_id_tender = cur2.fetchone()
    if not res_id_tender:
        break

    add_tender_kwords(res_id_tender['id_tender'], con)
con.close()
