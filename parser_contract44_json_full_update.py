#!/usr/bin/python3.5
import json
import pymysql
import datetime
import zipfile
import os
from urllib.error import HTTPError
import logging
import requests
import urllib.request, urllib.error


file_log = './log_contracts44/contracts44_' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.DEBUG, filename=file_log, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def contract44(line):
    global log_add_contract
    global log_add_supplier
    global log_add_customer
    global log_add_product
    global log_update_contract
    global file_log
    string = str(line)
    if (string.find('[', 0, 1) != -1):
        string = string[1:]
    size = len(string)
    if (string.find(',', (size - 2), size) != -1):
        string = string[: -2]
    if (string.find(']', (size - 2), size) != -1):
        string = string[: -2]
    end_string = string.strip()
    contract = json.loads(end_string)
    con = pymysql.connect(host="localhost", user="tender", passwd="Dft56Point", db="tender", charset='utf8',
                          init_command='SET NAMES UTF8')
    cur = con.cursor(pymysql.cursors.DictCursor)
    # Инициализация переменных контрактов
    result_exist_contract = 0
    id_contract = ''
    # doc_name = ''
    # doc_url = ''
    # doc_description = ''
    p_number = ''
    current_contract_stage = ''
    file_version = ''
    placing = ''
    region_code = ''
    url = ''
    sign_date = '0000-00-00'
    single_customer_reason_code = ''
    single_customer_reason_name = ''
    fz = ''
    placing_way_code = ''
    notification_number = ''
    lot_number = 1
    contract_price = 0.0
    currency = ''
    version_number = 0
    execution_start_date = '0000-00-00'
    execution_end_date = '0000-00-00'
    id_customer = 0
    id_supplier = 0
    id_od_contract = 0
    name = ''
    okpd2_code = ''
    okpd2_group_code = 0
    okpd2_group_level1_code = ''
    price = 0.0
    okpd2_name = ''
    quantity = 0.0
    sum = 0.0
    sid = ''
    customer_regnumber = ''
    supplier_inn = ''
    okei = ''

    # Инициализация переменных customer
    # regnumber_custimer = ''
    inn_customer = ''
    kpp_customer = ''
    contracts_count_customer = 0
    contracts223_count_customer = 0
    contracts_sum_customer = 0.0
    contracts223_sum_customer = 0.0
    ogrn_customer = ''
    region_code_customer = ''
    full_name_customer = ''
    postal_address_customer = ''
    phone_customer = ''
    fax_customer = ''
    email_customer = ''
    contact_name_customer = ''

    # Инициализация переменных supplier
    kpp_supplier = ''
    contracts_count_supplier = 0
    contracts223_count_supplier = 0
    contracts_sum_supplier = 0.0
    contracts223_sum_supplier = 0.0
    ogrn_supplier = ''
    region_code_supplier = ''
    organizationname_supplier = ''
    postal_address_supplier = ''
    contactphone_supplier = ''
    contactfax_supplier = ''
    contactemail_supplier = ''
    contact_name_supplier = ''

    if 'id' in contract:
        id_contract = str(contract['id'])
        if not id_contract:
            #print('У контракта нет номера, пропускаем')
            cur.close()
            con.close()
            return True

    if 'number' in contract:
        p_number = str(contract['number'])
    if 'currentContractStage' in contract:
        current_contract_stage = str(contract['currentContractStage'])
    if 'fileVersion' in contract:
        file_version = str(contract['fileVersion'])
    if 'placing' in contract:
        placing = str(contract['placing'])
    if 'regionCode' in contract:
        region_code = str(contract['regionCode'])
    if 'contractUrl' in contract:
        url = str(contract['contractUrl'])
    if 'signDate' in contract:
        sign_date = str(contract['signDate'])
    if 'singleCustomerReason' in contract:
        single_customer_reason_object = contract['singleCustomerReason']
        if 'code' in single_customer_reason_object:
            single_customer_reason_code = str(single_customer_reason_object['code'])
        if 'name' in single_customer_reason_object:
            single_customer_reason_name = str(single_customer_reason_object['name'])
    if 'fz' in contract:
        fz = str(contract['fz'])
    if 'placingWayCode' in contract:
        placing_way_code = str(contract['placingWayCode'])
    if 'foundation' in contract:
        if 'fcsOrder' in contract['foundation']:
            if 'notificationNumber' in contract['foundation']['fcsOrder']:
                notification_number = str(contract['foundation']['fcsOrder']['notificationNumber'])
            if 'lotNumber' in contract['foundation']['fcsOrder']:
                lot_number = int(contract['foundation']['fcsOrder']['lotNumber'])
    if 'price' in contract:
        contract_price = float(contract['price'])
    if 'currency' in contract:
        if 'name' in contract['currency']:
            currency = str(contract['currency']['name'])
    if 'versionNumber' in contract:
        version_number = int(contract['versionNumber'])
    if 'execution' in contract:
        if 'startDate' in contract['execution']:
            execution_start_date = str(contract['execution']['startDate'])
        if 'endDate' in contract['execution']:
            execution_end_date = str(contract['execution']['endDate'])
    if 'customer' in contract:
        if 'regNum' in contract['customer']:
            customer_regnumber = str(contract['customer']['regNum'])
    if 'suppliers' in contract:
        if 'inn' in contract['suppliers'][0]:
            supplier_inn = str(contract['suppliers'][0]['inn'])
    if customer_regnumber:
        cur.execute("""SELECT id FROM od_customer WHERE regNumber = %s """, (customer_regnumber,))
        result_customer = cur.fetchone()
        if result_customer:
            id_customer = result_customer['id']
        if not id_customer:
            if 'kpp' in contract['customer']:
                kpp_customer = str(contract['customer']['kpp'])
            if 'fullName' in contract['customer']:
                full_name_customer = str(contract['customer']['fullName'])
            if 'inn' in contract['customer']:
                inn_customer = str(contract['customer']['inn'])
            if 'postalAddress' in contract['customer']:
                postal_address_customer = str(contract['customer']['postalAddress'])
            contracts_count_customer = 1
            contracts_sum_customer = contract_price
            query_add_customer = 'INSERT INTO od_customer SET regNumber = %s, inn = %s, kpp = %s, contracts_count = %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, full_name = %s, postal_address = %s, phone = %s, fax = %s, email = %s, contact_name = %s'
            value_add_customer = (customer_regnumber, inn_customer, kpp_customer, contracts_count_customer,
                                  contracts223_count_customer, contracts_sum_customer,
                                  contracts223_sum_customer, ogrn_customer, region_code_customer,
                                  full_name_customer, postal_address_customer, phone_customer, fax_customer,
                                  email_customer, contact_name_customer)
            result_add_customer = cur.execute(query_add_customer, value_add_customer)
            con.commit()
            id_customer = con.insert_id()
            if result_add_customer:
                log_add_customer += 1
                #print('Добавлен customer с regNumber {0}'.format(customer_regnumber))
            else:
                flog1 = open(file_log, 'a')
                flog1.write('Customer  с regNumber: ' + str(customer_regnumber) + ' не добавлен' + '\n')
                flog1.close()

    if supplier_inn:
        cur.execute("""SELECT id FROM od_supplier WHERE inn = %s """, (supplier_inn,))
        result_supplier = cur.fetchone()
        if result_supplier:
            id_supplier = result_supplier['id']
        if not id_supplier:
            if 'kpp' in contract['suppliers']:
                kpp_supplier = str(contract['suppliers']['kpp'])
            if 'contactPhone' in contract['suppliers']:
                contactphone_supplier = str(contract['suppliers']['contactPhone'])
            if 'contactEMail' in contract['suppliers']:
                contactemail_supplier = str(contract['suppliers']['contactEMail'])
            if 'organizationName' in contract['suppliers']:
                organizationname_supplier = str(contract['suppliers']['organizationName'])
            contracts_count_supplier = 1
            contracts_sum_supplier = contract_price
            query_add_supplier = 'INSERT INTO od_supplier SET  inn = %s, kpp = %s, contracts_count = %s, contracts223_count = %s, contracts_sum = %s, contracts223_sum = %s, ogrn = %s, region_code = %s, organizationName = %s, postal_address = %s, contactPhone = %s, contactFax = %s, contactEMail = %s, contact_name = %s'
            value_add_supplier = (
                supplier_inn, kpp_supplier, contracts_count_supplier, contracts223_count_supplier,
                contracts_sum_supplier, contracts223_sum_supplier, ogrn_supplier, region_code_supplier,
                organizationname_supplier, postal_address_supplier, contactphone_supplier, contactfax_supplier,
                contactemail_supplier, contact_name_supplier)
            result_add_supplier = cur.execute(query_add_supplier, value_add_supplier)
            con.commit()
            id_supplier = con.insert_id()
            if result_add_supplier:
                log_add_supplier += 1
                #print('Добавлен supplier с inn {0}'.format(supplier_inn))
            else:
                flog2 = open(file_log, 'a')
                flog2.write('Supplier  с inn: ' + str(supplier_inn) + ' не добавлен' + '\n')
                flog2.close()

    cur.execute("""SELECT * FROM od_contract WHERE id_contract = %s """, (id_contract,))
    con.commit()
    result_exist_contract = cur.fetchone()
    if result_exist_contract:
        cur.execute("""DELETE FROM od_contract_product WHERE id_od_contract = %s """, (id_contract,))
        query_add_contract = 'UPDATE od_contract SET  p_number = %s, current_contract_stage = %s, file_version = %s, placing = %s, region_code = %s, url = %s, sign_date = %s, single_customer_reason_code = %s, single_customer_reason_name = %s, fz = %s, placing_way_code = %s, notification_number = %s, lot_number = %s, contract_price = %s, currency = %s, version_number = %s, execution_start_date = %s, execution_end_date = %s, id_customer = %s, id_supplier = %s WHERE id_contract = %s'
        value_add_contract = (
            p_number, current_contract_stage, file_version, placing, region_code, url, sign_date,
            single_customer_reason_code, single_customer_reason_name, fz, placing_way_code, notification_number,
            lot_number, contract_price, currency, version_number, execution_start_date, execution_end_date,
            id_customer, id_supplier, id_contract)
        try:
            cur.execute(query_add_contract, value_add_contract)
            con.commit()
            log_update_contract += 1
        except Exception:
            flog3 = open(file_log, 'a')
            flog3.write('Contract  с id_contract: ' + str(id_contract) + ' не обновлен' + '\n')
            flog3.close()

    else:
        query_add_contract = 'INSERT INTO od_contract SET  id_contract = %s, p_number = %s, current_contract_stage = %s, file_version = %s, placing = %s, region_code = %s, url = %s, sign_date = %s, single_customer_reason_code = %s, single_customer_reason_name = %s, fz = %s, placing_way_code = %s, notification_number = %s, lot_number = %s, contract_price = %s, currency = %s, version_number = %s, execution_start_date = %s, execution_end_date = %s, id_customer = %s, id_supplier = %s'
        value_add_contract = (
            id_contract, p_number, current_contract_stage, file_version, placing, region_code, url, sign_date,
            single_customer_reason_code, single_customer_reason_name, fz, placing_way_code, notification_number,
            lot_number, contract_price, currency, version_number, execution_start_date, execution_end_date,
            id_customer, id_supplier)
        result_add_contract = cur.execute(query_add_contract, value_add_contract)
        con.commit()
        if result_add_contract:
            log_add_contract += 1
            # print('Добавлен contract с id_contract {0}'.format(id_contract))
        else:
            flog3 = open(file_log, 'a')
            flog3.write('Contract  с id_contract: ' + str(id_contract) + ' не добавлен' + '\n')
            flog3.close()

    # id_od_contract = con.insert_id()
    cur.execute("""SELECT * FROM od_contract WHERE id_contract = %s """, (id_contract,))
    result_id_contract = cur.fetchone()
    if result_id_contract:
        id_od_contract = result_id_contract['id']
    if id_od_contract:
        if 'products' in contract:
            products = contract['products']
            for lineprod in products:
                if 'name' in lineprod:
                    name = str(lineprod['name'])
                if 'OKPD2' in lineprod:
                    if 'code' in lineprod['OKPD2']:
                        okpd2_code = str(lineprod['OKPD2']['code'])
                        if len(okpd2_code) > 1:
                            dot1 = okpd2_code.find('.')
                            if dot1 == -1:
                                dot1 = None
                            okpd2_group_code_temp = okpd2_code[:dot1]
                            okpd2_group_code = okpd2_group_code_temp[:2]
                            if len(okpd2_code) > 3:
                                if dot1:
                                    okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
                            if len(okpd2_code) > 4:
                                if dot1:
                                    okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
                    if 'name' in lineprod['OKPD2']:
                        okpd2_name = str(lineprod['OKPD2']['name'])
                if 'price' in lineprod:
                    price = float(lineprod['price'])
                if 'quantity' in lineprod:
                    quantity = float(lineprod['quantity'])
                if 'sum' in lineprod:
                    sum = float(lineprod['sum'])
                if 'sid' in lineprod:
                    sid = str(lineprod['sid'])
                if 'OKEI' in lineprod:
                    if 'name' in lineprod['OKEI']:
                        okei = str(lineprod['OKEI']['name'])
                query_add_products = 'INSERT INTO od_contract_product SET  id_od_contract = %s, name = %s, okpd2_code = %s, okpd2_group_code = %s, okpd2_group_level1_code = %s, price = %s, okpd2_name = %s, quantity = %s, okei = %s, sum = %s, sid = %s'
                value_add_products = (
                    id_od_contract, name, okpd2_code, okpd2_group_code, okpd2_group_level1_code, price,
                    okpd2_name, quantity, okei, sum, sid)
                result_add_product = cur.execute(query_add_products, value_add_products)
                con.commit()
                if result_add_product:
                    log_add_product += 1
                    #print('Добавлен product для контракта id_od_contract {0}'.format(id_od_contract))
                else:
                    flog4 = open(file_log, 'a')
                    flog4.write('Product  для контракта id_contract: ' + str(
                        id_od_contract) + ' не добавлен' + '\n')
                    flog4.close()
    cur.close()
    con.close()

currentyear = datetime.datetime.strftime(datetime.date.today(), "%Y")
# print(int(currentyear))
range_month = ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12')
years_temp = int(currentyear)
while True:
    if years_temp >= 2015:
        for month in range_month:
            per = 0
            down_count = 10
            while down_count:
                try:
                    period = datetime.timedelta(days=per)
                    lastdate = datetime.date.today() - period
                    format_date = datetime.datetime.strftime(lastdate, "%Y%m%d")
                    url_contract = 'https://clearspending.ru/download/opendata/contracts_44fz_' + str(years_temp) + str(month) + '-' + format_date + '.json.zip'
                    #print(url_contract)
                    archive = 'contracts_44fz_' + str(years_temp) + str(month) + '-' + format_date + '.json.zip'
                    # urllib.request.urlretrieve(url_contract, archive)
                    f = urllib.request.urlopen(url_contract, timeout=900)
                    with open(archive, "wb") as code:
                        code.write(f.read())
                    break
                except urllib.error.HTTPError as httperr:
                    # print(httperr)
                    with open(file_log, 'a') as flog_err:
                        flog_err.write('Ошибка скачивания: ' + str(datetime.datetime.now()) + ' ' + str(httperr) + str(url_contract) + '\n')
                    per += 1
                    logging.exception("Ошибка cкачивания:")
                except urllib.error.URLError as httperr:
                    # print(httperr)
                    with open(file_log, 'a') as flog_err:
                        flog_err.write('Ошибка скачивания: ' + str(datetime.datetime.now()) + ' ' + str(httperr) + str(url_contract) + '\n')
                    logging.exception("Ошибка cкачивания:")
                except Exception as ex:
                    with open(file_log, 'a') as flog_err0:
                        flog_err0.write('Неизвестная ошибка: ' + str(datetime.datetime.now()) + ' ' + str(ex) + str(url_contract) + '\n')
                    logging.exception("Неизвестная ошибка:")
                down_count -= 1
                # print(down_count)
            if down_count == 0:
                with open(file_log, 'a') as flog_err1:
                    flog_err1.write('Не удалось получить архив за: ' + str(years_temp) + ' ' + str(month) + '\n')
                continue
            try:
                z = zipfile.ZipFile(archive, 'r')
                z.extractall()
                z.close()
            except Exception:
                with open(file_log, 'a') as flog_err1:
                    flog_err1.write('Не удалось распаковать архив: ' + str(years_temp) + ' ' + str(month) + '\n')
                continue

            file_name = 'contracts_44fz_' + str(years_temp) + str(month) + '-' + format_date + '.json'
            #print(file_name)

            flog = open(file_log, 'a')
            flog.write('Время начала работы парсера: ' + str(datetime.datetime.now()) + '\n')
            flog.close()
            log_add_contract = 0
            log_add_supplier = 0
            log_add_customer = 0
            log_add_product = 0
            log_update_contract = 0
            f = open(file_name, 'r')
            for line in f:
                try:
                    contract44(line)
                except Exception:
                    logging.exception("Ошибка: ")
            f.close()
            flog = open(file_log, 'a')
            flog.write('Время окончания работы парсера: ' + str(datetime.datetime.now()) + '\n')
            flog.write('Добавлено customer: ' + str(log_add_customer) + '\n')
            flog.write('Добавлено supplier: ' + str(log_add_supplier) + '\n')
            flog.write('Добавлено contract: ' + str(log_add_contract) + '\n')
            flog.write('Обновлено contract: ' + str(log_update_contract) + '\n')
            flog.write('Добавлено product: ' + str(log_add_product) + '\n')
            flog.write('Пропарсили архив: ' + str(file_name) + '\n\n\n')
            flog.close()
            os.remove(archive)
            os.remove(file_name)

        years_temp -= 1
        #print(years_temp)
    else:
        break
