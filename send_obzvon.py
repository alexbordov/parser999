import pymysql
import logging
import os
import random
import string
import hashlib
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import datetime
import time

count_sent = 0
file_log = './log_send_email/log_send_email_obzvon' + str(datetime.date.today()) + '.log'
logging.basicConfig(level=logging.INFO, filename=file_log, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def connect():
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db="tender", charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def passwd():
    length = 10
    chars = string.ascii_letters + string.digits + '!'
    random.seed = (os.urandom(1024))
    pwd = ''.join(random.choice(chars) for i in range(length))
    return pwd


def md(pw):
    return hashlib.md5(pw.encode('utf-8')).hexdigest()
# cell_list = sheet.range('B2:C22')
# for i in cell_list:
#     print(i.value)


def get_list_user():
    list_user = []
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db="partners_1c", charset='utf8',
                          init_command='SET NAMES UTF8')
    cur = con.cursor(pymysql.cursors.DictCursor)
    cur.execute("""SELECT * FROM obzvon WHERE 1 """)
    for i in cur.fetchall():
        list_user.append(i)
    cur.close()
    con.close()
    return list_user


def sendemails(email, name, passwrd):
    global count_sent
    me = 'info@enter-it.ru'
    you = email
    text = """<div class='pre'>Здравствуйте"""
    text += """!</div>
                <div class='pre'><br />Не так давно с Вами связывались операторы call-центра и Вы выразили заинтресованность в конфигурации "Рабочее место специалиста по тендерам".</div>
                <div class='pre'><br /> Спасибо за проявленный интерес к нашему решению. По Вашему запросу Вам будет предоставлен демонстрационный доступ к сервису на срок 3 месяца.</div>
                <div class='pre'>&nbsp;</div>
                <div class='pre'>Узнать информацию об использования программного продукта Вы можете по следующей ссылке: <a href='http://enter-it.ru/oferta.pdf'>http://enter-it.ru/oferta.pdf</a><br /> <br /></div>
                <div class='pre'>Скачать последнюю версию конфигурации Вы можете по ссылке:  <a href='http://enter-it.ru/distributives/tenders/get_1c_conf.php'>Конфигурация "Рабочее место специалиста по тендерам" и Libre_Office</a> <br /> <br /> <br /> Данные для авторизации:</div>
                <div class='pre'>
                <table style='height: 76px;' width='299'>
                <tbody>
                <tr>
                <td>Ваш Логин:</td>
                <td><a href='mailto:'"""
    text += email
    text += """'>"""
    text += email
    text += """</a></td>
                </tr>
                <tr>
                <td>Ваш Пароль:</td>
                <td>"""
    text += passwrd
    text += """</td>
                </tr>
                <tr>
                <td>Адрес сервера:</td>
                <td>tenders.enter-it.ru</td>
                </tr>
                </tbody>
                </table>
                <br /> <br />&nbsp;
                <div class='pre'>Инструкции по установке и обновлению конфигурации Вы можете скачать по ссылкам ниже:</p>
                <div class='pre'><a href="http://enter-it.ru/distributives/documents/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20%D0%BE%D0%B1%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8E.docx">Инструкция по обновлению конфигурации</a></div>
                <div class='pre'><a href="http://enter-it.ru/distributives/documents/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B5%20%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D0%B8.docx">Инструкция по установке конфигурации</a></div>
                <div class='pre'>Спасибо что выбрали нас. <br />Связаться с нами можно по телефону: 8 (900) 365 55 40 или через e-mail <a href='mailto:'info@enter-it.ru'> info@enter-it.ru</a> <br /> <br /> <br /> </div>
                </div>"""
    subj = 'Демонстрационный доступ к 1С Предприятие: "Рабочее место специалиста по тендерам"'
    server = "smtp.jino.ru"
    port = 25
    user_name = "info@enter-it.ru"
    user_passwd = "*******"
    msg = MIMEText(text.encode('utf-8'), "html", "utf-8")
    msg['Subject'] = subj
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), me))
    msg['To'] = you
    s = smtplib.SMTP(server, port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user_name, user_passwd)
    s.sendmail(me, you, msg.as_string())
    s.sendmail(me, me, msg.as_string())
    count_sent += 1
    s.quit()


for n in get_list_user():
    conn = connect()
    cur = conn.cursor(pymysql.cursors.DictCursor)
    cur.execute("""SELECT * FROM user WHERE login = %s """, (n['email'],))
    if cur.fetchone():
        cur.close()
        conn.close()
        continue
    password = passwd()
    hash = md(password)
    period = datetime.timedelta(days=90)
    nextdate = datetime.date.today() + period
    data_expired = str(nextdate)
    #print(password, hash, n['mail'], n['usr'])
    try:
        sendemails(n['email'], n['name'], password)
        time.sleep(1)
        print(n['email'], n['name'], password, hash, data_expired)
        with open(file_log, 'a') as flog1:
            flog1.write('Письмо отправлено на адрес: ' + str(n['email']) + '\n\n')
    except Exception:
        logging.exception('Ошибка отправки письма: ')
        with open(file_log, 'a') as flog:
            flog.write('Ошибка отправки письма на адрес: ' + str(n['email']) + '\n\n')
    else:
        try:
            query_add = 'INSERT INTO user SET login = %s, hash_password = %s, password = %s, end_date = %s'
            query_value = (n['email'], hash, password, data_expired)
            cur.execute(query_add, query_value)
            cur.close()
            conn.close()
            with open(file_log, 'a') as flog3:
                flog3.write('Данные в базу записаны по пользователю: ' + str(n['email']) + '\n\n')
        except Exception:
            logging.exception('Ошибка записи в базу данных: ')
            with open(file_log, 'a') as flog4:
                flog4.write('Ошибка записи в базу по пользователю: ' + str(n['email']) + '\n\n')

if count_sent == 0:
    with open(file_log, 'a') as flog2:
        flog2.write('Новых пользователей не было ' + '\n\n')
