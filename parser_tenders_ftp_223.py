import datetime
import ftplib
import logging
import os
import shutil
import sys
import time
import xml
import zipfile
from warnings import filterwarnings
import pymysql
import timeout_decorator
import xmltodict
import parser_tenders223
from connect_to_db import connect_bd

filterwarnings('ignore', category=pymysql.Warning)
temp_dir = 'temp_tenders223'
log_dir = 'log_tenders223'
suffix = ''
name_db = 'tender'
file_log = './{1}/tenders223_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
purchase_dir = ['purchaseNotice', 'purchaseNoticeAE', 'purchaseNoticeAE94', 'purchaseNoticeEP',
                'purchaseNoticeIS', 'purchaseNoticeOA', 'purchaseNoticeOK', 'purchaseNoticeZK']


def check_inn():
    con_ch = connect_bd(name_db)
    cur_ch = con_ch.cursor()
    cur_ch1 = con_ch.cursor()
    cur_ch2 = con_ch.cursor()
    cur_ch.execute("""SELECT * FROM customer{0} WHERE inn = ''""".format(suffix))
    while True:
        res_cus_null = cur_ch.fetchone()
        if res_cus_null is None:
            break
        cur_ch1.execute("""SELECT * FROM organizer{0} WHERE reg_num =%s""".format(suffix), (res_cus_null['reg_num'],))
        res_reg_num = cur_ch1.fetchone()
        if res_reg_num:
            cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                            (res_reg_num['inn'], res_reg_num['reg_num']))
        else:
            cur_ch1.execute("""SELECT * FROM od_customer WHERE regNumber =%s""".format(suffix),
                            (res_cus_null['reg_num'],))
            res_reg_num_od = cur_ch1.fetchone()
            if res_reg_num_od:
                cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                                (res_reg_num_od['inn'], res_reg_num_od['regNumber']))
    cur_ch.close()
    cur_ch1.close()
    cur_ch2.close()
    con_ch.close()


def get_xml_to_dict(filexml, dirxml, region, reg_id, purchase):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    file_size = os.path.getsize(path_xml)
    if file_size == 0:
        # with open(file_log, 'a') as flog:
        #     flog.write('Размер файла 0 байт' + ' ' + path_xml + '\n\n')
        return
    fd = open(path_xml, 'r')
    s = fd.read()
    fd.close()
    try:
        doc = xmltodict.parse(s)
        parser_tenders223.parser(doc, path_xml, filexml, region, reg_id, purchase)
    except xml.parsers.expat.ExpatError as ex2:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь, нечитаемые символы ' + str(ex2) + ' ' + path_xml + '\n\n\n')
        str_r = s.replace('', '')
        f = open(path_xml, 'w')
        f.write(str_r)
        f.close()
        with open(path_xml) as fdr:
            try:
                doc = xmltodict.parse(fdr.read())
                parser_tenders223.parser(doc, path_xml, filexml, region, reg_id, purchase)
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Удалось пропарсить архив после удаления нечитаемых символов ' + ' ' + path_xml + '\n\n\n')
            except Exception as exs:
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка конвертации в словарь, удаление нечитаемых символов не помогло ' + str(exs) + ' '
                            + path_xml + '\n\n\n')
    except Exception as ex:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь ' + str(ex) + ' ' + path_xml + '\n\n\n')


def bolter(file, l_dir, region, reg_id, purchase):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    file_lower = file.lower()
    if not file_lower.endswith('.xml'):
        return
    try:
        get_xml_to_dict(file, l_dir, region, reg_id, purchase)
    except Exception as exppars:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Не удалось пропарсить файл ' + str(exppars) + ' ' + file + '\n')


@timeout_decorator.timeout(300)
def down_timeout(m, path_parse1):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'fz223free'
    password = 'fz223free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse1)
    local_f = './{1}/{0}'.format(str(m), temp_dir)
    lf = open(local_f, 'wb')
    ftp2.retrbinary('RETR ' + str(m), lf.write)
    lf.close()
    ftp2.close()
    return local_f


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            lf = down_timeout(m, path_parse1)
            retry = False
            if count > 0:
                with open(file_log, 'a') as flog:
                    flog.write('Удалось скачать архив после попытки ' + str(count) + ' ' + m + '\n')
            return lf
        except Exception as ex:
            # print('Не удалось скачать архив ' + str(ex) + ' ' + m)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив ' + str(ex) + ' ' + m + '\n')
            if count > 100:
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось скачать архив за 100 попыток ' + str(ex) + ' ' + m + '\n')
                return 0
            count += 1
            time.sleep(5)


def get_list_ftp_last(path_parse, region, purchase):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'fz223free'
    password = 'fz223free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    try:
        ftp2.cwd(path_parse)
    except Exception:
        return []
    data = ftp2.nlst()
    array_ar = []
    search2016 = purchase + '_' + region + '_2016'
    search2017 = purchase + '_' + region + '_2017'
    for i in data:
        if i.find(search2016) != -1 or i.find(search2017) != -1:
            array_ar.append(i)

    return array_ar


def get_list_ftp_daily(path_parse, region, purchase):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'fz223free'
    password = 'fz223free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    try:
        ftp2.cwd(path_parse)
    except Exception:
        return []
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(name_db)
    cur_arhiv = con_arhiv.cursor()
    search2016 = purchase + '_' + region + '_2016'
    search2017 = purchase + '_' + region + '_2017'
    for i in data:
        if i.find(search2016) != -1 or i.find(search2017) != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_tenders223{0} WHERE arhiv = %s AND region = %s""".format(suffix),
                              (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_tenders223{0} SET arhiv = %s, region = %s""".format(suffix)
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    ftp2.close()
    return array_ar


def extract_tend(m, path_parse1, region, region_id, purchase):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            print('Не удалось извлечь архив ' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
            except Exception as ear:
                print('Не удалось извлечь архив альтернативным методом')
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            print('Не удалось извлечь архив ' + str(e) + ' ' + l)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flogc:
                flogc.write('Не удалось извлечь архив {0} {1}\n'.format(str(e), l))
            return

        try:
            file_list = os.listdir(l_dir)
        except Exception as ex:
            print('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir + '\n')
        else:
            for f in file_list:
                bolter(f, l_dir, region, region_id, purchase)
        os.remove(l)
        shutil.rmtree(l_dir, ignore_errors=True)


def main():
    with open(file_log, 'a') as flog:
        flog.write('Время начала работы парсера: {0}\n\n\n'.format(datetime.datetime.now()))
    if len(sys.argv) == 1:
        print('Недостаточно параметров для запуска, используйте daily или last в качестве параметра')
        exit()
    shutil.rmtree(temp_dir, ignore_errors=True)
    os.mkdir(temp_dir)

    # забираем список регионов из базы данных
    con_region = connect_bd(name_db)
    cur_region = con_region.cursor()
    cur_region.execute("""SELECT * FROM region""")
    path_array = cur_region.fetchall()
    cur_region.close()
    con_region.close()
    for reg in path_array:
        for purchase in purchase_dir:
            try:
                if str(sys.argv[1]) == 'daily':
                    path_parse = '/out/published/' + reg['path223'] + '/' + purchase + '/daily'
                    arr_tenders = get_list_ftp_daily(path_parse, reg['path223'], purchase)
                elif str(sys.argv[1]) == 'last':
                    path_parse = '/out/published/' + reg['path223'] + '/' + purchase
                    arr_tenders = get_list_ftp_last(path_parse, reg['path223'], purchase)
                else:
                    print('Неверное имя параметра, используйте daily или last в качестве параметра')
                    arr_tenders = []
                    exit()
                if not arr_tenders:
                    with open(file_log, 'a') as flog:
                        flog.write('Получен пустой список архивов ' + path_parse + ' ' + '\n')
                for j in arr_tenders:
                    try:
                        extract_tend(j, path_parse, reg['conf'], reg['id'], purchase)
                    except Exception as exc:
                        # print('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j)
                        logging.exception("Ошибка: ")
                        with open(file_log, 'a') as flog:
                            flog.write('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j + '\n')
                        continue

            except Exception as ex:
                # print('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse)
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse + '\n')
                continue
    try:
        check_inn()
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка в функции check_inn ' + str(e) + ' ' + '\n')
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили Tender223 {0} \n'.format(str(parser_tenders223.Tender223.count_add_tender)))
        flog.write('Время окончания работы парсера: {0}\n\n\n'.format(datetime.datetime.now()))


if __name__ == "__main__":
    main()
