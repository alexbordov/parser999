import datetime
import smtplib
import time
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.utils import formataddr
import subprocess
import psutil
import requests
import telebot

user_name = '***************'
user_passwd = '****************'
port = 25
SINGLE_RUN = True
BOT_TOKEN = '************'
CHANNEL_NAME_SRV = '@*****************'
bot = telebot.TeleBot(BOT_TOKEN)


def send_to_bitrix(title, message):
    url = '*******************************'
    query_data = {'POST_MESSAGE': message, 'POST_TITLE': title}
    r = ""
    try:
        r = requests.post(url, data=query_data)
    except Exception as e:
        print(f'Ошибка отправки сообщения в битрикс: {str(e)} {r}\n\n')


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return f"{num:.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} {'Yi'}{suffix}"


def send_mail_uptime(send_from, send_to, server="mail.enter-it.ru"):
    msg = MIMEMultipart()
    msg['From'] = formataddr((str(Header('ООО Энтер Групп', 'utf-8')), send_from))
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    dsk = psutil.disk_usage('/mnt/hdd01')
    free_space = sizeof_fmt(dsk.free)
    msg['Subject'] = f'Сервер был перезагружен!!!'
    tm = time.time() - psutil.boot_time()
    text = f"""Сервер работает без перезагрузки уже {datetime.timedelta(seconds=tm)}\n\nСвободного места на диске {
    free_space} """
    msg.attach(MIMEText(text))
    smtp = smtplib.SMTP(server, port)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(user_name, user_passwd)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def main():
    try:
        send_mail_uptime('info@enter-it.ru', ['rummolprod999@gmail.com', '*************'])
    except Exception:
        pass
    try:
        bot.send_message(CHANNEL_NAME_SRV, "Сервер был перезагружен")
    except Exception:
        pass
    try:
        send_to_bitrix(f"Сервер был перезагружен в {datetime.datetime.today()}",
                       "Внимание, сервер был перезагружен, проверьте причины")
    except Exception:
        pass


if __name__ == '__main__':
    try:
        subprocess.Popen(['/usr/bin/Xvfb', ':1 -screen 0 640x480x24 -fbdir /var/tmp&'],
                         shell=False)
        subprocess.Popen(['export DISPLAY=:1.0'],
                         shell=False)
    except Exception as e:
        print(e)
    main()
