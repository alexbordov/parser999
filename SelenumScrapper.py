import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Firefox()
driver.wait = WebDriverWait(driver, 10)
driver.get("http://etprf.ru/BRNotification")
for i in range(10):
    button = driver.wait.until(EC.visibility_of_all_elements_located(
            (By.PARTIAL_LINK_TEXT, "Вперед")))

    # time.sleep(3)
    try:
        button.click()
    except Exception:
        button = driver.wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "Вперед")))
        button.click()

text = driver.page_source
print(text)
time.sleep(50)
driver.quit()