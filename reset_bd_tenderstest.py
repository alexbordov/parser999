import pymysql


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con

con = connect_bd('tender')
cur = con.cursor(pymysql.cursors.DictCursor)

cur.execute("""TRUNCATE TABLE arhiv_tenders_new""")
cur.execute("""TRUNCATE TABLE arhiv_tenders223_new""")
cur.execute("""TRUNCATE TABLE attachment_new""")
cur.execute("""TRUNCATE TABLE contract_sign_new""")
cur.execute("""TRUNCATE TABLE customer_new""")
cur.execute("""TRUNCATE TABLE customer_requirement_new""")
cur.execute("""TRUNCATE TABLE etp_new""")
cur.execute("""TRUNCATE TABLE lot_new""")
cur.execute("""TRUNCATE TABLE organizer_new""")
cur.execute("""TRUNCATE TABLE placing_way_new""")
cur.execute("""TRUNCATE TABLE purchase_object_new""")
cur.execute("""TRUNCATE TABLE requirement_new""")
cur.execute("""TRUNCATE TABLE restricts_new""")
cur.execute("""TRUNCATE TABLE supplier_new""")
cur.execute("""TRUNCATE TABLE tender_new""")
cur.execute("""TRUNCATE TABLE preferense_new""")
cur.execute("""TRUNCATE TABLE customer223_new""")
cur.execute("""TRUNCATE TABLE auction_applications_new""")
cur.execute("""TRUNCATE TABLE auction_end_applications_new""")
cur.execute("""TRUNCATE TABLE auction_end_protocol_new""")
cur.execute("""TRUNCATE TABLE auction_participant_new""")
cur.execute("""TRUNCATE TABLE auction_protocol_new""")

cur.execute("""ALTER TABLE arhiv_tenders_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE arhiv_tenders223_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE attachment_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE contract_sign_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE customer_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE customer_requirement_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE etp_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE lot_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE organizer_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE placing_way_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE purchase_object_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE requirement_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE restricts_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE supplier_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE tender_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE preferense_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE customer223_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_applications_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_end_applications_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_end_protocol_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_participant_new AUTO_INCREMENT = 1""")
cur.execute("""ALTER TABLE auction_protocol_new AUTO_INCREMENT = 1""")

cur.close()
con.close()
print('Готово')
