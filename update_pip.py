# coding=utf-8
import pip
from subprocess import call

for dist in pip.get_installed_distributions():
    call("pip3 install --upgrade " + dist.project_name, shell=True)

#$ python3.6 -m pip install pip-review
#$ pip-review --local --interactive
