import datetime
import logging
import dateutil.parser
import pymysql
from warnings import filterwarnings

filterwarnings('ignore', category=pymysql.Warning)
suffix = '_new'
name_db = 'tender'
log_dir = 'log_tenders44_dev'
# warnings.filterwarnings('error', category=Exception)
file_log = './{1}/tenders_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

type_ten_44 = ('EA44_', 'EP44_', 'OK44_', 'OKD44_', 'OKU44_', 'PO44_', 'ZA44_', 'ZK44_',
               'ZKB44_', 'ZKK44_', 'ZKKD44_', 'ZKKU44_', 'ZP44_', 'ProtocolZKBI_')
type_cancel = ('NotificationCancel_',)
type_cancel_failure = ('CancelFailure_',)
type_sign = ('ContractSign_',)
type_prolongation = ('Prolongation',)
type_datechange = ('DateChange_',)
type_orgchange = ('OrgChange_',)
type_lotcancel = ('LotCancel_',)

if __name__ == "__main__":
    print('Привет, этот файл только для импортирования!')


def connect_bd(baza):
    """
    :param baza: получаем базу для подключения
    :return: возвращаем объект соединения
    """
    con = pymysql.connect(host="localhost", user="test", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8')
    con.autocommit(True)
    return con


def generator_univ(c):
    if type(c) == list:
        for i in c:
            yield i
    else:
        yield c


class Tender44:
    count_add_tender = 0

    def __init__(self, tender44):
        self.tender44 = 0
        tender_root = list(tender44.keys())
        tender_child = tender44[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender44 = tender_child[i]

    @property
    def id_t(self):
        id_ten = ''
        if 'id' in self.tender44:
            id_ten = self.tender44['id']
        elif 'oos:id' in self.tender44:
            id_ten = self.tender44['oos:id']
        if id_ten is None:
            id_ten = ''
        return id_ten

    @property
    def docPublishDate(self):
        docPublishDate = ''
        if 'docPublishDate' in self.tender44:
            docPublishDate = self.tender44['docPublishDate']
        elif 'oos:docPublishDate' in self.tender44:
            docPublishDate = self.tender44['oos:docPublishDate']
        if docPublishDate is None:
            docPublishDate = ''
        return docPublishDate

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender44:
            purchaseNumber = self.tender44['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender44:
            purchaseNumber = self.tender44['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        purchaseNumber = purchaseNumber.strip()
        return purchaseNumber

    @property
    def href(self):
        href = ''
        if 'href' in self.tender44:
            href = self.tender44['href']
        elif 'oos:href' in self.tender44:
            href = self.tender44['oos:href']
        if href is None:
            href = ''
        return href

    @property
    def printform(self):
        printform = ''
        try:
            printform = self.tender44['printForm']['url']
        except Exception:
            try:
                printform = self.tender44['oos:printForm']['url']
            except Exception:
                printform = ''
        if printform is None:
            printform = ''
        if printform and printform.find('CDATA') != -1:
            printform = printform[9:-3]
        return printform

    @property
    def purchaseObjectInfo(self):
        purchaseObjectInfo = ''
        if 'purchaseObjectInfo' in self.tender44:
            purchaseObjectInfo = self.tender44['purchaseObjectInfo']
        elif 'oos:purchaseObjectInfo' in self.tender44:
            purchaseObjectInfo = self.tender44['oos:purchaseObjectInfo']
        if purchaseObjectInfo is None:
            purchaseObjectInfo = ''
        return purchaseObjectInfo

    @property
    def organizer_reg_num(self):
        try:
            organizer_reg_num = self.tender44['purchaseResponsible']['responsibleOrg']['regNum']
        except Exception:
            try:
                organizer_reg_num = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg']['oos:regNum']
            except Exception:
                organizer_reg_num = ''
        if organizer_reg_num is None:
            organizer_reg_num = ''
        organizer_reg_num = organizer_reg_num.strip()
        return organizer_reg_num

    @property
    def organizer_full_name(self):
        try:
            organizer_full_name = self.tender44['purchaseResponsible']['responsibleOrg']['fullName']
        except Exception:
            try:
                organizer_full_name = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg']['oos:fullName']
            except Exception:
                organizer_full_name = ''
        if organizer_full_name is None:
            organizer_full_name = ''
        return organizer_full_name

    @property
    def organizer_post_address(self):
        try:
            organizer_post_address = self.tender44['purchaseResponsible']['responsibleOrg']['postAddress']
        except Exception:
            try:
                organizer_post_address = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg'][
                    'oos:postAddress']
            except Exception:
                organizer_post_address = ''
        if organizer_post_address is None:
            organizer_post_address = ''
        return organizer_post_address

    @property
    def organizer_fact_address(self):
        try:
            organizer_fact_address = self.tender44['purchaseResponsible']['responsibleOrg']['factAddress']
        except Exception:
            try:
                organizer_fact_address = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg'][
                    'oos:factAddress']
            except Exception:
                organizer_fact_address = ''
        if organizer_fact_address is None:
            organizer_fact_address = ''
        return organizer_fact_address

    @property
    def organizer_inn(self):
        try:
            organizer_inn = self.tender44['purchaseResponsible']['responsibleOrg']['INN']
        except Exception:
            try:
                organizer_inn = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg']['oos:INN']
            except Exception:
                organizer_inn = ''
        if organizer_inn is None:
            organizer_inn = ''
        return organizer_inn

    @property
    def organizer_kpp(self):
        try:
            organizer_kpp = self.tender44['purchaseResponsible']['responsibleOrg']['KPP']
        except Exception:
            try:
                organizer_kpp = self.tender44['oos:purchaseResponsible']['oos:responsibleOrg']['oos:KPP']
            except Exception:
                organizer_kpp = ''
        if organizer_kpp is None:
            organizer_kpp = ''
        return organizer_kpp

    @property
    def organizer_responsible_role(self):
        try:
            organizer_responsible_role = self.tender44['purchaseResponsible']['responsibleRole']
        except Exception:
            try:
                organizer_responsible_role = self.tender44['oos:purchaseResponsible']['oos:responsibleRole']
            except Exception:
                organizer_responsible_role = ''
        if organizer_responsible_role is None:
            organizer_responsible_role = ''
        return organizer_responsible_role

    @property
    def organizer_last_name(self):
        try:
            organizer_last_name = self.tender44['purchaseResponsible']['responsibleInfo']['contactPerson']['lastName']
        except Exception:
            try:
                organizer_last_name = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactPerson']['oos:lastName']
            except Exception:
                organizer_last_name = ''
        if organizer_last_name is None:
            organizer_last_name = ''
        return organizer_last_name

    @property
    def organizer_first_name(self):
        try:
            organizer_first_name = self.tender44['purchaseResponsible']['responsibleInfo']['contactPerson']['firstName']
        except Exception:
            try:
                organizer_first_name = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactPerson'][
                        'oos:firstName']
            except Exception:
                organizer_first_name = ''
        if organizer_first_name is None:
            organizer_first_name = ''
        return organizer_first_name

    @property
    def organizer_middle_name(self):
        try:
            organizer_middle_name = self.tender44['purchaseResponsible']['responsibleInfo']['contactPerson'][
                'middleName']
        except Exception:
            try:
                organizer_middle_name = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactPerson'][
                        'oos:middleName']
            except Exception:
                organizer_middle_name = ''
        if organizer_middle_name is None:
            organizer_middle_name = ''
        return organizer_middle_name

    @property
    def organizer_email(self):
        try:
            organizer_email = self.tender44['purchaseResponsible']['responsibleInfo']['contactEMail']
        except Exception:
            try:
                organizer_email = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactEMail']
            except Exception:
                organizer_email = ''
        if organizer_email is None:
            organizer_email = ''
        return organizer_email

    @property
    def organizer_fax(self):
        try:
            organizer_fax = self.tender44['purchaseResponsible']['responsibleInfo']['contactFax']
        except Exception:
            try:
                organizer_fax = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactFax']
            except Exception:
                organizer_fax = ''
        if organizer_fax is None:
            organizer_fax = ''
        return organizer_fax

    @property
    def organizer_phone(self):
        try:
            organizer_phone = self.tender44['purchaseResponsible']['responsibleInfo']['contactPhone']
        except Exception:
            try:
                organizer_phone = \
                    self.tender44['oos:purchaseResponsible']['oos:responsibleInfo']['oos:contactPhone']
            except Exception:
                organizer_phone = ''
        if organizer_phone is None:
            organizer_phone = ''
        return organizer_phone

    @property
    def placingWay_code(self):
        try:
            placingWay_code = self.tender44['placingWay']['code']
        except Exception:
            try:
                placingWay_code = \
                    self.tender44['oos:placingWay']['oos:code']
            except Exception:
                placingWay_code = ''
        if placingWay_code is None:
            placingWay_code = ''
        return placingWay_code

    @property
    def placingWay_name(self):
        try:
            placingWay_name = self.tender44['placingWay']['name']
        except Exception:
            try:
                placingWay_name = \
                    self.tender44['oos:placingWay']['oos:name']
            except Exception:
                placingWay_name = ''
        if placingWay_name is None:
            placingWay_name = ''
        return placingWay_name

    @property
    def ETP_code(self):
        try:
            ETP_code = self.tender44['ETP']['code']
        except Exception:
            try:
                ETP_code = \
                    self.tender44['oos:ETP']['oos:code']
            except Exception:
                ETP_code = ''
        if ETP_code is None:
            ETP_code = ''
        return ETP_code

    @property
    def ETP_name(self):
        try:
            ETP_name = self.tender44['ETP']['name']
        except Exception:
            try:
                ETP_name = \
                    self.tender44['oos:ETP']['oos:name']
            except Exception:
                ETP_name = ''
        if ETP_name is None:
            ETP_name = ''
        return ETP_name

    @property
    def ETP_url(self):
        try:
            ETP_url = self.tender44['ETP']['url']
        except Exception:
            try:
                ETP_url = \
                    self.tender44['oos:ETP']['oos:url']
            except Exception:
                ETP_url = ''
        if ETP_url is None:
            ETP_url = ''
        return ETP_url

    def get_attachments(self):
        if 'attachments' in self.tender44:
            if self.tender44['attachments'] is None:
                return []
            elif 'attachment' in self.tender44['attachments']:
                return generator_univ(self.tender44['attachments']['attachment'])
            else:
                return []
        elif 'oos:attachments' in self.tender44:
            if self.tender44['oos:attachments'] is None:
                return []
            elif 'oos:attachment' in self.tender44['oos:attachments']:
                return generator_univ(self.tender44['oos:attachments']['oos:attachment'])
            else:
                return []
        else:
            return []

    def attach_name(self, attach):
        try:
            attach_name = attach['fileName']
        except Exception:
            try:
                attach_name = \
                    attach['oos:fileName']
            except Exception:
                attach_name = ''
        if attach_name is None:
            attach_name = ''
        return attach_name

    def attach_description(self, attach):
        try:
            attach_description = attach['docDescription']
        except Exception:
            try:
                attach_description = \
                    attach['oos:docDescription']
            except Exception:
                attach_description = ''
        if attach_description is None:
            attach_description = ''
        return attach_description

    def attach_url(self, attach):
        try:
            attach_url = attach['url']
        except Exception:
            try:
                attach_url = \
                    attach['oos:url']
            except Exception:
                attach_url = ''
        if attach_url is None:
            attach_url = ''
        return attach_url

    @property
    def end_date(self):
        try:
            end_date = self.tender44['procedureInfo']['collecting']['endDate']
        except Exception:
            try:
                end_date = \
                    self.tender44['oos:procedureInfo']['oos:collecting']['oos:endDate']
            except Exception:
                end_date = ''
        if end_date is None:
            end_date = ''
        return end_date

    @property
    def scoring_date(self):
        try:
            scoring_date = self.tender44['procedureInfo']['scoring']['date']
        except Exception:
            try:
                scoring_date = \
                    self.tender44['oos:procedureInfo']['oos:scoring']['oos:date']
            except Exception:
                scoring_date = ''
        if scoring_date is None:
            scoring_date = ''
        return scoring_date

    @property
    def bidding_date(self):
        try:
            bidding_date = self.tender44['procedureInfo']['bidding']['date']
        except Exception:
            try:
                bidding_date = \
                    self.tender44['oos:procedureInfo']['oos:bidding']['oos:date']
            except Exception:
                bidding_date = ''
        if bidding_date is None:
            bidding_date = ''
        return bidding_date

    def get_lots(self):
        if 'lots' in self.tender44:
            if self.tender44['lots'] is None:
                return []
            elif 'lot' in self.tender44['lots']:
                return generator_univ(self.tender44['lots']['lot'])
            else:
                return []
        elif 'lot' in self.tender44:
            if self.tender44['lot'] is None:
                return []
            else:
                return generator_univ(self.tender44['lot'])
        elif 'oos:lots' in self.tender44:
            if self.tender44['oos:lots'] is None:
                return []
            elif 'oos:lot' in self.tender44['oos:lots']:
                return generator_univ(self.tender44['oos:lots']['oos:lot'])
            else:
                return []
        elif 'oos:lot' in self.tender44:
            if self.tender44['oos:lot'] is None:
                return []
            else:
                return generator_univ(self.tender44['oos:lot'])
        else:
            return []

    def lot_max_price(self, lot):
        try:
            lot_max_price = lot['maxPrice']
        except Exception:
            try:
                lot_max_price = \
                    lot['oos:maxPrice']
            except Exception:
                lot_max_price = 0.0
        if lot_max_price is None:
            lot_max_price = 0.0
        return lot_max_price

    def lot_currency(self, lot):
        try:
            lot_currency = lot['currency']['name']
        except Exception:
            try:
                lot_currency = \
                    lot['oos:currency']['oos:name']
            except Exception:
                lot_currency = ''
        if lot_currency is None:
            lot_currency = ''
        return lot_currency

    def lot_finance_source(self, lot):
        try:
            lot_finance_source = lot['financeSource']
        except Exception:
            try:
                lot_finance_source = \
                    lot['oos:financeSource']
            except Exception:
                lot_finance_source = ''
        if lot_finance_source is None:
            lot_finance_source = ''
        return lot_finance_source

    def get_customerRequirements(self, lot):
        if 'customerRequirements' in lot:
            if lot['customerRequirements'] is None:
                return []
            elif 'customerRequirement' in lot['customerRequirements']:
                return generator_univ(lot['customerRequirements']['customerRequirement'])
            else:
                return []
        elif 'oos:customerRequirements' in lot:
            if lot['oos:customerRequirements'] is None:
                return []
            elif 'oos:customerRequirement' in lot['oos:customerRequirements']:
                return generator_univ(lot['oos:customerRequirements']['oos:customerRequirement'])
            else:
                return []
        else:
            return []

    def kladr_place(self, customerRequirement):
        try:
            kladr_place = customerRequirement['kladrPlaces']['kladrPlace']['kladr']['fullName']
        except Exception:
            try:
                kladr_place = \
                    customerRequirement['oos:kladrPlaces']['oos:kladrPlace']['oos:kladr']['oos:fullName']
            except Exception:
                kladr_place = ''
        if not kladr_place:
            try:
                kladr_place = customerRequirement['kladrPlaces']['kladrPlace'][0]['kladr']['fullName']
            except Exception:
                try:
                    kladr_place = \
                        customerRequirement['oos:kladrPlaces']['oos:kladrPlace'][0]['oos:kladr']['oos:fullName']
                except Exception:
                    kladr_place = ''
        if not kladr_place:
            try:
                kladr_place = customerRequirement['kladrPlace']['kladr']['fullName']
            except Exception:
                try:
                    kladr_place = \
                        customerRequirement['oos:kladrPlace']['oos:kladr']['oos:fullName']
                except Exception:
                    kladr_place = ''
        if kladr_place is None:
            kladr_place = ''
        return kladr_place

    def delivery_place(self, customerRequirement):
        try:
            delivery_place = customerRequirement['kladrPlaces']['kladrPlace']['deliveryPlace']
        except Exception:
            try:
                delivery_place = \
                    customerRequirement['oos:kladrPlaces']['oos:kladrPlace']['oos:deliveryPlace']
            except Exception:
                delivery_place = ''
        if not delivery_place:
            try:
                delivery_place = customerRequirement['kladrPlaces']['kladrPlace'][0]['deliveryPlace']
            except Exception:
                try:
                    delivery_place = \
                        customerRequirement['oos:kladrPlaces']['oos:kladrPlace'][0]['oos:deliveryPlace']
                except Exception:
                    delivery_place = ''
        if not delivery_place:
            try:
                delivery_place = customerRequirement['kladrPlace']['deliveryPlace']
            except Exception:
                try:
                    delivery_place = \
                        customerRequirement['oos:kladrPlace']['oos:deliveryPlace']
                except Exception:
                    delivery_place = ''
        if delivery_place is None:
            delivery_place = ''
        return delivery_place

    def delivery_term(self, customerRequirement):
        try:
            delivery_term = customerRequirement['deliveryTerm']
        except Exception:
            try:
                delivery_term = \
                    customerRequirement['oos:deliveryTerm']
            except Exception:
                delivery_term = ''
        if delivery_term is None:
            delivery_term = ''
        return delivery_term

    def application_guarantee_amount(self, customerRequirement):
        try:
            application_guarantee_amount = customerRequirement['applicationGuarantee']['amount']
        except Exception:
            try:
                application_guarantee_amount = \
                    customerRequirement['oos:applicationGuarantee']['oos:amount']
            except Exception:
                application_guarantee_amount = 0
        if application_guarantee_amount is None:
            application_guarantee_amount = 0
        return application_guarantee_amount

    def contract_guarantee_amount(self, customerRequirement):
        try:
            contract_guarantee_amount = customerRequirement['contractGuarantee']['amount']
        except Exception:
            try:
                contract_guarantee_amount = \
                    customerRequirement['oos:contractGuarantee']['oos:amount']
            except Exception:
                contract_guarantee_amount = 0
        if contract_guarantee_amount is None:
            contract_guarantee_amount = 0
        return contract_guarantee_amount

    def application_settlement_account(self, customerRequirement):
        try:
            application_settlement_account = customerRequirement['applicationGuarantee']['settlementAccount']
        except Exception:
            try:
                application_settlement_account = \
                    customerRequirement['oos:applicationGuarantee']['oos:settlementAccount']
            except Exception:
                application_settlement_account = ''
        if application_settlement_account is None:
            application_settlement_account = ''
        return application_settlement_account

    def application_personal_account(self, customerRequirement):
        try:
            application_personal_account = customerRequirement['applicationGuarantee']['personalAccount']
        except Exception:
            try:
                application_personal_account = \
                    customerRequirement['oos:applicationGuarantee']['oos:personalAccount']
            except Exception:
                application_personal_account = ''
        if application_personal_account is None:
            application_personal_account = ''
        return application_personal_account

    def application_bik(self, customerRequirement):
        try:
            application_bik = customerRequirement['applicationGuarantee']['bik']
        except Exception:
            try:
                application_bik = \
                    customerRequirement['oos:applicationGuarantee']['oos:bik']
            except Exception:
                application_bik = ''
        if application_bik is None:
            application_bik = ''
        return application_bik

    def contract_settlement_account(self, customerRequirement):
        try:
            contract_settlement_account = customerRequirement['contractGuarantee']['settlementAccount']
        except Exception:
            try:
                contract_settlement_account = \
                    customerRequirement['oos:contractGuarantee']['oos:settlementAccount']
            except Exception:
                contract_settlement_account = ''
        if contract_settlement_account is None:
            contract_settlement_account = ''
        return contract_settlement_account

    def contract_personal_account(self, customerRequirement):
        try:
            contract_personal_account = customerRequirement['contractGuarantee']['personalAccount']
        except Exception:
            try:
                contract_personal_account = \
                    customerRequirement['oos:contractGuarantee']['oos:personalAccount']
            except Exception:
                contract_personal_account = ''
        if contract_personal_account is None:
            contract_personal_account = ''
        return contract_personal_account

    def contract_bik(self, customerRequirement):
        try:
            contract_bik = customerRequirement['contractGuarantee']['bik']
        except Exception:
            try:
                contract_bik = \
                    customerRequirement['oos:contractGuarantee']['oos:bik']
            except Exception:
                contract_bik = ''
        if contract_bik is None:
            contract_bik = ''
        return contract_bik

    def customer_regNum(self, customerRequirement):
        try:
            customer_regNum = customerRequirement['customer']['regNum']
        except Exception:
            try:
                customer_regNum = \
                    customerRequirement['oos:customer']['oos:regNum']
            except Exception:
                customer_regNum = ''
        if customer_regNum is None:
            customer_regNum = ''
        return customer_regNum

    def customer_full_name(self, customerRequirement):
        try:
            customer_full_name = customerRequirement['customer']['fullName']
        except Exception:
            try:
                customer_full_name = \
                    customerRequirement['oos:customer']['oos:fullName']
            except Exception:
                customer_full_name = ''
        if customer_full_name is None:
            customer_full_name = ''
        return customer_full_name

    def customer_requirement_max_price(self, customerRequirement):
        try:
            customer_requirement_max_price = customerRequirement['maxPrice']
        except Exception:
            try:
                customer_requirement_max_price = \
                    customerRequirement['oos:maxPrice']
            except Exception:
                customer_requirement_max_price = 0.0
        if customer_requirement_max_price is None:
            customer_requirement_max_price = 0.0
        return customer_requirement_max_price

    def get_preferences(self, lot):
        if 'preferenses' in lot:
            if lot['preferenses'] is None:
                return []
            elif 'preferense' in lot['preferenses']:
                return generator_univ(lot['preferenses']['preferense'])
            else:
                return []
        elif 'oos:preferenses' in lot:
            if lot['oos:preferenses'] is None:
                return []
            elif 'oos:preferense' in lot['oos:preferenses']:
                return generator_univ(lot['oos:preferenses']['oos:preferense'])
            else:
                return []
        else:
            return []

    def preferense_name(self, preferense):
        try:
            preferense_name = preferense['name']
        except Exception:
            try:
                preferense_name = \
                    preferense['oos:name']
            except Exception:
                preferense_name = ''
        if preferense_name is None:
            preferense_name = ''
        return preferense_name

    def get_requirements(self, lot):
        if 'requirements' in lot:
            if lot['requirements'] is None:
                return []
            elif 'requirement' in lot['requirements']:
                return generator_univ(lot['requirements']['requirement'])
            else:
                return []
        elif 'oos:requirements' in lot:
            if lot['oos:requirements'] is None:
                return []
            elif 'oos:requirement' in lot['oos:requirements']:
                return generator_univ(lot['oos:requirements']['oos:requirement'])
            else:
                return []
        else:
            return []

    def requirement_name(self, requirement):
        try:
            requirement_name = requirement['name']
        except Exception:
            try:
                requirement_name = \
                    requirement['oos:name']
            except Exception:
                requirement_name = ''
        if requirement_name is None:
            requirement_name = ''
        return requirement_name

    def requirement_content(self, requirement):
        try:
            requirement_content = requirement['content']
        except Exception:
            try:
                requirement_content = \
                    requirement['oos:content']
            except Exception:
                requirement_content = ''
        if requirement_content is None:
            requirement_content = ''
        return requirement_content

    def requirement_code(self, requirement):
        try:
            requirement_code = requirement['code']
        except Exception:
            try:
                requirement_code = \
                    requirement['oos:code']
            except Exception:
                requirement_code = ''
        if requirement_code is None:
            requirement_code = ''
        return requirement_code

    def restrict_info(self, lot):
        try:
            restrict_info = lot['restrictInfo']
        except Exception:
            try:
                restrict_info = \
                    lot['oos:restrictInfo']
            except Exception:
                restrict_info = ''
        if restrict_info is None:
            restrict_info = ''
        return restrict_info

    def foreign_info(self, lot):
        try:
            foreign_info = lot['restrictForeignsInfo']
        except Exception:
            try:
                foreign_info = \
                    lot['oos:restrictForeignsInfo']
            except Exception:
                foreign_info = ''
        if foreign_info is None:
            foreign_info = ''
        return foreign_info

    def get_purchaseobjects(self, lot):
        if 'purchaseObjects' in lot:
            if lot['purchaseObjects'] is None:
                return []
            elif 'purchaseObject' in lot['purchaseObjects']:
                return generator_univ(lot['purchaseObjects']['purchaseObject'])
            else:
                return []
        elif 'oos:purchaseObjects' in lot:
            if lot['oos:purchaseObjects'] is None:
                return []
            elif 'oos:purchaseObject' in lot['oos:purchaseObjects']:
                return generator_univ(lot['oos:purchaseObjects']['oos:purchaseObject'])
            else:
                return []
        else:
            return []

    def okpd2_code(self, purchaseobject):
        try:
            okpd2_code = purchaseobject['OKPD2']['code']
        except Exception:
            try:
                okpd2_code = \
                    purchaseobject['oos:OKPD2']['oos:code']
            except Exception:
                okpd2_code = ''
        if okpd2_code is None:
            okpd2_code = ''
        return okpd2_code

    def okpd_code(self, purchaseobject):
        try:
            okpd_code = purchaseobject['OKPD']['code']
        except Exception:
            try:
                okpd_code = \
                    purchaseobject['oos:OKPD']['oos:code']
            except Exception:
                okpd_code = ''
        if okpd_code is None:
            okpd_code = ''
        return okpd_code

    def okpd_name(self, purchaseobject):
        try:
            okpd_name = purchaseobject['OKPD2']['name']
        except Exception:
            try:
                okpd_name = \
                    purchaseobject['oos:OKPD2']['oos:name']
            except Exception:
                okpd_name = ''
        if not okpd_name:
            try:
                okpd_name = purchaseobject['OKPD']['name']
            except Exception:
                try:
                    okpd_name = \
                        purchaseobject['oos:OKPD']['oos:name']
                except Exception:
                    okpd_name = ''
        if okpd_name is None:
            okpd_name = ''
        return okpd_name

    def name(self, purchaseobject):
        try:
            name = purchaseobject['name']
        except Exception:
            try:
                name = \
                    purchaseobject['oos:name']
            except Exception:
                name = ''
        if name is None:
            name = ''
        return name

    def quantity_value(self, purchaseobject):
        try:
            quantity_value = purchaseobject['quantity']['value']
        except Exception:
            try:
                quantity_value = \
                    purchaseobject['oos:quantity']['oos:value']
            except Exception:
                quantity_value = 0
        if quantity_value is None:
            quantity_value = 0
        return quantity_value

    def price(self, purchaseobject):
        try:
            price = purchaseobject['price']
        except Exception:
            try:
                price = \
                    purchaseobject['oos:price']
            except Exception:
                price = 0
        if price is None:
            price = 0
        return price

    def okei(self, purchaseobject):
        try:
            okei = purchaseobject['OKEI']['nationalCode']
        except Exception:
            try:
                okei = \
                    purchaseobject['oos:OKEI']['oos:nationalCode']
            except Exception:
                okei = ''
        if okei is None:
            okei = ''
        return okei

    def sum_p(self, purchaseobject):
        try:
            sum_p = purchaseobject['sum']
        except Exception:
            try:
                sum_p = \
                    purchaseobject['oos:sum']
            except Exception:
                sum_p = 0
        if sum_p is None:
            sum_p = 0
        return sum_p

    def get_customerquantities(self, purchaseobject):
        if 'customerQuantities' in purchaseobject:
            if purchaseobject['customerQuantities'] is None:
                return []
            elif 'customerQuantity' in purchaseobject['customerQuantities']:
                return generator_univ(purchaseobject['customerQuantities']['customerQuantity'])
            else:
                return []
        elif 'oos:customerQuantities' in purchaseobject:
            if purchaseobject['oos:customerQuantities'] is None:
                return []
            elif 'oos:customerQuantity' in purchaseobject['oos:customerQuantities']:
                return generator_univ(purchaseobject['oos:customerQuantities']['oos:customerQuantity'])
            else:
                return []
        else:
            return []

    def customer_quantity_value(self, customerquantity):
        try:
            customer_quantity_value = customerquantity['quantity']
        except Exception:
            try:
                customer_quantity_value = \
                    customerquantity['oos:quantity']
            except Exception:
                customer_quantity_value = ''

        if customer_quantity_value is None:
            customer_quantity_value = ''
        return customer_quantity_value

    def cust_regNum(self, customerquantity):
        try:
            cust_regNum = customerquantity['customer']['regNum']
        except Exception:
            try:
                cust_regNum = \
                    customerquantity['oos:customer']['oos:regNum']
            except Exception:
                cust_regNum = ''
        if cust_regNum is None:
            cust_regNum = ''
        return cust_regNum

    def cust_full_name(self, customerquantity):
        try:
            cust_full_name = customerquantity['customer']['fullName']
        except Exception:
            try:
                cust_full_name = \
                    customerquantity['oos:customer']['oos:fullName']
            except Exception:
                cust_full_name = ''
        if cust_full_name is None:
            cust_full_name = ''
        cust_full_name = cust_full_name.strip()
        return cust_full_name

    def add_tender_kwords(self, id_tender):
        con = connect_bd(name_db)
        cur = con.cursor(pymysql.cursors.DictCursor)
        res_string = ''
        set_po = set()
        cur.execute("""SELECT po.name, po.okpd_name FROM purchase_object{0} AS po
            LEFT JOIN lot{0} AS l ON l.id_lot = po.id_lot
            WHERE l.id_tender=%s""".format(suffix), (id_tender,))
        while True:
            row = cur.fetchone()
            if not row:
                break
            string_name = row['name']
            string_name = string_name.strip()
            string_okpd_name = row['okpd_name']
            string_okpd_name = string_okpd_name.strip()
            res = ' ' + string_name + ' ' + string_okpd_name
            set_po.add(res)
        for s in set_po:
            res_string += s

        set_att = set()
        cur.execute("""SELECT file_name FROM attachment{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        while True:
            row_1 = cur.fetchone()
            if not row_1:
                break
            sring_att = row_1['file_name']
            sring_att = sring_att.strip()
            res_a = ' ' + sring_att
            set_att.add(res_a)
        for n in set_att:
            res_string += n
        cur.execute("""SELECT purchase_object_info FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        res_po = cur.fetchone()
        if res_po:
            string_po = res_po['purchase_object_info']
            string_po = string_po.strip()
            res_string = string_po + ' ' + res_string
        cur.execute("""UPDATE tender{0} SET tender_kwords=%s WHERE id_tender=%s""".format(suffix),
                    (res_string, id_tender))
        cur.execute("""SELECT tender_kwords FROM tender{0} WHERE id_tender=%s""".format(suffix), (id_tender,))
        res_tk = cur.fetchone()
        if res_tk and (res_tk['tender_kwords'] is None):
            with open(file_log, 'a') as flog:
                flog.write('Не записали tender_kwords' + ' ' + id_tender + '\n\n\n')
        cur.close()
        con.close()

    def add_ver_number(self, p_num):
        ver_num = 1
        con = connect_bd(name_db)
        cur = con.cursor(pymysql.cursors.DictCursor)
        cur2 = con.cursor(pymysql.cursors.DictCursor)
        cur.execute("""SELECT id_tender FROM tender{0} WHERE purchase_number=%s 
                    ORDER BY UNIX_TIMESTAMP(doc_publish_date) ASC""".format(suffix), (p_num,))
        while True:
            res = cur.fetchone()
            if res is None:
                break
            cur2.execute("""UPDATE tender{0} SET num_version=%s WHERE id_tender=%s""".format(suffix),
                        (ver_num, res['id_tender']))
            ver_num += 1
        cur.close()
        cur2.close()
        con.close()


class Cancel:
    count_add_cancel = 0

    def __init__(self, tender_cancel):
        self.tender_cancel = 0
        tender_root = list(tender_cancel.keys())
        tender_child = tender_cancel[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_cancel = tender_child[i]

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender_cancel:
            purchaseNumber = self.tender_cancel['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender_cancel:
            purchaseNumber = self.tender_cancel['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber


class CancelFailure:
    count_add_cancel_failuure = 0

    def __init__(self, tender_cancel_failure):
        self.tender_cancel_failure = 0
        tender_root = list(tender_cancel_failure.keys())
        tender_child = tender_cancel_failure[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_cancel_failure = tender_child[i]

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender_cancel_failure:
            purchaseNumber = self.tender_cancel_failure['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender_cancel_failure:
            purchaseNumber = self.tender_cancel_failure['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber


class Sign:
    count_add_sign = 0

    def __init__(self, tender_sign):
        self.tender_sign = 0
        tender_root = list(tender_sign.keys())
        tender_child = tender_sign[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_sign = tender_child[i]

    @property
    def purchaseNumber(self):
        try:
            purchaseNumber = self.tender_sign['oos:foundation']['oos:order']['oos:purchaseNumber']
        except Exception:
            try:
                purchaseNumber = self.tender_sign['foundation']['order']['purchaseNumber']
            except Exception:
                purchaseNumber = ''
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def id_sign(self):
        try:
            id_sign = self.tender_sign['oos:id']
        except Exception:
            try:
                id_sign = self.tender_sign['id']
            except Exception:
                id_sign = ''
        if id_sign is None:
            id_sign = ''
        return id_sign

    @property
    def sign_number(self):
        try:
            sign_number = self.tender_sign['oos:foundation']['oos:order']['oos:foundationProtocolNumber']
        except Exception:
            try:
                sign_number = self.tender_sign['foundation']['order']['foundationProtocolNumber']
            except Exception:
                sign_number = ''
        if sign_number is None:
            sign_number = ''
        return sign_number

    @property
    def sign_date(self):
        try:
            sign_date = self.tender_sign['oos:signDate']
        except Exception:
            try:
                sign_date = self.tender_sign['signDate']
            except Exception:
                sign_date = ''
        if sign_date is None:
            sign_date = ''
        return sign_date

    @property
    def customer_reg_num(self):
        try:
            customer_reg_num = self.tender_sign['oos:customer']['oos:regNum']
        except Exception:
            try:
                customer_reg_num = self.tender_sign['customer']['regNum']
            except Exception:
                customer_reg_num = ''
        if customer_reg_num is None:
            customer_reg_num = ''
        return customer_reg_num

    @property
    def contract_sign_price(self):
        try:
            contract_sign_price = self.tender_sign['oos:price']
        except Exception:
            try:
                contract_sign_price = self.tender_sign['price']
            except Exception:
                contract_sign_price = 0
        if contract_sign_price is None:
            contract_sign_price = 0
        return contract_sign_price

    @property
    def sign_currency(self):
        try:
            sign_currency = self.tender_sign['oos:currency']['oos:name']
        except Exception:
            try:
                sign_currency = self.tender_sign['currency']['name']
            except Exception:
                sign_currency = ''
        if sign_currency is None:
            sign_currency = ''
        return sign_currency

    @property
    def conclude_contract_right(self):
        try:
            conclude_contract_right = self.tender_sign['oos:concludeContractRight']
        except Exception:
            try:
                conclude_contract_right = self.tender_sign['concludeContractRight']
            except Exception:
                conclude_contract_right = 0
        if conclude_contract_right is None:
            conclude_contract_right = 0
        return conclude_contract_right

    @property
    def protocole_date(self):
        try:
            protocole_date = self.tender_sign['oos:protocolDate']
        except Exception:
            try:
                protocole_date = self.tender_sign['protocolDate']
            except Exception:
                protocole_date = ''
        if protocole_date is None:
            protocole_date = ''
        return protocole_date

    def get_suppliers(self):
        if 'oos:suppliers' in self.tender_sign:
            if self.tender_sign['oos:suppliers'] is None:
                return []
            elif 'oos:supplier' in self.tender_sign['oos:suppliers']:
                return generator_univ(self.tender_sign['oos:suppliers']['oos:supplier'])
            else:
                return []
        elif 'suppliers' in self.tender_sign:
            if self.tender_sign['suppliers'] is None:
                return []
            elif 'supplier' in self.tender_sign['suppliers']:
                return generator_univ(self.tender_sign['suppliers']['supplier'])
            else:
                return []
        else:
            return []

    def supplier_lastName(self, supplier):
        try:
            supplier_lastName = supplier['oos:contactInfo']['oos:lastName']
        except Exception:
            try:
                supplier_lastName = supplier['contactInfo']['lastName']
            except Exception:
                supplier_lastName = ''
        if supplier_lastName is None:
            supplier_lastName = ''
        return supplier_lastName

    def supplier_firstName(self, supplier):
        try:
            supplier_firstName = supplier['oos:contactInfo']['oos:firstName']
        except Exception:
            try:
                supplier_firstName = supplier['contactInfo']['firstName']
            except Exception:
                supplier_firstName = ''
        if supplier_firstName is None:
            supplier_firstName = ''
        return supplier_firstName

    def supplier_middleName(self, supplier):
        try:
            supplier_middleName = supplier['oos:contactInfo']['oos:middleName']
        except Exception:
            try:
                supplier_middleName = supplier['contactInfo']['middleName']
            except Exception:
                supplier_middleName = ''
        if supplier_middleName is None:
            supplier_middleName = ''
        return supplier_middleName

    def supplier_email(self, supplier):
        try:
            supplier_email = supplier['oos:contactEMail']
        except Exception:
            try:
                supplier_email = supplier['contactEMail']
            except Exception:
                supplier_email = ''
        if supplier_email is None:
            supplier_email = ''
        return supplier_email

    def supplier_contact_phone(self, supplier):
        try:
            supplier_contact_phone = supplier['oos:contactPhone']
        except Exception:
            try:
                supplier_contact_phone = supplier['contactPhone']
            except Exception:
                supplier_contact_phone = ''
        if supplier_contact_phone is None:
            supplier_contact_phone = ''
        return supplier_contact_phone

    def supplier_contact_fax(self, supplier):
        try:
            supplier_contact_fax = supplier['oos:contactFax']
        except Exception:
            try:
                supplier_contact_fax = supplier['contactFax']
            except Exception:
                supplier_contact_fax = ''
        if supplier_contact_fax is None:
            supplier_contact_fax = ''
        return supplier_contact_fax

    def supplier_inn(self, supplier):
        try:
            supplier_inn = supplier['oos:inn']
        except Exception:
            try:
                supplier_inn = supplier['inn']
            except Exception:
                supplier_inn = ''
        if supplier_inn is None:
            supplier_inn = ''
        supplier_inn = supplier_inn.strip()
        return supplier_inn

    def supplier_kpp(self, supplier):
        try:
            supplier_kpp = supplier['oos:kpp']
        except Exception:
            try:
                supplier_kpp = supplier['kpp']
            except Exception:
                supplier_kpp = ''
        if supplier_kpp is None:
            supplier_kpp = ''
        supplier_kpp = supplier_kpp.strip()
        return supplier_kpp

    def participant_type(self, supplier):
        try:
            participant_type = supplier['oos:participantType']
        except Exception:
            try:
                participant_type = supplier['participantType']
            except Exception:
                participant_type = ''
        if participant_type is None:
            participant_type = ''
        return participant_type

    def organization_name(self, supplier):
        try:
            organization_name = supplier['oos:organizationName']
        except Exception:
            try:
                organization_name = supplier['organizationName']
            except Exception:
                organization_name = ''
        if organization_name is None:
            organization_name = ''
        return organization_name

    def country_full_name(self, supplier):
        try:
            country_full_name = supplier['oos:country']['oos:countryFullName']
        except Exception:
            try:
                country_full_name = supplier['country']['countryFullName']
            except Exception:
                country_full_name = ''
        if country_full_name is None:
            country_full_name = ''
        return country_full_name

    def factual_address(self, supplier):
        try:
            factual_address = supplier['oos:factualAddress']
        except Exception:
            try:
                factual_address = supplier['factualAddress']
            except Exception:
                factual_address = ''
        if factual_address is None:
            factual_address = ''
        return factual_address

    def post_address(self, supplier):
        try:
            post_address = supplier['oos:postAddress']
        except Exception:
            try:
                post_address = supplier['postAddress']
            except Exception:
                post_address = ''
        if post_address is None:
            post_address = ''
        return post_address


class LotCancel:
    count_add_lotcancel = 0

    def __init__(self, tender_lotcancel):
        self.tender_lotcancel = 0
        tender_root = list(tender_lotcancel.keys())
        tender_child = tender_lotcancel[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_lotcancel = tender_child[i]

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender_lotcancel:
            purchaseNumber = self.tender_lotcancel['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender_lotcancel:
            purchaseNumber = self.tender_lotcancel['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def lotNumber(self):
        try:
            lotNumber = self.tender_lotcancel['lot']['lotNumber']
        except Exception:
            try:
                lotNumber = self.tender_lotcancel['oos:lot']['oos:lotNumber']
            except Exception:
                lotNumber = 0
        if lotNumber is None:
            lotNumber = 0
        return lotNumber


class OrgChange:
    count_org_change = 0

    def __init__(self, tender_orgchange):
        self.tender_orgchange = 0
        tender_root = list(tender_orgchange.keys())
        tender_child = tender_orgchange[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_orgchange = tender_child[i]

    @property
    def purchaseNumber(self):
        try:
            purchaseNumber = self.tender_orgchange['purchase']['purchaseNumber']
        except Exception:
            try:
                purchaseNumber = self.tender_orgchange['oos:purchase']['oos:purchaseNumber']
            except Exception:
                purchaseNumber = ''
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def newRespOrg_regNum(self):
        try:
            newRespOrg_regNum = self.tender_orgchange['newRespOrg']['regNum']
        except Exception:
            try:
                newRespOrg_regNum = self.tender_orgchange['oos:newRespOrg']['oos:regNum']
            except Exception:
                newRespOrg_regNum = ''
        if newRespOrg_regNum is None:
            newRespOrg_regNum = ''
        return newRespOrg_regNum

    @property
    def newRespOrg_fullName(self):
        try:
            newRespOrg_fullName = self.tender_orgchange['newRespOrg']['fullName']
        except Exception:
            try:
                newRespOrg_fullName = self.tender_orgchange['oos:newRespOrg']['oos:fullName']
            except Exception:
                newRespOrg_fullName = ''
        if newRespOrg_fullName is None:
            newRespOrg_fullName = ''
        return newRespOrg_fullName

    @property
    def newRespOrg_postAddress(self):
        try:
            newRespOrg_postAddress = self.tender_orgchange['newRespOrg']['postAddress']
        except Exception:
            try:
                newRespOrg_postAddress = self.tender_orgchange['oos:newRespOrg']['oos:postAddress']
            except Exception:
                newRespOrg_postAddress = ''
        if newRespOrg_postAddress is None:
            newRespOrg_postAddress = ''
        return newRespOrg_postAddress

    @property
    def newRespOrg_factAddress(self):
        try:
            newRespOrg_factAddress = self.tender_orgchange['newRespOrg']['factAddress']
        except Exception:
            try:
                newRespOrg_factAddress = self.tender_orgchange['oos:newRespOrg']['oos:factAddress']
            except Exception:
                newRespOrg_factAddress = ''
        if newRespOrg_factAddress is None:
            newRespOrg_factAddress = ''
        return newRespOrg_factAddress

    @property
    def newRespOrg_INN(self):
        try:
            newRespOrg_INN = self.tender_orgchange['newRespOrg']['INN']
        except Exception:
            try:
                newRespOrg_INN = self.tender_orgchange['oos:newRespOrg']['oos:INN']
            except Exception:
                newRespOrg_INN = ''
        if newRespOrg_INN is None:
            newRespOrg_INN = ''
        return newRespOrg_INN

    @property
    def newRespOrg_KPP(self):
        try:
            newRespOrg_KPP = self.tender_orgchange['newRespOrg']['KPP']
        except Exception:
            try:
                newRespOrg_KPP = self.tender_orgchange['oos:newRespOrg']['oos:KPP']
            except Exception:
                newRespOrg_KPP = ''
        if newRespOrg_KPP is None:
            newRespOrg_KPP = ''
        return newRespOrg_KPP

    @property
    def newRespOrg_responsibleRole(self):
        try:
            newRespOrg_responsibleRole = self.tender_orgchange['newRespOrg']['responsibleRole']
        except Exception:
            try:
                newRespOrg_responsibleRole = self.tender_orgchange['oos:newRespOrg']['oos:responsibleRole']
            except Exception:
                newRespOrg_responsibleRole = ''
        if newRespOrg_responsibleRole is None:
            newRespOrg_responsibleRole = ''
        return newRespOrg_responsibleRole


class Prolongation:
    count_add_prolongation = 0

    def __init__(self, tender_prolongation):
        self.tender_prolongation = 0
        tender_root = list(tender_prolongation.keys())
        tender_child = tender_prolongation[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_prolongation = tender_child[i]

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender_prolongation:
            purchaseNumber = self.tender_prolongation['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender_prolongation:
            purchaseNumber = self.tender_prolongation['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def collectingEndDate(self):
        collectingEndDate = ''
        if 'collectingEndDate' in self.tender_prolongation:
            collectingEndDate = self.tender_prolongation['collectingEndDate']
        elif 'oos:collectingEndDate' in self.tender_prolongation:
            collectingEndDate = self.tender_prolongation['oos:collectingEndDate']
        if collectingEndDate is None:
            collectingEndDate = ''
        return collectingEndDate

    @property
    def collectingProlongationDate(self):
        collectingProlongationDate = ''
        if 'collectingProlongationDate' in self.tender_prolongation:
            collectingProlongationDate = self.tender_prolongation['collectingProlongationDate']
        elif 'oos:collectingProlongationDate' in self.tender_prolongation:
            collectingProlongationDate = self.tender_prolongation['oos:collectingProlongationDate']
        if collectingProlongationDate is None:
            collectingProlongationDate = ''
        return collectingProlongationDate

    @property
    def scoringDate(self):
        scoringDate = ''
        if 'scoringDate' in self.tender_prolongation:
            scoringDate = self.tender_prolongation['scoringDate']
        elif 'oos:scoringDate' in self.tender_prolongation:
            scoringDate = self.tender_prolongation['oos:scoringDate']
        if scoringDate is None:
            scoringDate = ''
        return scoringDate

    @property
    def scoringProlongationDate(self):
        scoringProlongationDate = ''
        if 'scoringProlongationDate' in self.tender_prolongation:
            scoringProlongationDate = self.tender_prolongation['scoringProlongationDate']
        elif 'oos:scoringProlongationDate' in self.tender_prolongation:
            scoringProlongationDate = self.tender_prolongation['oos:scoringProlongationDate']
        if scoringProlongationDate is None:
            scoringProlongationDate = ''
        return scoringProlongationDate


class Datechange:
    count_add_datechange = 0

    def __init__(self, tender_datechange):
        self.tender_datechange = 0
        tender_root = list(tender_datechange.keys())
        tender_child = tender_datechange[tender_root[0]]
        tender_child_2_list = list(tender_child.keys())
        for i in tender_child_2_list:
            if i.startswith('fcs') or i.startswith('ns2:fcs') or i.startswith('oos:fcs'):
                self.tender_datechange = tender_child[i]

    @property
    def purchaseNumber(self):
        purchaseNumber = ''
        if 'purchaseNumber' in self.tender_datechange:
            purchaseNumber = self.tender_datechange['purchaseNumber']
        elif 'oos:purchaseNumber' in self.tender_datechange:
            purchaseNumber = self.tender_datechange['oos:purchaseNumber']
        if purchaseNumber is None:
            purchaseNumber = ''
        return purchaseNumber

    @property
    def auctionTime(self):
        auctionTime = ''
        if 'auctionTime' in self.tender_datechange:
            auctionTime = self.tender_datechange['auctionTime']
        elif 'oos:auctionTime' in self.tender_datechange:
            auctionTime = self.tender_datechange['oos:auctionTime']
        if auctionTime is None:
            auctionTime = ''
        return auctionTime

    @property
    def newAuctionDate(self):
        newAuctionDate = ''
        if 'newAuctionDate' in self.tender_datechange:
            newAuctionDate = self.tender_datechange['newAuctionDate']
        elif 'oos:newAuctionDate' in self.tender_datechange:
            newAuctionDate = self.tender_datechange['oos:newAuctionDate']
        if newAuctionDate is None:
            newAuctionDate = ''
        return newAuctionDate


def parser_type_44(doc, path_xml, filexml, reg, reg_id):
    ten_44 = Tender44(doc)
    if ten_44.tender44 == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return
    id_t = ten_44.id_t
    if not id_t:
        with open(file_log, 'a') as flog:
            flog.write('У тендера нет id ' + ' ' + path_xml + '\n\n\n')
        return
    purchaseNumber = ten_44.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('У тендера нет purchaseNumber ' + ' ' + path_xml + '\n\n\n')
    if purchaseNumber and purchaseNumber[0] == '9':
        with open(file_log, 'a') as flog:
            flog.write('Тестовый тендер' + ' ' + path_xml + ' ' + purchaseNumber + '\n\n\n')
        return
    xml = path_xml[path_xml.find('/') + 1:][(path_xml[path_xml.find('/') + 1:]).find('/') + 1:]
    con_44 = connect_bd(name_db)
    cur_44 = con_44.cursor(pymysql.cursors.DictCursor)
    cur_44.execute(
            """SELECT id_tender FROM tender{0} WHERE id_xml = %s AND id_region = %s AND purchase_number=%s""".format(
                    suffix), (id_t, reg_id, purchaseNumber))
    res_id_ten_44 = cur_44.fetchone()
    if res_id_ten_44:
        # with open(file_log, 'a') as flog:
        #     flog.write('Тендер с таким id и purchaseNumber и id_region уже есть в базе ' + ' ' + path_xml + '\n\n\n')
        cur_44.close()
        con_44.close()
        return
    docPublishDate = ten_44.docPublishDate
    date_version = docPublishDate
    print_form = ten_44.printform
    notice_version = ''
    num_version = 0
    cancel_status = 0
    if docPublishDate:
        cur_44.execute(
                """SELECT id_tender, doc_publish_date FROM tender{0} WHERE id_region = %s AND purchase_number=%s""".format(
                        suffix),
                (reg_id, purchaseNumber))
        res_pur_n = cur_44.fetchall()
        if res_pur_n:
            for pur in res_pur_n:
                date_new = docPublishDate[:19]
                date_new = dateutil.parser.parse(date_new)
                date_old = datetime.datetime.strptime(str(pur['doc_publish_date']), "%Y-%m-%d %H:%M:%S")
                if date_new >= date_old:
                    cur_44.execute("""UPDATE tender{0} SET cancel = 1 WHERE id_tender=%s""".format(suffix),
                                   (pur['id_tender'],))
                else:
                    cancel_status = 1

    href = ten_44.href
    purchaseObjectInfo = ten_44.purchaseObjectInfo

    organizer_reg_num = ten_44.organizer_reg_num
    organizer_full_name = ten_44.organizer_full_name
    organizer_post_address = ten_44.organizer_post_address
    organizer_fact_address = ten_44.organizer_fact_address
    organizer_inn = ten_44.organizer_inn
    organizer_kpp = ten_44.organizer_kpp
    organizer_responsible_role = ten_44.organizer_responsible_role
    organizer_last_name = ten_44.organizer_last_name
    organizer_first_name = ten_44.organizer_first_name
    organizer_middle_name = ten_44.organizer_middle_name
    organizer_contact = organizer_last_name + ' ' + organizer_first_name + ' ' + organizer_middle_name
    organizer_email = ten_44.organizer_email
    organizer_fax = ten_44.organizer_fax
    organizer_phone = ten_44.organizer_phone
    id_organizer = 0
    id_customer = 0
    if organizer_reg_num:
        cur_44.execute("""SELECT id_organizer FROM organizer{0} WHERE reg_num = %s""".format(suffix),
                       (organizer_reg_num,))
        res_organizer_reg_num = cur_44.fetchone()
        if res_organizer_reg_num:
            id_organizer = res_organizer_reg_num['id_organizer']
        else:
            query_add_organizer = """INSERT INTO organizer{0} SET reg_num=%s, full_name=%s, post_address=%s,
                                                fact_address=%s, inn=%s, kpp=%s, responsible_role=%s,
                                                contact_person=%s, contact_email=%s, contact_phone=%s,
                                                contact_fax=%s""".format(suffix)
            query_value_organizer = (
                organizer_reg_num, organizer_full_name, organizer_post_address, organizer_fact_address, organizer_inn,
                organizer_kpp, organizer_responsible_role, organizer_contact, organizer_email, organizer_phone,
                organizer_fax)
            try:
                cur_44.execute(query_add_organizer, query_value_organizer)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных organizer:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_44.execute("""SELECT id_organizer FROM organizer{0} WHERE reg_num=%s""".format(suffix),
                           (organizer_reg_num,))
            res_id_organizer = cur_44.fetchone()
            id_organizer = res_id_organizer['id_organizer']
    if not id_organizer:
        with open(file_log, 'a') as flog:
            flog.write('Нет id_organizer' + ' ' + path_xml + '\n\n\n')
    if not organizer_reg_num:
        with open(file_log, 'a') as flog:
            flog.write('Нет organizer_reg_num' + ' ' + path_xml + '\n\n\n')
    id_placing_way = 0
    placingWay_code = ten_44.placingWay_code
    placingWay_name = ten_44.placingWay_name
    if placingWay_code:
        cur_44.execute("""SELECT id_placing_way FROM placing_way{0} WHERE code = %s""".format(suffix),
                       (placingWay_code,))
        res_placingway_code = cur_44.fetchone()
        if res_placingway_code:
            id_placing_way = res_placingway_code['id_placing_way']
        else:
            query_add_placing_way = """INSERT INTO placing_way{0} SET code=%s, name=%s""".format(suffix)
            query_value_placing_way = (placingWay_code, placingWay_name)
            try:
                cur_44.execute(query_add_placing_way, query_value_placing_way)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных placing_way:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_44.execute("""SELECT id_placing_way FROM placing_way{0} WHERE code = %s""".format(suffix),
                           (placingWay_code,))
            res_id_placing_way = cur_44.fetchone()
            id_placing_way = res_id_placing_way['id_placing_way']

    id_etp = 0
    ETP_code = ten_44.ETP_code
    ETP_name = ten_44.ETP_name
    ETP_url = ten_44.ETP_url
    if ETP_code:
        cur_44.execute("""SELECT id_etp FROM etp{0} WHERE code = %s""".format(suffix), (ETP_code,))
        res_etp_code = cur_44.fetchone()
        if res_etp_code:
            id_etp = res_etp_code['id_etp']
        else:
            query_add_etp = """INSERT INTO etp{0} SET code=%s, name=%s, url=%s, conf=0""".format(suffix)
            query_value_etp = (ETP_code, ETP_name, ETP_url)
            try:
                cur_44.execute(query_add_etp, query_value_etp)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных etp_code:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_44.execute("""SELECT id_etp FROM etp{0} WHERE code = %s""".format(suffix), (ETP_code,))
            res_id_etp = cur_44.fetchone()
            id_etp = res_id_etp['id_etp']

    end_date = ten_44.end_date
    scoring_date = ten_44.scoring_date
    bidding_date = ten_44.bidding_date
    query_add_tender = """INSERT INTO tender{0} SET id_region = %s, id_xml = %s, purchase_number = %s,
                              doc_publish_date = %s, href = %s, purchase_object_info = %s, type_fz = %s,
                              id_organizer=%s, id_placing_way=%s, id_etp=%s, end_date=%s,
                              scoring_date=%s, bidding_date=%s, cancel=%s, date_version=%s, 
                              num_version=%s, notice_version=%s, xml=%s, print_form=%s""".format(suffix)
    query_value_tender = (reg_id, id_t, purchaseNumber, docPublishDate, href, purchaseObjectInfo,
                          44, id_organizer, id_placing_way, id_etp, end_date, scoring_date, bidding_date, cancel_status,
                          date_version, num_version, notice_version, xml, print_form)
    try:
        cur_44.execute(query_add_tender, query_value_tender)
        Tender44.count_add_tender += 1
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи в базу данных tender:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_44.execute(
            """SELECT id_tender FROM tender{0} WHERE id_xml = %s AND id_region = %s AND purchase_number=%s""".format(
                    suffix),
            (id_t, reg_id, purchaseNumber))
    res_id_tender = cur_44.fetchone()
    id_tender = res_id_tender['id_tender']
    if not cancel_status:
        try:
            cur_44.execute("""UPDATE contract_sign{0} SET id_tender=%s WHERE purchase_number=%s""".format(suffix),
                           (id_tender, purchaseNumber))
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка обновления подписанных контрактов по purchase_number:' + ' ' + path_xml + ' ' + str(
                        e) + '\n\n\n')
    attachments_list = ten_44.get_attachments()
    for att in attachments_list:
        attach_name = ten_44.attach_name(att)
        attach_description = ten_44.attach_description(att)
        attach_url = ten_44.attach_url(att)
        if attach_name:
            query_add_attach = """INSERT INTO attachment{0} SET id_tender=%s, file_name=%s, url=%s,
                                description=%s""".format(suffix)
            query_value_attach = (id_tender, attach_name, attach_url, attach_description)
            try:
                cur_44.execute(query_add_attach, query_value_attach)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных attachments:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')

    lotNumber = 1
    lots_list = ten_44.get_lots()
    for lot in lots_list:
        lot_max_price = ten_44.lot_max_price(lot)
        lot_currency = ten_44.lot_currency(lot)
        lot_finance_source = ten_44.lot_finance_source(lot)
        query_add_lot = """INSERT INTO lot{0} SET id_tender=%s, lot_number=%s, max_price=%s, currency=%s,
                        finance_source=%s """.format(suffix)
        query_value_lot = (id_tender, lotNumber, lot_max_price, lot_currency, lot_finance_source)
        try:
            cur_44.execute(query_add_lot, query_value_lot)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка записи в базу данных lot:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        cur_44.execute(
                """SELECT id_lot FROM lot{0} WHERE id_tender=%s AND lot_number=%s AND max_price=%s""".format(suffix),
                (id_tender, lotNumber, lot_max_price))
        res_id_lot = cur_44.fetchone()
        id_lot = res_id_lot['id_lot']
        lotNumber += 1
        customerRequirements = ten_44.get_customerRequirements(lot)
        for customerRequirement in customerRequirements:
            kladr_place = ten_44.kladr_place(customerRequirement)
            delivery_place = ten_44.delivery_place(customerRequirement)
            delivery_term = ten_44.delivery_term(customerRequirement)
            application_guarantee_amount = ten_44.application_guarantee_amount(customerRequirement)
            contract_guarantee_amount = ten_44.contract_guarantee_amount(customerRequirement)
            application_settlement_account = ten_44.application_settlement_account(customerRequirement)
            application_personal_account = ten_44.application_personal_account(customerRequirement)
            application_bik = ten_44.application_bik(customerRequirement)
            contract_settlement_account = ten_44.contract_settlement_account(customerRequirement)
            contract_personal_account = ten_44.contract_personal_account(customerRequirement)
            contract_bik = ten_44.contract_bik(customerRequirement)
            customer_regNum = ten_44.customer_regNum(customerRequirement)
            customer_full_name = ten_44.customer_full_name(customerRequirement)
            customer_requirement_max_price = ten_44.customer_requirement_max_price(customerRequirement)
            if customer_regNum:
                cur_44.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix),
                               (customer_regNum,))
                res_customer = cur_44.fetchone()
                if res_customer:
                    id_customer = res_customer['id_customer']
                else:
                    customer_inn = ''
                    if organizer_inn:
                        if organizer_reg_num == customer_regNum:
                            customer_inn = organizer_inn

                    query_add_customer = """INSERT INTO customer{0} SET reg_num=%s, full_name=%s, inn=%s""".format(
                            suffix)
                    query_value_customer = (customer_regNum, customer_full_name, customer_inn)
                    try:
                        cur_44.execute(query_add_customer, query_value_customer)
                    except Exception as e:
                        with open(file_log, 'a') as flog:
                            flog.write(
                                    'Ошибка записи в базу данных customer:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
                    cur_44.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix),
                                   (customer_regNum,))
                    res_id_customer = cur_44.fetchone()
                    id_customer = res_id_customer['id_customer']
                    # print(id_customer)
            else:
                if customer_full_name:
                    cur_44.execute("""SELECT id_customer FROM customer{0} WHERE full_name=%s""".format(suffix),
                                   (customer_full_name,))
                    res_customer_fail = cur_44.fetchone()
                    if res_customer_fail:
                        id_customer = res_customer_fail['id_customer']
                        with open(file_log, 'a') as flog:
                            flog.write('Получили id_customer по customer_full_name' + ' ' + path_xml + '\n\n\n')
            query_add_customer_requirement = """INSERT INTO customer_requirement{0} SET id_lot=%s,
                  id_customer=%s, kladr_place=%s, delivery_place=%s, delivery_term=%s, application_guarantee_amount=%s,
                  application_settlement_account=%s, application_personal_account=%s, application_bik=%s,
                  contract_guarantee_amount=%s, contract_settlement_account=%s, contract_personal_account=%s,
                  contract_bik=%s, max_price=%s""".format(suffix)
            query_value_customer_requirement = (id_lot, id_customer, kladr_place, delivery_place, delivery_term,
                                                application_guarantee_amount, application_settlement_account,
                                                application_personal_account, application_bik,
                                                contract_guarantee_amount, contract_settlement_account,
                                                contract_personal_account, contract_bik, customer_requirement_max_price)
            try:
                cur_44.execute(query_add_customer_requirement, query_value_customer_requirement)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка записи в базу данных customer_requirement:' + ' ' + path_xml + ' ' + str(
                                    e) + '\n\n\n')
            # print(id_customer)
            if not id_customer:
                with open(file_log, 'a') as flog:
                    flog.write('Нет id_customer' + ' ' + path_xml + '\n\n\n')
            if not customer_regNum:
                with open(file_log, 'a') as flog:
                    flog.write('Нет customer_regNum' + ' ' + path_xml + '\n\n\n')

        preferenses = ten_44.get_preferences(lot)
        for preferense in preferenses:
            preferense_name = ten_44.preferense_name(preferense)
            query_add_preferense = """INSERT INTO preferense{0} SET id_lot=%s, name=%s""".format(suffix)
            query_value_preferense = (id_lot, preferense_name)
            try:
                cur_44.execute(query_add_preferense, query_value_preferense)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных preferense:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        requirements = ten_44.get_requirements(lot)
        for requirement in requirements:
            requirement_name = ten_44.requirement_name(requirement)
            requirement_content = ten_44.requirement_content(requirement)
            requirement_code = ten_44.requirement_code(requirement)
            query_add_requirement = """INSERT INTO requirement{0} SET id_lot=%s, name=%s, content=%s, code=%s""".format(
                    suffix)
            query_value_requirement = (id_lot, requirement_name, requirement_content, requirement_code)
            try:
                cur_44.execute(query_add_requirement, query_value_requirement)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных requirement:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        restrict_info = ten_44.restrict_info(lot)
        foreign_info = ten_44.foreign_info(lot)
        query_add_restrict = """INSERT INTO restricts{0} SET id_lot=%s, foreign_info=%s, info=%s""".format(suffix)
        query_value_restrict = (id_lot, foreign_info, restrict_info)
        try:
            cur_44.execute(query_add_restrict, query_value_restrict)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка записи в базу данных restrict:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        purchaseobjects = ten_44.get_purchaseobjects(lot)
        for purchaseobject in purchaseobjects:
            okpd2_code = ten_44.okpd2_code(purchaseobject)
            okpd_code = ten_44.okpd_code(purchaseobject)
            okpd_name = ten_44.okpd_name(purchaseobject)
            name = ten_44.name(purchaseobject)
            quantity_value = ten_44.quantity_value(purchaseobject)
            price = ten_44.price(purchaseobject)
            okei = ten_44.okei(purchaseobject)
            sum_p = ten_44.sum_p(purchaseobject)
            okpd2_group_code = ''
            okpd2_group_level1_code = ''
            if okpd2_code:
                if len(okpd2_code) > 1:
                    dot1 = okpd2_code.find('.')
                    if dot1 == -1:
                        dot1 = None
                    okpd2_group_code_temp = okpd2_code[:dot1]
                    okpd2_group_code = okpd2_group_code_temp[:2]
                    if len(okpd2_code) > 3:
                        if dot1:
                            okpd2_group_level1_code = okpd2_code[dot1 + 1:dot1 + 2]
            customerquantities = ten_44.get_customerquantities(purchaseobject)
            for customerquantity in customerquantities:
                customer_quantity_value = ten_44.customer_quantity_value(customerquantity)
                cust_regNum = ten_44.cust_regNum(customerquantity)
                cust_full_name = ten_44.cust_full_name(customerquantity)
                id_customer_q = 0
                if cust_regNum:
                    cur_44.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix),
                                   (cust_regNum,))
                    res_customer_q = cur_44.fetchone()
                    if res_customer_q:
                        id_customer_q = res_customer_q['id_customer']
                    else:
                        query_add_customer_q = """INSERT INTO customer{0} SET reg_num=%s, full_name=%s""".format(suffix)
                        query_value_customer_q = (cust_regNum, cust_full_name)
                        try:
                            cur_44.execute(query_add_customer_q, query_value_customer_q)
                        except Exception as e:
                            with open(file_log, 'a') as flog:
                                flog.write('Ошибка записи в базу данных customer_q:' + ' ' + path_xml + ' ' + str(
                                        e) + '\n\n\n')
                        cur_44.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix),
                                       (cust_regNum,))
                        res_id_customer_q = cur_44.fetchone()
                        id_customer_q = res_id_customer_q['id_customer']
                else:
                    if cust_full_name:
                        cur_44.execute("""SELECT id_customer FROM customer{0} WHERE full_name=%s""".format(suffix),
                                       (cust_full_name,))
                        res_customer_fail_q = cur_44.fetchone()
                        if res_customer_fail_q:
                            id_customer = res_customer_fail_q['id_customer']
                            with open(file_log, 'a') as flog:
                                flog.write('Получили id_customer_q по customer_full_name' + ' ' + path_xml + '\n\n\n')
                        else:
                            with open(file_log, 'a') as flog:
                                flog.write('Не смогли получить id_customer_q по customer_full_name' + ' ' + path_xml +
                                           '\n\n\n')
                query_add_customerquantity = """INSERT INTO purchase_object{0} SET id_lot=%s, id_customer=%s,
                                                  okpd2_code=%s, okpd2_group_code=%s, okpd2_group_level1_code=%s,
                                                  okpd_code=%s, okpd_name=%s, name=%s, quantity_value=%s, price=%s,
                                                  okei=%s, sum=%s, customer_quantity_value=%s""".format(suffix)
                query_value_customerquantity = (id_lot, id_customer_q, okpd2_code, okpd2_group_code,
                                                okpd2_group_level1_code, okpd_code, okpd_name, name,
                                                quantity_value, price, okei, sum_p, customer_quantity_value)
                try:
                    cur_44.execute(query_add_customerquantity, query_value_customerquantity)
                except Exception as e:
                    with open(file_log, 'a') as flog:
                        flog.write(
                                'Ошибка записи в базу данных customerquantity:' + ' ' + path_xml + ' ' + str(
                                        e) + '\n\n\n')
                if not id_customer_q:
                    with open(file_log, 'a') as flog:
                        flog.write('Нет id_customer_q' + ' ' + path_xml + '\n\n\n')
                if not cust_regNum:
                    with open(file_log, 'a') as flog:
                        flog.write('Нет cust_regNum' + ' ' + path_xml + '\n\n\n')
            if not customerquantities:
                query_add_purchaseobjects = """INSERT INTO purchase_object{0} SET id_lot=%s, id_customer=%s,
                                                  okpd2_code=%s, okpd2_group_code=%s, okpd2_group_level1_code=%s,
                                                  okpd_code=%s, okpd_name=%s, name=%s, quantity_value=%s, price=%s,
                                                  okei=%s, sum=%s, customer_quantity_value=%s""".format(suffix)
                query_value_purchaseobjects = (id_lot, id_customer, okpd2_code, okpd2_group_code,
                                               okpd2_group_level1_code, okpd_code, okpd_name, name,
                                               quantity_value, price, okei, sum_p, quantity_value)
                try:
                    cur_44.execute(query_add_purchaseobjects, query_value_purchaseobjects)
                except Exception as e:
                    with open(file_log, 'a') as flog:
                        flog.write(
                                'Ошибка записи в базу данных purchaseobjects:' + ' ' + path_xml + ' ' + str(
                                        e) + '\n\n\n')
    cur_44.close()
    con_44.close()
    try:
        ten_44.add_tender_kwords(id_tender)
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи tender_kwords: ' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    try:
        ten_44.add_ver_number(purchaseNumber)
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи ver_number: ' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')


def parser_type_cancel(doc, path_xml, filexml, reg, reg_id):
    ten_cancel = Cancel(doc)
    if ten_cancel.tender_cancel == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return
    purchaseNumber = ten_cancel.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у cancel ' + ' ' + path_xml + '\n\n\n')
        return
    con_cancel = connect_bd(name_db)
    cur_cancel = con_cancel.cursor(pymysql.cursors.DictCursor)
    try:
        cur_cancel.execute(
                """UPDATE tender{0} SET cancel = 1 WHERE id_region = %s AND purchase_number=%s""".format(suffix),
                (reg_id, purchaseNumber))
        Cancel.count_add_cancel += 1
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи в базу данных cancel:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_cancel.close()
    con_cancel.close()


def parser_type_sign(doc, path_xml, filexml, reg, reg_id):
    ten_sign = Sign(doc)
    if ten_sign.tender_sign == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return

    purchaseNumber = ten_sign.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у sign ' + ' ' + path_xml + '\n\n\n')
        return

    con_sign = connect_bd(name_db)
    cur_sign = con_sign.cursor(pymysql.cursors.DictCursor)
    id_tender = 0
    cur_sign.execute(
            """SELECT id_tender FROM tender{0} WHERE id_region=%s AND purchase_number=%s AND cancel=0""".format(suffix),
            (reg_id, purchaseNumber))
    res_id_tender = cur_sign.fetchone()
    if res_id_tender:
        id_tender = res_id_tender['id_tender']
    if not id_tender:
        # with open(file_log, 'a') as flog:
        #     flog.write('Не смогли найти id тендера в базе ' + ' ' + path_xml + '\n\n\n')
        cur_sign.close()
        con_sign.close()
        return
    id_sign = ten_sign.id_sign
    if not id_sign:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти id_sign у sign!!! ' + ' ' + path_xml + '\n\n\n')
        cur_sign.close()
        con_sign.close()
        return
    cur_sign.execute(
            """SELECT id_contract_sign FROM contract_sign{0} WHERE id_tender=%s AND id_sign=%s""".format(suffix),
            (id_tender, id_sign))
    res_id_sign = cur_sign.fetchone()
    if res_id_sign:
        # with open(file_log, 'a') as flog:
        #     flog.write('Контракт с таким sign_id уже есть в базе!!! ' + ' ' + path_xml + '\n\n\n')
        cur_sign.close()
        con_sign.close()
        return
    sign_number = ten_sign.sign_number
    sign_date = ten_sign.sign_date
    customer_reg_num = ten_sign.customer_reg_num
    contract_sign_price = ten_sign.contract_sign_price
    sign_currency = ten_sign.sign_currency
    conclude_contract_right = ten_sign.conclude_contract_right
    protocole_date = ten_sign.protocole_date

    supplier_contact = ''
    supplier_email = ''
    supplier_contact_phone = ''
    supplier_contact_fax = ''
    supplier_inn = ''
    supplier_kpp = ''
    participant_type = ''
    organization_name = ''
    country_full_name = ''
    factual_address = ''
    post_address = ''

    suppliers = ten_sign.get_suppliers()
    suppliers_list = []
    for supplier in suppliers:
        suppliers_list.append(supplier)
    if suppliers_list:
        supplier_lastName = ten_sign.supplier_lastName(suppliers_list[0])
        supplier_firstName = ten_sign.supplier_firstName(suppliers_list[0])
        supplier_middleName = ten_sign.supplier_middleName(suppliers_list[0])
        supplier_contact = supplier_lastName + ' ' + supplier_firstName + ' ' + supplier_middleName
        supplier_email = ten_sign.supplier_email(suppliers_list[0])
        supplier_contact_phone = ten_sign.supplier_contact_phone(suppliers_list[0])
        supplier_contact_fax = ten_sign.supplier_contact_fax(suppliers_list[0])
        supplier_inn = ten_sign.supplier_inn(suppliers_list[0])
        supplier_kpp = ten_sign.supplier_kpp(suppliers_list[0])
        participant_type = ten_sign.participant_type(suppliers_list[0])
        organization_name = ten_sign.organization_name(suppliers_list[0])
        country_full_name = ten_sign.country_full_name(suppliers_list[0])
        factual_address = ten_sign.factual_address(suppliers_list[0])
        post_address = ten_sign.post_address(suppliers_list[0])
    if len(suppliers_list) > 1:
        with open(file_log, 'a') as flog:
            flog.write('У тендера несколько supplier ' + ' ' + path_xml + '\n\n\n')
    if not suppliers_list:
        with open(file_log, 'a') as flog:
            flog.write('У тендера нет supplier ' + ' ' + path_xml + '\n\n\n')
    id_customer = 0
    if customer_reg_num:
        cur_sign.execute("""SELECT id_customer FROM customer{0} WHERE reg_num=%s""".format(suffix), (customer_reg_num,))
        res_id_customer = cur_sign.fetchone()
        if res_id_customer:
            id_customer = res_id_customer['id_customer']
    if not customer_reg_num:
        with open(file_log, 'a') as flog:
            flog.write('У тендера sign нет customer_reg_num ' + ' ' + path_xml + '\n\n\n')
    if not id_customer:
        with open(file_log, 'a') as flog:
            flog.write('У тендера sign нет id_customer ' + ' ' + path_xml + '\n\n\n')

    id_supplier = 0
    if supplier_inn:
        cur_sign.execute(
                """SELECT id_supplier FROM supplier{0} WHERE inn_supplier=%s AND kpp_supplier=%s""".format(suffix),
                (supplier_inn, supplier_kpp))
        res_id_supplier = cur_sign.fetchone()
        if res_id_supplier:
            id_supplier = res_id_supplier['id_supplier']
        else:
            query_add_supplier = """INSERT INTO supplier{0} SET participant_type=%s, inn_supplier=%s, kpp_supplier=%s,
                                    organization_name=%s, country_full_name=%s, factual_address=%s, post_address=%s,
                                    contact=%s, email=%s, phone=%s, fax=%s""".format(suffix)
            query_value_supplier = (
                participant_type, supplier_inn, supplier_kpp, organization_name, country_full_name, factual_address,
                post_address, supplier_contact, supplier_email, supplier_contact_phone, supplier_contact_fax)
            try:
                cur_sign.execute(query_add_supplier, query_value_supplier)
            except Exception as e:
                with open(file_log, 'a') as flog:
                    flog.write('Ошибка записи в базу данных supplier:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
            cur_sign.execute(
                    """SELECT id_supplier FROM supplier{0} WHERE inn_supplier=%s AND kpp_supplier=%s""".format(suffix),
                    (supplier_inn, supplier_kpp))
            res_sup = cur_sign.fetchone()
            if res_sup:
                id_supplier = res_sup['id_supplier']
    if not id_supplier:
        with open(file_log, 'a') as flog:
            flog.write('Нет id_supplier ' + ' ' + path_xml + '\n\n\n')
    if not supplier_inn:
        with open(file_log, 'a') as flog:
            flog.write('Нет supplier_inn в тенедере Sign ' + ' ' + path_xml + '\n\n\n')
    query_add_contract_sign = """INSERT INTO contract_sign{0} SET id_tender=%s, id_sign=%s, purchase_number=%s,
                              sign_number=%s,
                              sign_date=%s,
                              id_customer=%s, customer_reg_num=%s, id_supplier=%s, contract_sign_price=%s,
                              sign_currency=%s, conclude_contract_right=%s, protocole_date=%s, supplier_contact=%s,
                              supplier_email=%s, supplier_contact_phone=%s, supplier_contact_fax=%s""".format(suffix)
    query_value_contract_sign = (id_tender, id_sign, purchaseNumber, sign_number, sign_date, id_customer,
                                 customer_reg_num,
                                 id_supplier, contract_sign_price, sign_currency, conclude_contract_right,
                                 protocole_date, supplier_contact, supplier_email, supplier_contact_phone,
                                 supplier_contact_fax)
    try:
        cur_sign.execute(query_add_contract_sign, query_value_contract_sign)
        Sign.count_add_sign += 1
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи в базу данных contract_sign:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_sign.close()
    con_sign.close()


def parser_type_cancel_failure(doc, path_xml, filexml, reg, reg_id):
    ten_cancel_failure = CancelFailure(doc)
    if ten_cancel_failure.tender_cancel_failure == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return
    purchaseNumber = ten_cancel_failure.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у cancel_failure ' + ' ' + path_xml + '\n\n\n')
        return
    con_cancel_failure = connect_bd(name_db)
    cur_cancel_failure = con_cancel_failure.cursor(pymysql.cursors.DictCursor)
    try:
        cur_cancel_failure.execute(
                """UPDATE tender{0} SET cancel_failure = 1 WHERE id_region=%s AND purchase_number=%s 
                AND cancel=1""".format(suffix),
                (reg_id, purchaseNumber))
        CancelFailure.count_add_cancel_failuure += 1
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка записи в базу данных cancel_failure:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
    cur_cancel_failure.close()
    con_cancel_failure.close()


def parser_type_prolongation(doc, path_xml, filexml, reg, reg_id):
    ten_prolongation = Prolongation(doc)
    if ten_prolongation.tender_prolongation == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return

    purchaseNumber = ten_prolongation.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у prolongation ' + ' ' + path_xml + '\n\n\n')
        return
    con_prolongation = connect_bd(name_db)
    cur_prolongation = con_prolongation.cursor(pymysql.cursors.DictCursor)
    collectingEndDate = ten_prolongation.collectingEndDate
    collectingProlongationDate = ten_prolongation.collectingProlongationDate
    if collectingEndDate and collectingProlongationDate:
        cur_prolongation.execute(
                """UPDATE tender{0} SET end_date=%s WHERE id_region=%s AND purchase_number=%s 
                  AND cancel=0""".format(suffix),
                (collectingProlongationDate, reg_id, purchaseNumber))
        Prolongation.count_add_prolongation += 1
    scoringDate = ten_prolongation.scoringDate
    scoringProlongationDate = ten_prolongation.scoringProlongationDate
    if scoringDate and scoringProlongationDate:
        cur_prolongation.execute(
                """UPDATE tender{0} SET scoring_date=%s WHERE id_region=%s AND purchase_number=%s 
                AND cancel=0""".format(suffix),
                (scoringProlongationDate, reg_id, purchaseNumber))
        Prolongation.count_add_prolongation += 1
    if not collectingProlongationDate and not scoringProlongationDate:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти изменяемые даты у prolongation ' + ' ' + path_xml + '\n\n\n')
    cur_prolongation.close()
    con_prolongation.close()


def parser_type_datechange(doc, path_xml, filexml, reg, reg_id):
    ten_datechange = Datechange(doc)
    if ten_datechange.tender_datechange == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return

    purchaseNumber = ten_datechange.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у datechange ' + ' ' + path_xml + '\n\n\n')
        return
    con_datechange = connect_bd(name_db)
    cur_datechange = con_datechange.cursor(pymysql.cursors.DictCursor)
    auctionTime = ten_datechange.auctionTime
    newAuctionDate = ten_datechange.newAuctionDate
    if not newAuctionDate:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти newAuctionDate у datechange ' + ' ' + path_xml + '\n\n\n')
    if auctionTime and newAuctionDate:
        cur_datechange.execute(
                """UPDATE tender{0} SET bidding_date=%s WHERE id_region=%s AND purchase_number=%s 
                  AND cancel=0""".format(suffix),
                (newAuctionDate, reg_id, purchaseNumber))
        Datechange.count_add_datechange += 1
    cur_datechange.close()
    con_datechange.close()


def parser_type_orgchange(doc, path_xml, filexml, reg, reg_id):
    ten_org_change = OrgChange(doc)
    if ten_org_change.tender_orgchange == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return
    purchaseNumber = ten_org_change.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у orgchange ' + ' ' + path_xml + '\n\n\n')
        return
    newRespOrg_regNum = ten_org_change.newRespOrg_regNum
    if not newRespOrg_regNum:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти previousRespOrg_regNum у orgchange ' + ' ' + path_xml + '\n\n\n')
        return
    con_orgchange = connect_bd(name_db)
    cur_orgchange = con_orgchange.cursor(pymysql.cursors.DictCursor)
    cur_orgchange.execute("""SELECT id_organizer FROM organizer{0} WHERE reg_num=%s""".format(suffix),
                          (newRespOrg_regNum,))
    res_reg_num = cur_orgchange.fetchone()
    id_organizer = 0
    if res_reg_num:
        id_organizer = res_reg_num['id_organizer']
    else:
        newRespOrg_fullName = ten_org_change.newRespOrg_fullName
        newRespOrg_postAddress = ten_org_change.newRespOrg_postAddress
        newRespOrg_factAddress = ten_org_change.newRespOrg_factAddress
        newRespOrg_INN = ten_org_change.newRespOrg_INN
        newRespOrg_KPP = ten_org_change.newRespOrg_KPP
        newRespOrg_responsibleRole = ten_org_change.newRespOrg_responsibleRole
        query_add_organizer = """INSERT INTO organizer{0} SET reg_num=%s, full_name=%s, post_address=%s,
                                                        fact_address=%s, inn=%s, kpp=%s, responsible_role=%s""".format(
                suffix)
        query_value_organizer = (newRespOrg_regNum, newRespOrg_fullName, newRespOrg_postAddress, newRespOrg_factAddress,
                                 newRespOrg_INN, newRespOrg_KPP, newRespOrg_responsibleRole)
        try:
            cur_orgchange.execute(query_add_organizer, query_value_organizer)
        except Exception as e:
            with open(file_log, 'a') as flog:
                flog.write('Ошибка записи в базу данных organizer:' + ' ' + path_xml + ' ' + str(e) + '\n\n\n')
        cur_orgchange.execute("""SELECT id_organizer FROM organizer{0} WHERE reg_num=%s""".format(suffix),
                              (newRespOrg_regNum,))
        res_reg_num_id = cur_orgchange.fetchone()
        if res_reg_num_id:
            id_organizer = res_reg_num_id['id_organizer']
    if not id_organizer:
        with open(file_log, 'a') as flog:
            flog.write('Не получилось определить id_organizer:' + ' ' + path_xml + ' ' + '\n\n\n')
        cur_orgchange.close()
        con_orgchange.close()
        return
    try:
        cur_orgchange.execute(
                """UPDATE tender{0} SET id_organizer=%s WHERE id_region=%s AND purchase_number=%s 
                AND cancel=0""".format(suffix),
                (id_organizer, reg_id, purchaseNumber))
        OrgChange.count_org_change += 1
    except Exception:
        with open(file_log, 'a') as flog:
            flog.write('Не получилось обновить id_organizer в orgchange :' + ' ' + path_xml + ' ' + '\n\n\n')
    cur_orgchange.close()
    con_orgchange.close()


def parser_type_lotcancel(doc, path_xml, filexml, reg, reg_id):
    ten_lot_cancel = LotCancel(doc)
    if ten_lot_cancel.tender_lotcancel == 0:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти корневой тег у xml ' + ' ' + path_xml + '\n\n\n')
        return
    purchaseNumber = ten_lot_cancel.purchaseNumber
    if not purchaseNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти purchaseNumber у lotcancel ' + ' ' + path_xml + '\n\n\n')
        return
    lotNumber = ten_lot_cancel.lotNumber
    if not lotNumber:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти lotNumber у lotcancel ' + ' ' + path_xml + '\n\n\n')
        return
    con_lotcancel = connect_bd(name_db)
    cur_lotcancel = con_lotcancel.cursor(pymysql.cursors.DictCursor)
    cur_lotcancel.execute(
            """SELECT id_tender FROM tender{0} WHERE id_region=%s AND purchase_number=%s AND cancel=0""".format(suffix),
            (reg_id, purchaseNumber))
    res_ten = cur_lotcancel.fetchone()
    if res_ten:
        id_tender = res_ten['id_tender']
    else:
        with open(file_log, 'a') as flog:
            flog.write('Не могу найти id_tender в базе для lotcancel ' + ' ' + path_xml + '\n\n\n')
        cur_lotcancel.close()
        con_lotcancel.close()
        return
    cur_lotcancel.execute("""UPDATE lot{0} SET cancel=1 WHERE id_tender=%s AND lot_number=%s""".format(suffix),
                          (id_tender, lotNumber))
    LotCancel.count_add_lotcancel += 1
    cur_lotcancel.close()
    con_lotcancel.close()


def parser(doc, path_xml, filexml, reg, type_file, reg_id):
    global file_log
    if type_file in type_ten_44:
        try:
            parser_type_44(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа 44: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа 44 ' + ' ' + path_xml + ' ' + type_file + '\n\n\n')
    elif type_file in type_prolongation:
        try:
            parser_type_prolongation(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа prolongation: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа prolongation ' + ' ' + path_xml + ' ' + type_file +
                           '\n\n\n')
    elif type_file in type_datechange:
        try:
            parser_type_datechange(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа datechange: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа datechange ' + ' ' + path_xml + ' ' + type_file +
                           '\n\n\n')
    elif type_file in type_orgchange:
        try:
            parser_type_orgchange(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа orgchange: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа orgchange ' + ' ' + path_xml + ' ' + type_file +
                           '\n\n\n')
    elif type_file in type_lotcancel:
        try:
            parser_type_lotcancel(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа lotcancel: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа lotcancel ' + ' ' + path_xml + ' ' + type_file +
                           '\n\n\n')
    elif type_file in type_cancel:
        try:
            parser_type_cancel(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа cancel: ")
            with open(file_log, 'a') as flog:
                flog.write('Ошибка при парсинге тендера типа cancel ' + ' ' + path_xml + ' ' + type_file + '\n\n\n')
    elif type_file in type_sign:
        try:
            parser_type_sign(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа sign: ")
            with open(file_log, 'a') as flog:
                flog.write(
                        'Ошибка при парсинге тендера типа sign ' + ' ' + path_xml + ' ' + type_file + '\n\n\n')
    elif type_file in type_cancel_failure:
        try:
            parser_type_cancel_failure(doc, path_xml, filexml, reg, reg_id)
        except Exception:
            logging.exception("Ошибка при парсинге тендера типа cancel_failure: ")
            with open(file_log, 'a') as flog:
                flog.write(
                        'Ошибка при парсинге тендера типа cancel_failure ' + ' ' + path_xml + ' ' + type_file +
                        '\n\n\n')
