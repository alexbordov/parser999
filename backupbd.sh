#!/bin/sh
date_time=`date +"%Y-%m-%d_%H-%M"`
# Куда размещаем backup
bk_dir='/var/www/admin/data/www/backup/sql'
# Пользователь базы данных
user='root'
# Пароль пользователя
password='Dft56Point'
# Имя базы для бэкапа
bd_name='tenders_test'

# Выгружаем базу
/usr/bin/mysqldump --opt -v --databases $bd_name -u$user -p$password | /usr/bin/gzip -c > $bk_dir/mysql_$date_time.sql.gz

# Копирование резервного архива на удаленный FTP-сервер
ftpuser='Buckup'
password='Dft56Point'
ftpserver='31.132.162.154'
file=mysql_$date_time.sql.gz
ftp -n $ftpserver << EOF
user $ftpuser $password
binary
put $bk_dir/$file /tenders/$file
bye
EOF

find /var/www/admin/data/www/backup/sql* -type f -mtime +2 -exec rm {} \;