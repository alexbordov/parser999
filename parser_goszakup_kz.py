import os
import time
from datetime import datetime, date
import lxml.html as html
import pymysql
from grab import Grab
import random
import logging
import proxy_check

logging.basicConfig(level=logging.DEBUG)
DB = "tender"
PREFIX = ""
log_dir = "Log_tenders_goszakup_kz"
file_log = './{1}/tenders_goszakup_kz_{0}.log'.format(str(date.today()), log_dir)
max_number_str = 500
proxy_in = "./proxy/proxy.txt"
proxy_file = './proxy.txt'
try_count = 30


def get_proxy():
    clear_p = set()
    with open(proxy_in) as f:
        clear_p = {n for n in f.readlines()}
    if clear_p:
        with open(proxy_in, 'w') as f:
            f.writelines(clear_p)
    proxy_check.proxy_checker()


def get_connect_grub(log_f):
    # g = Grab(log_file=log_f)
    g = Grab()
    g.setup(connect_timeout=5, timeout=5)
    g.proxylist.load_file(proxy_file)
    return g


def logging_parser(*kwargs):
    s_log = '{0} '.format(datetime.now())
    for i in kwargs:
        s_log += ('{0} '.format(i))
    s_log += '\n\n'
    with open(file_log, 'a') as flog:
        flog.write(s_log)


def connect_bd(baza):
    con = pymysql.connect(host="localhost", port=3306, user="parser", passwd="Dft56Point", db=baza, charset='utf8',
                          init_command='SET NAMES UTF8', cursorclass=pymysql.cursors.DictCursor, autocommit=True)
    return con


def parser_list_tenders(i):
    page_url = f"https://v3bl.goszakup.gov.kz/ru/searchanno?&page={i}"
    tenders = []
    count = 1

    while True:
        try:
            # g = Grab(log_file=f'./{log_dir}/out_list_tenders.html')
            g = get_connect_grub(f'./{log_dir}/out_list_tenders.html')
            g.go(page_url)
            name_str = get_elem_text("//div[@class= 'content-block']//div/h4", g.doc)
            if name_str:
                ten = g.doc.select("//tr[@class = 'tr-favorite']")
                tenders = list(ten)
                tenders.reverse()
        except Exception as e:
            pass
        if tenders:
            break
        if count >= try_count:
            logging_parser(f"Не получили страницу за {try_count} попыток", page_url)
            break
        count += 1
        # random.seed()
        # r = random.randint(5, 7)
        # time.sleep(r)
    if tenders:
        for i in tenders:
            try:
                check_tenders(i)
            except Exception as e:
                logging_parser("Ошибка при проверке списка тендеров", page_url, e)
    logging_parser("Пропарсили", page_url)


def get_elem_text(search_str, resource):
    res = ""
    try:
        res = resource.select(search_str).text()
    except Exception as e:
        pass
    return "" if res is None else res.strip()


def check_tenders(ten):
    cancel = 0
    name_notif = ""
    href = ""
    full_num = ten.select("td[1]").text()
    if full_num:
        num_split = full_num.split("-")
        short_num = num_split[0]
        ver_num = num_split[1]
        con = connect_bd(DB)
        cur = con.cursor()
        cur.execute(f"""SELECT id FROM {PREFIX}goszakup_kz_notif WHERE short_num_notif=%s AND ver_num_notif=%s""",
                    (short_num, ver_num))
        if cur.fetchone():
            cur.close()
            con.close()
            # logging_parser("Такой тендер уже есть в базе", full_num)
            return
        cur.execute(f"""SELECT id FROM {PREFIX}goszakup_kz_notif WHERE short_num_notif=%s AND ver_num_notif < %s""",
                    (short_num, ver_num))
        res_low = cur.fetchall()
        if res_low:
            for i in res_low:
                cur.execute(f"""UPDATE {PREFIX}goszakup_kz_notif SET cancel=1 WHERE id=%s""", (i["id"],))
        cur.execute(f"""SELECT id FROM {PREFIX}goszakup_kz_notif WHERE short_num_notif=%s AND ver_num_notif > %s""",
                    (short_num, ver_num))
        if cur.fetchone():
            cancel = 1
        cur.close()
        con.close()
        name_notif_t = ten.select("td[@class = 'trd-name']//div/text()")
        for n in name_notif_t:
            name_notif += f"{n.text()}. "
        href = "https://v3bl.goszakup.gov.kz" + ten.select("td[@class = 'trd-name']//a/@href").text()
        tend = Tender(href, name_notif, cancel, full_num, short_num, ver_num, ten)
        # tend = Tender("https://v3bl.goszakup.gov.kz/ru/announce/index/1814901", name_notif, cancel, '1814901-1', '1814901', '1', ten)
        tend.parsing()
        try:
            tend.add_tender()
        except Exception as e:
            logging_parser("Не удалось дoбавить тендер", ten.href, e)
    else:
        logging_parser("Нет номера у тендера")


class Tender:
    def __init__(self, href, name_notif, cancel, full_num, short_num, ver_num, ten):
        self.href = href
        self.name_notif = name_notif
        self.cancel = cancel
        self.full_num = full_num
        self.short_num = short_num
        self.ver_num = ver_num
        self.doc = ten
        self.status = ""
        self.date_pub = ""
        self.date_start = ""
        self.date_end = ""
        self.change_reason = ""
        self.way_notif = ""
        self.type_notif = ""
        self.sum_notif = ""
        self.feature_notif = ""
        self.view_object_notif = ""
        self.org = {}
        self.lots_d = []
        self.lots = []
        self.docs_d = []
        self.docs = []

    def get_elem_text(self, search_str, resource=None):
        if resource is None: resource = self.doc
        res = ""
        try:
            res = resource.select(search_str).text()
        except Exception as e:
            pass
        return "" if res is None else res.strip()

    def get_elem(self, search_str, resource=None):
        if resource is None: resource = self.doc
        res = ""
        try:
            res = resource.select(search_str)
        except Exception as e:
            pass
        return [] if res is None else res

    def parsing(self):
        num_t = ""
        count = 1
        while True:
            try:
                # g = Grab(log_file=f'./{log_dir}/out_tender.html')
                g = get_connect_grub(f'./{log_dir}/out_tender.html')
                g.go(self.href)
                num_t = g.doc.select("(//div[@class = 'col-sm-7']/input/@value)[1]").text()
                if num_t == self.full_num:
                    self.doc = g.doc
                    break
                else:
                    count += 1

            except Exception:
                count += 1
            if count >= try_count:
                logging_parser(f"Не получили документ тендера за {try_count} попыток", self.href)
                return
            self.random_wait()
        self.status = self.get_elem_text("//div[@class = 'col-sm-7' and ../label = 'Статус объявления']/input/@value")
        self.date_pub = self.get_elem_text(
                "(//div[@class = 'col-sm-7' and parent::node()/child::label[contains(@class, 'col-sm-4') and contains(@class, 'control-label')]]/input/@value)[4]")
        self.date_start = self.get_elem_text(
                "(//div[@class = 'col-sm-7' and parent::node()/child::label[contains(@class, 'col-sm-4') and contains(@class, 'control-label')]]/input/@value)[5]")
        if not self.date_start:
            self.date_start = self.get_elem_text(
                    "(//div[@class = 'col-sm-7' and parent::node()/child::label[contains(@class, 'col-sm-4') and contains(@class, 'control-label')]]/div[@class = 'input-group date']/input/@value)[1]")
        self.date_end = self.get_elem_text(
                "(//div[@class = 'col-sm-7' and parent::node()/child::label[contains(@class, 'col-sm-4') and contains(@class, 'control-label')]]/input/@value)[6]")
        if not self.date_end:
            self.date_start = self.get_elem_text(
                    "(//div[@class = 'col-sm-7' and parent::node()/child::label[contains(@class, 'col-sm-4') and contains(@class, 'control-label')]]/div[@class = 'input-group date']/input/@value)[2]")
        self.change_reason = self.get_elem_text(
                "(//div[contains(@class, 'alert') and contains(@class, 'alert-success')])")
        self.way_notif = self.get_elem_text("//td[parent::node()/child::th = 'Способ проведения закупки']")
        self.type_notif = self.get_elem_text("//td[parent::node()/child::th = 'Тип закупки']")
        self.sum_notif = self.get_elem_text("//td[parent::node()/child::th = 'Сумма закупки']")
        self.feature_notif = self.get_elem_text("//td[parent::node()/child::th = 'Признаки']")
        self.view_object_notif = self.get_elem_text("//td[parent::node()/child::th = 'Вид предмета закупок']")
        org_name = self.get_elem_text("//td[parent::node()/child::th = 'Организатор']")
        self.org.update([('org_name', org_name)])
        org_reg_office = self.get_elem_text("//td[parent::node()/child::th = 'Юр. адрес организатора']")
        self.org.update([('org_reg_office', org_reg_office)])
        org_person = self.get_elem_text("//td[../th = 'ФИО представителя']")
        self.org.update([('org_person', org_person)])
        org_position = self.get_elem_text("//td[../th = 'Должность']")
        self.org.update([('org_position', org_position)])
        org_tel = self.get_elem_text("//td[../th = 'Контактный телефон']")
        self.org.update([('org_tel', org_tel)])
        org_email = self.get_elem_text("//td[../th = 'E-Mail']")
        self.org.update([('org_email', org_email)])
        org_bank = self.get_elem_text("//td[../th = 'Банковские реквизиты ']")
        self.org.update([('org_bank', org_bank)])
        num_lot = ""
        count = 1
        href_lot = f"{self.href}?tab=lots"
        while True:
            try:
                # g1 = Grab(log_file=f'./{log_dir}/out_lots.html')
                g1 = get_connect_grub(f'./{log_dir}/out_lots.html')
                g1.go(href_lot)
                num_lot = g1.doc.select("//div[@class = 'panel-body']/table/tr/th[. = 'Номер лота']").text()
                if num_lot == 'Номер лота':
                    self.lots_d = g1.doc.select("//div[@class = 'panel-body']/table/tr[position() > 1]")
                    break
                else:
                    count += 1

            except Exception:
                count += 1
            if count >= try_count:
                logging_parser(f"Не получили лоты за {try_count} попыток", href_lot)
                return
            self.random_wait()

        for l in self.lots_d:
            numb_lot = self.get_elem_text("td[2]", l)
            customer = self.get_elem_text("td[3]", l)
            name_lot = self.get_elem_text("td[4]", l)
            exend_info = self.get_elem_text("td[5]", l)
            price = self.get_elem_text("td[6]", l)
            amount = self.get_elem_text("td[7]", l)
            okei = self.get_elem_text("td[8]", l)
            sum = self.get_elem_text("td[9]", l)
            status_lot = self.get_elem_text("td[13]", l)
            self.lots.append(
                    {'num_lot': numb_lot, 'customer': customer, 'name_lot': name_lot, 'exend_info': exend_info,
                     'price': price, 'amount': amount, 'okei': okei, 'sum': sum, 'status_lot': status_lot})
        name_doc = ""
        count = 1
        href_doc = f"{self.href}?tab=documents"
        while True:
            try:
                # g2 = Grab(log_file=f'./{log_dir}/out_docs.html')
                g2 = get_connect_grub(f'./{log_dir}/out_docs.html')
                g2.go(href_doc)
                name_doc = g2.doc.select("//th[. = 'Наименование документа']").text()
                if name_doc == 'Наименование документа':
                    self.docs_d = g2.doc.select("//div[@class = 'panel-body']//table/tr[position() > 1]")
                    break
                else:
                    count += 1

            except Exception:
                count += 1
            if count >= try_count:
                logging_parser(f"Не получили документы за {try_count} попыток", href_doc)
                break
            self.random_wait()
        for d in self.docs_d:
            name_attach = self.get_elem_text("./td[1]", d)
            url_attach = self.get_elem_text("./td[1]/a/@href", d)
            if url_attach:
                url_attach = f"https://v3bl.goszakup.gov.kz{url_attach}"
            if not url_attach:
                try:
                    url_attach = self.get_attach(d)
                except Exception:
                    pass

            self.docs.append({'name_attach': name_attach, 'url_attach': url_attach})

    def get_attach(self, d):
        onclick = d.select("./td[3]/button/@onclick").text()
        onclick = onclick[21:-2]
        onclick = onclick.split(", ")
        url = ""
        if onclick:
            count = 1
            href_doc = f"https://v3bl.goszakup.gov.kz/ru/announce/actionAjaxModalShowFiles/{onclick[0]}/{onclick[1]}"
            while True:
                try:
                    # g2 = Grab(log_file=f'./{log_dir}/out_docs_url.html')
                    g2 = get_connect_grub(f'./{log_dir}/out_docs_url.html')
                    g2.go(href_doc)
                    name_doc = g2.doc.select("//tr/th[1]").text()
                    name_doc.strip()
                    if name_doc:
                        document = g2.doc.select("*")
                        url = self.get_elem_text("(//a)[1]/@href", document)
                        break
                    else:
                        count += 1

                except Exception:
                    count += 1
                if count >= try_count:
                    logging_parser(f"Не получили документ за {try_count} попыток", href_doc)
                    break
                self.random_wait()

        return url

    def random_wait(self):
        random.seed()
        r = random.randint(5, 7)
        # time.sleep(r)

    def add_tender(self):
        con = connect_bd(DB)
        cur = con.cursor()
        cur.execute(f"""SELECT id FROM  {PREFIX}goszakup_kz_org WHERE org_name=%s""", (self.org['org_name'],))
        res_c = cur.fetchone()
        if res_c:
            id_customer = res_c['id']
        else:
            cur.execute(f"""INSERT INTO {PREFIX}goszakup_kz_org SET org_name=%s, org_reg_office=%s, org_person=%s, org_position=%s,
            org_tel=%s, org_email=%s, org_bank=%s""",
                        (self.org['org_name'], self.org['org_reg_office'], self.org['org_person'],
                         self.org['org_position'], self.org['org_tel'], self.org['org_email'], self.org['org_bank']))
            id_customer = cur.lastrowid
        str_insert_notif = (
            f"""INSERT INTO {PREFIX}goszakup_kz_notif SET full_num_notif=%s, short_num_notif=%s, ver_num_notif=%s, 
            cancel=%s, status=%s, pub_date=%s, start_date=%s, end_date=%s, change_reason=%s, way_notif=%s, type_notif=%s, 
            sum_notif=%s, feature_notif=%s, view_object_notif=%s, url_notif=%s, id_customer=%s""")
        tuple_insert_notif = (self.full_num, self.short_num, self.ver_num, self.cancel, self.status, self.date_pub,
                              self.date_start, self.date_end, self.change_reason, self.way_notif, self.type_notif,
                              self.sum_notif, self.feature_notif, self.view_object_notif, self.href, id_customer)
        cur.execute(str_insert_notif, tuple_insert_notif)
        id_notif = cur.lastrowid
        for lot in self.lots:
            str_insert_lot = (f"""INSERT INTO {PREFIX}goszakup_kz_lots SET id_notif=%s, num_lot=%s, customer=%s, 
            name_lot=%s, exend_info=%s, price=%s, amount=%s, okei=%s, sum=%s, status_lot=%s""")
            tuple_insert_lot = (id_notif, lot['num_lot'], lot['customer'], lot['name_lot'], lot['exend_info'],
                                lot['price'], lot['amount'], lot['okei'], lot['sum'], lot['status_lot'])
            cur.execute(str_insert_lot, tuple_insert_lot)
        for doc in self.docs:
            str_insert_doc = f"""INSERT INTO {PREFIX}goszakup_kz_attach SET id_notif=%s, name_doc=%s, url_doc=%s"""
            tuple_insert_doc = (id_notif, doc['name_attach'], doc['url_attach'])
            cur.execute(str_insert_doc, tuple_insert_doc)
        cur.close()
        con.close()


def main(max_number_str):
    get_proxy()
    for i in range(1, max_number_str):
        try:
            parser_list_tenders(i)
        except Exception as ex:
            print(ex)


if __name__ == "__main__":
    if not os.path.exists(f"./{log_dir}"):
        os.mkdir(log_dir)
    try:
        os.remove(proxy_file)
    except Exception as e:
        print(e)
    logging_parser("Начало парсинга")
    # get_proxy()
    main(max_number_str)
    logging_parser("Конец парсинга")
