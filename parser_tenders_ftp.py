import datetime
import ftplib
import logging
import os
import shutil
import sys
import time
import xml
import zipfile
import pymysql
import timeout_decorator
import xmltodict
import parser_tenders
from warnings import filterwarnings
from connect_to_db import connect_bd

filterwarnings('ignore', category=pymysql.Warning)
suffix = ''
name_db = 'tender'
temp_dir = 'temp_tenders'
log_dir = 'log_tenders44'
file_log = './{1}/tenders_ftp_{0}.log'.format(str(datetime.date.today()), log_dir)
logging.basicConfig(level=logging.DEBUG, filename=file_log,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

file_count_god = './log_tenders44/count_good_' + str(datetime.date.today()) + '.log'
file_count_bad = './log_tenders44/count_bad_' + str(datetime.date.today()) + '.log'
need_file = ('EA44_', 'EP44_', 'OK44_', 'OKD44_', 'OKU44_', 'PO44_', 'ZA44_', 'ZK44_',
             'ZKB44_', 'ZKK44_', 'ZKKD44_', 'ZKKU44_', 'ZP44_', 'ContractSign_', 'NotificationCancel_',
             'CancelFailure_', 'DateChange_', 'LotCancel_', 'OrgChange_', 'ProtocolZKBI_', 'Prolongation')
unic_files = []
file_xml44 = ('EA44_', 'EP44_', 'OK44_', 'OKD44_', 'OKU44_', 'PO44_', 'ZA44_', 'ZK44_',
              'ZKB44_', 'ZKK44_', 'ZKKD44_', 'ZKKU44_', 'ZP44_', 'ProtocolZKBI_')
file_cancel = ('NotificationCancel_',)
file_sign = ('ContractSign_',)
file_cancelFailure = ('CancelFailure_',)
file_prolongation = ('Prolongation',)
file_datechange = ('DateChange_',)
file_orgchange = ('OrgChange_',)
file_lotcancel = ('LotCancel_',)


def check_inn():
    con_ch = connect_bd(name_db)
    cur_ch = con_ch.cursor()
    cur_ch1 = con_ch.cursor()
    cur_ch2 = con_ch.cursor()
    cur_ch.execute("""SELECT * FROM customer{0} WHERE inn = ''""".format(suffix))
    while True:
        res_cus_null = cur_ch.fetchone()
        if res_cus_null is None:
            break
        cur_ch1.execute("""SELECT * FROM organizer{0} WHERE reg_num =%s""".format(suffix), (res_cus_null['reg_num'],))
        res_reg_num = cur_ch1.fetchone()
        if res_reg_num:
            cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                            (res_reg_num['inn'], res_reg_num['reg_num']))
        else:
            cur_ch1.execute("""SELECT * FROM od_customer WHERE regNumber =%s""".format(suffix),
                            (res_cus_null['reg_num'],))
            res_reg_num_od = cur_ch1.fetchone()
            if res_reg_num_od:
                cur_ch2.execute("""UPDATE customer{0} SET inn=%s WHERE reg_num=%s""".format(suffix),
                                (res_reg_num_od['inn'], res_reg_num_od['regNumber']))
    cur_ch.close()
    cur_ch1.close()
    cur_ch2.close()
    con_ch.close()


def get_list_ftp_curr(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(name_db)
    cur_arhiv = con_arhiv.cursor()
    search2016 = 'notification_' + region + '_2016'
    search2017 = 'notification_' + region + '_2017'
    for i in data:
        if i.find(search2016) != -1 or i.find(search2017) != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_tenders{0} WHERE arhiv = %s AND region = %s""".format(suffix),
                              (i, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_tenders{0} SET arhiv = %s, region = %s""".format(suffix)
                query_par = (i, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    ftp2.close()
    return array_ar


def get_list_ftp_prev(path_parse, region):
    """
    :param region: регион архива
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    con_arhiv = connect_bd(name_db)
    cur_arhiv = con_arhiv.cursor()
    searchstring = datetime.datetime.now().strftime('%Y%m%d')
    for i in data:
        i_prev = "prev_{0}".format(i)
        if i.find(searchstring) != -1:
            cur_arhiv.execute("""SELECT id FROM arhiv_tenders{0} WHERE arhiv = %s AND region = %s""".format(suffix),
                              (i_prev, region))
            find_file = cur_arhiv.fetchone()
            if find_file:
                continue
            else:
                array_ar.append(i)
                query_ar = """INSERT INTO arhiv_tenders{0} SET arhiv = %s, region = %s""".format(suffix)
                query_par = (i_prev, region)
                cur_arhiv.execute(query_ar, query_par)
                # with open(file_log, 'a') as flog5:
                #     flog5.write('Добавлен новый архив ' + i + '\n')
    cur_arhiv.close()
    con_arhiv.close()
    ftp2.close()
    return array_ar


def get_list_ftp_last(path_parse, region):
    """
    :param path_parse: путь для архивов региона
    :return: возвращаем список архивов за 2016 и 2017
    """
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse)
    data = ftp2.nlst()
    array_ar = []
    search2016 = 'notification_' + region + '_2016'
    search2017 = 'notification_' + region + '_2017'
    for i in data:
        if i.find(search2016) != -1 or i.find(search2017) != -1:
            array_ar.append(i)

    return array_ar


def extract_tend(m, path_parse1, region, region_id):
    """
    :param m: имя архива с контрактами
    :param path_parse1: путь до папки с архивом
    """
    global need_file
    l = get_ar(m, path_parse1)
    if l:
        # print(l)
        r_ind = l.rindex('.')
        l_dir = l[:r_ind]
        os.mkdir(l_dir)
        array_xml44 = []
        array_cancel = []
        array_sign = []
        array_cancelFailure = []
        array_prolongation = []
        array_datechange = []
        array_orgchange = []
        array_lotcancel = []
        try:
            z = zipfile.ZipFile(l, 'r')
            z.extractall(l_dir)
            z.close()
        except UnicodeDecodeError as ea:
            print('Не удалось извлечь архив ' + str(ea) + ' ' + l)
            with open(file_log, 'a') as floga:
                floga.write('Не удалось извлечь архив {0} {1}\n'.format(str(ea), l))
            try:
                os.system('unzip %s -d %s' % (l, l_dir))
            except Exception as ear:
                print('Не удалось извлечь архив альтернативным методом')
                with open(file_log, 'a') as flogb:
                    flogb.write('Не удалось извлечь архив альтернативным методом {0} {1}\n'.format(str(ear), l))
                return
        except Exception as e:
            print('Не удалось извлечь архив ' + str(e) + ' ' + l)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flogc:
                flogc.write('Не удалось извлечь архив {0} {1}\n'.format(str(e), l))
            return

        try:
            file_list = os.listdir(l_dir)
            for file_e in file_list:
                countFlag = False
                file_e_lower = file_e.lower()
                for xml44 in file_xml44:
                    if file_e_lower.find(xml44.lower()) != -1:
                        array_xml44.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for prol in file_prolongation:
                    if file_e_lower.find(prol.lower()) != -1:
                        array_prolongation.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for datch in file_datechange:
                    if file_e_lower.find(datch.lower()) != -1:
                        array_datechange.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for orgch in file_orgchange:
                    if file_e_lower.find(orgch.lower()) != -1:
                        array_orgchange.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for lotcan in file_lotcancel:
                    if file_e_lower.find(lotcan.lower()) != -1:
                        array_lotcancel.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for cancel in file_cancel:
                    if file_e_lower.find(cancel.lower()) != -1:
                        array_cancel.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for sign in file_sign:
                    if file_e_lower.find(sign.lower()) != -1:
                        array_sign.append(file_e)
                        countFlag = True
                        break
                if countFlag: continue
                for cancelFailure in file_cancelFailure:
                    if file_e_lower.find(cancelFailure.lower()) != -1:
                        array_cancelFailure.append(file_e)
                        break
        except Exception as ex:
            print('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список файлов ' + str(ex) + ' ' + l_dir + '\n')
        else:
            for f in array_xml44:
                bolter(f, l_dir, region, region_id)

            for y in array_prolongation:
                bolter(y, l_dir, region, region_id)

            for x in array_datechange:
                bolter(x, l_dir, region, region_id)

            for w in array_orgchange:
                bolter(w, l_dir, region, region_id)

            for u in array_lotcancel:
                bolter(u, l_dir, region, region_id)

            for d in array_cancel:
                bolter(d, l_dir, region, region_id)

            for m in array_sign:
                bolter(m, l_dir, region, region_id)

            for s in array_cancelFailure:
                bolter(s, l_dir, region, region_id)

        os.remove(l)
        try:
            shutil.rmtree(l_dir, ignore_errors=True)
        except Exception as ex1:
            print('Не удалось удалить папку' + str(ex1) + ' ' + l_dir)


def unic(f, path):
    begin_file_list = f.split('_')
    con = connect_bd(name_db)
    cur = con.cursor()
    cur.execute("""SELECT * FROM unic_files{0} WHERE type_file=%s""".format(suffix), (begin_file_list[0],))
    res = cur.fetchone()
    if not res:
        file_ex = path + '/' + f
        file_target = './unic_tenders/' + f
        shutil.copy(file_ex, file_target)
        cur.execute("""INSERT INTO unic_files{0} SET type_file=%s""".format(suffix), (begin_file_list[0],))
        with open(file_log, 'a') as flog66:
            flog66.write('Добавлен новый тип файлов ' + ' ' + file_ex + '\n\n\n')
    cur.close()
    con.close()


def get_xml_to_dict(filexml, dirxml, region, type_f, reg_id):
    """
    :param filexml: имя xml
    :param dirxml: путь к локальному файлу
    :return:
    """
    # try:
    #     unic(filexml, dirxml)
    # except Exception as ex1:
    #     logging.exception("Ошибка копирования файла: ")
    #     with open(file_log, 'a') as flog9:
    #         flog9.write('Ошибка копирования файла ' + str(ex1) + ' ' + filexml + '\n\n\n')
    path_xml = dirxml + '/' + filexml
    # print(path_xml)
    fd = open(path_xml, 'r')
    s = fd.read()
    fd.close()
    try:
        doc = xmltodict.parse(s)
        parser_tenders.parser(doc, path_xml, filexml, region, type_f, reg_id)
    except xml.parsers.expat.ExpatError as ex2:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь, нечитаемые символы ' + str(ex2) + ' ' + path_xml + '\n\n\n')
        str_r = s.replace('', '')
        f = open(path_xml, 'w')
        f.write(str_r)
        f.close()
        with open(path_xml) as fdr:
            try:
                doc = xmltodict.parse(fdr.read())
                parser_tenders.parser(doc, path_xml, filexml, region, type_f, reg_id)
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Удалось пропарсить архив после удаления нечитаемых символов ' + ' ' + path_xml + '\n\n\n')
            except Exception as exs:
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write(
                            'Ошибка конвертации в словарь, удаление нечитаемых символов не помогло ' + str(exs) + ' '
                            + path_xml + '\n\n\n')
    except Exception as ex:
        logging.exception("Ошибка: ")
        with open(file_log, 'a') as flog:
            flog.write('Ошибка конвертации в словарь ' + str(ex) + ' ' + path_xml + '\n\n\n')


def bolter(file, l_dir, region, reg_id):
    """
    :param file: файл для проверки на черный список
    :param l_dir: директория локального файла
    :return: Если файл есть в черном списке - выходим
    """
    file_lower = file.lower()
    if not file_lower.endswith('.xml'):
        return
    for g in need_file:
        if file_lower.find(g.lower()) != -1:
            try:
                get_xml_to_dict(file, l_dir, region, g, reg_id)
            except Exception as exppars:
                # print('Не удалось пропарсить файл ' + str(exppars) + ' ' + file)
                logging.exception("Ошибка: ")
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось пропарсить файл ' + str(exppars) + ' ' + file + '\n')
                    # print(f)


@timeout_decorator.timeout(300)
def down_timeout(m, path_parse1):
    host = 'ftp.zakupki.gov.ru'
    ftpuser = 'free'
    password = 'free'
    ftp2 = ftplib.FTP(host)
    ftp2.set_debuglevel(0)
    ftp2.encoding = 'utf8'
    ftp2.login(ftpuser, password)
    ftp2.cwd(path_parse1)
    local_f = './{1}/{0}'.format(str(m), temp_dir)
    lf = open(local_f, 'wb')
    ftp2.retrbinary('RETR ' + str(m), lf.write)
    lf.close()
    ftp2.close()
    return local_f


def get_ar(m, path_parse1):
    """
    :param m: получаем имя архива
    :param path_parse1: получаем путь до архива
    :return: возвращаем локальный путь до архива или 0 в случае неудачи
    """
    retry = True
    count = 0
    while retry:
        try:
            lf = down_timeout(m, path_parse1)
            retry = False
            if count > 0:
                with open(file_log, 'a') as flog:
                    flog.write('Удалось скачать архив после попытки ' + str(count) + ' ' + m + '\n')
            return lf
        except Exception as ex:
            # print('Не удалось скачать архив ' + str(ex) + ' ' + m)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось скачать архив ' + str(ex) + ' ' + m + '\n')
            if count > 100:
                with open(file_log, 'a') as flog:
                    flog.write('Не удалось скачать архив за 100 попыток ' + str(ex) + ' ' + m + '\n')
                return 0
            count += 1
            time.sleep(5)


def main():
    with open(file_log, 'a') as flog:
        flog.write('Время начала работы парсера: {0}\n\n\n'.format(datetime.datetime.now()))
    shutil.rmtree(temp_dir, ignore_errors=True)
    os.mkdir(temp_dir)
    # забираем список регионов из базы данных
    con_region = connect_bd(name_db)
    cur_region = con_region.cursor()
    cur_region.execute("""SELECT * FROM region""")
    path_array = cur_region.fetchall()
    cur_region.close()
    con_region.close()

    for reg in path_array:
        if len(sys.argv) == 1:
            print(
                    'Недостаточно параметров для запуска, используйте curr для парсинга текущего месяца и last для '
                    'прошлых')
            exit()
        elif str(sys.argv[1]) == 'last':
            path_parse = 'fcs_regions/' + reg['path'] + '/notifications/'
        elif str(sys.argv[1]) == 'curr':
            path_parse = 'fcs_regions/' + reg['path'] + '/notifications/currMonth/'
        elif str(sys.argv[1]) == 'prev':
            path_parse = 'fcs_regions/' + reg['path'] + '/notifications/prevMonth/'
        else:
            print('Неверные параметры запуска, используйте last, prev или curr в качестве параметра')
            exit()

        try:
            # получаем список архивов
            if str(sys.argv[1]) == 'curr':
                arr_tenders = get_list_ftp_curr(path_parse, reg['path'])
            elif str(sys.argv[1]) == 'last':
                arr_tenders = get_list_ftp_last(path_parse, reg['path'])
            elif str(sys.argv[1]) == 'prev':
                arr_tenders = get_list_ftp_prev(path_parse, reg['path'])
            else:
                print('Неверное имя параметра, используйте curr для парсинга текущего месяца и last или prev для '
                      'прошлых')
                arr_tenders = []
                exit()
            if not arr_tenders:
                with open(file_log, 'a') as flog:
                    flog.write('Получен пустой список архивов ' + path_parse + ' ' + '\n')
            for j in arr_tenders:
                try:
                    extract_tend(j, path_parse, reg['conf'], reg['id'])
                except Exception as exc:
                    # print('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j)
                    logging.exception("Ошибка: ")
                    with open(file_log, 'a') as flog:
                        flog.write('Ошибка в экстракторе и парсере ' + str(exc) + ' ' + j + '\n')
                    continue

        except Exception as ex:
            # print('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse)
            logging.exception("Ошибка: ")
            with open(file_log, 'a') as flog:
                flog.write('Не удалось получить список архивов ' + str(ex) + ' ' + path_parse + '\n')
            continue
    try:
        check_inn()
    except Exception as e:
        with open(file_log, 'a') as flog:
            flog.write('Ошибка в функции check_inn {0} \n'.format(str(e)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили Tender44 {0} \n'.format(str(parser_tenders.Tender44.count_add_tender)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили Cancel {0} \n'.format(str(parser_tenders.Cancel.count_add_cancel)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили CancelFailure {0} \n'.format(
            str(parser_tenders.CancelFailure.count_add_cancel_failuure)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили Sign {0} \n'.format(str(parser_tenders.Sign.count_add_sign)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили LotCancel {0} \n'.format(str(parser_tenders.LotCancel.count_add_lotcancel)))
    with open(file_log, 'a') as flog:
        flog.write('Всего пропарсили OrgChange {0} \n'.format(str(parser_tenders.OrgChange.count_org_change)))
    with open(file_log, 'a') as flog:
        flog.write(
                'Всего пропарсили Prolongation {0} \n'.format(str(
                        parser_tenders.Prolongation.count_add_prolongation)))
    with open(file_log, 'a') as flog:
        flog.write(
                'Всего пропарсили Datechange {0} \n'.format(str(parser_tenders.Datechange.count_add_datechange)))
        flog.write('Время окончания работы парсера: {0}\n\n\n'.format(datetime.datetime.now()))


if __name__ == "__main__":
    main()
