#!/bin/sh
# Задаем переменные
# Текущая дата в формате 2015-09-29_04-10
date_time=`date +"%Y-%m-%d_%H-%M"`
# Куда размещаем backup
bk_dir='/var/www/admin/data/www/backup'
# Директория на уровень выше той, где лежат файлы
inf_dir='/var/www/admin/data/www'
# Название непосредственно директории с файлами
dir_to_bk='tenders.enter-it.ru'

# Создание архива
/usr/bin/tar -czvf $bk_dir/www_$date_time.tar.gz -C $inf_dir $dir_to_bk



# Копирование резервного архива на удаленный FTP-сервер
ftpuser='Buckup'
password='Dft56Point'
ftpserver='31.132.162.154'
file=www_$date_time.tar.gz
ftp -n $ftpserver << EOF
user $ftpuser $password
binary
put $bk_dir/$file /tenders/$file
bye
EOF

find /var/www/admin/data/www/backup* -type f -mtime +15 -exec rm {} \;
